﻿Option Strict On

Imports RenaissanceCLR.RenaissanceGlobals
Imports RenaissanceCLR.RenaissanceGlobals.Globals
Imports RenaissanceCLR.RenaissanceUtilities.DatePeriodFunctions
Imports System.Runtime.InteropServices
Imports System.Threading
Imports RenaissanceCLR.PertracAndStatsGlobals
Imports RenaissanceCLR.PertracAndStatsGlobals.Globals

Partial Public Class StatFunctions

	'Private Sub More_Code_Here(Optional ByVal Comment As String = "")

	'End Sub

	' *****************************************************************************
	'
	' If Bit31 is Set in the InstrumentID then the Instrument is assumed to be a Venice Instrument rather than a PERTRAC
	' Instrument.
	' 
	' *****************************************************************************

	' Minimum number of returns required for StDev, Correlation etc.
	Public Const MIN_Periods_For_Stats As Integer = 4

#Region " Private Class Defs"

	Public Class StatCacheClass
		' ************************************************************************************************
		' Cache object for basic statistics.
		' PertracID is a ULong in order to take both the Basic Pertrac ID and the Backfill ID (<< 32).
		' The two high order bits 27-31 indicate the type of instrument (Venice, Pertrac, Group etc.)
		'
		' ************************************************************************************************

		Private _StatsPeriodType As RenaissanceGlobals.DealingPeriod
		Public PertracID As ULong
		Public PeriodCount As Integer
		Public MatchBackfillVolatility As Boolean
		Public Lamda As Double

		Public DateArray() As Date
		Public NAVArray() As Double
		Public IsMilestoneArray() As Boolean
		Public ReturnsArray() As Double
		Public AverageArray() As Double
		Public StDevArray() As Double
		Private _NetDeltaArray() As NetDeltaItemClass

		Public LastAccessed As Date
		Public ToDelete As Boolean
		Private _I_DependOn As Dictionary(Of ULong, ULong) ' Key : StatsCacheKey, Value : Nothing
		Private _DependsOnMe As Dictionary(Of ULong, ULong)

		Private Const Ulong_32BitMask As ULong = CULng((2 ^ 32) - 1)

		Public Property StatsDatePeriod() As RenaissanceGlobals.DealingPeriod
			Get
				Return _StatsPeriodType
			End Get
			Set(ByVal value As RenaissanceGlobals.DealingPeriod)
				If (_StatsPeriodType <> value) Then
					_StatsPeriodType = value

					' ClearStats

					Me.Reset()

				End If
			End Set
		End Property

		Public ReadOnly Property HasVeniceInstrument(ByVal pInstrumentIDs() As Integer) As Boolean
			' ************************************************************************************************
			' Return True if either of the component Pertrac IDs is a Venice Instrument ID.
			' ************************************************************************************************
			Get
				Dim ThisID As Integer

				Try
					For Each ThisID In pInstrumentIDs
						If (HasVeniceInstrument(ThisID)) Then
							Return True
						End If
					Next
				Catch ex As Exception
				End Try

				Return False

			End Get
		End Property

		Public ReadOnly Property HasVeniceInstrument(Optional ByVal pInstrumentID As Integer = 0) As Boolean
			' ************************************************************************************************
			' Return True if either of the component Pertrac IDs is a Venice Instrument ID.
			' ************************************************************************************************

			Get
				Dim BasePertracID As UInteger
				Dim BackfillPertracID As UInteger
				Dim IsVeniceID As Boolean = False

				Try

					BasePertracID = CUInt(PertracID And Ulong_32BitMask)
					BackfillPertracID = CUInt((PertracID >> 32) And Ulong_32BitMask)

					If (PertracDataClass.GetFlagsFromID(BasePertracID) = PertracDataClass.PertracInstrumentFlags.VeniceInstrument) AndAlso ((pInstrumentID = 0) OrElse (PertracDataClass.GetInstrumentFromID(BasePertracID) = PertracDataClass.GetInstrumentFromID(CUInt(pInstrumentID)))) Then
						' The Base Instrument is a Venice Instrument and Matched the given value (if specified).

						IsVeniceID = True

					ElseIf (PertracDataClass.GetFlagsFromID(BackfillPertracID) = PertracDataClass.PertracInstrumentFlags.VeniceInstrument) AndAlso ((pInstrumentID = 0) OrElse (PertracDataClass.GetInstrumentFromID(BackfillPertracID) = PertracDataClass.GetInstrumentFromID(CUInt(pInstrumentID)))) Then
						' The Backfill Instrument is a Venice Instrument and Matched the given value (if specified).

						IsVeniceID = True

					End If

				Catch ex As Exception
				End Try

				Return IsVeniceID
			End Get
		End Property

		Public ReadOnly Property HasGroupInstrument(ByVal pInstrumentIDs() As Integer) As Boolean
			' ************************************************************************************************
			' Return True if either of the component Pertrac IDs is a Synthetic Group Instrument.
			' ************************************************************************************************

			Get
				Dim ThisID As Integer

				Try

					For Each ThisID In pInstrumentIDs
						If (HasGroupInstrument(ThisID)) Then
							Return True
						End If
					Next

				Catch ex As Exception
				End Try

				Return False

			End Get
		End Property

		Public ReadOnly Property HasGroupInstrument(Optional ByVal pInstrumentID As Integer = 0) As Boolean
			' ************************************************************************************************
			' Return True if either of the component Pertrac IDs is a Synthetic Group Instrument.
			' ************************************************************************************************

			Get

				Dim BasePertracID As UInteger
				Dim BackfillPertracID As UInteger
				Dim IsGroupID As Boolean = False

				Try

					BasePertracID = CUInt(PertracID And Ulong_32BitMask)
					BackfillPertracID = CUInt((PertracID >> 32) And Ulong_32BitMask)

					Dim ThisBaseFlag As PertracDataClass.PertracInstrumentFlags
					Dim ThisBackfillFlag As PertracDataClass.PertracInstrumentFlags

					ThisBaseFlag = PertracDataClass.GetFlagsFromID(BasePertracID)
					ThisBackfillFlag = PertracDataClass.GetFlagsFromID(BackfillPertracID)

					If (ThisBaseFlag = PertracDataClass.PertracInstrumentFlags.Group_Mean) OrElse (ThisBaseFlag = PertracDataClass.PertracInstrumentFlags.Group_Median) OrElse (ThisBaseFlag = PertracDataClass.PertracInstrumentFlags.Group_Weighted) Then
						If ((pInstrumentID = 0) OrElse (PertracDataClass.GetInstrumentFromID(BasePertracID) = PertracDataClass.GetInstrumentFromID(CUInt(pInstrumentID)))) Then
							IsGroupID = True
						End If
					End If

					If (Not IsGroupID) Then
						If (ThisBackfillFlag = PertracDataClass.PertracInstrumentFlags.Group_Mean) OrElse (ThisBackfillFlag = PertracDataClass.PertracInstrumentFlags.Group_Median) OrElse (ThisBackfillFlag = PertracDataClass.PertracInstrumentFlags.Group_Weighted) Then
							If ((pInstrumentID = 0) OrElse (PertracDataClass.GetInstrumentFromID(BackfillPertracID) = PertracDataClass.GetInstrumentFromID(CUInt(pInstrumentID)))) Then
								IsGroupID = True
							End If
						End If
					End If

				Catch ex As Exception
				End Try

				Return IsGroupID

			End Get

		End Property

		Public ReadOnly Property HasDynamicGroupInstrument(ByVal pInstrumentIDs() As Integer) As Boolean
			' ************************************************************************************************
			' Return True if either of the component Pertrac IDs is a Synthetic Group Instrument.
			' ************************************************************************************************

			Get
				Dim ThisID As Integer

				Try

					For Each ThisID In pInstrumentIDs
						If (HasDynamicGroupInstrument(ThisID)) Then
							Return True
						End If
					Next

				Catch ex As Exception
				End Try

				Return False

			End Get
		End Property

		Public ReadOnly Property HasDynamicGroupInstrument(Optional ByVal pInstrumentID As Integer = 0) As Boolean
			' ************************************************************************************************
			' Return True if either of the component Pertrac IDs is a Synthetic Group Instrument.
			' ************************************************************************************************

			Get

				Dim BasePertracID As UInteger
				Dim BackfillPertracID As UInteger
				Dim IsGroupID As Boolean = False

				Try

					BasePertracID = CUInt(PertracID And Ulong_32BitMask)
					BackfillPertracID = CUInt((PertracID >> 32) And Ulong_32BitMask)

					Dim ThisBaseFlag As PertracDataClass.PertracInstrumentFlags
					Dim ThisBackfillFlag As PertracDataClass.PertracInstrumentFlags

					ThisBaseFlag = PertracDataClass.GetFlagsFromID(BasePertracID)
					ThisBackfillFlag = PertracDataClass.GetFlagsFromID(BackfillPertracID)

					If (ThisBaseFlag = PertracDataClass.PertracInstrumentFlags.Dynamic_Group_Mean) OrElse (ThisBaseFlag = PertracDataClass.PertracInstrumentFlags.Dynamic_Group_Median) OrElse (ThisBaseFlag = PertracDataClass.PertracInstrumentFlags.Dynamic_Group_Weighted) Then
						If ((pInstrumentID = 0) OrElse (PertracDataClass.GetInstrumentFromID(BasePertracID) = PertracDataClass.GetInstrumentFromID(CUInt(pInstrumentID)))) Then
							IsGroupID = True
						End If
					End If

					If (Not IsGroupID) Then
						If (ThisBackfillFlag = PertracDataClass.PertracInstrumentFlags.Dynamic_Group_Mean) OrElse (ThisBackfillFlag = PertracDataClass.PertracInstrumentFlags.Dynamic_Group_Weighted) Then
							If ((pInstrumentID = 0) OrElse (PertracDataClass.GetInstrumentFromID(BackfillPertracID) = PertracDataClass.GetInstrumentFromID(CUInt(pInstrumentID)))) Then
								IsGroupID = True
							End If
						End If
					End If

				Catch ex As Exception
				End Try

				Return IsGroupID

			End Get

		End Property

		Public ReadOnly Property HasDrilldownGroupInstrument(ByVal pInstrumentIDs() As Integer) As Boolean
			' ************************************************************************************************
			' Return True if either of the component Pertrac IDs is a Synthetic Group Instrument.
			' ************************************************************************************************

			Get
				Dim ThisID As Integer

				Try

					For Each ThisID In pInstrumentIDs
						If (HasDrilldownGroupInstrument(ThisID)) Then
							Return True
						End If
					Next

				Catch ex As Exception
				End Try

				Return False

			End Get
		End Property

		Public ReadOnly Property HasDrilldownGroupInstrument(Optional ByVal pInstrumentID As Integer = 0) As Boolean
			' ************************************************************************************************
			' Return True if either of the component Pertrac IDs is a Synthetic Group Instrument.
			' ************************************************************************************************

			Get

				Dim BasePertracID As UInteger
				Dim BackfillPertracID As UInteger
				Dim IsGroupID As Boolean = False

				Try

					BasePertracID = CUInt(PertracID And Ulong_32BitMask)
					BackfillPertracID = CUInt((PertracID >> 32) And Ulong_32BitMask)

					Dim ThisBaseFlag As PertracDataClass.PertracInstrumentFlags
					Dim ThisBackfillFlag As PertracDataClass.PertracInstrumentFlags

					ThisBaseFlag = PertracDataClass.GetFlagsFromID(BasePertracID)
					ThisBackfillFlag = PertracDataClass.GetFlagsFromID(BackfillPertracID)

					If (ThisBaseFlag = PertracDataClass.PertracInstrumentFlags.DrillDown_Group_Median) Then
						If ((pInstrumentID = 0) OrElse (PertracDataClass.GetInstrumentFromID(BasePertracID) = PertracDataClass.GetInstrumentFromID(CUInt(pInstrumentID)))) Then
							IsGroupID = True
						End If
					End If

					If (Not IsGroupID) Then
						If (ThisBackfillFlag = PertracDataClass.PertracInstrumentFlags.DrillDown_Group_Median) Then
							If ((pInstrumentID = 0) OrElse (PertracDataClass.GetInstrumentFromID(BackfillPertracID) = PertracDataClass.GetInstrumentFromID(CUInt(pInstrumentID)))) Then
								IsGroupID = True
							End If
						End If
					End If

				Catch ex As Exception
				End Try

				Return IsGroupID

			End Get

		End Property

		Public ReadOnly Property HasPertracInstrument(ByVal pInstrumentIDs() As Integer) As Boolean
			' ************************************************************************************************
			' Return True if either of the component Pertrac IDs is a simple Pertrac ID.
			' Optionally check for the specified ID.
			' ************************************************************************************************

			Get
				Dim ThisID As Integer

				Try

					For Each ThisID In pInstrumentIDs
						If (HasPertracInstrument(ThisID)) Then
							Return True
						End If
					Next

				Catch ex As Exception
				End Try

				Return False

			End Get
		End Property

		Public ReadOnly Property HasPertracInstrument(Optional ByVal pInstrumentID As Integer = 0) As Boolean
			' ************************************************************************************************
			' Return True if either of the component Pertrac IDs is a simple Pertrac ID.
			' Optionally check for the specified ID.
			'
			' Guard against false positive for a Zero Backfill Instrument ID.
			' ************************************************************************************************

			Get
				Dim BasePertracID As UInteger
				Dim BackfillPertracID As UInteger
				Dim IsPertracID As Boolean = False

				Try

					BasePertracID = CUInt(PertracID And Ulong_32BitMask)
					BackfillPertracID = CUInt((PertracID >> 32) And Ulong_32BitMask)

					If (BasePertracID > 0) AndAlso _
					 (PertracDataClass.GetFlagsFromID(BasePertracID) = PertracDataClass.PertracInstrumentFlags.PertracInstrument) AndAlso _
					 ((pInstrumentID = 0) OrElse (PertracDataClass.GetInstrumentFromID(BasePertracID) = PertracDataClass.GetInstrumentFromID(CUInt(pInstrumentID)))) Then

						IsPertracID = True

					ElseIf (BackfillPertracID > 0) AndAlso _
					 (PertracDataClass.GetFlagsFromID(BackfillPertracID) = PertracDataClass.PertracInstrumentFlags.PertracInstrument) AndAlso _
					 ((pInstrumentID = 0) OrElse (PertracDataClass.GetInstrumentFromID(BackfillPertracID) = PertracDataClass.GetInstrumentFromID(CUInt(pInstrumentID)))) Then

						IsPertracID = True

					End If

				Catch ex As Exception
				End Try

				Return IsPertracID
			End Get
		End Property

		Public ReadOnly Property HasCTA_Simulation(ByVal pInstrumentIDs() As Integer) As Boolean
			' ************************************************************************************************
			' Return True if either of the component Pertrac IDs is a CTA Simulation Series.
			' Optionally check for the specified ID.
			' ************************************************************************************************

			Get
				Dim ThisID As Integer

				Try

					For Each ThisID In pInstrumentIDs
						If (HasCTA_Simulation(ThisID)) Then
							Return True
						End If
					Next

				Catch ex As Exception
				End Try

				Return False

			End Get
		End Property

		Public ReadOnly Property HasCTA_Simulation(Optional ByVal pInstrumentID As Integer = 0) As Boolean
			' ************************************************************************************************
			' Return True if either of the component Pertrac IDs is a CTA Simulation Series.
			' Optionally check for the specified ID.
			' ************************************************************************************************

			Get
				Dim BasePertracID As UInteger
				Dim BackfillPertracID As UInteger
				Dim IsSimulationID As Boolean = False

				Try

					BasePertracID = CUInt(PertracID And Ulong_32BitMask)
					BackfillPertracID = CUInt((PertracID >> 32) And Ulong_32BitMask)

					If (BasePertracID > 0) AndAlso _
					 (PertracDataClass.GetFlagsFromID(BasePertracID) = PertracDataClass.PertracInstrumentFlags.CTA_Simulation) AndAlso _
					 ((pInstrumentID = 0) OrElse (PertracDataClass.GetInstrumentFromID(BasePertracID) = PertracDataClass.GetInstrumentFromID(CUInt(pInstrumentID)))) Then

						IsSimulationID = True

					ElseIf (BackfillPertracID > 0) AndAlso _
					 (PertracDataClass.GetFlagsFromID(BackfillPertracID) = PertracDataClass.PertracInstrumentFlags.CTA_Simulation) AndAlso _
					 ((pInstrumentID = 0) OrElse (PertracDataClass.GetInstrumentFromID(BackfillPertracID) = PertracDataClass.GetInstrumentFromID(CUInt(pInstrumentID)))) Then

						IsSimulationID = True

					End If

				Catch ex As Exception
				End Try

				Return IsSimulationID
			End Get
		End Property

		Public Property NetDeltaArray() As NetDeltaItemClass()
			Get
				' If NetDelta is not set, assume it is an instrument type where Net Delta is always One.
				' For Simulation or Basket types, the NetDelta Array should already have been set.

				If (_NetDeltaArray Is Nothing) AndAlso (DateArray IsNot Nothing) Then
					Dim RVal(0) As NetDeltaItemClass

					Try
						Dim Index As Integer

						RVal(0) = New NetDeltaItemClass(PertracID, CType(Array.CreateInstance(GetType(Double), DateArray.Length), Double()))
						For Index = 0 To (RVal(0).DeltaArray.Length - 1)
							RVal(0).DeltaArray(Index) = 1.0#
						Next

					Catch ex As Exception
					End Try

					Return RVal
				Else
					Dim RVal(-1) As NetDeltaItemClass

					If (_NetDeltaArray.Length > 0) Then
						RVal = CType(Array.CreateInstance(GetType(NetDeltaItemClass), _NetDeltaArray.Length), NetDeltaItemClass())
						Array.Copy(_NetDeltaArray, RVal, _NetDeltaArray.Length)
					End If

					Return RVal
				End If
			End Get

			Set(ByVal value() As NetDeltaItemClass)
				_NetDeltaArray = value
			End Set
		End Property

		Public ReadOnly Property NetDeltaArrayIsNull() As Boolean
			Get
				If (_NetDeltaArray Is Nothing) Then
					Return True
				End If
				Return False
			End Get
		End Property

		Public Function Get_I_DependOn() As ULong()
			Dim RVal(-1) As ULong

			Try
				If (_I_DependOn IsNot Nothing) AndAlso (_I_DependOn.Count > 0) Then
					RVal = _I_DependOn.Keys.ToArray
				End If
			Catch ex As Exception
			End Try

			Return RVal

		End Function

		Public Sub Set_I_DependOn(ByVal pID As ULong)
			Try
				If (pID > 0) Then
					If (_I_DependOn Is Nothing) Then
						_I_DependOn = New Dictionary(Of ULong, ULong)
					End If

					SyncLock _I_DependOn
						If (Not _I_DependOn.ContainsKey(pID)) Then
							_I_DependOn.Add(pID, 0UL)
						End If
					End SyncLock
				End If
			Catch ex As Exception
			End Try
		End Sub

		Public Sub Set_I_DependOn(ByVal pIDs() As ULong)
			Try
				If (pIDs IsNot Nothing) AndAlso (pIDs.Length > 0) Then
					For Each ThisID As ULong In pIDs
						Set_I_DependOn(ThisID)
					Next
				End If
			Catch ex As Exception
			End Try
		End Sub

		Public Sub Remove_I_DependOn(ByVal pID As ULong)
			Try
				If (_I_DependOn IsNot Nothing) Then
					SyncLock _I_DependOn
						If (pID > 0) AndAlso (_I_DependOn.ContainsKey(pID)) Then
							_I_DependOn.Remove(pID)
						End If
					End SyncLock
				End If
			Catch ex As Exception
			End Try
		End Sub

		Public Sub Remove_I_DependOn(ByVal pIDs() As ULong)
			Try
				If (pIDs IsNot Nothing) AndAlso (pIDs.Length > 0) Then
					For Each ThisID As ULong In pIDs
						Remove_I_DependOn(ThisID)
					Next
				End If
			Catch ex As Exception
			End Try
		End Sub

		Public Function Get_DependsOnMe() As ULong()
			Dim RVal(-1) As ULong

			Try
				If (_DependsOnMe IsNot Nothing) AndAlso (_DependsOnMe.Count > 0) Then
					RVal = _DependsOnMe.Keys.ToArray
				End If
			Catch ex As Exception
			End Try

			Return RVal
		End Function

		Public Sub Set_DependsOnMe(ByVal pID As ULong)
			Try
				If (pID > 0) Then
					If (_DependsOnMe Is Nothing) Then
						_DependsOnMe = New Dictionary(Of ULong, ULong)
					End If

					SyncLock _DependsOnMe
						If (Not _DependsOnMe.ContainsKey(pID)) Then
							_DependsOnMe.Add(pID, 0UL)
						End If
					End SyncLock
				End If
			Catch ex As Exception
			End Try
		End Sub

		Public Sub Set_DependsOnMe(ByVal pIDs() As ULong)
			Try
				If (pIDs IsNot Nothing) AndAlso (pIDs.Length > 0) Then
					For Each ThisID As ULong In pIDs
						Set_DependsOnMe(ThisID)
					Next
				End If
			Catch ex As Exception
			End Try
		End Sub

		Public Sub Remove_DependsOnMe(ByVal pID As ULong)
			Try
				If (_DependsOnMe IsNot Nothing) Then
					SyncLock _DependsOnMe
						If (pID > 0) AndAlso (_DependsOnMe.ContainsKey(pID)) Then
							_DependsOnMe.Remove(pID)
						End If
					End SyncLock
				End If
			Catch ex As Exception
			End Try
		End Sub

		Public Sub Remove_DependsOnMe(ByVal pIDs() As ULong)
			Try
				If (pIDs IsNot Nothing) AndAlso (pIDs.Length > 0) Then
					For Each ThisID As ULong In pIDs
						Remove_DependsOnMe(ThisID)
					Next
				End If
			Catch ex As Exception
			End Try
		End Sub

		Public Sub Reset()
			' ************************************************************************************************
			'
			' ************************************************************************************************

			Try

				PertracID = 0
				PeriodCount = 0
				MatchBackfillVolatility = False
				Lamda = 0

				DateArray = Nothing
				NAVArray = Nothing
				ReturnsArray = Nothing
				IsMilestoneArray = Nothing
				AverageArray = Nothing
				StDevArray = Nothing
				_NetDeltaArray = Nothing

				LastAccessed = Now
				ToDelete = False

				If (_I_DependOn IsNot Nothing) Then
					Try
						_I_DependOn.Clear()
					Catch ex As Exception
					End Try
					_I_DependOn = Nothing
				End If

				If (_DependsOnMe IsNot Nothing) Then
					Try
						_DependsOnMe.Clear()
					Catch ex As Exception
					End Try
					_DependsOnMe = Nothing
				End If
			Catch ex As Exception
			End Try

		End Sub

		Private Sub New()
			' ************************************************************************************************
			'
			' ************************************************************************************************

			Me.Reset()

		End Sub

		Public Sub New(ByVal StatsDatePeriod As RenaissanceGlobals.DealingPeriod, ByVal pPertracID As ULong, ByVal pMatchBackfillVolatility As Boolean, ByVal pPeriodCount As Integer, ByVal pLamda As Double)
			' ************************************************************************************************
			'
			' ************************************************************************************************
			Me.New()

			_StatsPeriodType = StatsDatePeriod
			PertracID = pPertracID
			MatchBackfillVolatility = pMatchBackfillVolatility
			PeriodCount = pPeriodCount
			Lamda = pLamda

		End Sub

		Public Sub New(ByVal StatsDatePeriod As RenaissanceGlobals.DealingPeriod, ByVal pPertracID As ULong, ByVal pMatchBackfillVolatility As Boolean, ByVal pPeriodCount As Integer, ByVal pLamda As Double, ByVal pDateArray() As Date, ByRef pNAVArray() As Double, ByRef pReturnsArray() As Double, ByRef pIsMilestoneArray() As Boolean, ByRef pAverageArray() As Double, ByRef pStDevArray() As Double, ByRef pNetDeltaArray() As NetDeltaItemClass)
			' ************************************************************************************************
			'
			' ************************************************************************************************
			Me.New()

			_StatsPeriodType = StatsDatePeriod
			PertracID = pPertracID
			MatchBackfillVolatility = pMatchBackfillVolatility
			PeriodCount = pPeriodCount
			Lamda = pLamda

			DateArray = pDateArray
			NAVArray = pNAVArray
			ReturnsArray = pReturnsArray
			IsMilestoneArray = pIsMilestoneArray
			AverageArray = pAverageArray
			StDevArray = pStDevArray
			_NetDeltaArray = pNetDeltaArray

		End Sub

	End Class

	Private Class StatCacheDateComparer
		Implements IComparer

		Public Function Compare(ByVal x As Object, ByVal y As Object) As Integer Implements System.Collections.IComparer.Compare
			' ************************************************************************************************
			'
			' ************************************************************************************************

			Dim X_CacheItem As StatCacheClass
			Dim Y_CacheItem As StatCacheClass

			Try
				If (TypeOf x Is StatCacheClass) Then
					X_CacheItem = CType(x, StatCacheClass)
				Else
					Return 0
				End If

				If (TypeOf y Is StatCacheClass) Then
					Y_CacheItem = CType(y, StatCacheClass)
				Else
					Return 0
				End If

				Return X_CacheItem.LastAccessed.CompareTo(Y_CacheItem.LastAccessed)
			Catch ex As Exception

				Return 0
			End Try

		End Function

	End Class

	Public Enum DataItemEnum As Integer
		Dates
		NAVs
		Returns
		Averages
		StandardDeviation
		Correlation
		Alpha
		Beta
		NetDelta
	End Enum

	Public Enum ContingentSelect As Integer

		ConditionDown = (-1)
		ConditionAll = 0
		ConditionUp = 1

	End Enum

	Private Function GetStatCacheKey(ByVal pStatsDatePeriod As RenaissanceGlobals.DealingPeriod, ByVal pPertracID As Integer, ByVal pPertracBackfillID As Integer, ByVal MatchBackfillVolatility As Boolean) As ULong
		' *********************************************************************************
		' Key is a 64 Bit Unsigned Integer composed of the various Pertrac IDs and the
		' Volatility adjustment flag.
		'    '
		' Bits  1 - 8  : StatsDatePeriod    - 8 Bits
		' Bit   9      : Volatility Flag    - 1 Bit
		' Bits 10 - 35 : PertracID          - 
		' Bits 36 - 51 : ReferencePertracID - 
		'
		' *********************************************************************************

		Try
			Dim RVal As ULong = 0L
			Dim NormalisedPertracID As UInteger
			Dim NormalisedBackfillID As UInteger

			If (pPertracID <= 0) AndAlso (pPertracBackfillID <= 0) Then
				Return 0L
			End If

			SyncLock _StatsIDDictionary

				If (pPertracID > 0) Then
					Try
						If _StatsIDDictionary.ContainsKey(pPertracID) Then
							NormalisedPertracID = _StatsIDDictionary(pPertracID)
						Else
							NormalisedPertracID = CUInt(_StatsIDDictionary.Count + 1)
							_StatsIDDictionary.Add(pPertracID, NormalisedPertracID)
						End If
					Catch ex As KeyNotFoundException
						NormalisedPertracID = CUInt(_StatsIDDictionary.Count + 1)
						_StatsIDDictionary.Add(pPertracID, NormalisedPertracID)
					Catch ex As Exception
						Return 0L
					End Try

				End If

				If (pPertracBackfillID > 0) Then
					Try
						If _StatsIDDictionary.ContainsKey(pPertracBackfillID) Then
							NormalisedBackfillID = _StatsIDDictionary(pPertracBackfillID)
						Else
							NormalisedBackfillID = CUInt(_StatsIDDictionary.Count + 1)
							_StatsIDDictionary.Add(pPertracBackfillID, NormalisedBackfillID)
						End If

					Catch ex As KeyNotFoundException
						NormalisedBackfillID = CUInt(_StatsIDDictionary.Count + 1)
						_StatsIDDictionary.Add(pPertracBackfillID, NormalisedBackfillID)
					Catch ex As Exception
						Return 0L
					End Try

				End If

				RVal = (CULng(pStatsDatePeriod) And Ulong_8BitMask) Or ((NormalisedPertracID And Ulong_26BitMask) << 9) Or ((NormalisedBackfillID And Ulong_26BitMask) << 35)

				If (MatchBackfillVolatility) Then
					RVal = (RVal Or Ulong_Bit9Mask)
				End If

			End SyncLock

			Return RVal
		Catch ex As Exception
		End Try

		Return 0
	End Function

	Friend Function GetStatCacheKey(ByVal pStatsDatePeriod As RenaissanceGlobals.DealingPeriod, ByVal pPertracID As ULong, ByVal MatchBackfillVolatility As Boolean) As ULong
		' *********************************************************************************
		' Key is a 64 Bit Unsigned Integer composed of the various Pertrac IDs and the
		' Volatility adjustment flag.
		'    '
		' Bits  1 - 8  : StatsDatePeriod    - 8 Bits
		' Bit   9      : Volatility Flag    - 1 Bit
		' Bits 10 - 35 : PertracID          - 
		' Bits 36 - 51 : ReferencePertracID - 
		' *********************************************************************************

		Try
			Return GetStatCacheKey(pStatsDatePeriod, CInt(pPertracID And Ulong_31BitMask), CInt((pPertracID >> 32) And Ulong_31BitMask), MatchBackfillVolatility)
		Catch ex As Exception
		End Try

		Return 0L

	End Function

	Private Function GetStatCacheKey(ByRef pStatCacheItem As StatCacheClass) As ULong
		' *********************************************************************************
		'
		'
		' *********************************************************************************
		Try
			Return GetStatCacheKey(pStatCacheItem.StatsDatePeriod, CInt(pStatCacheItem.PertracID And Ulong_31BitMask), CInt((pStatCacheItem.PertracID >> 32) And Ulong_31BitMask), pStatCacheItem.MatchBackfillVolatility)
		Catch ex As Exception
		End Try

		Return 0L

	End Function

	Private Class DynamicGroupDefinitionClass
		' ******************************************************************************************
		' Class to hold Dynamc Group members and associated weights.
		' Default Weights to 1.0# if no weights array is given
		'
		' Members and Weights are sorted by Member ID.
		'
		' ******************************************************************************************

		Private _InstrumentsArray() As Integer
		Private _WeightsArray() As Double
		Private _MaxPeriodCount As Integer

		Private Sub New()
			' Disable default constructor
		End Sub

		Public Sub New(ByVal pInstruments() As Integer, ByVal MaxPeriodCount As Integer)
			Dim MembersChanged As Boolean

			Me.SetDynamicGroup(pInstruments, Nothing, MembersChanged)
			_MaxPeriodCount = MaxPeriodCount
		End Sub

		Public Sub New(ByVal pInstruments() As Integer, ByVal pWeights() As Double, ByVal MaxPeriodCount As Integer)
			Dim MembersChanged As Boolean

			Me.SetDynamicGroup(pInstruments, pWeights, MembersChanged)
			_MaxPeriodCount = MaxPeriodCount
		End Sub

		Public Function SetDynamicGroup(ByVal pInstruments() As Integer, ByVal pWeights() As Double, ByVal MaxPeriodCount As Integer, ByRef pMembersChanged As Boolean) As Boolean
			' ******************************************************************************************
			' ******************************************************************************************

			Try
				_MaxPeriodCount = MaxPeriodCount

				SetDynamicGroup(pInstruments, pWeights, pMembersChanged)

			Catch ex As Exception
			End Try

		End Function

		Public Function SetDynamicGroup(ByVal pInstruments() As Integer, ByVal pWeights() As Double, ByRef pMembersChanged As Boolean) As Boolean
			' ******************************************************************************************
			' Set Members and Weights arrays.
			' Default Weights to 1.0# if no weights array is given
			'
			' Members and Weights are sorted by Member ID.
			'
			' ******************************************************************************************
			Dim MembersChanged As Boolean = True
			Dim WeightsChanged As Boolean = True

			Try

				If (pInstruments Is Nothing) OrElse (pInstruments.Length <= 0) Then
					_InstrumentsArray = CType(Array.CreateInstance(GetType(Integer), 0), Integer())
					_WeightsArray = CType(Array.CreateInstance(GetType(Double), 0), Double())
					Return True
				Else
					Dim TempInstruments() As Integer
					Dim TempWeights() As Double

					TempInstruments = CType(Array.CreateInstance(GetType(Integer), pInstruments.Length), Integer())
					TempWeights = CType(Array.CreateInstance(GetType(Double), pInstruments.Length), Double())

					Array.Copy(pInstruments, TempInstruments, TempInstruments.Length)

					If (pWeights IsNot Nothing) AndAlso (pWeights.Length > 0) Then
						Array.Copy(pWeights, TempWeights, Math.Min(TempWeights.Length, pWeights.Length))
					Else
						' Default Weights Array

						For ThisIndex As Integer = 0 To (TempWeights.Length - 1)
							TempWeights(ThisIndex) = 1.0#
						Next

					End If

					' Sort the constituents

					Array.Sort(TempInstruments, TempWeights)

					' Compare

					If (_InstrumentsArray Is Nothing) OrElse (_InstrumentsArray.Length <> TempInstruments.Length) OrElse (_WeightsArray Is Nothing) OrElse (_WeightsArray.Length <> TempWeights.Length) Then
						_InstrumentsArray = TempInstruments
						_WeightsArray = TempWeights
						Return True
					Else
						' Sizes are the same, compare
						MembersChanged = False
						WeightsChanged = False

						For ItemIndex As Integer = 0 To (_InstrumentsArray.Length - 1)
							If (_InstrumentsArray(ItemIndex) <> TempInstruments(ItemIndex)) Then
								MembersChanged = True
							End If
							If (_WeightsArray(ItemIndex) <> TempWeights(ItemIndex)) Then
								WeightsChanged = True
							End If

							If (MembersChanged AndAlso WeightsChanged) Then
								Exit For
							End If
						Next

						pMembersChanged = MembersChanged

						If (MembersChanged OrElse WeightsChanged) Then
							_InstrumentsArray = TempInstruments
							_WeightsArray = TempWeights
							Return True
						End If

						' New Itens are the Same, dont change.

						'_InstrumentsArray = TempInstruments
						'_WeightsArray = TempWeights
						Return False

					End If

				End If

			Catch ex As Exception
				_InstrumentsArray = CType(Array.CreateInstance(GetType(Integer), 0), Integer())
				_WeightsArray = CType(Array.CreateInstance(GetType(Double), 0), Double())
				Return True
			Finally
				Try
					pMembersChanged = MembersChanged
				Catch ex As Exception
				End Try
			End Try

		End Function

		Public ReadOnly Property MemberCount() As Integer
			Get
				Dim RVal As Integer = 0

				Try
					If (_InstrumentsArray IsNot Nothing) Then
						RVal = _InstrumentsArray.Length
					End If
				Catch ex As Exception
				End Try

				Return RVal
			End Get
		End Property

		Public ReadOnly Property MaxPeriodCount() As Integer
			Get
				Return _MaxPeriodCount
			End Get
		End Property

		Public Function GetGroupMembers() As Integer()

			Try
				Return _InstrumentsArray
			Catch ex As Exception
			End Try

			Return Nothing

		End Function

		Public Function GetGroupWeights() As Double()

			Try
				Return _WeightsArray
			Catch ex As Exception
			End Try

			Return Nothing

		End Function

	End Class

	Public Function SetDependsOnMe(ByVal pParentStatsKey As ULong, ByVal pChildStatsKey As ULong) As ULong
		' ********************************************************************************
		'
		'
		' ********************************************************************************

		Try
			SyncLock _StatsCache
				If _StatsCache.ContainsKey(pChildStatsKey) Then
					Dim ChildCacheItem As StatCacheClass = _StatsCache(pChildStatsKey)
					ChildCacheItem.Set_DependsOnMe(pParentStatsKey)
					Return pChildStatsKey
				End If
			End SyncLock
		Catch ex As Exception
		End Try

		Return 0UL

	End Function

#End Region

#Region " Locals and Constants"

	Private _PertracData As PertracDataClass

	Private DynamicGroupMembersLock As ReaderWriterLock
	Private Const DynamicGroupMembersTimeout As Integer = 1000

	Private _StatsCache As Dictionary(Of ULong, StatCacheClass)
	Private _ComparisonStatisticsCache As ComparisonStatisticsCacheClass
	Private _StatsIDDictionary As Dictionary(Of Integer, UInteger)
	Private _DrawDownCache As Dictionary(Of ULong, DrawDownInstanceClass())

	Private DynamicGroupDefinitions As Dictionary(Of UInteger, DynamicGroupDefinitionClass)

	Private _MAX_StatsCacheSize As Integer
	Public Const MIN_StatsCacheSize As Integer = 100
	Public Const Default_MAX_StatsCacheSize As Integer = 400
	Private Const EPSILON As Double = 0.0000001# ' Used for comparing Doubles.
	Private Const MAX_GROUP_NESTINGLEVEL As Integer = 10 ' Used to block recursion caused by nesting Groups withing groups.

	Private Lock_BuildNewStatSeries As New Object
	Private Lock_StdDevSeries As New Object
	Private Lock_RefreshStatsSeries As New Object

	'Private _Sqrt12 As Double
	'Private _AnnualPeriodCount As Integer
	'Private _BACKFILL_DateLimit As Date

	'Private _StatsDatePeriod As RenaissanceGlobals.DealingPeriod '  = DealingPeriod.Monthly
	Private BackfillMonthCount As Integer

	' Stats Cache Key Constants :-
	Private Const Ulong_8BitMask As ULong = CULng((2 ^ 8) - 1)
	Private Const Ulong_26BitMask As ULong = CULng((2 ^ 26) - 1)
	Private Const Ulong_Bit9Mask As ULong = CULng(2 ^ 8)	' Yes, this is the 9th bit. Used to indicate Matched Volatility in backfill series.

#End Region

#Region " Properties"

	Private ReadOnly Property PertracData() As PertracDataClass
		Get
			Return _PertracData
		End Get
	End Property

	Public Property MAX_StatCacheSize() As Integer
		Get
			Return _MAX_StatsCacheSize
		End Get
		Set(ByVal value As Integer)
			If value >= MIN_StatsCacheSize Then
				_MAX_StatsCacheSize = value
			End If
		End Set
	End Property

	Public Property BACKFILL_DateLimit(ByVal StatsDatePeriod As RenaissanceGlobals.DealingPeriod) As Date
		Get
			Return FitDateToPeriod(StatsDatePeriod, Now.Date.AddMonths(-BackfillMonthCount), True)
		End Get
		Set(ByVal value As Date)

			BackfillMonthCount = GetPeriodCount(DealingPeriod.Monthly, value, Now()) + 1

			Me.ClearCache()
		End Set
	End Property

	Public ReadOnly Property AnnualPeriodCount(ByVal StatsDatePeriod As RenaissanceGlobals.DealingPeriod) As Integer
		Get
			Select Case StatsDatePeriod
				Case DealingPeriod.Daily
					Return 261 ' Business days

				Case DealingPeriod.Weekly
					Return 52	' (365 / 7)

				Case DealingPeriod.Fortnightly
					Return 26	' (365 / 14)

				Case DealingPeriod.Monthly
					Return 12

				Case DealingPeriod.Quarterly
					Return 4

				Case DealingPeriod.SemiAnnually
					Return 2

				Case DealingPeriod.Annually
					Return 1

				Case Else
					Return 12
			End Select
		End Get
	End Property

	Public ReadOnly Property Sqrt12(ByVal StatsDatePeriod As RenaissanceGlobals.DealingPeriod) As Double
		Get
			Select Case StatsDatePeriod
				Case DealingPeriod.Daily
					Return 16.15549442140351 ' Math.Sqrt(261) ' 19.1049731745428 ' Math.Sqrt(365)

				Case DealingPeriod.Weekly
					Return 7.21110255092798	' Math.Sqrt(52)

				Case DealingPeriod.Fortnightly
					Return 5.09901951359278	' Math.Sqrt(26)

				Case DealingPeriod.Monthly
					Return 3.46410161513775	' Math.Sqrt(12)

				Case DealingPeriod.Quarterly
					Return 2.0 ' Math.Sqrt(4)

				Case DealingPeriod.SemiAnnually
					Return 3.46410161513775	' Math.Sqrt(2)

				Case DealingPeriod.Annually
					Return 1.0

				Case Else
					Return 3.46410161513775	' Math.Sqrt(12)
			End Select
		End Get
	End Property

#End Region

#Region " New()"

	Private Sub New()
		' ************************************************************************
		'
		' ************************************************************************

		' Initialise Cache Object

		_StatsCache = New Dictionary(Of ULong, StatCacheClass)
		_ComparisonStatisticsCache = New ComparisonStatisticsCacheClass(Me)
		_DrawDownCache = New Dictionary(Of ULong, DrawDownInstanceClass())
		_StatsIDDictionary = New Dictionary(Of Integer, UInteger)

		DynamicGroupDefinitions = New Dictionary(Of UInteger, DynamicGroupDefinitionClass)
		DynamicGroupMembersLock = New ReaderWriterLock

		' Initialise variable Cache MAX figure.

		_MAX_StatsCacheSize = Default_MAX_StatsCacheSize

		'

		BackfillMonthCount = 60

	End Sub

	Public Sub New(ByRef pPertracData As PertracDataClass)
		' ************************************************************************
		'
		' ************************************************************************

		Me.New()

		_PertracData = pPertracData

		' InitialiseStatsObject()

	End Sub

#End Region

#Region " Combine Pertrac ID Functions."

	Public Function CombinePertracIDs(ByVal pPertracID As Integer, ByVal pPertracBackfillID As Integer) As ULong
		' *********************************************************************************
		'
		' *********************************************************************************

		Return CombinedStatsID(pPertracID, PertracDataClass.PertracInstrumentFlags.PertracInstrument, pPertracBackfillID, PertracDataClass.PertracInstrumentFlags.PertracInstrument)

	End Function

	Public Function CombinePertracIDs(ByVal pInstrumentID As Integer, ByVal pIsVeniceInstrument As Boolean, Optional ByVal pBackfillInstrumentID As Integer = 0, Optional ByVal pBackfillIsVeniceInstrument As Boolean = False) As ULong

		Try
			Dim InstrumentFlags As PertracDataClass.PertracInstrumentFlags
			Dim BackfillFlags As PertracDataClass.PertracInstrumentFlags

			If (pIsVeniceInstrument) Then
				InstrumentFlags = PertracDataClass.PertracInstrumentFlags.VeniceInstrument
			Else
				InstrumentFlags = PertracDataClass.PertracInstrumentFlags.PertracInstrument
			End If

			If (pBackfillIsVeniceInstrument) Then
				BackfillFlags = PertracDataClass.PertracInstrumentFlags.VeniceInstrument
			Else
				BackfillFlags = PertracDataClass.PertracInstrumentFlags.PertracInstrument
			End If

			Return CombinedStatsID(pInstrumentID, InstrumentFlags, pBackfillInstrumentID, BackfillFlags)

		Catch ex As Exception
		End Try

	End Function

	Public Function CombinedStatsID(ByVal pInstrumentID As Integer, ByVal pIsVeniceInstrument As Boolean, Optional ByVal pBackfillInstrumentID As Integer = 0, Optional ByVal pBackfillIsVeniceInstrument As Boolean = False) As ULong
		' ***********************************************************************
		' Combine One or more Instrument (Pertrac) ID to a single Stats ID
		' 
		' Existing Instrument flags will be lost if the instrument type is specified
		' by one of the boolean 'IsVenice' parameters. If the 'IsVenice' flag is False,
		' then any existing flags are preserved.
		' ***********************************************************************

		Try
			Dim InstrumentFlags As PertracDataClass.PertracInstrumentFlags
			Dim BackfillFlags As PertracDataClass.PertracInstrumentFlags
			Dim ExistingFlags As PertracDataClass.PertracInstrumentFlags

			If (pIsVeniceInstrument) Then
				InstrumentFlags = PertracDataClass.PertracInstrumentFlags.VeniceInstrument
			Else
				ExistingFlags = PertracDataClass.GetFlagsFromID(pInstrumentID)

				If (CInt(ExistingFlags) = 0) Then
					InstrumentFlags = PertracDataClass.PertracInstrumentFlags.PertracInstrument
				Else
					InstrumentFlags = ExistingFlags
				End If
			End If

			If (pBackfillIsVeniceInstrument) Then
				BackfillFlags = PertracDataClass.PertracInstrumentFlags.VeniceInstrument
			Else
				ExistingFlags = PertracDataClass.GetFlagsFromID(pBackfillInstrumentID)

				If (CInt(ExistingFlags) = 0) Then
					BackfillFlags = PertracDataClass.PertracInstrumentFlags.PertracInstrument
				Else
					BackfillFlags = ExistingFlags
				End If
			End If

			Return CombinedStatsID(pInstrumentID, InstrumentFlags, pBackfillInstrumentID, BackfillFlags)

		Catch ex As Exception
		End Try

	End Function

	Public Function CombinedStatsID(ByVal pInstrumentID As Integer, ByVal pInstrumentFlags As PertracDataClass.PertracInstrumentFlags, ByVal pBackfillInstrumentID As Integer, ByVal pBackfillFlags As PertracDataClass.PertracInstrumentFlags) As ULong
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		Dim PrimaryInstrumentID As UInteger
		Dim BackfillInstrumentID As UInteger
		Dim ThisInstrumentID As ULong

		Try

			' Derive Instrument ID

			PrimaryInstrumentID = PertracDataClass.SetFlagsToID(CUInt(pInstrumentID), pInstrumentFlags)

			If (pBackfillInstrumentID > 0) Then

				BackfillInstrumentID = PertracDataClass.SetFlagsToID(CUInt(pBackfillInstrumentID), pBackfillFlags)


				ThisInstrumentID = CombinedStatsID(PrimaryInstrumentID, BackfillInstrumentID)	' (CULng(BackfillInstrumentID) << 32) Or CULng(PrimaryInstrumentID)

			Else

				ThisInstrumentID = CULng(PrimaryInstrumentID)

			End If

		Catch ex As Exception

			ThisInstrumentID = 0UL

		End Try

		Return ThisInstrumentID

	End Function

	Public Function CombinedStatsID(ByVal pInstrumentID As UInteger, ByVal pBackfillInstrumentID As UInteger) As ULong
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		Try

			Return (CULng(pBackfillInstrumentID) << 32) Or CULng(pInstrumentID)

		Catch ex As Exception
		End Try

		Return 0UL

	End Function

#End Region

#Region " Cache Control methods"

	Public Function ClearCache() As ULong()
		' ********************************************************************************
		' Clear all Entries in the Statistics Cache.
		'
		'
		' ********************************************************************************

		Return ClearCache(_StatsCache, False, False, False, False, False, "")

	End Function

	Public Function ClearCache(ByVal OnlyVeniceInstruments As Boolean, ByVal OnlyGroupInstruments As Boolean, ByVal OnlyDynamicGroupSeries As Boolean, ByVal OnlyDrilldownGroupSeries As Boolean, ByVal OnlyCTA_SimulationSeries As Boolean, ByVal InstrumentIdentifier As String) As ULong()
		' ********************************************************************************
		' Clear specified Entries in the Statistics Cache.
		'
		'
		' ********************************************************************************

		Return ClearCache(_StatsCache, OnlyVeniceInstruments, OnlyGroupInstruments, OnlyDynamicGroupSeries, OnlyDrilldownGroupSeries, OnlyCTA_SimulationSeries, InstrumentIdentifier)

	End Function

	Private Function ClearCache(ByVal pStatCache As Dictionary(Of ULong, StatCacheClass), ByVal OnlyVeniceInstruments As Boolean, ByVal OnlyGroupInstruments As Boolean, ByVal OnlyDynamicGroupSeries As Boolean, ByVal OnlyDrilldownGroupSeries As Boolean, ByVal OnlyCTA_SimulationSeries As Boolean, ByVal InstrumentIdentifier As String) As ULong()
		' ********************************************************************************
		' Clear specified Entries in the Statistics Cache.
		'
		'
		' ********************************************************************************

		Dim ThisID As Integer = 0

		Try
			If (InstrumentIdentifier IsNot Nothing) AndAlso (IsNumeric(InstrumentIdentifier)) Then
				ThisID = CInt(InstrumentIdentifier)
			End If
		Catch ex As Exception
			ThisID = 0
		End Try

		Return ClearCache(pStatCache, OnlyVeniceInstruments, OnlyGroupInstruments, OnlyDynamicGroupSeries, OnlyDrilldownGroupSeries, OnlyCTA_SimulationSeries, ThisID)

	End Function

	Private Function ClearCache(ByVal pStatCache As Dictionary(Of ULong, StatCacheClass), ByVal OnlyVeniceInstruments As Boolean, ByVal OnlyGroupInstruments As Boolean, ByVal OnlyDynamicGroupSeries As Boolean, ByVal OnlyDrilldownGroupSeries As Boolean, ByVal OnlyCTA_SimulationSeries As Boolean, ByVal InstrumentIdentifier As Integer) As ULong()
		' ********************************************************************************
		' Clear specified Entries in the Statistics Cache.
		'
		' 
		' ********************************************************************************

		Dim ThisID As Integer = 0
		Dim ID_Provided As Boolean = False
		Dim RVal(-1) As ULong
		Dim DeletedIDs As New ArrayList

		If (pStatCache Is Nothing) OrElse (pStatCache.Count <= 0) Then
			Return RVal
			Exit Function
		End If

		If (InstrumentIdentifier > 0) Then
			ThisID = CInt(InstrumentIdentifier)

			If (ThisID > 0) Then
				ID_Provided = True
			Else
				ThisID = 0
			End If
		End If

		SyncLock pStatCache
			If (ID_Provided) OrElse (OnlyVeniceInstruments) OrElse (OnlyGroupInstruments) OrElse (OnlyDynamicGroupSeries) OrElse (OnlyDrilldownGroupSeries) Then

				Dim ValueArray(pStatCache.Count - 1) As StatCacheClass
				Dim ItemCounter As Integer
				Dim ThisKey As ULong
				Dim ThisItemRemoved As Boolean
				Dim ThisCacheItem As StatCacheClass

				pStatCache.Values.CopyTo(ValueArray, 0)

				For ItemCounter = 0 To (ValueArray.Count - 1)
					ThisItemRemoved = False

					If (ValueArray(ItemCounter) IsNot Nothing) Then

						ThisCacheItem = ValueArray(ItemCounter)

						If (OnlyVeniceInstruments) AndAlso (ThisCacheItem.HasVeniceInstrument(ThisID)) Then

							ThisItemRemoved = True

						ElseIf (OnlyGroupInstruments) AndAlso (ThisCacheItem.HasGroupInstrument(ThisID)) Then

							ThisItemRemoved = True

						ElseIf (OnlyDynamicGroupSeries) AndAlso (ThisCacheItem.HasDynamicGroupInstrument(ThisID)) Then

							ThisItemRemoved = True

						ElseIf (OnlyDrilldownGroupSeries) AndAlso (ThisCacheItem.HasDrilldownGroupInstrument(ThisID)) Then

							ThisItemRemoved = True

						ElseIf (OnlyCTA_SimulationSeries) AndAlso (ThisCacheItem.HasCTA_Simulation(ThisID)) Then

							ThisItemRemoved = True

						ElseIf (Not (OnlyVeniceInstruments Or OnlyGroupInstruments Or OnlyDynamicGroupSeries Or OnlyDrilldownGroupSeries)) AndAlso (ID_Provided) AndAlso (ThisCacheItem.HasPertracInstrument(ThisID)) Then

							ThisItemRemoved = True

						ElseIf (ThisCacheItem.HasDynamicGroupInstrument(ThisID)) Then
							' Why is this here ?

							ThisItemRemoved = True

						End If

						If (ThisItemRemoved) Then

							ThisKey = GetStatCacheKey(ThisCacheItem)

							' Don't patch dependencies as this process will(Should) delete the whole dependency chain.
							' PatchDependencyLists(pStatCache, ThisCacheItem)

							If (pStatCache.ContainsKey(ThisKey)) Then
								pStatCache.Remove(ThisKey)
							End If
							DeletedIDs.Add(ThisKey)

							If (_DrawDownCache.ContainsKey(ThisKey)) Then
								_DrawDownCache.Remove(ThisKey)
							End If

							' Check for Dependant cache objects.

							ValueArray(ItemCounter) = Nothing	' Prevents Recursive problems

							ClearParentCacheItems(pStatCache, ThisCacheItem, ValueArray, DeletedIDs)

						End If

						Try
							' Just in case.

							If (pStatCache.Count <= 0) Then
								Exit For
							End If
						Catch ex As Exception
						End Try

					End If

				Next

				If (ValueArray IsNot Nothing) AndAlso (ValueArray.Length > 0) Then
					Array.Clear(ValueArray, 0, ValueArray.Length)
				End If

				' Comparison Cache

				If (ID_Provided) Then

					If (OnlyVeniceInstruments) Then
						_ComparisonStatisticsCache.ClearCache(PertracDataClass.SetFlagsToID(CUInt(ThisID), PertracDataClass.PertracInstrumentFlags.VeniceInstrument))
					ElseIf (OnlyGroupInstruments) Then
						_ComparisonStatisticsCache.ClearCache(PertracDataClass.SetFlagsToID(CUInt(ThisID), PertracDataClass.PertracInstrumentFlags.Group_Mean))
						_ComparisonStatisticsCache.ClearCache(PertracDataClass.SetFlagsToID(CUInt(ThisID), PertracDataClass.PertracInstrumentFlags.Group_Median))
						_ComparisonStatisticsCache.ClearCache(PertracDataClass.SetFlagsToID(CUInt(ThisID), PertracDataClass.PertracInstrumentFlags.Group_Weighted))
					ElseIf (OnlyCTA_SimulationSeries) Then
						_ComparisonStatisticsCache.ClearCache(PertracDataClass.SetFlagsToID(CUInt(ThisID), PertracDataClass.PertracInstrumentFlags.CTA_Simulation))
					Else
						_ComparisonStatisticsCache.ClearCache(CUInt(ThisID))
					End If

				Else

					_ComparisonStatisticsCache.ClearCache()

				End If

			Else

				SyncLock pStatCache
					RVal = pStatCache.Keys.ToArray
					pStatCache.Clear()
					_DrawDownCache.Clear()
				End SyncLock

				SyncLock DynamicGroupDefinitions
					DynamicGroupDefinitions.Clear()
				End SyncLock

				_ComparisonStatisticsCache.ClearCache()

			End If

		End SyncLock

		Return RVal

	End Function

	Private Sub ClearParentCacheItems(ByVal pStatCache As Dictionary(Of ULong, StatCacheClass), ByVal DeletedCacheItem As StatCacheClass, ByVal CacheClassArray() As StatCacheClass, ByVal DeletedIDs As ArrayList)
		' *****************************************************************************
		' Recurse up the dependency List, removing items from the cache.
		'
		' Don't forget to delete from the DrawDownCache and the ComparisonStatisticsCache
		'
		' *****************************************************************************

		Try
			If (CacheClassArray Is Nothing) OrElse (CacheClassArray.Length <= 0) Then
				Exit Sub
			End If

			Dim ParentKeys() As ULong = DeletedCacheItem.Get_DependsOnMe
			Dim ThisParentKey As ULong
			Dim ThisParentCacheItem As StatCacheClass
			Dim CacheClassIndex As Integer

			SyncLock pStatCache

				For Each ThisParentKey In ParentKeys

					' Check the CacheClassArray for items matching the 'Parent' IDs

					For CacheClassIndex = 0 To (CacheClassArray.Length - 1)

						If (CacheClassArray(CacheClassIndex) IsNot Nothing) Then

							' Key match found.

							If ThisParentKey = GetStatCacheKey(CacheClassArray(CacheClassIndex)) Then

								If (DeletedIDs IsNot Nothing) Then
									DeletedIDs.Add(ThisParentKey)
								End If

								' Clear Drawdown cache.

								Try
									If (_DrawDownCache.ContainsKey(ThisParentKey)) Then
										_DrawDownCache.Remove(ThisParentKey)
									End If
								Catch ex As Exception
								End Try

								' Clear Stats Cache.

								If (pStatCache.ContainsKey(ThisParentKey)) Then

									ThisParentCacheItem = pStatCache(ThisParentKey)
									pStatCache.Remove(ThisParentKey)
									CacheClassArray(CacheClassIndex) = Nothing

									If (ThisParentCacheItem.Get_DependsOnMe.Length > 0) Then

										ClearParentCacheItems(pStatCache, ThisParentCacheItem, CacheClassArray, DeletedIDs)

									End If

									' Clear Comparisons Cache.

									Try
										Dim BasePertracID As Integer
										Dim BackfillPertracID As Integer
										BasePertracID = CInt(ThisParentCacheItem.PertracID And Ulong_31BitMask)	' Don't use Ulong_32BitMask as the 32nd bit is the sign bit for a signed integer.
										BackfillPertracID = CInt((ThisParentCacheItem.PertracID >> 32) And Ulong_31BitMask)

										If (BasePertracID > 0) Then
											_ComparisonStatisticsCache.ClearCache(CUInt(BasePertracID))
										End If

										If (BackfillPertracID > 0) Then
											_ComparisonStatisticsCache.ClearCache(CUInt(BackfillPertracID))
										End If

									Catch ex As Exception
									End Try

								End If

							End If
						End If
					Next

				Next

			End SyncLock

		Catch ex As Exception
		End Try

	End Sub

	Private Sub PatchDependencyLists(ByVal pStatCache As Dictionary(Of ULong, StatCacheClass), ByVal DeletedCacheItem As StatCacheClass)
		' *****************************************************************************
		' Pass Through Parent-Child dependency Links
		' 
		' Only used when cache items are trimmed due to cache size, rather than deleted
		' due to underlying instrument updates.
		'
		' *****************************************************************************

		Try

			Dim ParentKeys() As ULong = DeletedCacheItem.Get_DependsOnMe
			Dim ChildKeys() As ULong = DeletedCacheItem.Get_I_DependOn
			Dim ThisParentKey As ULong
			Dim ThisChildKey As ULong
			Dim DeletedCacheKey As ULong = GetStatCacheKey(DeletedCacheItem)

			Dim ThisParentCacheItem As StatCacheClass
			Dim ThisChildCacheItem As StatCacheClass

			SyncLock pStatCache


				If (ParentKeys.Length > 0) Then
					For Each ThisParentKey In ParentKeys
						If (ThisParentKey > 0UL) Then
							If (pStatCache.ContainsKey(ThisParentKey)) Then
								ThisParentCacheItem = pStatCache(ThisParentKey)

								If (ThisParentCacheItem IsNot Nothing) Then
									' ThisParentCacheItem.Remove_I_DependOn(DeletedCacheKey)

									If (ChildKeys.Length > 0) Then
										For Each ThisChildKey In ChildKeys
											If (ThisChildKey > 0UL) Then
												ThisParentCacheItem.Set_I_DependOn(ThisChildKey)
											End If
										Next
									End If

									ThisParentCacheItem = Nothing
								End If
							End If
						End If
					Next
				End If

				If (ChildKeys.Length > 0) Then
					For Each ThisChildKey In ChildKeys
						If (ThisChildKey > 0UL) Then
							If (pStatCache.ContainsKey(ThisChildKey)) Then
								ThisChildCacheItem = pStatCache(ThisChildKey)

								If (ThisChildCacheItem IsNot Nothing) Then
									' ThisChildCacheItem.Remove_DependsOnMe(DeletedCacheKey)

									If (ParentKeys.Length > 0) Then
										For Each ThisParentKey In ParentKeys
											If (ThisParentKey > 0UL) Then
												ThisChildCacheItem.Set_DependsOnMe(ThisParentKey)
											End If
										Next
									End If

									ThisParentCacheItem = Nothing
								End If

							End If
						End If
					Next
				End If

			End SyncLock

		Catch ex As Exception
		End Try

	End Sub


	Private Sub TrimStatCache(ByVal pStatCache As Dictionary(Of ULong, StatCacheClass), ByVal pDrawDownCache As Dictionary(Of ULong, DrawDownInstanceClass()), ByVal pNewCacheSize As Integer)
		' *****************************************************************************
		'
		'
		' *****************************************************************************

		Try
			If (pStatCache.Count <= pNewCacheSize) Then
				Exit Sub
			End If
		Catch ex As Exception
		End Try

		SyncLock pStatCache
			Try

				If (pNewCacheSize <= 0) Then

					pStatCache.Clear()
					pDrawDownCache.Clear()

				Else

					Dim ValueArray(pStatCache.Count - 1) As StatCacheClass
					Dim ItemCounter As Integer
					Dim ThisKey As ULong

					pStatCache.Values.CopyTo(ValueArray, 0)
					Array.Sort(ValueArray, New StatCacheDateComparer)

					For ItemCounter = 0 To ((pStatCache.Count - pNewCacheSize) - 1)
						ThisKey = GetStatCacheKey(ValueArray(ItemCounter).StatsDatePeriod, ValueArray(ItemCounter).PertracID, ValueArray(ItemCounter).MatchBackfillVolatility)
						pStatCache.Remove(ThisKey)
						PatchDependencyLists(pStatCache, ValueArray(ItemCounter))

						If (pDrawDownCache.ContainsKey(ThisKey)) Then
							pDrawDownCache.Remove(ThisKey)
						End If

					Next

				End If

			Catch ex As Exception
			End Try

		End SyncLock

	End Sub

	Public Function StatCacheItemExist(ByVal StatsDatePeriod As DealingPeriod, ByVal pPertracID As ULong, ByVal MatchBackfillVol As Boolean) As Boolean
		' *****************************************************************************
		'
		'
		' *****************************************************************************

		Try

			SyncLock _StatsCache

				Dim CacheKey As ULong = GetStatCacheKey(StatsDatePeriod, pPertracID, MatchBackfillVol)

				If _StatsCache.ContainsKey(CacheKey) Then
					Return True
				End If

			End SyncLock

		Catch ex As Exception
		End Try

		Return False

	End Function

	Private Function GetStatCacheItem(ByVal StatsDatePeriod As DealingPeriod, ByVal pPertracID As ULong, ByVal MatchBackfillVol As Boolean, Optional ByVal pPeriodCount As Integer = (-1), Optional ByVal pLamda As Double = (-1)) As StatCacheClass
		' *****************************************************************************
		'
		'
		' *****************************************************************************


		SyncLock _StatsCache
			Dim RVal As StatCacheClass = Nothing
			Dim CacheKey As ULong = GetStatCacheKey(StatsDatePeriod, pPertracID, MatchBackfillVol)

			If _StatsCache.ContainsKey(CacheKey) = False Then
				Return Nothing
			End If

			RVal = _StatsCache(CacheKey)

			If ((pPeriodCount <= 0) Or (RVal.PeriodCount = pPeriodCount)) AndAlso ((pLamda <= (-1.0#)) Or (RVal.Lamda = pLamda)) Then
				Return RVal
			End If

			' If PeriodCount or Lamda do not match, re-calculate Average and StDev Series

			Dim PeriodCount As Integer = RVal.PeriodCount
			Dim Lamda As Double = RVal.Lamda

			If (pPeriodCount > 0) Then
				PeriodCount = pPeriodCount
			End If

			If (pLamda > 0) Then
				Lamda = pLamda
			End If

			RVal = Me.RefreshStatsSeries(RVal, PeriodCount, Lamda)

			Return RVal

		End SyncLock

	End Function

	Private Sub SetStatCacheItem(ByRef thisStatCache As StatCacheClass)
		' *****************************************************************************
		'
		'
		' *****************************************************************************

		SyncLock _StatsCache
			If (thisStatCache Is Nothing) Then
				Exit Sub
			End If

			' Trim Cache ?

			If (_StatsCache.Count) > MAX_StatCacheSize Then
				TrimStatCache(_StatsCache, _DrawDownCache, CInt(MAX_StatCacheSize * 0.8#))
			End If

			' Add Item.

			Dim CacheKey As ULong = GetStatCacheKey(thisStatCache.StatsDatePeriod, thisStatCache.PertracID, thisStatCache.MatchBackfillVolatility)

			If _StatsCache.ContainsKey(CacheKey) Then
				_StatsCache.Remove(CacheKey)
			End If

			_StatsCache.Add(CacheKey, thisStatCache)
		End SyncLock
	End Sub

#End Region

#Region " Series Creation / Retrieval Code."

	Public Function GetStartDate(ByVal pPertracID As ULong, ByVal Offset As Integer, ByRef eMessage As String) As Date
		' ****************************************************************************************************
		' Return Eariest date in the series
		'
		' 
		' ****************************************************************************************************

		Dim MatchBackfillVol As Boolean = False
		Dim RVal As Date = RenaissanceGlobals.Globals.Renaissance_BaseDate
		Dim StatsDatePeriod As DealingPeriod

		Try
			StatsDatePeriod = Me.PertracData.GetPertracDataPeriod(CInt(pPertracID And Ulong_32BitMask), eMessage)	 ' Base ID

			SyncLock Lock_StdDevSeries
				Dim thisStatCache As StatCacheClass

				' Get Existing Pertrac Series ?
				thisStatCache = Me.GetStatCacheItem(StatsDatePeriod, pPertracID, MatchBackfillVol)

				If (thisStatCache Is Nothing) Then
					thisStatCache = BuildNewStatSeries(StatsDatePeriod, 0, pPertracID, MatchBackfillVol, AnnualPeriodCount(StatsDatePeriod), 1.0#, -1, False, RenaissanceGlobals.Globals.KNOWLEDGEDATE_NOW, eMessage)
					SetStatCacheItem(thisStatCache)
				End If

				If (thisStatCache IsNot Nothing) AndAlso (thisStatCache.DateArray IsNot Nothing) AndAlso (thisStatCache.DateArray.Count > 0) Then
					RVal = thisStatCache.DateArray(Math.Max(0, Offset))
				End If

			End SyncLock

		Catch ex As Exception
		End Try

		Return RVal

	End Function

	Public Function DateSeries(ByVal StatsDatePeriod As DealingPeriod, ByVal pPertracID As ULong, ByVal MatchBackfillVol As Boolean, ByVal PeriodCount As Integer, ByVal pLamda As Double, ByVal Annualised As Boolean, ByVal pStatsStartDate As Date, ByVal pStatsEndDate As Date, ByRef eMessage As String) As Date()
		' *****************************************************************************
		'
		'
		' *****************************************************************************

		Try

			Return DateSeries(StatsDatePeriod, 0, pPertracID, MatchBackfillVol, PeriodCount, pLamda, -1, Annualised, pStatsStartDate, pStatsEndDate, eMessage)

		Catch ex As Exception
		End Try

		Return New Date() {}

	End Function

	Public Function DateSeries(ByVal StatsDatePeriod As DealingPeriod, ByVal pNestingLevel As Integer, ByVal pPertracID As ULong, ByVal MatchBackfillVol As Boolean, ByVal PeriodCount As Integer, ByVal pLamda As Double, ByVal pSampleSize As Integer, ByVal Annualised As Boolean, ByVal pStatsStartDate As Date, ByVal pStatsEndDate As Date, ByRef eMessage As String) As Date()
		' *****************************************************************************
		'
		'
		' *****************************************************************************

		Try
			SyncLock Lock_StdDevSeries
				Dim thisStatCache As StatCacheClass

				' Get Existing Series ?
				thisStatCache = Me.GetStatCacheItem(StatsDatePeriod, pPertracID, MatchBackfillVol)

				If (thisStatCache Is Nothing) Then
					Dim Lamda As Double
					Lamda = pLamda
					If (Lamda < 0) Then
						Lamda = 1.0#
					End If

					thisStatCache = BuildNewStatSeries(StatsDatePeriod, pNestingLevel, pPertracID, MatchBackfillVol, PeriodCount, Lamda, pSampleSize, False, RenaissanceGlobals.Globals.KNOWLEDGEDATE_NOW, eMessage)
					SetStatCacheItem(thisStatCache)
				End If

				If (thisStatCache Is Nothing) Then
					Return Nothing
				End If

				' Return Date Series

				Return CType(ReturnFittedDataArray(StatsDatePeriod, thisStatCache.DateArray, thisStatCache.DateArray, DataItemEnum.Dates, pStatsStartDate, pStatsEndDate, 1.0#, False, 100.0#), Date())

			End SyncLock
		Catch ex As Exception
			Return Nothing
		End Try

		Return Nothing

	End Function

	Public Function ReturnSeries(ByVal StatsDatePeriod As DealingPeriod, ByVal pPertracID As ULong, ByVal MatchBackfillVol As Boolean, ByVal PeriodCount As Integer, ByVal pLamda As Double, ByVal Annualised As Boolean, ByVal pStatsStartDate As Date, ByVal pStatsEndDate As Date, ByVal pScalingFactor As Double, ByRef eMessage As String) As Double()
		' *****************************************************************************
		'
		'
		' *****************************************************************************

		Return ReturnSeries_Contingent(StatsDatePeriod, 0, pPertracID, 0, MatchBackfillVol, ContingentSelect.ConditionAll, PeriodCount, pLamda, Annualised, -1, pStatsStartDate, pStatsEndDate, pScalingFactor, eMessage)

	End Function

	Public Function ReturnSeries(ByVal StatsDatePeriod As DealingPeriod, ByVal pNestingLevel As Integer, ByVal pPertracID As ULong, ByVal MatchBackfillVol As Boolean, ByVal PeriodCount As Integer, ByVal pLamda As Double, ByVal pSampleSize As Integer, ByVal Annualised As Boolean, ByVal pStatsStartDate As Date, ByVal pStatsEndDate As Date, ByVal pScalingFactor As Double, ByRef eMessage As String) As Double()
		' *****************************************************************************
		'
		'
		' *****************************************************************************

		Return ReturnSeries_Contingent(StatsDatePeriod, pNestingLevel, pPertracID, 0, MatchBackfillVol, ContingentSelect.ConditionAll, PeriodCount, pLamda, Annualised, pSampleSize, pStatsStartDate, pStatsEndDate, pScalingFactor, eMessage)

	End Function

	Public Function ReturnSeries_Contingent(ByVal StatsDatePeriod As DealingPeriod, ByVal pPertracID As ULong, ByVal pConditionalPertracID As ULong, ByVal MatchBackfillVol As Boolean, ByVal pCondition As ContingentSelect, ByVal PeriodCount As Integer, ByVal pLamda As Double, ByVal Annualised As Boolean, ByVal pStatsStartDate As Date, ByVal pStatsEndDate As Date, ByVal pScalingFactor As Double, ByRef eMessage As String) As Double()
		' *****************************************************************************
		'
		'
		' *****************************************************************************

		Return ReturnSeries_Contingent(StatsDatePeriod, 0, pPertracID, pConditionalPertracID, MatchBackfillVol, pCondition, PeriodCount, pLamda, Annualised, -1, pStatsStartDate, pStatsEndDate, pScalingFactor, eMessage)

	End Function

	Public Function ReturnSeries_Contingent(ByVal StatsDatePeriod As DealingPeriod, ByVal pNestingLevel As Integer, ByVal pPertracID As ULong, ByVal pConditionalPertracID As ULong, ByVal MatchBackfillVol As Boolean, ByVal pCondition As ContingentSelect, ByVal PeriodCount As Integer, ByVal pLamda As Double, ByVal Annualised As Boolean, ByVal pSampleSize As Integer, ByVal pStatsStartDate As Date, ByVal pStatsEndDate As Date, ByVal pScalingFactor As Double, ByRef eMessage As String) As Double()
		' *****************************************************************************
		'
		'
		' *****************************************************************************

		Dim Lamda As Double

		Lamda = pLamda
		If (Lamda < 0) Then
			Lamda = 1
		End If

		Try
			SyncLock Lock_StdDevSeries
				Dim thisStatCache As StatCacheClass
				Dim ConditionalStatCache As StatCacheClass

				Try
					' Get Existing Pertrac Series ?
					thisStatCache = Me.GetStatCacheItem(StatsDatePeriod, pPertracID, MatchBackfillVol)

					If (thisStatCache Is Nothing) Then
						thisStatCache = BuildNewStatSeries(StatsDatePeriod, pNestingLevel, pPertracID, MatchBackfillVol, PeriodCount, Lamda, pSampleSize, False, RenaissanceGlobals.Globals.KNOWLEDGEDATE_NOW, eMessage)
						SetStatCacheItem(thisStatCache)
					End If
				Catch ex As Exception
					thisStatCache = Nothing
				End Try

				If (thisStatCache Is Nothing) Then
					Return Nothing
				End If

				' Quick Exit - All Returns Selected?
				If (pCondition = ContingentSelect.ConditionAll) Then
					Return CType(ReturnFittedDataArray(StatsDatePeriod, thisStatCache.DateArray, thisStatCache.ReturnsArray, DataItemEnum.Returns, pStatsStartDate, pStatsEndDate, pScalingFactor, False, 100.0#), Double())
				End If

				Try
					' Get Existing Conditional Series ?
					ConditionalStatCache = Me.GetStatCacheItem(StatsDatePeriod, pConditionalPertracID, MatchBackfillVol)

					If (ConditionalStatCache Is Nothing) Then
						ConditionalStatCache = BuildNewStatSeries(StatsDatePeriod, pConditionalPertracID, MatchBackfillVol, PeriodCount, Lamda, eMessage)
						SetStatCacheItem(ConditionalStatCache)
					End If
				Catch ex As Exception
					ConditionalStatCache = Nothing
				End Try

				If (ConditionalStatCache Is Nothing) Then
					Return Nothing
				End If

				' Derive Partial Return series

				Dim RVal() As Double
				Dim ThisStartIndex As Integer = 0
				Dim CompareStartIndex As Integer = 0
				Dim ThisMAXIndex As Integer = (thisStatCache.DateArray.Length - 1)
				Dim CompareMAXIndex As Integer = (ConditionalStatCache.DateArray.Length - 1)
				Dim thisReturnsArray() As Double = thisStatCache.ReturnsArray
				Dim thisConditionalArray() As Double = ConditionalStatCache.ReturnsArray

				RVal = CType(Array.CreateInstance(GetType(Double), thisStatCache.DateArray.Length), Double())

				CompareStartIndex = GetPriceIndex(StatsDatePeriod, ConditionalStatCache.DateArray(0), thisStatCache.DateArray(0))

				If (CompareStartIndex < 0) Then
					ThisStartIndex -= CompareStartIndex
					CompareStartIndex = 0
				End If

				While ((ThisStartIndex <= ThisMAXIndex) And (CompareStartIndex <= CompareMAXIndex))
					If pCondition = ContingentSelect.ConditionDown Then
						If (thisConditionalArray(CompareStartIndex) < 0) Then
							RVal(ThisStartIndex) = thisReturnsArray(ThisStartIndex)
						End If
					Else ' Condition Up
						If (thisConditionalArray(CompareStartIndex) > 0) Then
							RVal(ThisStartIndex) = thisReturnsArray(ThisStartIndex)
						End If
					End If

					ThisStartIndex += 1
					CompareStartIndex += 1
				End While

				' Return Series

				Return CType(ReturnFittedDataArray(StatsDatePeriod, thisStatCache.DateArray, RVal, DataItemEnum.Returns, pStatsStartDate, pStatsEndDate, pScalingFactor, False, 100.0#), Double())

			End SyncLock
		Catch ex As Exception
			Return Nothing
		End Try

		Return Nothing
	End Function

	Public Function AverageSeries(ByVal StatsDatePeriod As DealingPeriod, ByVal pPertracID As ULong, ByVal MatchBackfillVol As Boolean, ByVal PeriodCount As Integer, ByVal pLamda As Double, ByVal Annualised As Boolean, ByVal pStatsStartDate As Date, ByVal pStatsEndDate As Date, ByVal pScalingFactor As Double, ByRef eMessage As String) As Double()
		' *****************************************************************************
		'
		'
		' *****************************************************************************

		Return AverageSeries_Contingent(StatsDatePeriod, pPertracID, 0, MatchBackfillVol, ContingentSelect.ConditionAll, PeriodCount, pLamda, Annualised, pStatsStartDate, pStatsEndDate, pScalingFactor, eMessage)

	End Function

	Public Function AverageSeries_Contingent(ByVal StatsDatePeriod As DealingPeriod, ByVal pPertracID As ULong, ByVal pConditionalPertracID As ULong, ByVal MatchBackfillVol As Boolean, ByVal pCondition As ContingentSelect, ByVal PeriodCount As Integer, ByVal pLamda As Double, ByVal Annualised As Boolean, ByVal pStatsStartDate As Date, ByVal pStatsEndDate As Date, ByVal pScalingFactor As Double, ByRef eMessage As String) As Double()
		' *****************************************************************************************
		' Return Full-Value Average series.
		' That is, Lamda has no impact. This is a straight average, not weighted.
		'
		' *****************************************************************************************

		Dim Lamda As Double
		Lamda = pLamda
		If (Lamda < 0) Then
			Lamda = 1
		End If

		Try
			SyncLock Lock_StdDevSeries

				' Quick Exit - All Returns Selected?
				If (pCondition = ContingentSelect.ConditionAll) Then
					Dim thisStatCache As StatCacheClass

					Try
						' Get Existing Series ?
						thisStatCache = Me.GetStatCacheItem(StatsDatePeriod, pPertracID, MatchBackfillVol, PeriodCount)

						If (thisStatCache Is Nothing) Then

							thisStatCache = BuildNewStatSeries(StatsDatePeriod, pPertracID, MatchBackfillVol, PeriodCount, Lamda, eMessage)
							SetStatCacheItem(thisStatCache)
						End If
					Catch ex As Exception
						thisStatCache = Nothing
					End Try

					If (thisStatCache Is Nothing) Then
						Return Nothing
					End If

					Return CType(ReturnFittedDataArray(StatsDatePeriod, thisStatCache.DateArray, thisStatCache.AverageArray, DataItemEnum.Returns, pStatsStartDate, pStatsEndDate, pScalingFactor, False, 100.0#), Double())
				End If

				Dim thisReturnsArray() As Double
				Dim thisAverageArray() As Double
				Dim thisDateArray() As Date

				thisDateArray = DateSeries(StatsDatePeriod, pPertracID, MatchBackfillVol, PeriodCount, pLamda, Annualised, pStatsStartDate, pStatsEndDate, eMessage)
				thisReturnsArray = ReturnSeries_Contingent(StatsDatePeriod, pPertracID, pConditionalPertracID, MatchBackfillVol, pCondition, PeriodCount, pLamda, Annualised, pStatsStartDate, pStatsEndDate, pScalingFactor, eMessage)
				thisAverageArray = CType(Array.CreateInstance(GetType(Double), thisReturnsArray.Length), Double())

				' Calculate Averages Array

				Try
					Dim RollingSum As Double = 0
					Dim RowCounter As Integer
					Dim ExponentMultiplier(PeriodCount) As Double

					If (PeriodCount > 0) Then
						For RowCounter = 1 To (thisAverageArray.Length - 1)
							RollingSum += thisReturnsArray(RowCounter)
							If (RowCounter >= PeriodCount) Then	' Remember Return(0) is Zero.
								RollingSum -= thisReturnsArray(RowCounter - PeriodCount)

								thisAverageArray(RowCounter) = (RollingSum / PeriodCount)
							End If
						Next
					End If

				Catch ex As Exception
				End Try

				' Return Date Series

				Return CType(ReturnFittedDataArray(StatsDatePeriod, thisDateArray, thisAverageArray, DataItemEnum.Returns, pStatsStartDate, pStatsEndDate, 1.0#, False, 100.0#), Double()) ' No scaling here, the returns are already scaled.

			End SyncLock
		Catch ex As Exception
			Return Nothing
		End Try

		Return Nothing

	End Function

	Public Function NAVSeries(ByVal StatsDatePeriod As DealingPeriod, ByVal pPertracID As ULong, ByVal MatchBackfillVol As Boolean, ByVal PeriodCount As Integer, ByVal pLamda As Double, ByVal Annualised As Boolean, ByVal pStatsStartDate As Date, ByVal pStatsEndDate As Date, ByVal pScalingFactor As Double, ByVal pNormaliseNAVs As Boolean, ByVal InitialNormalisationValue As Double, ByRef eMessage As String) As Double()

		Try
			SyncLock Lock_StdDevSeries
				Dim thisStatCache As StatCacheClass

				' Get Existing Series ?
				thisStatCache = Me.GetStatCacheItem(StatsDatePeriod, pPertracID, MatchBackfillVol)

				If (thisStatCache Is Nothing) Then
					Dim Lamda As Double
					Lamda = pLamda
					If (Lamda < 0) Then
						Lamda = 1
					End If

					thisStatCache = BuildNewStatSeries(StatsDatePeriod, pPertracID, MatchBackfillVol, PeriodCount, Lamda, eMessage)
					SetStatCacheItem(thisStatCache)
				End If

				If (thisStatCache Is Nothing) Then
					Return Nothing
				End If

				'If (thisStatCache.NAVArray Is Nothing) Then

				'  Dim RCount As Integer

				'  thisStatCache.NAVArray = CType(Array.CreateInstance(GetType(Double), Math.Max(thisStatCache.ReturnsArray.Length, 1)), Double())

				'  thisStatCache.NAVArray(0) = 100
				'  For RCount = 1 To (thisStatCache.ReturnsArray.Length - 1)
				'    thisStatCache.NAVArray(RCount) = thisStatCache.NAVArray(RCount - 1) * (1.0 + thisStatCache.ReturnsArray(RCount))
				'  Next

				'  Return CType(ReturnFittedDataArray(StatsDatePeriod, thisStatCache.DateArray, thisStatCache.NAVArray, DataItemEnum.NAVs, pStatsStartDate, pStatsEndDate, pScalingFactor, pNormaliseNAVs), Double()) ' NAVs Don't Scale here.

				'Else

				'  Return CType(ReturnFittedDataArray(StatsDatePeriod, thisStatCache.DateArray, thisStatCache.NAVArray, DataItemEnum.NAVs, pStatsStartDate, pStatsEndDate, pScalingFactor, pNormaliseNAVs), Double()) ' NAVs Don't Scale here.

				'End If

				If (thisStatCache.NAVArray Is Nothing) Then

					SetNAVSeries(thisStatCache)

				End If

				If (thisStatCache.NAVArray IsNot Nothing) Then

					Return CType(ReturnFittedDataArray(StatsDatePeriod, thisStatCache.DateArray, thisStatCache.NAVArray, DataItemEnum.NAVs, pStatsStartDate, pStatsEndDate, pScalingFactor, pNormaliseNAVs, InitialNormalisationValue), Double())	' NAVs Don't Scale here.

				End If

			End SyncLock
		Catch ex As Exception
			Return Nothing
		End Try

		Return Nothing
	End Function

	Private Sub SetNAVSeries(ByVal pStatCache As StatCacheClass)

		Try

			SyncLock pStatCache

				If (pStatCache.ReturnsArray IsNot Nothing) Then

					' Return NAV Series, Create it from the returns if it was not taken from the Database (Venice Instrument series only at present - 8 Jan 2008).

					If (pStatCache.NAVArray Is Nothing) OrElse (pStatCache.NAVArray.Length <> pStatCache.ReturnsArray.Length) Then

						pStatCache.NAVArray = CType(Array.CreateInstance(GetType(Double), Math.Max(pStatCache.ReturnsArray.Length, 1)), Double())

					End If

					If (pStatCache.NAVArray IsNot Nothing) AndAlso (pStatCache.NAVArray.Length > 0) Then

						Dim RCount As Integer

						pStatCache.NAVArray(0) = 100
						For RCount = 1 To (pStatCache.ReturnsArray.Length - 1)
							pStatCache.NAVArray(RCount) = pStatCache.NAVArray(RCount - 1) * (1.0 + pStatCache.ReturnsArray(RCount))
						Next

					End If

				Else

					pStatCache.NAVArray = Nothing

				End If

			End SyncLock

		Catch ex As Exception
		End Try

	End Sub

	Public Function PeriodReturnSeries(ByVal StatsDatePeriod As DealingPeriod, ByVal pPertracID As ULong, ByVal MatchBackfillVol As Boolean, ByVal pPeriodCount As Integer, ByVal pLamda As Double, ByVal Annualised As Boolean, ByVal pStatsStartDate As Date, ByVal pStatsEndDate As Date, ByVal pScalingFactor As Double, ByRef eMessage As String) As Double()

		Dim Date_Series() As Date = Nothing
		Dim NAV_Series() As Double = Nothing
		Dim RVal() As Double = Nothing
		Dim PeriodCount As Integer

		Try
			If (pPertracID <= 0) Then
				Return Nothing
			End If

			Date_Series = DateSeries(StatsDatePeriod, pPertracID, MatchBackfillVol, AnnualPeriodCount(StatsDatePeriod), pLamda, False, Renaissance_BaseDate, Renaissance_EndDate_Data, eMessage)
			NAV_Series = NAVSeries(StatsDatePeriod, pPertracID, MatchBackfillVol, AnnualPeriodCount(StatsDatePeriod), pLamda, False, Renaissance_BaseDate, Renaissance_EndDate_Data, pScalingFactor, eMessage)

			PeriodCount = pPeriodCount
			If (PeriodCount <= 0) OrElse (PeriodCount > (Date_Series.Length - 1)) Then
				PeriodCount = (Date_Series.Length - 1)
			End If

			If (Date_Series Is Nothing) OrElse (NAV_Series Is Nothing) OrElse (Date_Series.Length <= 0) Then
				Return Nothing
			End If

			Dim RCount As Integer

			RVal = CType(Array.CreateInstance(GetType(Double), NAV_Series.Length), Double())

			For RCount = 0 To (PeriodCount - 1)
				If (RCount < RVal.Length) Then
					RVal(RCount) = Double.NaN
				End If
			Next

			For RCount = PeriodCount To (NAV_Series.Length - 1)
				RVal(RCount) = (NAV_Series(RCount) / NAV_Series(RCount - PeriodCount)) - 1.0
			Next
		Catch ex As Exception
		End Try

		If (RVal IsNot Nothing) AndAlso (RVal.Length > 0) Then
			Return CType(ReturnFittedDataArray(StatsDatePeriod, Date_Series, RVal, DataItemEnum.Returns, pStatsStartDate, pStatsEndDate, 1.0#, False, 100.0#), Double()) ' Scaling already done at NAV Level.
		End If
		Return RVal

	End Function

	Public Function StdDevSeries(ByVal StatsDatePeriod As DealingPeriod, ByVal pPertracID As ULong, ByVal MatchBackfillVol As Boolean, ByVal PeriodCount As Integer, ByVal Lamda As Double, ByVal Annualised As Boolean, ByVal pStatsStartDate As Date, ByVal pStatsEndDate As Date, ByVal pScalingFactor As Double, ByRef eMessage As String) As Double()

		Try

			SyncLock Lock_StdDevSeries
				Dim thisStatCache As StatCacheClass

				' Get Existing Series ?
				thisStatCache = Me.GetStatCacheItem(StatsDatePeriod, pPertracID, MatchBackfillVol, PeriodCount, Lamda)

				If (thisStatCache Is Nothing) Then
					thisStatCache = BuildNewStatSeries(StatsDatePeriod, pPertracID, MatchBackfillVol, PeriodCount, Lamda, eMessage)
					SetStatCacheItem(thisStatCache)
				End If

				If (thisStatCache Is Nothing) Then
					Return Nothing
				End If

				' Return StDev Series

				If (Annualised) Then
					' Multiply by Sqrt(12) - Effectively, this returns the Volatility
					Dim RVal(thisStatCache.StDevArray.Length - 1) As Double
					Dim RCount As Integer
					Dim _Sqrt12 As Double = Sqrt12(StatsDatePeriod)

					For RCount = 0 To (thisStatCache.StDevArray.Length - 1)
						RVal(RCount) = thisStatCache.StDevArray(RCount) * _Sqrt12
					Next

					Return CType(ReturnFittedDataArray(StatsDatePeriod, thisStatCache.DateArray, RVal, DataItemEnum.StandardDeviation, pStatsStartDate, pStatsEndDate, pScalingFactor, False, 100.0#), Double())
				Else
					Return CType(ReturnFittedDataArray(StatsDatePeriod, thisStatCache.DateArray, thisStatCache.StDevArray, DataItemEnum.StandardDeviation, pStatsStartDate, pStatsEndDate, pScalingFactor, False, 100.0#), Double())
				End If

			End SyncLock
		Catch ex As Exception
			Return Nothing
		End Try

		Return Nothing

	End Function

	Public Function StdDevSeries_Contingent(ByVal StatsDatePeriod As DealingPeriod, ByVal pPertracID As ULong, ByVal pConditionalPertracID As ULong, ByVal MatchBackfillVol As Boolean, ByVal pCondition As ContingentSelect, ByVal PeriodCount As Integer, ByVal Lamda As Double, ByVal Annualised As Boolean, ByVal pStatsStartDate As Date, ByVal pStatsEndDate As Date, ByVal pScalingFactor As Double, ByRef eMessage As String) As Double()

		Try
			SyncLock Lock_StdDevSeries

				' Return pre-calculated StdDev series if Lamda is One.

				If (pCondition = ContingentSelect.ConditionAll) Then
					Return StdDevSeries(StatsDatePeriod, pPertracID, MatchBackfillVol, PeriodCount, Lamda, Annualised, pStatsStartDate, pStatsEndDate, pScalingFactor, eMessage)
				End If

				' Get Returns Series
				Dim DateSeries() As Date
				Dim ReturnSeries() As Double
				Dim StDevSeries() As Double
				Dim AverageSeries() As Double

				ReturnSeries = Me.ReturnSeries_Contingent(StatsDatePeriod, pPertracID, pConditionalPertracID, MatchBackfillVol, pCondition, PeriodCount, Lamda, Annualised, pStatsStartDate, pStatsEndDate, pScalingFactor, eMessage)
				DateSeries = Me.DateSeries(StatsDatePeriod, pPertracID, MatchBackfillVol, PeriodCount, Lamda, Annualised, pStatsStartDate, pStatsEndDate, eMessage)
				StDevSeries = CType(Array.CreateInstance(GetType(Double), ReturnSeries.Length), Double())
				ReDim AverageSeries(ReturnSeries.Length - 1)

				' Calculate Averages Array

				Try
					Dim RollingSum As Double = 0
					Dim RowCounter As Integer

					If (PeriodCount > 0) Then
						For RowCounter = 1 To (AverageSeries.Length - 1)
							RollingSum += ReturnSeries(RowCounter)
							If (RowCounter >= PeriodCount) Then	' Remember Return(0) is Zero.
								RollingSum -= ReturnSeries(RowCounter - PeriodCount)

								AverageSeries(RowCounter) = (RollingSum / PeriodCount)
							End If
						Next
					End If
				Catch ex As Exception
				End Try

				Dim CurrentCounter As Integer
				Dim ExponentMultiplier(PeriodCount) As Double
				Dim sumExponentWeight As Double

				ExponentMultiplier(0) = 1
				sumExponentWeight = 1
				For CurrentCounter = 1 To PeriodCount
					If (Lamda = 1) Then
						ExponentMultiplier(CurrentCounter) = 1
						sumExponentWeight += 1
					Else
						ExponentMultiplier(CurrentCounter) = ExponentMultiplier(CurrentCounter - 1) * Lamda
						sumExponentWeight += ExponentMultiplier(CurrentCounter)
					End If
				Next

				' Set StdDev Array

				Try
					Dim SDCounter As Integer
					Dim _Sqrt12 As Double = Sqrt12(StatsDatePeriod)

					For CurrentCounter = PeriodCount To (ReturnSeries.Length - 1)
						Dim avgReturn As Double = 0
						Dim sumReturn2 As Double = 0
						Dim Exponent As Double = 1
						Dim thisMultiplier As Double = 1

						avgReturn = AverageSeries(CurrentCounter)

						' Calculate Sum((X - Mean) ^ 2)
						For SDCounter = 0 To (PeriodCount - 1)
							thisMultiplier = ExponentMultiplier(SDCounter)

							sumReturn2 += thisMultiplier * ((ReturnSeries(CurrentCounter - SDCounter) - avgReturn) ^ 2)
						Next

						If (sumExponentWeight = 0) Then
							StDevSeries(CurrentCounter) = 0
						Else
							If (Annualised) Then
								StDevSeries(CurrentCounter) = Math.Sqrt(sumReturn2 / sumExponentWeight) * _Sqrt12
							Else
								StDevSeries(CurrentCounter) = Math.Sqrt(sumReturn2 / sumExponentWeight)
							End If

						End If
					Next

				Catch ex As Exception
				End Try

				' Return StDev Series

				Return CType(ReturnFittedDataArray(StatsDatePeriod, DateSeries, StDevSeries, DataItemEnum.StandardDeviation, pStatsStartDate, pStatsEndDate, 1.0#, False, 100.0#), Double()) ' Returns already scaled.

			End SyncLock
		Catch ex As Exception
			Return Nothing
		End Try

		Return Nothing

	End Function

	Public Function CorrelationSeries_Contingent(ByVal StatsDatePeriod As DealingPeriod, ByVal X_PertracID As ULong, ByVal Y_PertracID As ULong, ByVal MatchBackfillVol As Boolean, ByVal pCondition As ContingentSelect, ByVal PeriodCount As Integer, ByVal Lamda As Double, ByVal pStatsStartDate As Date, ByVal pStatsEndDate As Date, ByVal pXScalingFactor As Double, ByVal pYScalingFactor As Double, ByRef eMessage As String) As Double()
		' *********************************************************************************
		'
		'
		' *********************************************************************************
		Dim RVal_Correlation() As Double

		Dim X_DateSeries() As Date = Nothing
		Dim X_ReturnSeries() As Double = Nothing
		Dim X_AverageSeries() As Double = Nothing
		Dim X_StDevSeries() As Double = Nothing
		Dim Y_DateSeries() As Date = Nothing
		Dim Y_ReturnSeries() As Double = Nothing
		Dim Y_AverageSeries() As Double = Nothing
		Dim Y_StDevSeries() As Double = Nothing

		' Validate
		If (X_PertracID <= 0) OrElse (Y_PertracID <= 0) Then
			Return Nothing
		End If

		' Get Data
		Try
			Dim thisStatCache As StatCacheClass

			' Get Existing Series ?
			thisStatCache = Me.GetStatCacheItem(StatsDatePeriod, X_PertracID, MatchBackfillVol, PeriodCount, Lamda)

			If (thisStatCache Is Nothing) Then
				thisStatCache = BuildNewStatSeries(StatsDatePeriod, X_PertracID, MatchBackfillVol, PeriodCount, Lamda, eMessage)
				SetStatCacheItem(thisStatCache)
			End If

			If (thisStatCache Is Nothing) Then
				X_DateSeries = Nothing
			Else
				' X_DateSeries = thisStatCache.DateArray
				X_DateSeries = DateSeries(StatsDatePeriod, X_PertracID, False, PeriodCount, Lamda, False, Renaissance_BaseDate, pStatsEndDate, eMessage) ' pStatsStartDate

				If (pCondition = ContingentSelect.ConditionAll) Then
					X_ReturnSeries = ReturnSeries_Contingent(StatsDatePeriod, X_PertracID, 0, MatchBackfillVol, pCondition, PeriodCount, Lamda, False, Renaissance_BaseDate, pStatsEndDate, pXScalingFactor, eMessage) ' pStatsStartDate
					X_AverageSeries = AverageSeries_Contingent(StatsDatePeriod, X_PertracID, 0, MatchBackfillVol, pCondition, PeriodCount, Lamda, False, Renaissance_BaseDate, pStatsEndDate, pXScalingFactor, eMessage) ' pStatsStartDate
					X_StDevSeries = StdDevSeries_Contingent(StatsDatePeriod, X_PertracID, 0, MatchBackfillVol, pCondition, PeriodCount, Lamda, False, Renaissance_BaseDate, pStatsEndDate, pXScalingFactor, eMessage)	 ' pStatsStartDate
				Else
					X_ReturnSeries = ReturnSeries_Contingent(StatsDatePeriod, X_PertracID, X_PertracID, MatchBackfillVol, pCondition, PeriodCount, Lamda, False, Renaissance_BaseDate, pStatsEndDate, pXScalingFactor, eMessage) ' pStatsStartDate
					X_AverageSeries = AverageSeries_Contingent(StatsDatePeriod, X_PertracID, X_PertracID, MatchBackfillVol, pCondition, PeriodCount, Lamda, False, Renaissance_BaseDate, pStatsEndDate, pXScalingFactor, eMessage) ' pStatsStartDate
					X_StDevSeries = StdDevSeries_Contingent(StatsDatePeriod, X_PertracID, X_PertracID, MatchBackfillVol, pCondition, PeriodCount, Lamda, False, Renaissance_BaseDate, pStatsEndDate, pXScalingFactor, eMessage)	 ' pStatsStartDate
				End If
			End If
		Catch ex As Exception
			X_DateSeries = Nothing
		End Try

		Try
			Dim thisStatCache As StatCacheClass

			' Get Existing Series ?
			thisStatCache = Me.GetStatCacheItem(StatsDatePeriod, Y_PertracID, MatchBackfillVol, PeriodCount, Lamda)

			If (thisStatCache Is Nothing) Then
				thisStatCache = BuildNewStatSeries(StatsDatePeriod, Y_PertracID, MatchBackfillVol, PeriodCount, Lamda, eMessage)
				SetStatCacheItem(thisStatCache)
			End If

			If (thisStatCache Is Nothing) Then
				Y_DateSeries = Nothing
			Else
				' Y_DateSeries = thisStatCache.DateArray
				Y_DateSeries = DateSeries(StatsDatePeriod, Y_PertracID, False, PeriodCount, Lamda, False, Renaissance_BaseDate, pStatsEndDate, eMessage) ' pStatsStartDate

				If (pCondition = ContingentSelect.ConditionAll) Then
					Y_ReturnSeries = ReturnSeries_Contingent(StatsDatePeriod, Y_PertracID, 0, MatchBackfillVol, pCondition, PeriodCount, Lamda, False, Renaissance_BaseDate, pStatsEndDate, pYScalingFactor, eMessage) ' pStatsStartDate
					Y_AverageSeries = AverageSeries_Contingent(StatsDatePeriod, Y_PertracID, 0, MatchBackfillVol, pCondition, PeriodCount, Lamda, False, Renaissance_BaseDate, pStatsEndDate, pYScalingFactor, eMessage) ' pStatsStartDate
					Y_StDevSeries = StdDevSeries_Contingent(StatsDatePeriod, Y_PertracID, 0, MatchBackfillVol, pCondition, PeriodCount, Lamda, False, Renaissance_BaseDate, pStatsEndDate, pYScalingFactor, eMessage)	 ' pStatsStartDate
				Else
					Y_ReturnSeries = ReturnSeries_Contingent(StatsDatePeriod, Y_PertracID, X_PertracID, MatchBackfillVol, pCondition, PeriodCount, Lamda, False, Renaissance_BaseDate, pStatsEndDate, pYScalingFactor, eMessage) ' pStatsStartDate
					Y_AverageSeries = AverageSeries_Contingent(StatsDatePeriod, Y_PertracID, X_PertracID, MatchBackfillVol, pCondition, PeriodCount, Lamda, False, Renaissance_BaseDate, pStatsEndDate, pYScalingFactor, eMessage) ' pStatsStartDate
					Y_StDevSeries = StdDevSeries_Contingent(StatsDatePeriod, Y_PertracID, X_PertracID, MatchBackfillVol, pCondition, PeriodCount, Lamda, False, Renaissance_BaseDate, pStatsEndDate, pYScalingFactor, eMessage)	 ' pStatsStartDate
				End If
			End If
		Catch ex As Exception
			Y_DateSeries = Nothing
		End Try

		If (X_DateSeries Is Nothing) OrElse (X_DateSeries.Length <= 0) Then
			Return Nothing
		End If
		If (Y_DateSeries Is Nothing) OrElse (Y_DateSeries.Length <= 0) Then
			Return Nothing
		End If

		' Calculate Data Overlap.

		RVal_Correlation = CType(Array.CreateInstance(GetType(Double), X_DateSeries.Length), Double())

		Dim X_Start_Index As Integer
		Dim Y_Start_Index As Integer
		Dim OverlapLength As Integer

		If (X_DateSeries(0) = Y_DateSeries(0)) Then
			X_Start_Index = 1
			Y_Start_Index = 1

			If (X_DateSeries.Length <= Y_DateSeries.Length) Then
				OverlapLength = (X_DateSeries.Length - X_Start_Index)
			Else
				OverlapLength = (Y_DateSeries.Length - Y_Start_Index)
			End If

		ElseIf (X_DateSeries(0) < Y_DateSeries(0)) Then

			X_Start_Index = GetPriceIndex(StatsDatePeriod, X_DateSeries(0), Y_DateSeries(1))
			Y_Start_Index = 1

			OverlapLength = (X_DateSeries.Length - X_Start_Index)
			If (OverlapLength > (Y_DateSeries.Length - Y_Start_Index)) Then
				OverlapLength = (Y_DateSeries.Length - Y_Start_Index)
			End If

		ElseIf (X_DateSeries(0) > Y_DateSeries(0)) Then

			Y_Start_Index = GetPriceIndex(StatsDatePeriod, Y_DateSeries(0), X_DateSeries(1))
			X_Start_Index = 1

			OverlapLength = (X_DateSeries.Length - X_Start_Index)
			If (OverlapLength > (Y_DateSeries.Length - Y_Start_Index)) Then
				OverlapLength = (Y_DateSeries.Length - Y_Start_Index)
			End If

		Else
			' ???
			Return Nothing
		End If

		' Calculate Correlation

		Dim CurrentCounter As Integer
		Dim CorrCounter As Integer
		Dim Covariance As Double
		Dim XIndex As Integer
		Dim YIndex As Integer

		Dim SumZxZy As Double
		Dim SumXY As Double
		Dim Zx As Double
		Dim Zy As Double

		If (Lamda = 1) Then
			' If Lamda = 1 then use quick calculation method.

			For CurrentCounter = (PeriodCount - 1) To (OverlapLength - 1)
				SumZxZy = 0
				XIndex = CurrentCounter + X_Start_Index
				YIndex = CurrentCounter + Y_Start_Index

				For CorrCounter = 0 To (PeriodCount - 1)
					Zx = (X_ReturnSeries(XIndex - CorrCounter) - X_AverageSeries(XIndex)) / X_StDevSeries(XIndex)
					Zy = (Y_ReturnSeries(YIndex - CorrCounter) - Y_AverageSeries(YIndex)) / Y_StDevSeries(YIndex)

					SumZxZy += (Zx * Zy)
				Next

				RVal_Correlation(XIndex) = Math.Round(SumZxZy / PeriodCount, 6)	' Rounded so that Correlations of One appear as 1.
			Next

		Else
			Dim ExponentMultiplier(PeriodCount - 1) As Double
			Dim SumExponentWeight As Double

			ExponentMultiplier(0) = 1
			SumExponentWeight = 1
			For CurrentCounter = 1 To (PeriodCount - 1)
				If (Lamda = 1) Then
					ExponentMultiplier(CurrentCounter) = 1
					SumExponentWeight += 1
				Else
					ExponentMultiplier(CurrentCounter) = ExponentMultiplier(CurrentCounter - 1) * Lamda
					SumExponentWeight += ExponentMultiplier(CurrentCounter)
				End If
			Next

			' Calculate Co-Variance and Correlation series

			For CurrentCounter = (PeriodCount - 1) To (OverlapLength - 1)
				XIndex = CurrentCounter + X_Start_Index
				YIndex = CurrentCounter + Y_Start_Index
				SumXY = 0

				For CorrCounter = 0 To (PeriodCount - 1)
					SumXY += ExponentMultiplier(CorrCounter) * ((X_ReturnSeries(XIndex - CorrCounter) - X_AverageSeries(XIndex)) * (Y_ReturnSeries(YIndex - CorrCounter) - Y_AverageSeries(YIndex)))
				Next
				Covariance = SumXY / SumExponentWeight

				RVal_Correlation(XIndex) = Math.Round(Covariance / (X_StDevSeries(XIndex) * Y_StDevSeries(YIndex)), 6)	' Rounded so that Correlations of One appear as 1.

			Next

		End If

		Return CType(ReturnFittedDataArray(StatsDatePeriod, X_DateSeries, RVal_Correlation, DataItemEnum.Correlation, pStatsStartDate, pStatsEndDate, 1.0#, False, 100.0#), Double())	' No Scaling as source data was scaled.

	End Function

	Public Function Beta_Contingent(ByVal StatsDatePeriod As DealingPeriod, ByVal X_PertracID As ULong, ByVal Y_PertracID As ULong, ByVal MatchBackfillVol As Boolean, ByVal pCondition As ContingentSelect, ByVal PeriodCount As Integer, ByVal Lamda As Double, ByVal pStatsStartDate As Date, ByVal pStatsEndDate As Date, ByVal pXScalingFactor As Double, ByVal pYScalingFactor As Double, ByRef eMessage As String) As Double()
		' *********************************************************************************
		'
		'
		' *********************************************************************************

		' Original ADC Code :-
		'Try
		'  n = UBound(Returns1)
		'  If UBound(Returns2) = n Then
		'    valid = True
		'    For i = 0 To n
		'      sumX = sumX + Returns1(i)
		'      sumY = sumY + Returns2(i)
		'      SumXY = SumXY + (Returns1(i) * Returns2(i))
		'      Sum_X2 = Sum_X2 + (Returns1(i) * Returns1(i))
		'    Next
		'    SumX_2 = sumX * sumX
		'    CalcBeta = ((n + 1) * SumXY - sumX * sumY) / ((n + 1) * Sum_X2 - SumX_2)
		'  End If
		'  If Not valid Then CalcBeta = 0
		'Catch
		'  CalcBeta = DefaultValue
		'End Try

		Dim RVal_Beta() As Double

		Dim X_DateSeries() As Date = Nothing
		Dim X_ReturnSeries() As Double = Nothing
		Dim X_AverageSeries() As Double = Nothing
		Dim X_StDevSeries() As Double = Nothing
		Dim Y_DateSeries() As Date = Nothing
		Dim Y_ReturnSeries() As Double = Nothing
		Dim Y_AverageSeries() As Double = Nothing

		Dim LamdaFlag As Boolean = True
		If (Lamda = 1) Then
			LamdaFlag = False
		End If

		' Validate
		If (X_PertracID <= 0) OrElse (Y_PertracID <= 0) Then
			Return Nothing
		End If

		' Get Data
		Try
			Dim thisStatCache As StatCacheClass

			' Get Existing Series ?
			thisStatCache = Me.GetStatCacheItem(StatsDatePeriod, X_PertracID, MatchBackfillVol, PeriodCount, Lamda)

			If (thisStatCache Is Nothing) Then
				thisStatCache = BuildNewStatSeries(StatsDatePeriod, X_PertracID, MatchBackfillVol, PeriodCount, Lamda, eMessage)
				SetStatCacheItem(thisStatCache)
			End If

			If (thisStatCache Is Nothing) Then
				X_DateSeries = Nothing
			Else
				' X_DateSeries = thisStatCache.DateArray
				X_DateSeries = DateSeries(StatsDatePeriod, X_PertracID, False, PeriodCount, Lamda, False, Renaissance_BaseDate, pStatsEndDate, eMessage) ' pStatsStartDate

				If (pCondition = ContingentSelect.ConditionAll) Then
					X_ReturnSeries = ReturnSeries_Contingent(StatsDatePeriod, X_PertracID, 0, MatchBackfillVol, pCondition, PeriodCount, Lamda, False, Renaissance_BaseDate, pStatsEndDate, pXScalingFactor, eMessage) ' pStatsStartDate
					X_AverageSeries = AverageSeries_Contingent(StatsDatePeriod, X_PertracID, 0, MatchBackfillVol, pCondition, PeriodCount, Lamda, False, Renaissance_BaseDate, pStatsEndDate, pXScalingFactor, eMessage) ' pStatsStartDate
					X_StDevSeries = StdDevSeries_Contingent(StatsDatePeriod, X_PertracID, 0, MatchBackfillVol, pCondition, PeriodCount, Lamda, False, Renaissance_BaseDate, pStatsEndDate, pXScalingFactor, eMessage)	 ' pStatsStartDate
				Else
					X_ReturnSeries = ReturnSeries_Contingent(StatsDatePeriod, X_PertracID, X_PertracID, MatchBackfillVol, pCondition, PeriodCount, Lamda, False, Renaissance_BaseDate, pStatsEndDate, pXScalingFactor, eMessage) ' pStatsStartDate
					X_AverageSeries = AverageSeries_Contingent(StatsDatePeriod, X_PertracID, X_PertracID, MatchBackfillVol, pCondition, PeriodCount, Lamda, False, Renaissance_BaseDate, pStatsEndDate, pXScalingFactor, eMessage) ' pStatsStartDate
					X_StDevSeries = StdDevSeries_Contingent(StatsDatePeriod, X_PertracID, X_PertracID, MatchBackfillVol, pCondition, PeriodCount, Lamda, False, Renaissance_BaseDate, pStatsEndDate, pXScalingFactor, eMessage)	 ' pStatsStartDate
				End If
			End If
		Catch ex As Exception
			X_DateSeries = Nothing
		End Try

		Try
			Dim thisStatCache As StatCacheClass

			' Get Existing Series ?
			thisStatCache = Me.GetStatCacheItem(StatsDatePeriod, Y_PertracID, MatchBackfillVol, PeriodCount, Lamda)

			If (thisStatCache Is Nothing) Then
				thisStatCache = BuildNewStatSeries(StatsDatePeriod, Y_PertracID, MatchBackfillVol, PeriodCount, Lamda, eMessage)
				SetStatCacheItem(thisStatCache)
			End If

			If (thisStatCache Is Nothing) Then
				Y_DateSeries = Nothing
			Else
				' Y_DateSeries = thisStatCache.DateArray
				Y_DateSeries = DateSeries(StatsDatePeriod, Y_PertracID, False, PeriodCount, Lamda, False, Renaissance_BaseDate, pStatsEndDate, eMessage) ' pStatsStartDate

				If (pCondition = ContingentSelect.ConditionAll) Then
					Y_ReturnSeries = ReturnSeries_Contingent(StatsDatePeriod, Y_PertracID, 0, MatchBackfillVol, pCondition, PeriodCount, Lamda, False, Renaissance_BaseDate, pStatsEndDate, pYScalingFactor, eMessage) ' pStatsStartDate
					Y_AverageSeries = AverageSeries_Contingent(StatsDatePeriod, Y_PertracID, 0, MatchBackfillVol, pCondition, PeriodCount, Lamda, False, Renaissance_BaseDate, pStatsEndDate, pYScalingFactor, eMessage) ' pStatsStartDate
				Else
					Y_ReturnSeries = ReturnSeries_Contingent(StatsDatePeriod, Y_PertracID, X_PertracID, MatchBackfillVol, pCondition, PeriodCount, Lamda, False, Renaissance_BaseDate, pStatsEndDate, pYScalingFactor, eMessage) ' pStatsStartDate
					Y_AverageSeries = AverageSeries_Contingent(StatsDatePeriod, Y_PertracID, X_PertracID, MatchBackfillVol, pCondition, PeriodCount, Lamda, False, Renaissance_BaseDate, pStatsEndDate, pYScalingFactor, eMessage) ' pStatsStartDate
				End If
			End If
		Catch ex As Exception
			Y_DateSeries = Nothing
		End Try

		If (X_DateSeries Is Nothing) OrElse (X_DateSeries.Length <= 0) Then
			Return Nothing
		End If
		If (Y_DateSeries Is Nothing) OrElse (Y_DateSeries.Length <= 0) Then
			Return Nothing
		End If

		' Calculate Data Overlap.

		RVal_Beta = CType(Array.CreateInstance(GetType(Double), X_DateSeries.Length), Double())

		Dim X_Start_Index As Integer
		Dim Y_Start_Index As Integer
		Dim OverlapLength As Integer

		If (X_DateSeries(0) = Y_DateSeries(0)) Then
			X_Start_Index = 1
			Y_Start_Index = 1

			If (X_DateSeries.Length <= Y_DateSeries.Length) Then
				OverlapLength = (X_DateSeries.Length - X_Start_Index)
			Else
				OverlapLength = (Y_DateSeries.Length - Y_Start_Index)
			End If

		ElseIf (X_DateSeries(0) < Y_DateSeries(0)) Then

			X_Start_Index = GetPriceIndex(StatsDatePeriod, X_DateSeries(0), Y_DateSeries(1))
			Y_Start_Index = 1

			OverlapLength = (X_DateSeries.Length - X_Start_Index)
			If (OverlapLength > (Y_DateSeries.Length - Y_Start_Index)) Then
				OverlapLength = (Y_DateSeries.Length - Y_Start_Index)
			End If

		ElseIf (X_DateSeries(0) > Y_DateSeries(0)) Then

			Y_Start_Index = GetPriceIndex(StatsDatePeriod, Y_DateSeries(0), X_DateSeries(1))
			X_Start_Index = 1

			OverlapLength = (X_DateSeries.Length - X_Start_Index)
			If (OverlapLength > (Y_DateSeries.Length - Y_Start_Index)) Then
				OverlapLength = (Y_DateSeries.Length - Y_Start_Index)
			End If

		Else
			' ???
			Return Nothing
		End If


		' Calculate Beta

		Dim CurrentCounter As Integer
		Dim BetaCounter As Integer
		Dim XIndex As Integer
		Dim YIndex As Integer
		Dim ThisXReturn As Double
		Dim ThisYReturn As Double
		Dim Covariance As Double

		Dim SumXY As Double

		' Build Lamda Multiplier Table
		Dim ExponentMultiplier(PeriodCount - 1) As Double
		Dim SumExponentWeight As Double

		ExponentMultiplier(0) = 1
		SumExponentWeight = 1
		For CurrentCounter = 1 To (PeriodCount - 1)
			ExponentMultiplier(CurrentCounter) = ExponentMultiplier(CurrentCounter - 1) * Lamda
			SumExponentWeight += ExponentMultiplier(CurrentCounter)
		Next

		For CurrentCounter = (PeriodCount - 1) To (OverlapLength - 1)
			XIndex = CurrentCounter + X_Start_Index
			YIndex = CurrentCounter + Y_Start_Index

			SumXY = 0
			For BetaCounter = 0 To (PeriodCount - 1)
				ThisXReturn = X_ReturnSeries(XIndex - BetaCounter) * ExponentMultiplier(BetaCounter)
				ThisYReturn = Y_ReturnSeries(YIndex - BetaCounter) * ExponentMultiplier(BetaCounter)

				SumXY += ExponentMultiplier(BetaCounter) * ((X_ReturnSeries(XIndex - BetaCounter) - X_AverageSeries(XIndex)) * (Y_ReturnSeries(YIndex - BetaCounter) - Y_AverageSeries(YIndex)))
			Next
			Covariance = SumXY / SumExponentWeight

			RVal_Beta(XIndex) = Math.Round(Covariance / (X_StDevSeries(XIndex) * X_StDevSeries(XIndex)), 7)
		Next

		Return CType(ReturnFittedDataArray(StatsDatePeriod, X_DateSeries, RVal_Beta, DataItemEnum.Beta, pStatsStartDate, pStatsEndDate, 1.0#, False, 100.0#), Double())	' Source Data was scaled, not required here.

	End Function

	Public Function Alpha_Contingent(ByVal StatsDatePeriod As DealingPeriod, ByVal X_PertracID As ULong, ByVal Y_PertracID As ULong, ByVal MatchBackfillVol As Boolean, ByVal pCondition As ContingentSelect, ByVal PeriodCount As Integer, ByVal Lamda As Double, ByVal pStatsStartDate As Date, ByVal pStatsEndDate As Date, ByVal pXScalingFactor As Double, ByVal pYScalingFactor As Double, ByRef eMessage As String) As Double()
		' *********************************************************************************
		'
		'
		' *********************************************************************************
		Dim RVal_Alpha() As Double

		' Original ADC Code
		' CalcAlpha = Ybar - (BetaXY * Xbar)

		Dim X_DateSeries() As Date = Nothing
		Dim X_AverageSeries() As Double = Nothing
		Dim Y_DateSeries() As Date = Nothing
		Dim Y_AverageSeries() As Double = Nothing
		Dim XY_BetaSeries() As Double = Nothing

		' Validate
		If (X_PertracID <= 0) OrElse (Y_PertracID <= 0) Then
			Return Nothing
		End If

		' Get Data
		Try
			Dim thisStatCache As StatCacheClass

			' Get Existing Series ?
			thisStatCache = Me.GetStatCacheItem(StatsDatePeriod, X_PertracID, MatchBackfillVol, PeriodCount, Lamda)

			If (thisStatCache Is Nothing) Then
				thisStatCache = BuildNewStatSeries(StatsDatePeriod, X_PertracID, MatchBackfillVol, PeriodCount, Lamda, eMessage)
				SetStatCacheItem(thisStatCache)
			End If

			If (thisStatCache Is Nothing) Then
				X_DateSeries = Nothing
			Else
				' X_DateSeries = thisStatCache.DateArray
				X_DateSeries = DateSeries(StatsDatePeriod, X_PertracID, False, PeriodCount, Lamda, False, Renaissance_BaseDate, pStatsEndDate, eMessage) ' pStatsStartDate

				If (pCondition = ContingentSelect.ConditionAll) AndAlso (Lamda = 1) Then
					X_AverageSeries = AverageSeries_Contingent(StatsDatePeriod, X_PertracID, 0, MatchBackfillVol, pCondition, PeriodCount, Lamda, False, Renaissance_BaseDate, pStatsEndDate, pXScalingFactor, eMessage) ' pStatsStartDate
				Else
					X_AverageSeries = AverageSeries_Contingent(StatsDatePeriod, X_PertracID, X_PertracID, MatchBackfillVol, pCondition, PeriodCount, Lamda, False, Renaissance_BaseDate, pStatsEndDate, pXScalingFactor, eMessage) ' pStatsStartDate
				End If
			End If
		Catch ex As Exception
			X_DateSeries = Nothing
		End Try

		Try
			Dim thisStatCache As StatCacheClass

			' Get Existing Series ?
			thisStatCache = Me.GetStatCacheItem(StatsDatePeriod, Y_PertracID, MatchBackfillVol, PeriodCount, Lamda)

			If (thisStatCache Is Nothing) Then
				thisStatCache = BuildNewStatSeries(StatsDatePeriod, Y_PertracID, MatchBackfillVol, PeriodCount, Lamda, eMessage)
				SetStatCacheItem(thisStatCache)
			End If

			If (thisStatCache Is Nothing) Then
				Y_DateSeries = Nothing
			Else
				' Y_DateSeries = thisStatCache.DateArray
				Y_DateSeries = DateSeries(StatsDatePeriod, Y_PertracID, False, PeriodCount, Lamda, False, Renaissance_BaseDate, pStatsEndDate, eMessage) ' pStatsStartDate

				If (pCondition = ContingentSelect.ConditionAll) AndAlso (Lamda = 1) Then
					Y_AverageSeries = AverageSeries_Contingent(StatsDatePeriod, Y_PertracID, 0, MatchBackfillVol, pCondition, PeriodCount, Lamda, False, Renaissance_BaseDate, pStatsEndDate, pYScalingFactor, eMessage) ' pStatsStartDate
				Else
					Y_AverageSeries = AverageSeries_Contingent(StatsDatePeriod, Y_PertracID, X_PertracID, MatchBackfillVol, pCondition, PeriodCount, Lamda, False, Renaissance_BaseDate, pStatsEndDate, pYScalingFactor, eMessage) ' pStatsStartDate
				End If
			End If
		Catch ex As Exception
			Y_DateSeries = Nothing
		End Try

		XY_BetaSeries = Beta_Contingent(StatsDatePeriod, X_PertracID, Y_PertracID, MatchBackfillVol, pCondition, PeriodCount, Lamda, Renaissance_BaseDate, Renaissance_EndDate_Data, pXScalingFactor, pYScalingFactor, eMessage)

		If (X_DateSeries Is Nothing) OrElse (X_DateSeries.Length <= 0) Then
			Return Nothing
		End If
		If (Y_DateSeries Is Nothing) OrElse (Y_DateSeries.Length <= 0) Then
			Return Nothing
		End If
		If (XY_BetaSeries Is Nothing) OrElse (XY_BetaSeries.Length <= 0) Then
			Return Nothing
		End If

		' Calculate Data Overlap.

		RVal_Alpha = CType(Array.CreateInstance(GetType(Double), X_DateSeries.Length), Double())

		Dim X_Start_Index As Integer
		Dim Y_Start_Index As Integer
		Dim OverlapLength As Integer

		If (X_DateSeries(0) = Y_DateSeries(0)) Then
			X_Start_Index = 1
			Y_Start_Index = 1

			If (X_DateSeries.Length <= Y_DateSeries.Length) Then
				OverlapLength = (X_DateSeries.Length - X_Start_Index)
			Else
				OverlapLength = (Y_DateSeries.Length - Y_Start_Index)
			End If

		ElseIf (X_DateSeries(0) < Y_DateSeries(0)) Then

			X_Start_Index = GetPriceIndex(StatsDatePeriod, X_DateSeries(0), Y_DateSeries(1))
			Y_Start_Index = 1

			OverlapLength = (X_DateSeries.Length - X_Start_Index)
			If (OverlapLength > (Y_DateSeries.Length - Y_Start_Index)) Then
				OverlapLength = (Y_DateSeries.Length - Y_Start_Index)
			End If

		ElseIf (X_DateSeries(0) > Y_DateSeries(0)) Then

			Y_Start_Index = GetPriceIndex(StatsDatePeriod, Y_DateSeries(0), X_DateSeries(1))
			X_Start_Index = 1

			OverlapLength = (X_DateSeries.Length - X_Start_Index)
			If (OverlapLength > (Y_DateSeries.Length - Y_Start_Index)) Then
				OverlapLength = (Y_DateSeries.Length - Y_Start_Index)
			End If

		Else
			' ???
			Return Nothing
		End If

		' Calculate Alpha

		Dim CurrentCounter As Integer
		Dim XIndex As Integer
		Dim YIndex As Integer
		Dim _AnnualPeriodCount As Integer = AnnualPeriodCount(StatsDatePeriod)

		For CurrentCounter = (PeriodCount - 1) To (OverlapLength - 1)
			XIndex = CurrentCounter + X_Start_Index
			YIndex = CurrentCounter + Y_Start_Index

			' CalcAlpha = Ybar - (BetaXY * Xbar)

			RVal_Alpha(XIndex) = Math.Round((Y_AverageSeries(YIndex) - (XY_BetaSeries(XIndex) * X_AverageSeries(XIndex))) * _AnnualPeriodCount, 8) ' 
		Next

		Return CType(ReturnFittedDataArray(StatsDatePeriod, X_DateSeries, RVal_Alpha, DataItemEnum.Alpha, pStatsStartDate, pStatsEndDate, 1.0#, False, 100.0#), Double())	' No Scaling required, done already.

	End Function

	Private Function BuildNewStatSeries(ByVal StatsDatePeriod As DealingPeriod, ByVal cPertracID As ULong, ByVal MatchBackfillVol As Boolean, ByVal pPeriodCount As Integer, ByVal pLamda As Double, ByRef eMessage As String) As StatCacheClass
		' ******************************************************************************************
		'
		'
		' ******************************************************************************************

		Return BuildNewStatSeries(StatsDatePeriod, 0, cPertracID, MatchBackfillVol, pPeriodCount, pLamda, -1, False, RenaissanceGlobals.Globals.KNOWLEDGEDATE_NOW, eMessage)

	End Function

	Public Function BuildNewStatSeries(ByVal StatsDatePeriod As DealingPeriod, ByVal cPertracID As ULong, ByVal pMatchBackfillVol As Boolean, ByVal pPeriodCount As Integer, ByVal pLamda As Double, ByVal NoCache As Boolean, ByVal KnowledgeDate As Date, ByRef eMessage As String) As StatCacheClass
		' ******************************************************************************************
		'
		'
		' ******************************************************************************************

		Return BuildNewStatSeries(StatsDatePeriod, 0, cPertracID, pMatchBackfillVol, pPeriodCount, pLamda, -1, False, RenaissanceGlobals.Globals.KNOWLEDGEDATE_NOW,eMessage )

	End Function

	Public Function BuildNewStatSeries(ByVal StatsDatePeriod As DealingPeriod, ByVal pNestingLevel As Integer, ByVal cPertracID As ULong, ByVal pMatchBackfillVol As Boolean, ByVal pPeriodCount As Integer, ByVal pLamda As Double, ByVal pSampleSize As Integer, ByVal NoCache As Boolean, ByVal KnowledgeDate As Date, ByRef eMessage As String) As StatCacheClass
		' ******************************************************************************************
		'
		'
		' ******************************************************************************************

		Dim BasePertracID As Integer
		Dim BackfillPertracID As Integer
		Dim MatchBackfillVol As Boolean = pMatchBackfillVol
		Dim ThisStatCacheKey As ULong = GetStatCacheKey(StatsDatePeriod, cPertracID, MatchBackfillVol)
		Dim List_IDependOn As New List(Of ULong)

		Dim DataTable As DataTable = Nothing
		Dim SortedRows(-1) As DataRow
		Dim DataStartDate As Date
		Dim DataEndDate As Date

		Dim RealReturnArray() As Boolean = Nothing
		Dim DateArray() As Date = Nothing
		Dim ReturnsArray() As Double = Nothing
		Dim IsMilestoneArray() As Boolean = Nothing
		Dim NAVArray() As Double = Nothing
		Dim AverageArray() As Double = Nothing
		Dim StDevArray() As Double = Nothing
		Dim NetDeltaItemArray() As NetDeltaItemClass = Nothing

		Dim PerformanceDateOrdinal As Integer
		Dim ReturnOrdinal As Integer
		Dim IsMilestoneOrdinal As Integer = (-1)
		Dim NAVOrdinal As Integer
		Dim ArrayLength As Integer
		Dim ArrayIndex As Integer
		Dim RowCounter As Integer
		Dim Lamda As Double
		Dim PeriodCount As Integer

		' Get Pertrac IDs
		BasePertracID = CInt(cPertracID And Ulong_31BitMask) ' Don't use Ulong_32BitMask as the 32nd bit is the sign bit for a signed integer.
		BackfillPertracID = CInt((cPertracID >> 32) And Ulong_31BitMask)

		If (BasePertracID = 0) Then
			BasePertracID = BackfillPertracID
			BackfillPertracID = 0
		End If
		If (BasePertracID = 0) Then
			Return Nothing
		End If

		' Validate PeriodCount, Default to a reasonable value if not supplied.
		PeriodCount = pPeriodCount

		If (pPeriodCount <= 0) Then

			Select Case StatsDatePeriod
				Case DealingPeriod.Daily, DealingPeriod.Weekly, DealingPeriod.Fortnightly, DealingPeriod.Monthly
					PeriodCount = AnnualPeriodCount(StatsDatePeriod)

				Case DealingPeriod.Quarterly
					PeriodCount = 10

				Case DealingPeriod.SemiAnnually
					PeriodCount = 10

				Case DealingPeriod.Annually
					PeriodCount = 10

				Case Else
					PeriodCount = 12

			End Select
		End If

		' Validate Lamda
		Lamda = pLamda
		If (Lamda <= 0.0#) Then
			Lamda = 1.0#
		End If

		If (BackfillPertracID > 0) AndAlso (BasePertracID > 0) Then
			Dim BasePertracStatCache As StatCacheClass = Nothing
			Dim BackfillPertracStatCache As StatCacheClass = Nothing
			Dim VolatilityRatio As Double = 1.0#

			' Remember BACKFILL_DateLimit

			' Get Underlying Data for the two Pertrac IDs.

			If (Not NoCache) Then
				BasePertracStatCache = Me.GetStatCacheItem(StatsDatePeriod, CULng(BasePertracID), False)
			End If
			If (BasePertracStatCache Is Nothing) Then
				BasePertracStatCache = BuildNewStatSeries(StatsDatePeriod, pNestingLevel, CULng(BasePertracID), False, PeriodCount, Lamda, pSampleSize, NoCache, KnowledgeDate, eMessage)

				If (Not NoCache) Then
					SetStatCacheItem(BasePertracStatCache)
				End If
			End If
			If (BasePertracStatCache IsNot Nothing) Then
				BasePertracStatCache.Set_DependsOnMe(ThisStatCacheKey)
				List_IDependOn.Add(GetStatCacheKey(BasePertracStatCache))
			End If

			Try
				If (BasePertracStatCache IsNot Nothing) AndAlso (BasePertracStatCache.DateArray(0) <= BACKFILL_DateLimit(StatsDatePeriod)) Then
					Return BasePertracStatCache
				End If
			Catch ex As Exception
			End Try

			If (Not NoCache) Then
				BackfillPertracStatCache = Me.GetStatCacheItem(StatsDatePeriod, CULng(BackfillPertracID), False)
			End If
			If (BackfillPertracStatCache Is Nothing) Then
				BackfillPertracStatCache = BuildNewStatSeries(StatsDatePeriod, pNestingLevel, CULng(BackfillPertracID), False, PeriodCount, Lamda, pSampleSize, NoCache, KnowledgeDate, eMessage)

				If (Not NoCache) Then
					SetStatCacheItem(BackfillPertracStatCache)
				End If
			End If
			If (BackfillPertracStatCache IsNot Nothing) Then
				BackfillPertracStatCache.Set_DependsOnMe(ThisStatCacheKey)
				List_IDependOn.Add(GetStatCacheKey(BackfillPertracStatCache))
			End If

			' *********
			' Get Outs.
			' *********

			' If both series return Nothing, then return Nothing.
			If (BasePertracStatCache Is Nothing) AndAlso (BackfillPertracStatCache Is Nothing) Then
				Return BasePertracStatCache
			End If

			' If the Backfill Series is empty, return the base series.
			If (BackfillPertracStatCache Is Nothing) OrElse (BackfillPertracStatCache.DateArray.Length <= 0) Then
				Return BasePertracStatCache
			End If

			' If the Base series is empty, return the backfill series.
			If (BasePertracStatCache Is Nothing) OrElse (BasePertracStatCache.DateArray.Length <= 0) Then
				Return BackfillPertracStatCache
			End If

			' If the Base series starts before the backfill series, return the base series.
			If (BasePertracStatCache.DateArray(0) < BackfillPertracStatCache.DateArray(0)) Then
				Return BasePertracStatCache
			End If

			' *******************
			' Set VolatilityRatio
			' 
			' Sets the Volatility ratio based on StdDev from UPTO 24 Months of data.
			'
			' *******************

			' Don't backfill Volatility if there are too few items in the front (base) series to accurately calculate volatility

			If ((BasePertracStatCache.DateArray.Length - 2) < MIN_Periods_For_Stats) Then
				MatchBackfillVol = False
			End If

			' OK, Work out this Item.

			Dim BaseSeries_DestinationStartIndex As Integer
			Dim BackfillSeries_SourceStartIndex As Integer

			DataStartDate = BackfillPertracStatCache.DateArray(0)
			If (DataStartDate < BACKFILL_DateLimit(StatsDatePeriod)) Then
				DataStartDate = BACKFILL_DateLimit(StatsDatePeriod)
			End If

			DataEndDate = BackfillPertracStatCache.DateArray(BackfillPertracStatCache.DateArray.Length - 1)
			If (DataEndDate < BasePertracStatCache.DateArray(BasePertracStatCache.DateArray.Length - 1)) Then
				DataEndDate = BasePertracStatCache.DateArray(BasePertracStatCache.DateArray.Length - 1)
			End If
			DataStartDate = FitDateToPeriod(StatsDatePeriod, DataStartDate)
			DataEndDate = FitDateToPeriod(StatsDatePeriod, DataEndDate)
			ArrayLength = GetPeriodCount(StatsDatePeriod, DataStartDate, DataEndDate)

			DateArray = CType(Array.CreateInstance(GetType(Date), ArrayLength), Date())
			ReturnsArray = CType(Array.CreateInstance(GetType(Double), ArrayLength), Double())
			AverageArray = CType(Array.CreateInstance(GetType(Double), ArrayLength), Double())
			StDevArray = CType(Array.CreateInstance(GetType(Double), ArrayLength), Double())

			' Set Date and Averages Arrays

			Try
				DateArray(0) = DataStartDate
				For RowCounter = 1 To (DateArray.Length - 1)
					DateArray(RowCounter) = AddPeriodToDate(StatsDatePeriod, DateArray(RowCounter - 1), 1)
				Next

			Catch ex As Exception
				Return Nothing
			End Try

			BaseSeries_DestinationStartIndex = GetPriceIndex(StatsDatePeriod, DataStartDate, BasePertracStatCache.DateArray(0))
			BackfillSeries_SourceStartIndex = GetPriceIndex(StatsDatePeriod, BackfillPertracStatCache.DateArray(0), DataStartDate)

			' Bear in mind that Return(0) of the Base series will be Zero.

			Array.ConstrainedCopy(BackfillPertracStatCache.ReturnsArray, BackfillSeries_SourceStartIndex, ReturnsArray, 0, Math.Min((BackfillPertracStatCache.ReturnsArray.Length - BackfillSeries_SourceStartIndex), BaseSeries_DestinationStartIndex + 1))
			Array.ConstrainedCopy(BasePertracStatCache.ReturnsArray, 1, ReturnsArray, BaseSeries_DestinationStartIndex + 1, (BasePertracStatCache.ReturnsArray.Length - 1))
			ReturnsArray(0) = 0

			' *******************
			' Set VolatilityRatio
			' *******************

			Dim BaseSeriesSDev As Double
			Dim BackfillSeriesSDev As Double
			Dim BaseAverage As Double
			Dim BackfillAverage As Double

			If (MatchBackfillVol) Then
				BaseAverage = SeriesAverage(BasePertracStatCache.ReturnsArray, 1, (BasePertracStatCache.ReturnsArray.Length - 1))
				BackfillAverage = SeriesAverage(ReturnsArray, 1, Math.Min(BackfillPertracStatCache.ReturnsArray.Length - 1, BaseSeries_DestinationStartIndex))

				BaseSeriesSDev = StandardDeviation(BasePertracStatCache.ReturnsArray, 1, (BasePertracStatCache.ReturnsArray.Length - 1), False)
				BackfillSeriesSDev = StandardDeviation(ReturnsArray, 1, Math.Min(BackfillPertracStatCache.ReturnsArray.Length - 1, BaseSeries_DestinationStartIndex), False)

				If (BaseSeriesSDev > 0) AndAlso (BackfillSeriesSDev > 0) Then
					VolatilityRatio = (BaseSeriesSDev / BackfillSeriesSDev)
				End If

			End If

			' Adjust Backfill returns to match the desired volatility
			' Note, for the conbined series StDev to match, the average for the Backfill series must be adjusted.

			Dim IndexCounter As Integer

			If (MatchBackfillVol) AndAlso (VolatilityRatio <> 1.0#) Then
				Dim AverageModifier As Double

				AverageModifier = (BaseAverage / VolatilityRatio) - BackfillAverage

				For IndexCounter = 1 To Math.Min(BackfillPertracStatCache.ReturnsArray.Length - 1, BaseSeries_DestinationStartIndex)
					ReturnsArray(IndexCounter) = (ReturnsArray(IndexCounter) + AverageModifier) * VolatilityRatio
				Next

			End If

			' Derive Net Delta Array, accounting for VolatilityRatio

			If True Then ' (Not BasePertracStatCache.NetDeltaArrayIsNull) OrElse (Not BackfillPertracStatCache.NetDeltaArrayIsNull) Then
				' If Either item has a real Delta Array then create one for this instrument.
				' Dim NetDeltaItemArray() As NetDeltaItemClass = Nothing
				'
				' 1 ) Get list of unique Instruments
				' 2 ) Build Aggregated Net Delta Array.

				' 1)

				Dim InstrumentDictionary As New Dictionary(Of ULong, Integer)

				If (Not BasePertracStatCache.NetDeltaArrayIsNull) Then
					For IndexCounter = 0 To (BasePertracStatCache.NetDeltaArray.Length - 1)
						If (Not InstrumentDictionary.ContainsKey(BasePertracStatCache.NetDeltaArray(IndexCounter).PertracId)) Then
							InstrumentDictionary.Add(BasePertracStatCache.NetDeltaArray(IndexCounter).PertracId, InstrumentDictionary.Count)
						End If
					Next
				Else
					' Base Pertrac StatsCache does not have a Net Delta Array, Thus Assume It is a non Group / simulation instrument
					' and the internal Pertrac ID is the final ID.

					If (Not InstrumentDictionary.ContainsKey(BasePertracStatCache.PertracID)) Then
						InstrumentDictionary.Add(BasePertracStatCache.PertracID, InstrumentDictionary.Count)
					End If
				End If

				If (Not BackfillPertracStatCache.NetDeltaArrayIsNull) Then
					For IndexCounter = 0 To (BackfillPertracStatCache.NetDeltaArray.Length - 1)
						If (Not InstrumentDictionary.ContainsKey(BackfillPertracStatCache.NetDeltaArray(IndexCounter).PertracId)) Then
							InstrumentDictionary.Add(BackfillPertracStatCache.NetDeltaArray(IndexCounter).PertracId, InstrumentDictionary.Count)
						End If
					Next
				Else
					' Backfill Pertrac StatsCache does not have a Net Delta Array, Thus Assume It is a non Group / simulation instrument
					' and the internal Pertrac ID is the final ID.

					If (Not InstrumentDictionary.ContainsKey(BackfillPertracStatCache.PertracID)) Then
						InstrumentDictionary.Add(BackfillPertracStatCache.PertracID, InstrumentDictionary.Count)
					End If
				End If

				' Size Array

				If (InstrumentDictionary.Count > 0) Then

					' Initialise Arrays

					NetDeltaItemArray = CType(Array.CreateInstance(GetType(NetDeltaItemClass), InstrumentDictionary.Count), NetDeltaItemClass())

					Dim PertracIDs(InstrumentDictionary.Count - 1) As ULong
					InstrumentDictionary.Keys.CopyTo(PertracIDs, 0)

					For IndexCounter = 0 To (PertracIDs.Length - 1)
						NetDeltaItemArray(InstrumentDictionary(PertracIDs(IndexCounter))) = New NetDeltaItemClass(PertracIDs(IndexCounter), CType(Array.CreateInstance(GetType(Double), ArrayLength), Double()))
					Next

					' Copy in the Data.

					' Backfill Series

					Dim ThisNetDeltaIndex As Integer
					Dim NetDeltaCounter As Integer

					If (Not BackfillPertracStatCache.NetDeltaArrayIsNull) Then

						For NetDeltaCounter = 0 To (BackfillPertracStatCache.NetDeltaArray.Length - 1)

							ThisNetDeltaIndex = InstrumentDictionary(BackfillPertracStatCache.NetDeltaArray(NetDeltaCounter).PertracId)

							For IndexCounter = 0 To Math.Min(BackfillPertracStatCache.ReturnsArray.Length - 1, BaseSeries_DestinationStartIndex - 1)
								NetDeltaItemArray(ThisNetDeltaIndex).DeltaArray(IndexCounter) = BackfillPertracStatCache.NetDeltaArray(NetDeltaCounter).DeltaArray(Math.Min(IndexCounter, BackfillPertracStatCache.NetDeltaArray(NetDeltaCounter).DeltaArray.Length - 1)) * VolatilityRatio
							Next

						Next

					Else

						ThisNetDeltaIndex = InstrumentDictionary(BackfillPertracStatCache.PertracID)

						For IndexCounter = 0 To Math.Min(BackfillPertracStatCache.ReturnsArray.Length - 1, BaseSeries_DestinationStartIndex - 1)
							NetDeltaItemArray(ThisNetDeltaIndex).DeltaArray(IndexCounter) = VolatilityRatio	' 1.0# * VolatilityRatio
						Next

					End If

					' Front Series

					If (Not BasePertracStatCache.NetDeltaArrayIsNull) Then

						For NetDeltaCounter = 0 To (BasePertracStatCache.NetDeltaArray.Length - 1)

							ThisNetDeltaIndex = InstrumentDictionary(BasePertracStatCache.NetDeltaArray(NetDeltaCounter).PertracId)

							For IndexCounter = BaseSeries_DestinationStartIndex To (ArrayIndex - 1)

								NetDeltaItemArray(ThisNetDeltaIndex).DeltaArray(IndexCounter) = BasePertracStatCache.NetDeltaArray(NetDeltaCounter).DeltaArray(Math.Min(IndexCounter - BaseSeries_DestinationStartIndex, BasePertracStatCache.NetDeltaArray(NetDeltaCounter).DeltaArray.Length - 1))

							Next

						Next

					Else

						ThisNetDeltaIndex = InstrumentDictionary(BasePertracStatCache.PertracID)

						For IndexCounter = BaseSeries_DestinationStartIndex To (ArrayIndex - 1)
							NetDeltaItemArray(ThisNetDeltaIndex).DeltaArray(IndexCounter) = 1.0#
						Next

					End If

				End If

			End If

		Else

			' Not Backfilled.

			' Is the requested Period of data equal to the 'Native' period of this data ?

			Dim NativeDatePeriod As DealingPeriod = PertracData.GetPertracDataPeriod(BasePertracID, eMessage)

			If (NativeDatePeriod = StatsDatePeriod) Then ' IsNativeDatePeriod

				' OK, Get data normally.

				Dim IsVeniceID As Boolean = False
				Dim IsPertracID As Boolean = False
				Dim InstrumentFlags As PertracDataClass.PertracInstrumentFlags = PertracDataClass.GetFlagsFromID(CUInt(Math.Max(BasePertracID, 0)))

				If InstrumentFlags = PertracDataClass.PertracInstrumentFlags.VeniceInstrument Then
					IsVeniceID = True
				ElseIf InstrumentFlags = PertracDataClass.PertracInstrumentFlags.PertracInstrument Then
					IsPertracID = True
				End If

				Select Case InstrumentFlags

					Case PertracDataClass.PertracInstrumentFlags.Dynamic_Group_Mean, PertracDataClass.PertracInstrumentFlags.Dynamic_Group_Median, PertracDataClass.PertracInstrumentFlags.Dynamic_Group_Weighted, PertracDataClass.PertracInstrumentFlags.Group_Mean, PertracDataClass.PertracInstrumentFlags.Group_Median, PertracDataClass.PertracInstrumentFlags.Group_Weighted, PertracDataClass.PertracInstrumentFlags.DrillDown_Group_Median, PertracDataClass.PertracInstrumentFlags.DrillDown_Group_Mean

						Dim NetDeltaArray() As NetDeltaItemClass = Nothing

						GetGroupSeries(pNestingLevel + 1, BasePertracID, NoCache, KnowledgeDate, StatsDatePeriod, DateArray, ReturnsArray, NetDeltaArray, ThisStatCacheKey, List_IDependOn, pSampleSize, eMessage)

						If (DateArray IsNot Nothing) Then
							IsMilestoneArray = CType(Array.CreateInstance(GetType(Boolean), DateArray.Length), Boolean())
							NAVArray = Nothing
							AverageArray = CType(Array.CreateInstance(GetType(Double), DateArray.Length), Double())
							StDevArray = CType(Array.CreateInstance(GetType(Double), DateArray.Length), Double())
							NetDeltaItemArray = NetDeltaArray
						Else
							IsMilestoneArray = CType(Array.CreateInstance(GetType(Boolean), 1), Boolean())
							NAVArray = Nothing
							AverageArray = CType(Array.CreateInstance(GetType(Double), 1), Double())
							StDevArray = CType(Array.CreateInstance(GetType(Double), 1), Double())
						End If

					Case PertracDataClass.PertracInstrumentFlags.PertracInstrument, PertracDataClass.PertracInstrumentFlags.VeniceInstrument

						' Venice Instrument or Pertrac Instrument 

						SyncLock Lock_BuildNewStatSeries

							Try
								If BasePertracID <= 0 Then
									DataTable = _PertracData.GetPertracTable(StatsDatePeriod, 0, eMessage)
								Else
									DataTable = _PertracData.GetPertracTable(StatsDatePeriod, pNestingLevel + 1, BasePertracID, -1, NoCache, KnowledgeDate, eMessage)
								End If

								' Ensure the data is in date order

								If (DataTable.Columns.Contains("PerformanceDate")) Then
									SortedRows = DataTable.Select("True", "PerformanceDate")
								End If

							Catch ex As Exception
								ReDim SortedRows(-1)
							End Try

							If (SortedRows.Length <= 0) Then
								Return Nothing
							End If

							' Get Data Column Ordinals, used as a performance improver.

							Try
								PerformanceDateOrdinal = DataTable.Columns.IndexOf("PerformanceDate")
								ReturnOrdinal = DataTable.Columns.IndexOf("PerformanceReturn")
								NAVOrdinal = DataTable.Columns.IndexOf("NAV")

								If (DataTable.Columns.Contains("PriceIsMilestone")) Then
									IsMilestoneOrdinal = DataTable.Columns.IndexOf("PriceIsMilestone")
								End If
							Catch ex As Exception
								Return Nothing
							End Try

							' Initialise Data Arrays

							Try

								DataStartDate = CDate(SortedRows(0)(PerformanceDateOrdinal))

								If (IsVeniceID) AndAlso (SortedRows.Length > 1) Then
									' Some (Many) Venice price series start with a default starting value on 1 Jan 1970, 
									' Check for this and eliminate it.

									Dim StartIndex As Integer = 0

									' If the NAVs of the first two dates are the same, then ignore the first one, otherwise dont.

									While (StartIndex < (SortedRows.Length - 1)) AndAlso ((CDate(SortedRows(StartIndex + 1)(PerformanceDateOrdinal)) - CDate(SortedRows(StartIndex)(PerformanceDateOrdinal))).TotalDays > 50) AndAlso (Math.Abs(CDbl(SortedRows(StartIndex + 1)(NAVOrdinal)) - CDbl(SortedRows(StartIndex)(NAVOrdinal))) < 0.00000001#)
										DataStartDate = CDate(SortedRows(StartIndex + 1)(PerformanceDateOrdinal))

										StartIndex += 1
									End While

								End If

								DataStartDate = FitDateToPeriod(StatsDatePeriod, DataStartDate, False) ' move to start of period.

							Catch ex As Exception
								Return Nothing
							End Try

							If (IsVeniceID) Then

								' If it's a Venice Series, Build returns from NAV data as returns are not always present for Venice Instruments.
								' Also Build IsMilestone Array (Only for Venice Instruments).

								' Fill the returns array using the latest value in each period.
								' Note, this may not work if the StatsDatePeriod does not match the returns period in the pertrac DB.

								Dim ThisRow As DataRow

								ArrayLength = GetPeriodCount(StatsDatePeriod, DataStartDate, CDate(SortedRows(SortedRows.Length - 1)(PerformanceDateOrdinal)))

								' NAVArray
								' RealReturnArray
								' IsMilestoneArray

								RealReturnArray = CType(Array.CreateInstance(GetType(Boolean), ArrayLength + 1), Boolean())	' Flag to indicate 'real' returns - don't fill.
								IsMilestoneArray = CType(Array.CreateInstance(GetType(Boolean), ArrayLength + 1), Boolean())
								NAVArray = CType(Array.CreateInstance(GetType(Double), ArrayLength + 1), Double())

								Try
									Dim InitIndex As Integer

									If (RealReturnArray IsNot Nothing) AndAlso (RealReturnArray.Length > 0) Then
										For InitIndex = 0 To (RealReturnArray.Length - 1)
											RealReturnArray(InitIndex) = False
										Next
									End If
								Catch ex As Exception
								End Try

								For RowCounter = 0 To (SortedRows.Length - 1)
									ThisRow = SortedRows(RowCounter)
									ArrayIndex = Math.Max(GetPriceIndex(StatsDatePeriod, DataStartDate, CDate(ThisRow(PerformanceDateOrdinal))), 0)

									NAVArray(ArrayIndex) = CDbl(ThisRow(NAVOrdinal))
									RealReturnArray(ArrayIndex) = True
									If (IsMilestoneOrdinal >= 0) Then
										IsMilestoneArray(ArrayIndex) = CBool(ThisRow(IsMilestoneOrdinal))
									End If
								Next

								' Check for missing prices - Forward fill and Back fill.

								For RowCounter = 1 To (NAVArray.Length - 1)
									If (NAVArray(RowCounter) = 0) AndAlso (RealReturnArray(RowCounter) = False) Then
										NAVArray(RowCounter) = NAVArray(RowCounter - 1)
									End If
								Next

								For RowCounter = (NAVArray.Length - 2) To 0
									If (NAVArray(RowCounter) = 0) AndAlso (RealReturnArray(RowCounter) = False) Then
										NAVArray(RowCounter) = NAVArray(RowCounter + 1)
									End If
								Next

								' Venice Obsolete data points fix.
								' OK, We are trying to eliminate the PreStart Default (often '100.0') fund valuations in Venice.
								' Now these values play an important role for Venice, but thay are not valid for the purpose of Statistics
								' or The Reporter App.

								Try
									Dim InitIndex As Integer
									InitIndex = 0

									While (InitIndex < (RealReturnArray.Length - 2)) AndAlso (Not (RealReturnArray(InitIndex) And RealReturnArray(InitIndex + 1)) AndAlso (Math.Abs(NAVArray(InitIndex) - NAVArray(InitIndex + 1)) < 0.00000001#))
										InitIndex += 1
									End While

									If (InitIndex > 0) Then
										DataStartDate = AddPeriodToDate(StatsDatePeriod, DataStartDate, InitIndex)
										DataStartDate = FitDateToPeriod(StatsDatePeriod, DataStartDate, False) ' move to start of period.

										ArrayLength -= InitIndex

										Dim OldDoubleArray() As Double
										Dim OldBooleanArray() As Boolean

										OldDoubleArray = NAVArray
										NAVArray = CType(Array.CreateInstance(GetType(Double), ArrayLength + 1), Double())
										Array.Copy(OldDoubleArray, InitIndex, NAVArray, 0, ArrayLength + 1)

										OldBooleanArray = RealReturnArray
										RealReturnArray = CType(Array.CreateInstance(GetType(Boolean), ArrayLength + 1), Boolean())
										Array.Copy(OldBooleanArray, InitIndex, RealReturnArray, 0, ArrayLength + 1)

										OldBooleanArray = IsMilestoneArray
										IsMilestoneArray = CType(Array.CreateInstance(GetType(Boolean), ArrayLength + 1), Boolean())
										Array.Copy(OldBooleanArray, InitIndex, IsMilestoneArray, 0, ArrayLength + 1)

										OldDoubleArray = Nothing
										OldBooleanArray = Nothing
									End If
								Catch ex As Exception
									Return Nothing
								End Try

								' Note ArrayLength returns the actual number of periods, thus dimensioning to 'ArrayLength + 1'
								' Creates space for the initial NAV figure.

								DateArray = CType(Array.CreateInstance(GetType(Date), ArrayLength + 1), Date())
								ReturnsArray = CType(Array.CreateInstance(GetType(Double), ArrayLength + 1), Double())
								AverageArray = CType(Array.CreateInstance(GetType(Double), ArrayLength + 1), Double())
								StDevArray = CType(Array.CreateInstance(GetType(Double), ArrayLength + 1), Double())

								' Now Generate Return series.

								For RowCounter = 1 To (NAVArray.Length - 1)
									If (NAVArray(RowCounter - 1) <> 0) Then
										ReturnsArray(RowCounter) = (NAVArray(RowCounter) / NAVArray(RowCounter - 1)) - 1.0#
									Else
										ReturnsArray(RowCounter) = 0
									End If
								Next

							Else ' <not> If (IsVeniceID) Then

								Dim IsNAV_OK As Boolean = False
								Dim IsReturns_OK As Boolean = False
								Dim ThisRow As DataRow
								Dim thisDate As Date

								Try
									' Hmmm, Pertrac series are usually Returns Series, However there are some Price (NAV) series - especially the daily
									' series entered by me.
									' Try to evaluate if the NAV series is valid or if this should be a returns series only.

									' Note ArrayLength returns the actual number of periods, thus dimensioning to 'ArrayLength + 1'
									' Creates space for the initial NAV figure.

									ArrayLength = GetPeriodCount(StatsDatePeriod, DataStartDate, CDate(SortedRows(SortedRows.Length - 1)(PerformanceDateOrdinal)))

									' Check for (probably) a NAV Series and don't add an initial NAV date if it is.

									If (SortedRows IsNot Nothing) AndAlso (SortedRows.Length > 0) Then
										ThisRow = SortedRows(0)
									Else
										ThisRow = Nothing
									End If

									If (ThisRow IsNot Nothing) AndAlso ((CDbl(ThisRow(NAVOrdinal)) <> 0.0#) And (CDbl(ThisRow(ReturnOrdinal)) = 0.0#)) Then
										ArrayLength -= 1
									Else
										DataStartDate = AddPeriodToDate(StatsDatePeriod, DataStartDate, -1)
									End If

									' Dimension  arrays

									RealReturnArray = CType(Array.CreateInstance(GetType(Boolean), ArrayLength + 1), Boolean())	' Flag to indicate 'real' returns - don't fill.
									DateArray = CType(Array.CreateInstance(GetType(Date), ArrayLength + 1), Date())
									ReturnsArray = CType(Array.CreateInstance(GetType(Double), ArrayLength + 1), Double())
									IsMilestoneArray = CType(Array.CreateInstance(GetType(Boolean), ArrayLength + 1), Boolean())
									NAVArray = CType(Array.CreateInstance(GetType(Double), ArrayLength + 1), Double())
									AverageArray = CType(Array.CreateInstance(GetType(Double), ArrayLength + 1), Double())
									StDevArray = CType(Array.CreateInstance(GetType(Double), ArrayLength + 1), Double())

									Try
										Dim InitIndex As Integer

										If (RealReturnArray IsNot Nothing) AndAlso (RealReturnArray.Length > 0) Then
											For InitIndex = 0 To (RealReturnArray.Length - 1)
												RealReturnArray(InitIndex) = False
											Next
										End If
									Catch ex As Exception
									End Try

								Catch ex As Exception
									Return Nothing
								End Try

								' Build Returns Array

								Try
									' Fill the returns array using the latest value in each period.
									' Note, this may not work if the StatsDatePeriod does not match the returns period in the pertrac DB.

									For RowCounter = 0 To (SortedRows.Length - 1)
										ThisRow = SortedRows(RowCounter)
										ArrayIndex = GetPriceIndex(StatsDatePeriod, DataStartDate, CDate(ThisRow(PerformanceDateOrdinal)))
										thisDate = CDate(ThisRow(PerformanceDateOrdinal))

										' Set Return value, or compound if this index already set.
										If (RealReturnArray(ArrayIndex)) Then
											If (CDbl(ThisRow(ReturnOrdinal)) <> 0.0#) Then
												ReturnsArray(ArrayIndex) = ((1.0# + ReturnsArray(ArrayIndex)) * (1.0# + CDbl(ThisRow(ReturnOrdinal)))) - 1.0#
											End If
										Else
											ReturnsArray(ArrayIndex) = CDbl(ThisRow(ReturnOrdinal))
										End If
										NAVArray(ArrayIndex) = CDbl(ThisRow(NAVOrdinal))

										RealReturnArray(ArrayIndex) = True

										If (Not IsReturns_OK) AndAlso (ReturnsArray(ArrayIndex) <> 0.0#) Then
											IsReturns_OK = True
										End If
										If (Not IsNAV_OK) AndAlso (NAVArray(ArrayIndex) <> 0.0#) Then
											IsNAV_OK = True
										End If
									Next
								Catch ex As Exception
									Return Nothing
								End Try

								If (IsNAV_OK) Then
									' Forward and Back fill NAVs if Navs are OK.

									' Hmmm, always an issue.
									' Often Pertrac IDs do not have NAVs and when they do they are not consistent with the returns.
									' 'Try' to sort this out.

									' Check for 'Real Return' NAVs without 'Return' values
									' Try to infill Real Return values implied by Real NAV Values.

									If (IsReturns_OK) Then ' Some returns were returned, this is not just a NAV Only series.

										For ArrayIndex = 1 To (NAVArray.Length - 1)
											If (RealReturnArray(ArrayIndex) And RealReturnArray(ArrayIndex - 1)) Then
												If (ReturnsArray(ArrayIndex) = 0.0#) AndAlso (NAVArray(ArrayIndex) <> 0.0#) AndAlso (NAVArray(ArrayIndex - 1) <> 0.0#) Then
													' OK we have Consecutive 'Real' Returns with NO 'Return' Value but BOTH 'NAV' Values

													ReturnsArray(ArrayIndex) = (NAVArray(ArrayIndex) / NAVArray(ArrayIndex - 1)) - 1.0#
												End If
											End If
										Next

										' Find the latest date with a valid NAV

										Dim LatestDateIndex As Integer

										LatestDateIndex = (NAVArray.Length - 1)
										While (LatestDateIndex >= 0) AndAlso ((RealReturnArray(LatestDateIndex) = False) Or (NAVArray(LatestDateIndex) = 0.0#))
											LatestDateIndex -= 1
										End While

										If (LatestDateIndex < 0) Then
											' No NAVs ?  How oddd..

											LatestDateIndex = 0
											NAVArray(0) = 100.0#
										End If

										' Now, Assuming the RETURNS are correct...

										' Forward Fill

										For ArrayIndex = (LatestDateIndex + 1) To (NAVArray.Length - 1)
											If (RealReturnArray(ArrayIndex)) Then
												NAVArray(ArrayIndex) = NAVArray(ArrayIndex - 1) * (1.0# + ReturnsArray(ArrayIndex))
											Else
												NAVArray(ArrayIndex) = NAVArray(ArrayIndex - 1)
											End If
										Next

										' Back Fill

										Dim NewNAV As Double

										For ArrayIndex = (LatestDateIndex - 1) To 0 Step -1
											NewNAV = NAVArray(ArrayIndex + 1) / (1.0# + ReturnsArray(ArrayIndex + 1))

											If (NAVArray(ArrayIndex) = 0.0#) Then
												NAVArray(ArrayIndex) = NewNAV
											Else
												If (Math.Abs((NewNAV / NAVArray(ArrayIndex)) - 1.0#)) > 0.0001 Then
													NAVArray(ArrayIndex) = NewNAV
												End If
											End If
										Next

									Else ' IsReturns_OK = False

										' Forward / Back fill

										For ArrayIndex = 1 To (NAVArray.Length - 1)
											If (Not RealReturnArray(ArrayIndex)) Then
												NAVArray(ArrayIndex) = NAVArray(ArrayIndex - 1)
											End If
										Next

										For ArrayIndex = (NAVArray.Length - 2) To 0 Step -1
											If (Not RealReturnArray(ArrayIndex)) Then
												NAVArray(ArrayIndex) = NAVArray(ArrayIndex + 1)
											End If
										Next

										' Imply Returns Array

										For ArrayIndex = 1 To (NAVArray.Length - 1)
											If (NAVArray(ArrayIndex) <> 0) Then
												ReturnsArray(ArrayIndex) = (NAVArray(ArrayIndex) / NAVArray(ArrayIndex - 1)) - 1.0#
											End If
										Next

									End If

								Else
									NAVArray = Nothing
								End If

							End If

							' Set Date Array

							Try
								DateArray(0) = DataStartDate

								For RowCounter = 1 To (DateArray.Length - 1)
									DateArray(RowCounter) = AddPeriodToDate(StatsDatePeriod, DateArray(RowCounter - 1), 1)
								Next

							Catch ex As Exception
								Return Nothing
							End Try

						End SyncLock

				End Select

			Else

				' Derive Non-Native Series.

				Dim NativeStatCache As StatCacheClass = Nothing
				Dim NativeStartDate As Date
				Dim NativeEndDate As Date
				Dim NativeIndex As Integer
				Dim NativeNAVArray() As Double

				' Get Native Series.

				If (Not NoCache) Then
					NativeStatCache = Me.GetStatCacheItem(NativeDatePeriod, CULng(BasePertracID), False)
				End If
				If (NativeStatCache Is Nothing) Then
					NativeStatCache = BuildNewStatSeries(NativeDatePeriod, pNestingLevel, CULng(BasePertracID), False, PeriodCount, Lamda, pSampleSize, NoCache, KnowledgeDate, eMessage)

					If (Not NoCache) Then
						SetStatCacheItem(NativeStatCache)
					End If
				End If
				If (NativeStatCache IsNot Nothing) Then
					NativeStatCache.Set_DependsOnMe(ThisStatCacheKey)
					List_IDependOn.Add(GetStatCacheKey(NativeStatCache))
				End If

				' Extrapolate to required Frequency.

				If (NativeStatCache IsNot Nothing) AndAlso (NativeStatCache.DateArray IsNot Nothing) AndAlso (NativeStatCache.DateArray.Length > 0) Then
					NativeStartDate = NativeStatCache.DateArray(0)
					NativeEndDate = NativeStatCache.DateArray(NativeStatCache.DateArray.Length - 1)

					' Check Native NAV series is set.

					If (NativeStatCache.NAVArray Is Nothing) OrElse (NativeStatCache.NAVArray.Length <> NativeStatCache.ReturnsArray.Length) Then
						SetNAVSeries(NativeStatCache)
					End If

					' Get native NAV Series.

					NativeNAVArray = NativeStatCache.NAVArray

					' Initialise Data Arrays.

					DataStartDate = FitDateToPeriod(StatsDatePeriod, NativeStartDate)
					DataEndDate = FitDateToPeriod(StatsDatePeriod, NativeEndDate)
					ArrayLength = GetPeriodCount(StatsDatePeriod, DataStartDate, DataEndDate)

					DateArray = CType(Array.CreateInstance(GetType(Date), ArrayLength), Date())
					NAVArray = CType(Array.CreateInstance(GetType(Double), ArrayLength), Double())
					ReturnsArray = CType(Array.CreateInstance(GetType(Double), ArrayLength), Double())
					AverageArray = CType(Array.CreateInstance(GetType(Double), ArrayLength), Double())
					StDevArray = CType(Array.CreateInstance(GetType(Double), ArrayLength), Double())

					Dim InstrumentIndex As Integer

					' Create Net Delta Array if required.

					If (Not NativeStatCache.NetDeltaArrayIsNull) Then
						NetDeltaItemArray = CType(Array.CreateInstance(GetType(NetDeltaItemClass), NativeStatCache.NetDeltaArray.Length), NetDeltaItemClass())

						For InstrumentIndex = 0 To (NetDeltaItemArray.Length - 1)
							NetDeltaItemArray(InstrumentIndex) = New NetDeltaItemClass(NativeStatCache.NetDeltaArray(InstrumentIndex).PertracId, CType(Array.CreateInstance(GetType(Double), ArrayLength), Double()))
						Next

					End If

					' Build Date and NAV Arrays, then create Returns array.

					DateArray(0) = DataStartDate
					NativeIndex = GetPriceIndex(NativeStatCache.StatsDatePeriod, NativeStatCache.DateArray(0), DataStartDate)
					NAVArray(0) = NativeNAVArray(Math.Min(Math.Max(NativeIndex, 0), NativeNAVArray.Length - 1))

					' It can happen when, for example, going from Monthly to Daily data that the monthly date does not exist in the daily series.
					' e.g. If the Month ends an a Weekend, then the Day would be the previous Friday.
					' This can result in a negative (-1 only, I think) Native Index.
					' Cope with this :-

					Dim tmpArrayIndex As Integer = 1
					While NativeIndex < 0
						DateArray(tmpArrayIndex) = AddPeriodToDate(StatsDatePeriod, DateArray(tmpArrayIndex - 1), 1)
						NativeIndex = GetPriceIndex(NativeStatCache.StatsDatePeriod, NativeStatCache.DateArray(0), DateArray(tmpArrayIndex))
						NAVArray(tmpArrayIndex) = NativeNAVArray(Math.Max(NativeIndex, 0))
						ReturnsArray(tmpArrayIndex) = (NAVArray(tmpArrayIndex) / NAVArray(tmpArrayIndex - 1)) - 1.0#

						' Copy Net Delta Items

						If (NetDeltaItemArray IsNot Nothing) Then
							For InstrumentIndex = 0 To (NetDeltaItemArray.Length - 1)
								NetDeltaItemArray(InstrumentIndex).DeltaArray(tmpArrayIndex) = NativeStatCache.NetDeltaArray(InstrumentIndex).DeltaArray(Math.Max(NativeIndex, 0))
							Next
						End If

						tmpArrayIndex += 1
					End While

					' Once NativeIndex is positive, process the rest of the data.
					' Note : the NativeIndex '+=' construct is done for performance reasons. It should always be getting the Next or Current Period.

					For ArrayIndex = tmpArrayIndex To (ArrayLength - 1)
						DateArray(ArrayIndex) = AddPeriodToDate(StatsDatePeriod, DateArray(ArrayIndex - 1), 1)

						NativeIndex = Math.Min(NativeIndex + GetPriceIndex(NativeStatCache.StatsDatePeriod, NativeStatCache.DateArray(NativeIndex), DateArray(ArrayIndex)), NativeNAVArray.Length - 1)
						NAVArray(ArrayIndex) = NativeNAVArray(NativeIndex)

						ReturnsArray(ArrayIndex) = (NAVArray(ArrayIndex) / NAVArray(ArrayIndex - 1)) - 1.0#

						' Copy Net Delta Items

						If (NetDeltaItemArray IsNot Nothing) Then
							For InstrumentIndex = 0 To (NetDeltaItemArray.Length - 1)
								NetDeltaItemArray(InstrumentIndex).DeltaArray(ArrayIndex) = NativeStatCache.NetDeltaArray(InstrumentIndex).DeltaArray(NativeIndex)
							Next
						End If

					Next

					' Move Date Array to Start of period values in order to be consistent with Normal Conventions.

					If (StatsDatePeriod <> DealingPeriod.Daily) Then
						For ArrayIndex = 0 To (DateArray.Length - 1)
							DateArray(ArrayIndex) = FitDateToPeriod(StatsDatePeriod, DateArray(ArrayIndex), False)
						Next
					End If
				End If

			End If

		End If

		' Construct Return object

		Dim RVal As StatCacheClass

		RVal = New StatCacheClass(StatsDatePeriod, cPertracID, MatchBackfillVol, PeriodCount, Lamda, DateArray, NAVArray, ReturnsArray, IsMilestoneArray, AverageArray, StDevArray, NetDeltaItemArray)

		RVal = RefreshStatsSeries(RVal, PeriodCount, pLamda)

		If (List_IDependOn IsNot Nothing) AndAlso (List_IDependOn.Count > 0) Then
			RVal.Set_I_DependOn(List_IDependOn.ToArray)
		End If

		Return RVal

	End Function

	Private Function RefreshStatsSeries(ByVal pStatsObject As StatCacheClass, ByVal pPeriodCount As Integer, ByVal pLamda As Double) As StatCacheClass
		' **************************************************************************************
		' Set Average and StDev series.
		'
		' The Average Series is a Straight Average.
		' The Standard Deviation Series is Exponentially weighted, amd must be recalculated.
		' if Lamda changes.
		' The Standard Deviation is a Population StdDev - Divisor is 'N' rather than 'N-1'.
		'
		' **************************************************************************************

		Try
			If (pStatsObject.DateArray Is Nothing) Then
				Return pStatsObject
				Exit Function
			End If
		Catch ex As Exception
		End Try

		SyncLock Lock_RefreshStatsSeries

			Dim RowCounter As Integer
			Dim thisReturnsArray() As Double = pStatsObject.ReturnsArray
			Dim thisAverageArray() As Double = pStatsObject.AverageArray
			Dim thisStDevArray() As Double = pStatsObject.StDevArray

			Try

				' Validate / Flush Average and StDev Arrays.

				If (thisAverageArray Is Nothing) OrElse (thisAverageArray.Length <> pStatsObject.DateArray.Length) Then
					thisAverageArray = CType(Array.CreateInstance(GetType(Double), pStatsObject.DateArray.Length), Double())
				Else
					Array.Clear(thisAverageArray, 0, thisAverageArray.Length)
				End If

				If (thisStDevArray Is Nothing) OrElse (thisStDevArray.Length <> pStatsObject.DateArray.Length) Then
					thisStDevArray = CType(Array.CreateInstance(GetType(Double), pStatsObject.DateArray.Length), Double())
				Else
					Array.Clear(thisStDevArray, 0, thisStDevArray.Length)
				End If

				' Calculate Averages Array

				Try
					Dim RollingSum As Double = 0

					If (pPeriodCount > 0) Then
						For RowCounter = 1 To (thisAverageArray.Length - 1)
							RollingSum += thisReturnsArray(RowCounter)
							If (RowCounter >= pPeriodCount) Then ' Remember Return(0) is Zero.
								RollingSum -= thisReturnsArray(RowCounter - pPeriodCount)

								thisAverageArray(RowCounter) = (RollingSum / pPeriodCount)
							End If
						Next
					End If
				Catch ex As Exception
				End Try

				' Set StdDev Array

				Try
					Dim CurrentCounter As Integer
					Dim SDCounter As Integer
					Dim thisMultiplier As Double

					Dim ExponentMultiplier(pPeriodCount - 1) As Double
					Dim SumExponentWeight As Double

					ExponentMultiplier(0) = 1
					SumExponentWeight = 1
					For CurrentCounter = 1 To (pPeriodCount - 1)
						If (pLamda = 1) Then
							ExponentMultiplier(CurrentCounter) = 1
							SumExponentWeight += 1
						Else
							ExponentMultiplier(CurrentCounter) = ExponentMultiplier(CurrentCounter - 1) * pLamda
							SumExponentWeight += ExponentMultiplier(CurrentCounter)
						End If
					Next

					If (SumExponentWeight > 0) Then
						For CurrentCounter = pPeriodCount To (thisReturnsArray.Length - 1)
							Dim avgReturn As Double = 0
							Dim sumReturn2 As Double = 0
							Dim Exponent As Double = 1

							avgReturn = thisAverageArray(CurrentCounter)

							' Calculate Sum((X - Mean) ^ 2)
							For SDCounter = 0 To (pPeriodCount - 1)
								thisMultiplier = ExponentMultiplier(SDCounter)

								sumReturn2 += thisMultiplier * ((thisReturnsArray(CurrentCounter - SDCounter) - avgReturn) ^ 2)
							Next

							thisStDevArray(CurrentCounter) = Math.Sqrt(sumReturn2 / SumExponentWeight)
						Next
					End If

				Catch ex As Exception
				End Try

			Catch ex As Exception
			Finally

				' Ensure StatsObject members are set

				pStatsObject.PeriodCount = pPeriodCount
				pStatsObject.Lamda = pLamda
				pStatsObject.AverageArray = thisAverageArray
				pStatsObject.StDevArray = thisStDevArray

			End Try

			Return pStatsObject
		End SyncLock

	End Function

	Public Sub CalcAvgStDevSeries(ByVal thisReturnsArray() As Double, ByRef thisAverageArray() As Double, ByRef thisStDevArray() As Double, ByVal pPeriodCount As Integer, ByVal pLamda As Double)
		' **************************************************************************************
		' Set Average and StDev series.
		'
		' The Average Series is a Straight Average.
		' The Standard Deviation Series is Exponentially weighted, amd must be recalculated.
		' if Lamda changes.
		' The Standard Deviation is a Population StdDev - Divisor is 'N' rather than 'N-1'.
		'
		' **************************************************************************************

		Try
			If (thisReturnsArray Is Nothing) Then
				Exit Sub
			End If
		Catch ex As Exception
		End Try

		SyncLock Lock_RefreshStatsSeries

			Dim RowCounter As Integer
			Try

				' Validate / Flush Average and StDev Arrays.

				If (thisAverageArray Is Nothing) OrElse (thisAverageArray.Length <> thisReturnsArray.Length) Then
					thisAverageArray = CType(Array.CreateInstance(GetType(Double), thisReturnsArray.Length), Double())
				Else
					Array.Clear(thisAverageArray, 0, thisAverageArray.Length)
				End If

				If (thisStDevArray Is Nothing) OrElse (thisStDevArray.Length <> thisReturnsArray.Length) Then
					thisStDevArray = CType(Array.CreateInstance(GetType(Double), thisReturnsArray.Length), Double())
				Else
					Array.Clear(thisStDevArray, 0, thisStDevArray.Length)
				End If

				' Calculate Averages Array

				Try
					Dim RollingSum As Double = 0

					If (pPeriodCount > 0) Then
						For RowCounter = 1 To (thisAverageArray.Length - 1)
							RollingSum += thisReturnsArray(RowCounter)

							If (RowCounter >= pPeriodCount) Then ' Remember Return(0) is Zero.
								RollingSum -= thisReturnsArray(RowCounter - pPeriodCount)

								thisAverageArray(RowCounter) = (RollingSum / pPeriodCount)
							End If
						Next
					End If
				Catch ex As Exception
				End Try

				' Set StdDev Array

				Try
					Dim CurrentCounter As Integer
					Dim SDCounter As Integer
					Dim thisMultiplier As Double

					Dim ExponentMultiplier(pPeriodCount - 1) As Double
					Dim SumExponentWeight As Double

					ExponentMultiplier(0) = 1
					SumExponentWeight = 1
					For CurrentCounter = 1 To (pPeriodCount - 1)
						If (pLamda = 1) Then
							ExponentMultiplier(CurrentCounter) = 1
							SumExponentWeight += 1
						Else
							ExponentMultiplier(CurrentCounter) = ExponentMultiplier(CurrentCounter - 1) * pLamda
							SumExponentWeight += ExponentMultiplier(CurrentCounter)
						End If
					Next

					If (SumExponentWeight > 0) Then
						For CurrentCounter = pPeriodCount To (thisReturnsArray.Length - 1)
							Dim avgReturn As Double = 0
							Dim sumReturn2 As Double = 0
							Dim Exponent As Double = 1

							avgReturn = thisAverageArray(CurrentCounter)

							' Calculate Sum((X - Mean) ^ 2)
							For SDCounter = 0 To (pPeriodCount - 1)
								thisMultiplier = ExponentMultiplier(SDCounter)

								sumReturn2 += thisMultiplier * ((thisReturnsArray(CurrentCounter - SDCounter) - avgReturn) ^ 2)
							Next

							thisStDevArray(CurrentCounter) = Math.Sqrt(sumReturn2 / SumExponentWeight)
						Next
					End If

				Catch ex As Exception
				End Try

			Catch ex As Exception
			Finally

			End Try

		End SyncLock

	End Sub

#End Region

#Region " Comparison Stats Classes and Functions"

	' *********************************************************************************
	' Note : Comparison Stats Class does not differentiate based on pStatsDatePeriod, if the
	' required value does not match the stored instance, then it is simply re-calculated.
	'
	' If this becomes a performance issue, then the GetComparisonObject() function and the 
	' Cache Key function will need to be updated.
	'
	' *********************************************************************************

	Public Class ComparisonStatsClass
		' **********************************************************************************************
		'
		'
		' **********************************************************************************************

		Public PertracID As ULong
		Public ReferencePertracID As ULong
		Public MatchBackfillVol As Boolean
		Public _PertracScalingFactor As Double
		Public _ReferenceScalingFactor As Double
		Public StatsFromDate As Date
		Public StatsToDate As Date
		Public StatsPeriodCount As Integer
		Public ActualRollingPeriod As Integer

		Private _StatsPeriodType As RenaissanceGlobals.DealingPeriod
		Public StatsLamda As Double
		Public Correlation As Double
		Public Covariance As Double
		Public CorrelationUpOnly As Double
		Public CorrelationDownOnly As Double

		Public PertracStDev As Double
		Public ReferenceStDev As Double
		Public PertracAPR As Double
		Public ReferenceAPR As Double
		Public PertracCompoundReturn As Double
		Public ReferenceCompoundReturn As Double

		Public PertracStDevUpOnly As Double
		Public ReferenceStDevUpOnly As Double
		Public PertracStDevDownOnly As Double
		Public ReferenceStDevDownOnly As Double

		Public LastAccessed As Date

		Public Sub New(ByVal pStatsPeriodType As RenaissanceGlobals.DealingPeriod)
			' *************************************************************
			'
			' *************************************************************

			PertracID = 0
			ReferencePertracID = 0
			MatchBackfillVol = False
			StatsFromDate = Renaissance_BaseDate
			StatsToDate = Renaissance_BaseDate
			StatsPeriodCount = 0
			_PertracScalingFactor = 1.0#
			_ReferenceScalingFactor = 1.0#

			' Default Period to StatsClass Default
			StatsPeriodType = pStatsPeriodType

			StatsLamda = 1

			Me.ClearStats()

		End Sub

		Public ReadOnly Property AnnualPeriodCount() As Integer
			' *************************************************************
			' This function is a mirror of the one in the main Stats Class.
			' They MUST be identical
			'
			' *************************************************************

			Get
				Select Case Me.StatsPeriodType
					Case DealingPeriod.Daily
						Return 261 ' 365

					Case DealingPeriod.Weekly
						Return 52	' (365 / 7)

					Case DealingPeriod.Fortnightly
						Return 26	' (365 / 14)

					Case DealingPeriod.Monthly
						Return 12

					Case DealingPeriod.Quarterly
						Return 4

					Case DealingPeriod.SemiAnnually
						Return 2

					Case DealingPeriod.Annually
						Return 1

					Case Else
						Return 12
				End Select
			End Get
		End Property

		Public ReadOnly Property Sqrt12() As Double
			' *************************************************************
			' This function is a mirror of the one in the main Stats Class.
			' They MUST be identical
			'
			' *************************************************************
			Get
				Select Case Me.StatsPeriodType
					Case DealingPeriod.Daily
						Return 16.15549442140351 ' Math.Sqrt(261) ' 19.1049731745428 ' Math.Sqrt(365)

					Case DealingPeriod.Weekly
						Return 7.21110255092798	' Math.Sqrt(52)

					Case DealingPeriod.Fortnightly
						Return 5.09901951359278	' Math.Sqrt(26)

					Case DealingPeriod.Monthly
						Return 3.46410161513775	' Math.Sqrt(12)

					Case DealingPeriod.Quarterly
						Return 2.0 ' Math.Sqrt(4)

					Case DealingPeriod.SemiAnnually
						Return 3.46410161513775	' Math.Sqrt(2)

					Case DealingPeriod.Annually
						Return 1.0

					Case Else
						Return 3.46410161513775	' Math.Sqrt(12)
				End Select
			End Get
		End Property

		Public Property StatsPeriodType() As RenaissanceGlobals.DealingPeriod
			Get
				Return _StatsPeriodType
			End Get
			Set(ByVal value As RenaissanceGlobals.DealingPeriod)
				If (_StatsPeriodType <> value) Then
					_StatsPeriodType = value
					Me.ClearStats()
				End If
			End Set
		End Property

		Public Property PertracScalingFactor() As Double
			Get
				Return _PertracScalingFactor
			End Get
			Set(ByVal value As Double)
				If (_PertracScalingFactor <> value) Then
					Me.ClearStats()
					_PertracScalingFactor = value
				End If
			End Set
		End Property

		Public Property ReferenceScalingFactor() As Double
			Get
				Return _ReferenceScalingFactor
			End Get
			Set(ByVal value As Double)
				If (_ReferenceScalingFactor <> value) Then
					Me.ClearStats()
					_ReferenceScalingFactor = value
				End If
			End Set
		End Property

		Public Sub ClearStats()
			' *************************************************************
			'
			' *************************************************************

			Me.Correlation = 0
			Me.Covariance = 0
			Me.CorrelationDownOnly = 0
			Me.CorrelationUpOnly = 0

			Me.PertracStDev = 0
			Me.ReferenceStDev = 0
			Me.PertracAPR = 0
			Me.ReferenceAPR = 0
			Me.PertracCompoundReturn = 0
			Me.ReferenceCompoundReturn = 0
			Me.PertracStDevUpOnly = 0
			Me.ReferenceStDevUpOnly = 0
			Me.PertracStDevDownOnly = 0
			Me.ReferenceStDevDownOnly = 0

			Me.ActualRollingPeriod = Me.StatsPeriodCount
		End Sub

		Public ReadOnly Property Beta(ByVal pBetaStock As Integer) As Double
			' *************************************************************
			'
			' *************************************************************

			Get
				If (pBetaStock = Me.PertracID) Then
					If Me.ReferenceStDev = 0 Then
						Return 0
					Else
						Return Math.Round((Me.Correlation * Me.PertracStDev) / Me.ReferenceStDev, 6)
					End If
				ElseIf (pBetaStock = Me.ReferencePertracID) Then
					If Me.PertracStDev = 0 Then
						Return 0
					Else
						Return Math.Round((Me.Correlation * Me.ReferenceStDev) / Me.PertracStDev, 6)
					End If
				Else
					Return 0
				End If
			End Get
		End Property

		Public ReadOnly Property BetaUpOnly(ByVal pBetaStock As Integer) As Double
			' *************************************************************
			'
			' *************************************************************

			Get
				If (pBetaStock = Me.PertracID) Then
					If Me.ReferenceStDevUpOnly = 0 Then
						Return 0
					Else
						Return Math.Round((Me.CorrelationUpOnly * Me.PertracStDevUpOnly) / Me.ReferenceStDevUpOnly, 6)
					End If
				ElseIf (pBetaStock = Me.ReferencePertracID) Then
					If Me.PertracStDevUpOnly = 0 Then
						Return 0
					Else
						Return Math.Round((Me.CorrelationUpOnly * Me.ReferenceStDevUpOnly) / Me.PertracStDevUpOnly, 6)
					End If
				Else
					Return 0
				End If

			End Get
		End Property

		Public ReadOnly Property BetaDownOnly(ByVal pBetaStock As Integer) As Double
			' *************************************************************
			'
			' *************************************************************

			Get
				If (pBetaStock = Me.PertracID) Then
					If Me.ReferenceStDevDownOnly = 0 Then
						Return 0
					Else
						Return Math.Round((Me.CorrelationDownOnly * Me.PertracStDevDownOnly) / Me.ReferenceStDevDownOnly, 6)
					End If
				ElseIf (pBetaStock = Me.ReferencePertracID) Then
					If Me.PertracStDevDownOnly = 0 Then
						Return 0
					Else
						Return Math.Round((Me.CorrelationDownOnly * Me.ReferenceStDevDownOnly) / Me.PertracStDevDownOnly, 6)
					End If
				Else
					Return 0
				End If

			End Get
		End Property

		Public ReadOnly Property Alpha(ByVal pAlphaStock As Integer) As Double
			' *************************************************************
			'
			' *************************************************************

			Get
				Dim R_Alpha As Double = 0

				If Me.ActualRollingPeriod <= 0 Then
					Return 0
				End If

				If (pAlphaStock = Me.PertracID) Then

					R_Alpha = (Me.PertracCompoundReturn - (Me.Beta(pAlphaStock) * Me.ReferenceCompoundReturn))

				ElseIf (pAlphaStock = Me.ReferencePertracID) Then

					R_Alpha = (Me.ReferenceCompoundReturn - (Me.Beta(pAlphaStock) * Me.PertracCompoundReturn))

				Else
					Return 0
				End If

				If (R_Alpha < 0) Then
					R_Alpha = -(((1.0 + Math.Abs(R_Alpha)) ^ (Me.AnnualPeriodCount / Me.ActualRollingPeriod)) - 1)
				Else
					R_Alpha = ((1.0 + R_Alpha) ^ (Me.AnnualPeriodCount / Me.ActualRollingPeriod)) - 1
				End If

				Return R_Alpha

			End Get
		End Property

		Public ReadOnly Property APR(ByVal pAlphaStock As Integer) As Double
			' *************************************************************
			'
			' *************************************************************

			Get
				If (pAlphaStock = Me.PertracID) Then
					Return Me.PertracAPR
				ElseIf (pAlphaStock = Me.ReferencePertracID) Then
					Return Me.ReferenceAPR
				Else
					Return 0
				End If

			End Get
		End Property

		Public ReadOnly Property Volatility(ByVal pAlphaStock As Integer) As Double
			' *************************************************************
			'
			' *************************************************************

			Get
				If (pAlphaStock = Me.PertracID) Then
					Return Me.PertracStDev * Me.Sqrt12
				ElseIf (pAlphaStock = Me.ReferencePertracID) Then
					Return Me.ReferenceStDev * Me.Sqrt12
				Else
					Return 0
				End If

			End Get
		End Property

		Public ReadOnly Property StandardError(ByVal pErrorStock As Integer) As Double
			' *************************************************************
			'
			' *************************************************************

			Get
				If (pErrorStock = Me.PertracID) Then
					If (Me.PertracStDev = 0) OrElse (Me.ActualRollingPeriod <= 0) Then
						Return 0
					Else
						Return Math.Round((Me.PertracStDev * Me.Sqrt12) / Math.Sqrt(Me.ActualRollingPeriod), 8)
					End If
				ElseIf (pErrorStock = Me.ReferencePertracID) Then
					If (Me.ReferenceStDev = 0) OrElse (Me.ActualRollingPeriod <= 0) Then
						Return 0
					Else
						Return Math.Round((Me.ReferenceStDev * Me.Sqrt12) / Math.Sqrt(Me.ActualRollingPeriod), 8)
					End If
				Else
					Return 0
				End If
			End Get
		End Property

	End Class

	Private Class ComparisonStatisticsCacheClass
		' ************************************************************************
		' This class is designed to encapsulate and control the Cache of Comparison
		' objects. 
		' The cache operates using a ULLong key and in order to maintain a unique key 
		' structure it has been necessary to normalise the Instrument IDs.
		' This process is designed to be transparent to the users of this object.
		'
		' The BackFillVol flag is integrated into the ReferenceBackfillID
		'
		' ************************************************************************
		' *********************************************************************************
		' Note : Comparison Stats Class does not differentiate based on pStatsDatePeriod, if the
		' required value does not match the stored instance, then it is simply re-calculated.
		'
		' If this becomes a performance issue, then the GetComparisonObject() function and the 
		' Cache Key function will need to be updated.
		'
		' *********************************************************************************

		Public Const UInt_Bit31Mask As UInteger = CUInt(2 ^ 30)
		Public Const UInt_Bit32Mask As UInteger = CUInt(2 ^ 31)	' Used to indicate Matched Volatility in backfill series.

		Private StatsObject As StatFunctions
		Private ComparisonCache As Dictionary(Of ULong, ComparisonStatsClass)
		Private PrincipalIDDictionary As Dictionary(Of UInt32, UInt16)
		Private PrincipalBackfillIDDictionary As Dictionary(Of UInt32, UInt16)
		Private ReferenceIDDictionary As Dictionary(Of UInt32, UInt16)
		Private ReferenceBackfillIDDictionary As Dictionary(Of UInt32, UInt16)
		Private CacheLockObject As Object

		Private Const DEFAULT_MaxComparisonStatCacheSize As Integer = 10000
		Private _MaxComparisonStatCacheSize As Integer

		Private Class ComparisonStatsCacheComparer
			Implements IComparer

			Public Function Compare(ByVal x As Object, ByVal y As Object) As Integer Implements System.Collections.IComparer.Compare
				Dim X_CacheItem As ComparisonStatsClass
				Dim Y_CacheItem As ComparisonStatsClass

				Try
					If (TypeOf x Is ComparisonStatsClass) Then
						X_CacheItem = CType(x, ComparisonStatsClass)
					Else
						Return 0
					End If

					If (TypeOf y Is ComparisonStatsClass) Then
						Y_CacheItem = CType(y, ComparisonStatsClass)
					Else
						Return 0
					End If

					Return X_CacheItem.LastAccessed.CompareTo(Y_CacheItem.LastAccessed)
				Catch ex As Exception

					Return 0
				End Try

			End Function
		End Class

		<StructLayout(LayoutKind.Explicit)> Private Structure KeySplitter
			<FieldOffset(0)> Public CacheKeyValue As ULong
			<FieldOffset(0)> Public ReferenceBackfillID As UInt16
			<FieldOffset(2)> Public PrincipalBackFillID As UInt16
			<FieldOffset(4)> Public ReferenceID As UInt16
			<FieldOffset(6)> Public PrincipalID As UInt16
		End Structure

		Private Sub New()
			' *****************************************************************************
			'
			'
			' *****************************************************************************

			Try

				CacheLockObject = New Object
				ComparisonCache = New Dictionary(Of ULong, ComparisonStatsClass)
				PrincipalIDDictionary = New Dictionary(Of UInt32, UInt16)
				PrincipalBackfillIDDictionary = New Dictionary(Of UInt32, UInt16)
				ReferenceIDDictionary = New Dictionary(Of UInt32, UInt16)
				ReferenceBackfillIDDictionary = New Dictionary(Of UInt32, UInt16)

				_MaxComparisonStatCacheSize = DEFAULT_MaxComparisonStatCacheSize

			Catch ex As Exception
			End Try

		End Sub

		Public Sub New(ByRef pStatsObject As StatFunctions)
			' *****************************************************************************
			'
			'
			' *****************************************************************************

			Me.New()
			StatsObject = pStatsObject

		End Sub

		Public Sub New(ByRef pStatsObject As StatFunctions, ByVal pMaxCacheSize As Integer)
			' *****************************************************************************
			'
			'
			' *****************************************************************************

			Me.New()
			StatsObject = pStatsObject
			_MaxComparisonStatCacheSize = pMaxCacheSize

		End Sub

		Private Function GetCompCacheKey(ByRef pStatsClass As ComparisonStatsClass) As ULong
			' ************************************************************************
			'
			' ************************************************************************

			Try
				Return GetCompCacheKey(CUInt(pStatsClass.PertracID And Ulong_32BitMask), CUInt(pStatsClass.ReferencePertracID And Ulong_32BitMask), CUInt((pStatsClass.PertracID >> 32) And Ulong_32BitMask), CUInt((pStatsClass.ReferencePertracID >> 32) And Ulong_32BitMask), pStatsClass.MatchBackfillVol)
			Catch ex As Exception
				Return 0UL
			End Try

		End Function

		Private Function GetCompCacheKey(ByVal pPrincipalInstrumentID As UInteger, ByVal pReferenceInstrumentID As UInteger, ByVal pBackfillInstrumentID As UInteger, ByVal pReferenceBackfillInstrumentID As UInteger, ByVal MatchBackfillVolatility As Boolean) As ULong
			' ************************************************************************
			'
			' ************************************************************************

			Dim KeyStruc As New KeySplitter
			Dim CombinedReferenceBackfillID As UInteger

			Try
				SyncLock CacheLockObject

					CombinedReferenceBackfillID = pReferenceBackfillInstrumentID
					If (MatchBackfillVolatility) Then
						CombinedReferenceBackfillID = CombinedReferenceBackfillID Or UInt_Bit32Mask
					End If

					' Check Normalisation dictionary values
					Try
						Dim ClearDownRequired As Boolean = False

						If (Not PrincipalIDDictionary.ContainsKey(pPrincipalInstrumentID)) Then
							If (PrincipalIDDictionary.Count >= UShort.MaxValue) Then
								ClearDownRequired = True
							Else
								PrincipalIDDictionary.Add(pPrincipalInstrumentID, CUShort(PrincipalIDDictionary.Count + 1))
							End If
						End If

						If (Not ReferenceIDDictionary.ContainsKey(pReferenceInstrumentID)) Then
							If (ReferenceIDDictionary.Count >= UShort.MaxValue) Then
								ClearDownRequired = True
							Else
								ReferenceIDDictionary.Add(pReferenceInstrumentID, CUShort(ReferenceIDDictionary.Count + 1))
							End If
						End If

						If (Not PrincipalBackfillIDDictionary.ContainsKey(pBackfillInstrumentID)) Then
							If (PrincipalBackfillIDDictionary.Count >= UShort.MaxValue) Then
								ClearDownRequired = True
							Else
								PrincipalBackfillIDDictionary.Add(pBackfillInstrumentID, CUShort(PrincipalBackfillIDDictionary.Count + 1))
							End If
						End If

						If (Not ReferenceBackfillIDDictionary.ContainsKey(CombinedReferenceBackfillID)) Then
							If (ReferenceBackfillIDDictionary.Count >= UShort.MaxValue) Then
								ClearDownRequired = True
							Else
								ReferenceBackfillIDDictionary.Add(CombinedReferenceBackfillID, CUShort(ReferenceBackfillIDDictionary.Count + 1))
							End If
						End If

						If (ClearDownRequired) Then
							ComparisonCache.Clear()
							PrincipalIDDictionary.Clear()
							PrincipalBackfillIDDictionary.Clear()
							ReferenceIDDictionary.Clear()
							ReferenceBackfillIDDictionary.Clear()

							' Iterate to return Newly created Cache Key.

							Return GetCompCacheKey(pPrincipalInstrumentID, pReferenceInstrumentID, pBackfillInstrumentID, pReferenceBackfillInstrumentID, MatchBackfillVolatility)
						End If

					Catch ex As Exception
						Return 0UL
					End Try

					KeyStruc.PrincipalID = PrincipalIDDictionary(pPrincipalInstrumentID)
					KeyStruc.ReferenceID = ReferenceIDDictionary(pReferenceInstrumentID)
					KeyStruc.PrincipalBackFillID = PrincipalBackfillIDDictionary(pBackfillInstrumentID)
					KeyStruc.ReferenceBackfillID = ReferenceBackfillIDDictionary(CombinedReferenceBackfillID)

				End SyncLock

				Return KeyStruc.CacheKeyValue

			Catch ex As Exception
				Return 0UL
			End Try

		End Function

		Private Function GetComparisonObject(ByVal pPrincipalInstrumentID As ULong, ByVal pReferenceInstrumentID As ULong, ByVal MatchBackfillVolatility As Boolean) As ComparisonStatsClass
			' ************************************************************************
			'
			' ************************************************************************

			Try

				Return GetComparisonObject(CUInt(pPrincipalInstrumentID And Ulong_32BitMask), CUInt(pReferenceInstrumentID And Ulong_32BitMask), CUInt((pPrincipalInstrumentID >> 32) And Ulong_32BitMask), CUInt((pReferenceInstrumentID >> 32) And Ulong_32BitMask), MatchBackfillVolatility)

			Catch ex As Exception
				Return Nothing
			End Try

			Return Nothing

		End Function

		Private Function GetComparisonObject(ByVal pPrincipalInstrumentID As Integer, ByVal pReferenceInstrumentID As Integer, ByVal pBackfillInstrumentID As Integer, ByVal pReferenceBackfillInstrumentID As Integer, ByVal MatchBackfillVolatility As Boolean) As ComparisonStatsClass
			' ************************************************************************
			'
			' ************************************************************************

			Try
				Return GetComparisonObject(CUInt(pPrincipalInstrumentID), CUInt(pReferenceInstrumentID), CUInt(pBackfillInstrumentID), CUInt(pReferenceBackfillInstrumentID), MatchBackfillVolatility)
			Catch ex As Exception
				Return Nothing
			End Try

		End Function

		Private Function GetComparisonObject(ByVal pPrincipalInstrumentID As UInteger, ByVal pReferenceInstrumentID As UInteger, ByVal pBackfillInstrumentID As UInteger, ByVal pReferenceBackfillInstrumentID As UInteger, ByVal MatchBackfillVolatility As Boolean) As ComparisonStatsClass
			' ************************************************************************
			'
			' ************************************************************************

			Try
				SyncLock CacheLockObject

					Dim CacheKey As ULong
					CacheKey = GetCompCacheKey(pPrincipalInstrumentID, pReferenceInstrumentID, pBackfillInstrumentID, pReferenceBackfillInstrumentID, MatchBackfillVolatility)

					If (ComparisonCache.ContainsKey(CacheKey)) Then
						Return ComparisonCache(CacheKey)
					Else
						Return Nothing
					End If

				End SyncLock
			Catch ex As Exception
				Return Nothing
			End Try

		End Function

		Private Function PopulateComparisonStatsItem(ByRef pStatsItem As ComparisonStatsClass, ByVal pStatsDatePeriod As DealingPeriod, ByVal pPertracID As ULong, ByVal pReferencePertracID As ULong, ByVal MatchBackfillVol As Boolean, ByVal pNoLongerUsed As ULong, ByVal pDateFrom As Date, ByVal pDateTo As Date, ByVal pLamda As Double, ByVal pRollingPeriod As Integer, ByVal pGroupSampleSize As Integer, ByVal pPertracScalingFactor As Double, ByVal pReferenceScalingFactor As Double, ByRef eMessage As String) As ComparisonStatsClass
			' *********************************************************************************
			'
			'
			' StdDeviations are Population StdDevs : Divisor is 'N' rather than 'N-1'.
			' *********************************************************************************
			Dim RollingPeriod As Integer = pRollingPeriod

			Dim RVal As ComparisonStatsClass
			If (pStatsItem Is Nothing) Then
				RVal = New ComparisonStatsClass(pStatsDatePeriod)
				RVal.PertracID = pPertracID
				RVal.ReferencePertracID = pReferencePertracID
				RVal.MatchBackfillVol = MatchBackfillVol
			Else
				RVal = pStatsItem
			End If

			' Clear existing Data
			' ByVal pDateFrom As Date, ByVal pDateTo As Date, ByVal pLamda As Double, ByVal RollingPeriod As Integer

			RVal.StatsFromDate = pDateFrom
			RVal.StatsToDate = pDateTo
			RVal.StatsLamda = pLamda
			RVal.StatsPeriodCount = RollingPeriod
			RVal.StatsPeriodType = pStatsDatePeriod
			RVal.PertracScalingFactor = pPertracScalingFactor
			RVal.ReferenceScalingFactor = pReferenceScalingFactor

			RVal.ClearStats()

			' OK, Here We Go...

			' First Get Date and Return Series for the two instruments.

			Dim Pertrac_DateSeries() As Date
			Dim Pertrac_ReturnSeries() As Double
			Dim Reference_DateSeries() As Date
			Dim Reference_ReturnSeries() As Double
			Dim ReferenceSeriesOK As Boolean = True

			Pertrac_DateSeries = StatsObject.DateSeries(pStatsDatePeriod, 0, pPertracID, MatchBackfillVol, RollingPeriod, pLamda, pGroupSampleSize, False, Renaissance_BaseDate, pDateTo, eMessage)	 ' Remember First return will not be real.
			Pertrac_ReturnSeries = StatsObject.ReturnSeries(pStatsDatePeriod, 0, pPertracID, MatchBackfillVol, RollingPeriod, pLamda, pGroupSampleSize, False, Renaissance_BaseDate, pDateTo, pPertracScalingFactor, eMessage)
			Reference_DateSeries = StatsObject.DateSeries(pStatsDatePeriod, 0, pReferencePertracID, MatchBackfillVol, RollingPeriod, pLamda, pGroupSampleSize, False, Renaissance_BaseDate, pDateTo, eMessage)
			Reference_ReturnSeries = StatsObject.ReturnSeries(pStatsDatePeriod, 0, pReferencePertracID, MatchBackfillVol, RollingPeriod, pLamda, pGroupSampleSize, False, Renaissance_BaseDate, pDateTo, pReferenceScalingFactor, eMessage)

			' Exit if there is no pertrac data

			If (Pertrac_DateSeries Is Nothing) OrElse (Pertrac_DateSeries.Length <= 0) OrElse (Pertrac_ReturnSeries Is Nothing) OrElse (Pertrac_ReturnSeries.Length <= 0) Then
				Return RVal
			End If
			If (Reference_DateSeries Is Nothing) OrElse (Reference_DateSeries.Length < MIN_Periods_For_Stats) Then
				ReferenceSeriesOK = False
			End If

			' Calculate Data Overlap.

			Dim Pertrac_Start_Index As Integer = 0
			Dim Reference_Start_Index As Integer = 0
			Dim OverlapLength As Integer = Pertrac_DateSeries.Length

			If (ReferenceSeriesOK) Then
				Try

					If (Pertrac_DateSeries(0) = Reference_DateSeries(0)) Then
						Pertrac_Start_Index = 1
						Reference_Start_Index = 1

						If (Pertrac_DateSeries.Length <= Reference_DateSeries.Length) Then
							OverlapLength = (Pertrac_DateSeries.Length - Pertrac_Start_Index)
						Else
							OverlapLength = (Reference_DateSeries.Length - Reference_Start_Index)
						End If

					ElseIf (Pertrac_DateSeries(0) < Reference_DateSeries(0)) Then

						Pertrac_Start_Index = GetPriceIndex(pStatsDatePeriod, Pertrac_DateSeries(0), Reference_DateSeries(1))
						Reference_Start_Index = 1

						OverlapLength = (Pertrac_DateSeries.Length - Pertrac_Start_Index)
						If (OverlapLength > (Reference_DateSeries.Length - Reference_Start_Index)) Then
							OverlapLength = (Reference_DateSeries.Length - Reference_Start_Index)
						End If

					ElseIf (Pertrac_DateSeries(0) > Reference_DateSeries(0)) Then

						Reference_Start_Index = GetPriceIndex(pStatsDatePeriod, Reference_DateSeries(0), Pertrac_DateSeries(1))
						Pertrac_Start_Index = 1

						OverlapLength = (Pertrac_DateSeries.Length - Pertrac_Start_Index)
						If (OverlapLength > (Reference_DateSeries.Length - Reference_Start_Index)) Then
							OverlapLength = (Reference_DateSeries.Length - Reference_Start_Index)
						End If

					End If

				Catch ex As Exception
				End Try

				' Adjust for Given Start Date (pDateFrom)

				If (Pertrac_Start_Index < Pertrac_DateSeries.Length) AndAlso (pDateFrom > Pertrac_DateSeries(Pertrac_Start_Index)) Then
					Dim PeriodShift As Integer

					PeriodShift = GetPeriodCount(pStatsDatePeriod, Pertrac_DateSeries(Pertrac_Start_Index), pDateFrom) - 1

					If (PeriodShift > 0) Then
						Pertrac_Start_Index += PeriodShift
						Reference_Start_Index += PeriodShift
						OverlapLength -= PeriodShift
					End If
				End If

				' Check there is enough overlap to make it worthwhile
				If OverlapLength < MIN_Periods_For_Stats Then
					ReferenceSeriesOK = False
				End If
			End If

			If (OverlapLength <= 0) Then
				Return RVal
			End If

			' Adjust Rolling period down if it is longer than the available data 
			If (OverlapLength < RollingPeriod) Then
				RollingPeriod = OverlapLength
			End If
			RVal.ActualRollingPeriod = RollingPeriod


			' Lamda Flag

			Dim LamdaFlag As Boolean = True
			If (pLamda = 1) Then
				LamdaFlag = False
			End If

			' Set Exponent Multiplier Array

			Dim CurrentCounter As Integer
			Dim ExponentMultiplier(RollingPeriod - 1) As Double
			Dim SumExponentWeight As Double
			Dim thisExponentWeight As Double

			Try
				ExponentMultiplier(0) = 1
				SumExponentWeight = 1
				For CurrentCounter = 1 To (RollingPeriod - 1)
					If (LamdaFlag) Then
						ExponentMultiplier(CurrentCounter) = ExponentMultiplier(CurrentCounter - 1) * pLamda
						SumExponentWeight += ExponentMultiplier(CurrentCounter)
					Else
						ExponentMultiplier(CurrentCounter) = 1
						SumExponentWeight += 1
					End If
				Next
			Catch ex As Exception
			End Try

			' Get Selected Data

			Dim ReturnCounter As Integer
			Dim X_EndIndex As Integer = (Reference_Start_Index + OverlapLength - 1)
			Dim X_StartIndex As Integer = (X_EndIndex - RollingPeriod) + 1
			Dim Y_EndIndex As Integer = (Pertrac_Start_Index + OverlapLength - 1)
			Dim Y_StartIndex As Integer = (Y_EndIndex - RollingPeriod) + 1
			Dim XY_Index_Differential As Integer = X_StartIndex - Y_StartIndex

			Dim CompoundReturn As Double
			Dim Compound_X As Double = 0
			Dim Sum_X As Double = 0
			Dim Avg_X As Double = 0
			Dim Sum_X2 As Double = 0
			Dim Compound_Y As Double = 0
			Dim Sum_Y As Double = 0
			Dim Avg_Y As Double = 0
			Dim Sum_Y2 As Double = 0
			Dim Sum_XY As Double = 0
			Dim X_Value As Double = 0
			Dim Y_Value As Double = 0

			Dim StDev_X As Double
			Dim StDev_Y As Double
			Dim CoVariance_XY As Double
			Dim Correlation_XY As Double

			' 1) Non Contingent Items
			'		RVal.APR = 0
			'		RVal.Volatility = 0

			Try
				Sum_Y = 0
				CompoundReturn = 1.0

				For ReturnCounter = Y_StartIndex To Y_EndIndex
					Y_Value = Pertrac_ReturnSeries(ReturnCounter)

					Sum_Y += Y_Value
					CompoundReturn *= (1 + Y_Value)
				Next
				Avg_Y = Sum_Y / RollingPeriod

				For ReturnCounter = Y_StartIndex To Y_EndIndex
					Y_Value = Pertrac_ReturnSeries(ReturnCounter)

					Sum_Y2 += ExponentMultiplier(Y_EndIndex - ReturnCounter) * ((Y_Value - Avg_Y) ^ 2)
				Next
			Catch ex As Exception
			End Try

			If (SumExponentWeight = 0) Then
				StDev_Y = 0
				RVal.PertracStDev = 0
			Else
				StDev_Y = Math.Sqrt(Sum_Y2 / SumExponentWeight)
				RVal.PertracStDev = StDev_Y
			End If
			Compound_Y = CompoundReturn - 1

			RVal.PertracAPR = ((CompoundReturn) ^ (RVal.AnnualPeriodCount / RollingPeriod)) - 1
			RVal.PertracCompoundReturn = CompoundReturn - 1

			' 2) Not Up/Down
			'		RVal.Alpha = 0
			'		RVal.Beta = 0
			'		RVal.Correlation = 0

			RVal.ReferenceStDev = 0
			RVal.ReferenceAPR = 0
			RVal.ReferenceCompoundReturn = 0

			If (ReferenceSeriesOK) Then
				CompoundReturn = 1.0

				For ReturnCounter = X_StartIndex To X_EndIndex
					X_Value = Reference_ReturnSeries(ReturnCounter)

					Sum_X += X_Value
					CompoundReturn *= (1 + X_Value)
				Next
				Avg_X = Sum_X / RollingPeriod

				For ReturnCounter = X_StartIndex To X_EndIndex
					X_Value = Reference_ReturnSeries(ReturnCounter)
					Y_Value = Pertrac_ReturnSeries(ReturnCounter - XY_Index_Differential)

					thisExponentWeight = ExponentMultiplier(X_EndIndex - ReturnCounter)

					Sum_X2 += thisExponentWeight * ((X_Value - Avg_X) ^ 2)
					Sum_XY += thisExponentWeight * ((X_Value - Avg_X) * (Y_Value - Avg_Y))
				Next
				If (SumExponentWeight = 0) Then
					StDev_X = 0
					CoVariance_XY = 0
					Correlation_XY = 0
				Else
					StDev_X = Math.Sqrt(Sum_X2 / SumExponentWeight)
					CoVariance_XY = (Sum_XY / SumExponentWeight)

					If (StDev_X * StDev_Y) = 0 Then
						Correlation_XY = 0
					Else
						Correlation_XY = CoVariance_XY / (StDev_X * StDev_Y)
					End If
				End If

				RVal.ReferenceStDev = StDev_X
				RVal.ReferenceAPR = ((CompoundReturn) ^ (RVal.AnnualPeriodCount / RollingPeriod)) - 1
				RVal.ReferenceCompoundReturn = CompoundReturn - 1

				Compound_X = CompoundReturn - 1

				RVal.Covariance = CoVariance_XY
				RVal.Correlation = Correlation_XY

			End If


			' 3) Up
			'		RVal.BetaUpOnly = 0
			'		RVal.CorrelationUpOnly = 0
			Dim Pertrac_PartialReturnSeries(RollingPeriod - 1) As Double
			Dim Reference_PartialReturnSeries(RollingPeriod - 1) As Double

			Sum_X = 0
			Avg_X = 0
			Sum_X2 = 0
			Sum_Y = 0
			Avg_Y = 0
			Sum_Y2 = 0
			Sum_XY = 0

			Try
				If (ReferenceSeriesOK) Then
					For ReturnCounter = 0 To (RollingPeriod - 1)
						X_Value = Reference_ReturnSeries(X_StartIndex + ReturnCounter)
						Y_Value = Pertrac_ReturnSeries(Y_StartIndex + ReturnCounter)

						If X_Value > 0 Then
							Reference_PartialReturnSeries(ReturnCounter) = X_Value
							Pertrac_PartialReturnSeries(ReturnCounter) = Y_Value
							Sum_X += X_Value
						Else
							Reference_PartialReturnSeries(ReturnCounter) = 0
							Pertrac_PartialReturnSeries(ReturnCounter) = 0
						End If

					Next

					Avg_X = Sum_X / RollingPeriod
					Avg_Y = Sum_Y / RollingPeriod

					For ReturnCounter = 0 To (RollingPeriod - 1)
						X_Value = Reference_PartialReturnSeries(ReturnCounter)
						Y_Value = Pertrac_PartialReturnSeries(ReturnCounter)

						thisExponentWeight = ExponentMultiplier((RollingPeriod - 1) - ReturnCounter)

						Sum_X2 += thisExponentWeight * ((X_Value - Avg_X) ^ 2)
						Sum_Y2 += thisExponentWeight * ((Y_Value - Avg_Y) ^ 2)

						Sum_XY += thisExponentWeight * ((X_Value - Avg_X) * (Y_Value - Avg_Y))

					Next

					If (SumExponentWeight > 0) Then
						StDev_X = Math.Sqrt(Sum_X2 / SumExponentWeight)
						StDev_Y = Math.Sqrt(Sum_Y2 / SumExponentWeight)
						CoVariance_XY = (Sum_XY / SumExponentWeight)

						If ((StDev_X * StDev_Y) = 0) Then
							Correlation_XY = 0
						Else
							Correlation_XY = CoVariance_XY / (StDev_X * StDev_Y)
						End If

						RVal.CorrelationUpOnly = Correlation_XY

						RVal.ReferenceStDevUpOnly = StDev_X
						RVal.PertracStDevUpOnly = StDev_Y

					End If

				End If

			Catch ex As Exception
			End Try


			' 4) Down
			'		RVal.BetaDownOnly = 0
			'		RVal.CorrelationDownOnly = 0

			Sum_X = 0
			Avg_X = 0
			Sum_X2 = 0
			Sum_Y = 0
			Avg_Y = 0
			Sum_Y2 = 0
			Sum_XY = 0

			Try
				If (ReferenceSeriesOK) Then
					For ReturnCounter = 0 To (RollingPeriod - 1)
						X_Value = Reference_ReturnSeries(X_StartIndex + ReturnCounter)
						Y_Value = Pertrac_ReturnSeries(Y_StartIndex + ReturnCounter)

						If X_Value < 0 Then
							Reference_PartialReturnSeries(ReturnCounter) = X_Value
							Pertrac_PartialReturnSeries(ReturnCounter) = Y_Value
							Sum_X += X_Value
						Else
							Reference_PartialReturnSeries(ReturnCounter) = 0
							Pertrac_PartialReturnSeries(ReturnCounter) = 0
						End If

					Next

					Avg_X = Sum_X / RollingPeriod
					Avg_Y = Sum_Y / RollingPeriod

					For ReturnCounter = 0 To (RollingPeriod - 1)
						X_Value = Reference_PartialReturnSeries(ReturnCounter)
						Y_Value = Pertrac_PartialReturnSeries(ReturnCounter)

						thisExponentWeight = ExponentMultiplier((RollingPeriod - 1) - ReturnCounter)

						Sum_X2 += thisExponentWeight * ((X_Value - Avg_X) ^ 2)
						Sum_Y2 += thisExponentWeight * ((Y_Value - Avg_Y) ^ 2)

						Sum_XY += thisExponentWeight * ((X_Value - Avg_X) * (Y_Value - Avg_Y))

					Next

					If (SumExponentWeight > 0) Then
						StDev_X = Math.Sqrt(Sum_X2 / SumExponentWeight)
						StDev_Y = Math.Sqrt(Sum_Y2 / SumExponentWeight)
						CoVariance_XY = (Sum_XY / SumExponentWeight)

						If ((StDev_X * StDev_Y) = 0) Then
							Correlation_XY = 0
						Else
							Correlation_XY = CoVariance_XY / (StDev_X * StDev_Y)
						End If

						RVal.CorrelationDownOnly = Correlation_XY
						RVal.ReferenceStDevDownOnly = StDev_X
						RVal.PertracStDevDownOnly = StDev_Y

					End If

				End If

			Catch ex As Exception
			End Try

			Return RVal
		End Function

		Public Function GetComparisonStatsItem(ByVal pStatsDatePeriod As DealingPeriod, ByVal pPrincipalInstrumentID As ULong, ByVal pReferenceInstrumentID As ULong, ByVal MatchBackfillVol As Boolean, ByVal pListPertracID As ULong, ByVal pDateFrom As Date, ByVal pDateTo As Date, ByVal pLamda As Double, ByVal pRollingPeriod As Integer, ByVal pGroupSampleSize As Integer, ByVal pPertracScalingFactor As Double, ByVal pReferenceScalingFactor As Double, ByRef eMessage As String) As ComparisonStatsClass
			' *********************************************************************************
			' Note : Comparison Stats Class does not differentiate based on pStatsDatePeriod, if the
			' required value does not match the stored instance, then it is simply re-calculated.
			'
			' If this becomes a performance issue, then the GetComparisonObject() function and the 
			' Cache Key function will need to be updated.
			'
			' *********************************************************************************

			Dim thisStatsItem As ComparisonStatsClass = Nothing

			Try
				If pListPertracID <> pPrincipalInstrumentID Then
					pListPertracID = pReferenceInstrumentID
				End If
			Catch ex As Exception

			End Try
			Try

				SyncLock CacheLockObject
					If (ComparisonCache.Count > _MaxComparisonStatCacheSize) Then
						Call TrimComparisonStatCache(CInt(_MaxComparisonStatCacheSize / 2))
					End If

					' Retrieve From Cache ?

					thisStatsItem = GetComparisonObject(pPrincipalInstrumentID, pReferenceInstrumentID, MatchBackfillVol)

					If (thisStatsItem Is Nothing) Then
						thisStatsItem = New ComparisonStatsClass(pStatsDatePeriod)
						thisStatsItem.PertracID = pPrincipalInstrumentID
						thisStatsItem.ReferencePertracID = pReferenceInstrumentID
						thisStatsItem.MatchBackfillVol = MatchBackfillVol

						ComparisonCache.Add(GetCompCacheKey(thisStatsItem), thisStatsItem)

					Else

						' Compare ? Update if requiremants have changed.

						If (thisStatsItem.StatsFromDate = pDateFrom) AndAlso _
						 (thisStatsItem.StatsToDate = pDateTo) AndAlso _
						 (thisStatsItem.StatsPeriodCount = pRollingPeriod) AndAlso _
						 (Math.Abs(thisStatsItem.PertracScalingFactor - pPertracScalingFactor) < EPSILON) AndAlso _
						 (Math.Abs(thisStatsItem.ReferenceScalingFactor - pReferenceScalingFactor) < EPSILON) AndAlso _
						 (thisStatsItem.StatsPeriodType = pStatsDatePeriod) AndAlso _
						 (thisStatsItem.StatsLamda = pLamda) Then

							Return thisStatsItem

						End If

					End If
					If (thisStatsItem Is Nothing) Then
						Return Nothing
					End If

					thisStatsItem.LastAccessed = Now()

					' Update

					thisStatsItem = PopulateComparisonStatsItem(thisStatsItem, pStatsDatePeriod, pPrincipalInstrumentID, pReferenceInstrumentID, MatchBackfillVol, pListPertracID, pDateFrom, pDateTo, pLamda, pRollingPeriod, pGroupSampleSize, pPertracScalingFactor, pReferenceScalingFactor, eMessage)

					Return thisStatsItem

				End SyncLock
			Catch ex As Exception
				Return Nothing
			End Try

			Return Nothing

		End Function

		Private Sub TrimComparisonStatCache(ByVal pNewCacheSize As Integer)
			' *****************************************************************************
			' Trim the ComparisonCache down to the required size.
			'
			' *****************************************************************************

			Try
				If (ComparisonCache.Count <= pNewCacheSize) Then
					Exit Sub
				End If
			Catch ex As Exception
			End Try

			Try
				If (pNewCacheSize <= 0) Then
					SyncLock ComparisonCache
						ComparisonCache.Clear()
					End SyncLock

					Exit Sub
				End If
			Catch ex As Exception
			End Try

			SyncLock ComparisonCache
				Try
					Dim ValueArray(ComparisonCache.Count - 1) As ComparisonStatsClass
					Dim ItemCounter As Integer

					ComparisonCache.Values.CopyTo(ValueArray, 0)
					Array.Sort(ValueArray, New ComparisonStatsCacheComparer)

					For ItemCounter = 0 To ((ComparisonCache.Count - pNewCacheSize) - 1)
						ComparisonCache.Remove(GetCompCacheKey(ValueArray(ItemCounter)))
					Next

					Array.Clear(ValueArray, 0, ValueArray.Length)

				Catch ex As Exception
				End Try
			End SyncLock

		End Sub

		Public Sub ClearCache()
			' *********************************************************************************
			'
			'
			' *********************************************************************************

			Try

				SyncLock CacheLockObject

					If (ComparisonCache Is Nothing) OrElse (ComparisonCache.Count <= 0) Then
						Exit Sub
					End If

					ComparisonCache.Clear()

				End SyncLock

			Catch ex As Exception
			End Try

		End Sub

		Public Sub ClearCache(ByVal InstrumentID As String)
			' *********************************************************************************
			'
			'
			' *********************************************************************************

			Try

				SyncLock CacheLockObject

					If (ComparisonCache Is Nothing) OrElse (ComparisonCache.Count <= 0) Then
						Exit Sub
					End If

					If (InstrumentID Is Nothing) OrElse (InstrumentID.Length <= 0) OrElse (Not IsNumeric(InstrumentID)) Then
						ComparisonCache.Clear()
						Exit Sub
					End If

					ClearCache(CUInt(InstrumentID))

				End SyncLock

			Catch ex As Exception

			End Try
		End Sub

		Public Sub ClearCache(ByVal InstrumentID As UInteger)
			' *********************************************************************************
			'
			'
			' *********************************************************************************

			Try

				SyncLock CacheLockObject

					If (ComparisonCache Is Nothing) OrElse (ComparisonCache.Count <= 0) Then
						Exit Sub
					End If

					If (InstrumentID = 0) Then
						ComparisonCache.Clear()
						Exit Sub
					End If

					Dim IDFound As Boolean = False
					Dim MatchingKeys As New ArrayList
					Dim ThisKeySplitter As New KeySplitter

					Dim PrincipalID As UInt16 = 0
					Dim ReferenceID As UInt16 = 0
					Dim PrincipalBackfillID As UInt16 = 0
					Dim ReferenceBackfillID As UInt16 = 0

					' Is the given Instrument ID Used in any of the normalisation caches ?

					If (PrincipalIDDictionary.ContainsKey(InstrumentID)) Then
						PrincipalID = PrincipalIDDictionary(InstrumentID)
						IDFound = True
					End If

					If (ReferenceIDDictionary.ContainsKey(InstrumentID)) Then
						ReferenceID = ReferenceIDDictionary(InstrumentID)
						IDFound = True
					End If

					If (PrincipalBackfillIDDictionary.ContainsKey(InstrumentID)) Then
						PrincipalBackfillID = PrincipalBackfillIDDictionary(InstrumentID)
						IDFound = True
					End If

					If (ReferenceBackfillIDDictionary.ContainsKey(InstrumentID)) Then
						ReferenceBackfillID = ReferenceBackfillIDDictionary(InstrumentID)
						IDFound = True
					ElseIf (ReferenceBackfillIDDictionary.ContainsKey(InstrumentID Or UInt_Bit32Mask)) Then
						ReferenceBackfillID = ReferenceBackfillIDDictionary(InstrumentID Or UInt_Bit32Mask)
						IDFound = True
					End If

					' Instrument appears to be used, check each cache entry for a match.

					If IDFound Then
						Dim ExistingKeys(ComparisonCache.Count - 1) As ULong
						Dim KeyCounter As Integer


						ComparisonCache.Keys.CopyTo(ExistingKeys, 0)

						For KeyCounter = 0 To (ExistingKeys.Count - 1)
							ThisKeySplitter.CacheKeyValue = ExistingKeys(KeyCounter)

							If (ThisKeySplitter.PrincipalID = PrincipalID) OrElse _
							(ThisKeySplitter.ReferenceID = ReferenceID) OrElse _
							(ThisKeySplitter.PrincipalBackFillID = PrincipalBackfillID) OrElse _
							(ThisKeySplitter.ReferenceBackfillID = ReferenceBackfillID) Then

								Try

									ComparisonCache.Remove(ExistingKeys(KeyCounter))

								Catch ex As Exception
								End Try

							End If

						Next

					End If

				End SyncLock

			Catch ex As Exception
			End Try

		End Sub

	End Class

	Public Function GetComparisonStatsItem(ByVal pStatsDatePeriod As RenaissanceGlobals.DealingPeriod, ByVal pPertracID As ULong, ByVal pReferencePertracID As ULong, ByVal MatchBackfillVol As Boolean, ByVal pListPertracID As ULong, ByVal pDateFrom As Date, ByVal pDateTo As Date, ByVal pLamda As Double, ByVal pRollingPeriod As Integer, ByVal pGroupSampleSize As Integer, ByVal pPertracScalingFactor As Double, ByVal pReferenceScalingFactor As Double, ByRef eMessage As String) As ComparisonStatsClass
		' *********************************************************************************
		'
		'
		' *********************************************************************************

		Try
			Return _ComparisonStatisticsCache.GetComparisonStatsItem(pStatsDatePeriod, pPertracID, pReferencePertracID, MatchBackfillVol, pListPertracID, pDateFrom, pDateTo, pLamda, pRollingPeriod, pGroupSampleSize, pPertracScalingFactor, pReferenceScalingFactor, eMessage)
		Catch ex As Exception
			Return Nothing
		End Try

		Return Nothing

	End Function

#End Region

#Region " Simple Stats Array and DrawDown stuff"

	Public Class SeriesStatsClass
		' **************************************************************************************
		'
		'
		'
		' **************************************************************************************

		Public Class DatedStatsClass
			Public M12 As Double
			Public M24 As Double
			Public M36 As Double
			Public M60 As Double
			Public ITD As Double
			Public YTD As Double
			Public CTD As Double ' Custom To Date

			Public Sub New()
				M12 = 0
				M24 = 0
				M36 = 0
				M60 = 0
				ITD = 0
				YTD = 0
				CTD = 0
			End Sub
		End Class

		Public Class PeriodReturns
			Public ReferenceDate As Date

			Public P1 As Double
			Public P2 As Double
			Public P3 As Double
			Public P4 As Double
			Public P5 As Double
			Public P6 As Double

			Public Sub New()
				ReferenceDate = #1/1/1900#

				Me.P1 = 0
				Me.P2 = 0
				Me.P3 = 0
				Me.P4 = 0
				Me.P5 = 0
				Me.P6 = 0
			End Sub

		End Class

		Public StatsDatePeriod As DealingPeriod
		Public Scalingfactor As Double
		Public SimpleReturn As DatedStatsClass
		Public AnnualisedReturn As DatedStatsClass
		Public Volatility As DatedStatsClass
		Public Volatility_Series As DatedStatsClass
		Public StandardDeviation As DatedStatsClass
		Public StandardDeviation_Series As DatedStatsClass
		Public PercentPositive As DatedStatsClass
		Public SharpeRatio As DatedStatsClass
		Public BestPeriodReturn As Double
		Public BestPeriodDate As Date
		Public WorstPeriodReturn As Double
		Public WorstPeriodDate As Date
		Public PeriodCount As Integer

		Public MonthReturns As PeriodReturns
		Public YearReturns As PeriodReturns

		Public FirstReturnDate As Date
		Public CustomStartDate As Date
		Public CustomEndDate As Date

		Private Sub New()

		End Sub

		Public Sub New(ByVal pStatsDatePeriod As DealingPeriod)
			' **********************************************************************************************
			'
			'
			' **********************************************************************************************

			StatsDatePeriod = pStatsDatePeriod
			Scalingfactor = 1.0#
			SimpleReturn = New DatedStatsClass
			AnnualisedReturn = New DatedStatsClass
			Volatility = New DatedStatsClass ' Population Vol
			Volatility_Series = New DatedStatsClass	' Series Vol (Divisor is 'N-1')
			StandardDeviation = New DatedStatsClass	' Population StDev
			StandardDeviation_Series = New DatedStatsClass ' Series StDev (Divisor is 'N-1')
			PercentPositive = New DatedStatsClass
			SharpeRatio = New DatedStatsClass
			MonthReturns = New PeriodReturns
			YearReturns = New PeriodReturns
			PeriodCount = 0
			BestPeriodReturn = 0.0#
			WorstPeriodReturn = 0.0#
			BestPeriodDate = RenaissanceGlobals.Globals.KNOWLEDGEDATE_NOW
			WorstPeriodDate = RenaissanceGlobals.Globals.KNOWLEDGEDATE_NOW

			FirstReturnDate = Renaissance_BaseDate
			CustomStartDate = Renaissance_BaseDate

		End Sub

		Public ReadOnly Property StandardError() As DatedStatsClass
			' **********************************************************************************************
			'
			'
			' **********************************************************************************************

			Get
				Dim RVal As New DatedStatsClass
				Dim SqrtVal As Double = Math.Sqrt(Me.PeriodCount)

				RVal.ITD = StandardDeviation.ITD / SqrtVal
				RVal.M12 = StandardDeviation.M12 / SqrtVal
				RVal.M36 = StandardDeviation.M36 / SqrtVal
				RVal.M60 = StandardDeviation.M60 / SqrtVal
				RVal.CTD = StandardDeviation.CTD / SqrtVal
				RVal.YTD = StandardDeviation_Series.YTD / SqrtVal

				Return RVal
			End Get
		End Property

		Public ReadOnly Property StandardError_Series() As DatedStatsClass
			' **********************************************************************************************
			'
			'
			' **********************************************************************************************

			Get

				Dim RVal As New DatedStatsClass
				Dim SqrtVal As Double

				If (Me.PeriodCount = 0) Then
					SqrtVal = 1.0#
				Else
					SqrtVal = Math.Sqrt(Math.Abs(Me.PeriodCount))
				End If

				RVal.ITD = StandardDeviation_Series.ITD / SqrtVal
				RVal.M12 = StandardDeviation_Series.M12 / SqrtVal
				RVal.M36 = StandardDeviation_Series.M36 / SqrtVal
				RVal.M60 = StandardDeviation_Series.M60 / SqrtVal
				RVal.CTD = StandardDeviation_Series.CTD / SqrtVal
				RVal.YTD = StandardDeviation_Series.YTD / SqrtVal

				Return RVal
			End Get
		End Property

	End Class

	Public Function GetSimpleStats(ByVal StatsDatePeriod As DealingPeriod, ByVal pPertracID As ULong, ByVal MatchBackfillVol As Boolean, ByVal pRiskFreeRate As Double, ByVal pScalingFactor As Double, ByRef eMessage As String) As SeriesStatsClass
		' **************************************************************************************
		'
		' **************************************************************************************

		Return GetSimpleStats(StatsDatePeriod, pPertracID, MatchBackfillVol, Renaissance_BaseDate, AddPeriodToDate(StatsDatePeriod, Now.Date, -1), Renaissance_EndDate_Data, AddPeriodToDate(StatsDatePeriod, Now.Date, -1), pRiskFreeRate, pScalingFactor, -1, eMessage)

	End Function

	Public Function GetSimpleStats(ByVal StatsDatePeriod As DealingPeriod, ByVal pPertracID As ULong, ByVal MatchBackfillVol As Boolean, ByVal pReferenceDate As Date, ByVal pRiskFreeRate As Double, ByVal pScalingFactor As Double, ByRef eMessage As String) As SeriesStatsClass
		' **************************************************************************************
		'
		' **************************************************************************************

		Return GetSimpleStats(StatsDatePeriod, pPertracID, MatchBackfillVol, Renaissance_BaseDate, pReferenceDate, Renaissance_EndDate_Data, pReferenceDate, pRiskFreeRate, pScalingFactor, -1, eMessage)

	End Function

	Public Function GetSimpleStats(ByVal StatsDatePeriod As DealingPeriod, ByVal pPertracID As ULong, ByVal MatchBackfillVol As Boolean, ByVal pDataStartDate As Date, ByVal pReferenceDate As Date, ByVal pRiskFreeRate As Double, ByVal pScalingFactor As Double, ByRef eMessage As String) As SeriesStatsClass
		' **************************************************************************************
		'
		' **************************************************************************************

		Return GetSimpleStats(StatsDatePeriod, pPertracID, MatchBackfillVol, pDataStartDate, pReferenceDate, Renaissance_EndDate_Data, pReferenceDate, pRiskFreeRate, pScalingFactor, -1, eMessage)

	End Function

	Public Function GetSimpleStats(ByVal StatsDatePeriod As DealingPeriod, ByVal pPertracID As ULong, ByVal MatchBackfillVol As Boolean, ByVal pDataStartDate As Date, ByVal pCustomPeriodStartDate As Date, ByVal pCustomPeriodEndDate As Date, ByVal pReferenceDate As Date, ByVal pRiskFreeRate As Double, ByVal pScalingFactor As Double, ByVal pSampleSize As Integer, ByRef eMessage As String) As SeriesStatsClass
		' **************************************************************************************
		' Return a set of simple statistics on a given stock.
		' 
		' These statistics are designed for use on the FundBrowser Stats Grid and in the Group Informations and 
		' comparison reports.
		' The duration statistics are designed to be valued from the last calendar month, missing data is assumed
		' to be zero or void as appropriate, thus a 12M value will be zero if there is no data in the last 12 Months, 
		' Originally this code used the last 12Months of available data, but this seems inappropriate for the 
		' reports on which this appears.
		'
		' Updated, NPP 14 May 2007
		' Updated, NPP 03 Oct 2007, Add Stats for the competitor Groups Report.
		' Updated, NPP 09 Jun 2008, Add variable start date.
		'
		' **************************************************************************************
		' Dim pCustomPeriodEndDate As Date = Renaissance_EndDate_Data

		Dim DataStartDate As Date
		Dim RVal As SeriesStatsClass

		Dim ReturnSeries() As Double
		Dim NAVSeries() As Double
		Dim DateSeries() As Date


		Dim ReferenceDate As Date = FitDateToPeriod(StatsDatePeriod, pReferenceDate, True)

		If (pDataStartDate > ReferenceDate) Then
			DataStartDate = FitDateToPeriod(StatsDatePeriod, AddPeriodToDate(StatsDatePeriod, ReferenceDate, -1), True)
		Else
			DataStartDate = FitDateToPeriod(StatsDatePeriod, pDataStartDate, True)
		End If

		Try

			ReturnSeries = Me.ReturnSeries(StatsDatePeriod, 0, pPertracID, MatchBackfillVol, AnnualPeriodCount(StatsDatePeriod), 1.0#, pSampleSize, False, DataStartDate, Renaissance_EndDate_Data, pScalingFactor, eMessage)
			NAVSeries = Me.NAVSeries(StatsDatePeriod, pPertracID, MatchBackfillVol, AnnualPeriodCount(StatsDatePeriod), 1.0#, False, DataStartDate, Renaissance_EndDate_Data, pScalingFactor, eMessage)
			DateSeries = Me.DateSeries(StatsDatePeriod, pPertracID, MatchBackfillVol, AnnualPeriodCount(StatsDatePeriod), 1.0#, False, DataStartDate, Renaissance_EndDate_Data, eMessage)

			RVal = GetSimpleStats(StatsDatePeriod, DateSeries, NAVSeries, ReturnSeries, pCustomPeriodStartDate, pCustomPeriodEndDate, pReferenceDate, pRiskFreeRate)
			RVal.Scalingfactor = pScalingFactor

			Return RVal

		Catch ex As Exception
		End Try

		Return New SeriesStatsClass(StatsDatePeriod)

	End Function

	Public Function GetSimpleStats(ByVal StatsDatePeriod As DealingPeriod, ByVal DateSeries() As Date, ByVal NAVSeries() As Double, ByVal ReturnSeries() As Double, ByVal pCustomPeriodStartDate As Date, ByVal pCustomPeriodEndDate As Date, ByVal pReferenceDate As Date, ByVal pRiskFreeRate As Double) As SeriesStatsClass

		' **************************************************************************************
		' Return a set of simple statistics on a given stock.
		' 
		' These statistics are designed for use on the FundBrowser Stats Grid and in the Group Informations and 
		' comparison reports.
		' The duration statistics are designed to be valued from the last calendar month, missing data is assumed
		' to be zero or void as appropriate, thus a 12M value will be zero if there is no data in the last 12 Months, 
		' Originally this code used the last 12Months of available data, but this seems inappropriate for the 
		' reports on which this appears.
		'
		' Updated, NPP 14 May 2007
		' Updated, NPP 03 Oct 2007, Add Stats for the competitor Groups Report.
		' Updated, NPP 09 Jun 2008, Add variable start date.
		'
		' **************************************************************************************
		' Dim pCustomPeriodEndDate As Date = Renaissance_EndDate_Data

		Dim RVal As New SeriesStatsClass(StatsDatePeriod)
		RVal.Scalingfactor = 1.0#

		Try
			If (ReturnSeries Is Nothing) OrElse (NAVSeries Is Nothing) OrElse (DateSeries Is Nothing) Then
				Return RVal
			End If
		Catch ex As Exception
			Return RVal
		End Try

		Dim IndexCounter As Integer
		Dim ReturnCount As Integer
		Dim PositiveCount As Integer
		Dim CustomReturnCount As Integer
		Dim CustomPositiveCount As Integer
		Dim CalcCustomToDate As Boolean = True

		If (pCustomPeriodStartDate >= pReferenceDate) Then
			CalcCustomToDate = False
		End If

		' Creation of ReferenceMonth has been altered to allow the user to specify an end month (Date) for statistics generation.
		' This change means I now assume the given date is in the required end period, I no longer use the end of the previous period
		' on the assumption that the data for the current month is incomplete - as is usually the case for fund data.

		Dim ReferenceDate As Date = FitDateToPeriod(StatsDatePeriod, pReferenceDate, True)

		Dim ReferenceMonthIndex_Actual As Integer = (-1) ' The actual Date Index for the Reference Month. This may be off the end of the available data
		Dim ReferenceMonthIndex_Valid As Integer = (-1)	' An existing Index to use. If ReferenceMonthIndex_Actual is off the end, then this will be the last item.
		Dim DataStartIndex As Integer
		Dim CustomStartIndex As Integer
		Dim CustomEndIndex_Valid As Integer = (-1) ' An existing Index to use. If ReferenceMonthIndex_Actual is off the end, then this will be the last item.

		' DealingPeriod.Monthly

		Try

			ReferenceMonthIndex_Actual = GetPriceIndex(StatsDatePeriod, DateSeries(0), ReferenceDate)
			ReferenceMonthIndex_Valid = ReferenceMonthIndex_Actual

			If (ReferenceMonthIndex_Valid >= ReturnSeries.Length) Then
				ReferenceMonthIndex_Valid = (ReturnSeries.Length - 1)
			ElseIf (ReferenceMonthIndex_Valid < 0) Then
				' No Data
				RVal.FirstReturnDate = RenaissanceGlobals.Globals.KNOWLEDGEDATE_NOW
				Return RVal
			End If
			RVal.PeriodCount = ReferenceMonthIndex_Valid ' (ReturnSeries.Length - 1)	' (First Element is fabricated)

			' Calculate Start Index for Custom Date period calculations.

			If (CalcCustomToDate) Then
				RVal.CustomStartDate = pCustomPeriodStartDate
				If (pCustomPeriodEndDate > ReferenceDate) Then
					RVal.CustomEndDate = ReferenceDate
				Else
					RVal.CustomEndDate = pCustomPeriodEndDate
				End If
				CustomStartIndex = GetPriceIndex(StatsDatePeriod, DateSeries(0), pCustomPeriodStartDate)

				If (CustomStartIndex > ReferenceMonthIndex_Valid) Then
					CustomStartIndex = ReferenceMonthIndex_Valid
					RVal.CustomStartDate = Renaissance_BaseDate
					CalcCustomToDate = False
				ElseIf (CustomStartIndex < 0) Then
					CustomStartIndex = 0
				End If

				CustomEndIndex_Valid = Math.Max(Math.Min(GetPriceIndex(StatsDatePeriod, DateSeries(0), RVal.CustomEndDate), ReferenceMonthIndex_Valid), CustomStartIndex)
			End If

			If (CustomEndIndex_Valid - CustomStartIndex) < 1 Then
				CalcCustomToDate = False
			End If

			' Set RVAL Data Start Date .

			If (DateSeries IsNot Nothing) AndAlso (DateSeries.Length > 0) Then
				RVal.FirstReturnDate = DateSeries(0)
			Else
				RVal.FirstReturnDate = RenaissanceGlobals.Globals.KNOWLEDGEDATE_NOW
			End If

			If (NAVSeries IsNot Nothing) AndAlso (NAVSeries.Length > 0) AndAlso (ReferenceMonthIndex_Actual > 0) Then

				' Simple Returns & Annualised Returns

				DataStartIndex = ReferenceMonthIndex_Actual - (AnnualPeriodCount(StatsDatePeriod)) ' 1 Year

				If (DataStartIndex < ReferenceMonthIndex_Valid) Then
					RVal.SimpleReturn.M12 = (NAVSeries(ReferenceMonthIndex_Valid) / NAVSeries(Math.Max(DataStartIndex, 0))) - 1.0

					If (ReferenceMonthIndex_Valid > 0) Then
						If (DataStartIndex < 0) Then
							RVal.AnnualisedReturn.M12 = ((RVal.SimpleReturn.M12 + 1.0) ^ CDbl(AnnualPeriodCount(StatsDatePeriod) / ReferenceMonthIndex_Valid)) - 1.0
						Else
							RVal.AnnualisedReturn.M12 = RVal.SimpleReturn.M12
						End If
					End If
				End If

				DataStartIndex = ReferenceMonthIndex_Actual - (AnnualPeriodCount(StatsDatePeriod) * 2) ' 2 Years
				If (DataStartIndex < ReferenceMonthIndex_Valid) Then
					RVal.SimpleReturn.M24 = (NAVSeries(ReferenceMonthIndex_Valid) / NAVSeries(Math.Max(DataStartIndex, 0))) - 1.0

					If (ReferenceMonthIndex_Valid > 0) Then
						If (ReferenceMonthIndex_Actual < AnnualPeriodCount(StatsDatePeriod)) Then	 ' Less than One Years returns
							' Set Annualised return = actual return (At fiona's request).
							RVal.AnnualisedReturn.M24 = RVal.SimpleReturn.M24
						ElseIf (DataStartIndex < 0) Then
							RVal.AnnualisedReturn.M24 = ((RVal.SimpleReturn.M24 + 1.0) ^ CDbl(AnnualPeriodCount(StatsDatePeriod) / ReferenceMonthIndex_Valid)) - 1.0
						Else
							RVal.AnnualisedReturn.M24 = ((RVal.SimpleReturn.M24 + 1.0) ^ 0.5) - 1.0
						End If
					End If
				End If

				DataStartIndex = ReferenceMonthIndex_Actual - (AnnualPeriodCount(StatsDatePeriod) * 3) ' 3 Years 
				If (DataStartIndex < ReferenceMonthIndex_Valid) Then
					RVal.SimpleReturn.M36 = (NAVSeries(ReferenceMonthIndex_Valid) / NAVSeries(Math.Max(DataStartIndex, 0))) - 1.0

					If (ReferenceMonthIndex_Valid > 0) Then
						If (ReferenceMonthIndex_Actual < AnnualPeriodCount(StatsDatePeriod)) Then	' Less than One Years returns
							' Set Annualised return = actual return (At fiona's request).
							RVal.AnnualisedReturn.M36 = RVal.SimpleReturn.M36
						ElseIf (DataStartIndex < 0) Then
							RVal.AnnualisedReturn.M36 = ((RVal.SimpleReturn.M36 + 1.0) ^ CDbl(AnnualPeriodCount(StatsDatePeriod) / ReferenceMonthIndex_Valid)) - 1.0
						Else
							RVal.AnnualisedReturn.M36 = ((RVal.SimpleReturn.M36 + 1.0) ^ (1.0 / 3.0)) - 1.0
						End If
					End If
				End If

				DataStartIndex = ReferenceMonthIndex_Actual - (AnnualPeriodCount(StatsDatePeriod) * 5) ' 5 Years
				If (DataStartIndex < ReferenceMonthIndex_Valid) Then
					RVal.SimpleReturn.M60 = (NAVSeries(ReferenceMonthIndex_Valid) / NAVSeries(Math.Max(DataStartIndex, 0))) - 1.0

					If (ReferenceMonthIndex_Valid > 0) Then
						If (ReferenceMonthIndex_Actual < AnnualPeriodCount(StatsDatePeriod)) Then	' Less than One Years returns
							' Set Annualised return = actual return (At fiona's request).
							RVal.AnnualisedReturn.M60 = RVal.SimpleReturn.M60
						ElseIf (DataStartIndex < 0) Then
							RVal.AnnualisedReturn.M60 = ((RVal.SimpleReturn.M60 + 1.0) ^ CDbl(AnnualPeriodCount(StatsDatePeriod) / ReferenceMonthIndex_Valid)) - 1.0
						Else
							RVal.AnnualisedReturn.M60 = ((RVal.SimpleReturn.M60 + 1.0) ^ 0.2) - 1.0
						End If
					End If
				End If

				' YTD
				DataStartIndex = GetPriceIndex(StatsDatePeriod, DateSeries(0), New Date(ReferenceDate.Year, 1, 1).AddDays(-1))
				If (DataStartIndex < ReferenceMonthIndex_Valid) Then
					RVal.SimpleReturn.YTD = (NAVSeries(ReferenceMonthIndex_Valid) / NAVSeries(Math.Max(DataStartIndex, 0))) - 1.0
				End If

				RVal.SimpleReturn.ITD = (NAVSeries(ReferenceMonthIndex_Valid) / NAVSeries(0)) - 1.0	' (NAVSeries(NAVSeries.Length - 1) / NAVSeries(0)) - 1.0

				If (ReferenceMonthIndex_Actual < AnnualPeriodCount(StatsDatePeriod)) Then	' Less than One Years returns
					' Set Annualised return = actual return (At fiona's request).
					RVal.AnnualisedReturn.ITD = RVal.SimpleReturn.ITD
				Else
					RVal.AnnualisedReturn.ITD = ((RVal.SimpleReturn.ITD + 1.0) ^ CDbl(AnnualPeriodCount(StatsDatePeriod) / (ReferenceMonthIndex_Valid))) - 1.0	' ((RVal.SimpleReturn.ITD + 1.0) ^ CDbl(_AnnualPeriodCount / (NAVSeries.Length - 1))) - 1.0
				End If

				' CTD ' CustomStartIndex
				If (CalcCustomToDate) Then
					RVal.SimpleReturn.CTD = (NAVSeries(CustomEndIndex_Valid) / NAVSeries(Math.Max(CustomStartIndex - 1, 0))) - 1.0

					If ((CustomEndIndex_Valid - (CustomStartIndex - 1)) < AnnualPeriodCount(StatsDatePeriod)) Then	 ' Less than One Years returns
						RVal.AnnualisedReturn.CTD = RVal.SimpleReturn.CTD
					Else
						RVal.AnnualisedReturn.CTD = ((RVal.SimpleReturn.CTD + 1.0) ^ CDbl(AnnualPeriodCount(StatsDatePeriod) / (CustomEndIndex_Valid - (CustomStartIndex - 1)))) - 1.0
					End If
				End If

				' Best / Worst returns

				If (ReturnSeries.Length > 1) Then

					RVal.BestPeriodReturn = ReturnSeries(1)
					RVal.WorstPeriodReturn = ReturnSeries(1)

					For IndexCounter = 2 To ReferenceMonthIndex_Valid
						If (ReturnSeries(IndexCounter) > RVal.BestPeriodReturn) Then
							RVal.BestPeriodReturn = ReturnSeries(IndexCounter)
							RVal.BestPeriodDate = DateSeries(IndexCounter)
						End If

						If (ReturnSeries(IndexCounter) < RVal.WorstPeriodReturn) Then
							RVal.WorstPeriodReturn = ReturnSeries(IndexCounter)
							RVal.WorstPeriodDate = DateSeries(IndexCounter)
						End If
					Next
				End If

				' Month Returns

				Dim EndIndex As Integer
				Dim StartIndex As Integer
				'Dim ThisMonth As Date
				'ThisMonth = ReferenceDate

				RVal.MonthReturns.ReferenceDate = ReferenceDate

				EndIndex = ReferenceMonthIndex_Valid ' GetPriceIndex(StatsDatePeriod, DateSeries(0), ReferenceDate)
				StartIndex = GetPriceIndex(StatsDatePeriod, DateSeries(0), ReferenceDate.AddMonths(-1))

				If (StartIndex < ReturnSeries.Length) AndAlso (StartIndex >= 0) Then

					RVal.MonthReturns.P1 = (NAVSeries(EndIndex) / NAVSeries(Math.Max(StartIndex, 0))) - 1.0#

				End If

				EndIndex = Math.Min(ReferenceMonthIndex_Valid, StartIndex)
				StartIndex = GetPriceIndex(StatsDatePeriod, DateSeries(0), ReferenceDate.AddMonths(-2))

				If (StartIndex < ReturnSeries.Length) AndAlso (StartIndex >= 0) Then

					RVal.MonthReturns.P2 = (NAVSeries(EndIndex) / NAVSeries(Math.Max(StartIndex, 0))) - 1.0#

				End If

				EndIndex = Math.Min(ReferenceMonthIndex_Valid, StartIndex)
				StartIndex = GetPriceIndex(StatsDatePeriod, DateSeries(0), ReferenceDate.AddMonths(-3))

				If (StartIndex < ReturnSeries.Length) AndAlso (StartIndex >= 0) Then

					RVal.MonthReturns.P3 = (NAVSeries(EndIndex) / NAVSeries(Math.Max(StartIndex, 0))) - 1.0#

				End If

				EndIndex = Math.Min(ReferenceMonthIndex_Valid, StartIndex)
				StartIndex = GetPriceIndex(StatsDatePeriod, DateSeries(0), ReferenceDate.AddMonths(-4))

				If (StartIndex < ReturnSeries.Length) AndAlso (StartIndex >= 0) Then

					RVal.MonthReturns.P4 = (NAVSeries(EndIndex) / NAVSeries(Math.Max(StartIndex, 0))) - 1.0#

				End If

				EndIndex = Math.Min(ReferenceMonthIndex_Valid, StartIndex)
				StartIndex = GetPriceIndex(StatsDatePeriod, DateSeries(0), ReferenceDate.AddMonths(-5))

				If (StartIndex < ReturnSeries.Length) AndAlso (StartIndex >= 0) Then

					RVal.MonthReturns.P5 = (NAVSeries(EndIndex) / NAVSeries(Math.Max(StartIndex, 0))) - 1.0#

				End If

				EndIndex = Math.Min(ReferenceMonthIndex_Valid, StartIndex)
				StartIndex = GetPriceIndex(StatsDatePeriod, DateSeries(0), ReferenceDate.AddMonths(-6))

				If (StartIndex < ReturnSeries.Length) AndAlso (StartIndex >= 0) Then

					RVal.MonthReturns.P6 = (NAVSeries(EndIndex) / NAVSeries(Math.Max(StartIndex, 0))) - 1.0#

				End If

				' Year Returns

				Dim ThisYear As Integer
				Dim thisYearReturn As Double = 1.0

				ThisYear = ReferenceDate.Year

				For IndexCounter = ReferenceMonthIndex_Valid To 0 Step -1	' (ReturnSeries.Length - 1) To 0 Step -1
					If DateSeries(IndexCounter).Year <= ThisYear Then
						If DateSeries(IndexCounter).Year <> ThisYear Then

							Select Case (ReferenceDate.Year - ThisYear)
								Case 0
									RVal.YearReturns.P1 = thisYearReturn - 1.0

								Case 1
									RVal.YearReturns.P2 = thisYearReturn - 1.0

								Case 2
									RVal.YearReturns.P3 = thisYearReturn - 1.0

								Case 3
									RVal.YearReturns.P4 = thisYearReturn - 1.0

								Case 4
									RVal.YearReturns.P5 = thisYearReturn - 1.0

								Case 5
									RVal.YearReturns.P6 = thisYearReturn - 1.0

								Case Else
									Exit For

							End Select

							ThisYear = DateSeries(IndexCounter).Year
							thisYearReturn = 1.0
						End If

						thisYearReturn *= (1.0 + ReturnSeries(IndexCounter))
					End If
				Next

				' Set values for final year

				Select Case (ReferenceDate.Year - ThisYear)
					Case 0
						RVal.YearReturns.P1 = thisYearReturn - 1.0

					Case 1
						RVal.YearReturns.P2 = thisYearReturn - 1.0

					Case 2
						RVal.YearReturns.P3 = thisYearReturn - 1.0

					Case 3
						RVal.YearReturns.P4 = thisYearReturn - 1.0

					Case 4
						RVal.YearReturns.P5 = thisYearReturn - 1.0

					Case 5
						RVal.YearReturns.P6 = thisYearReturn - 1.0

				End Select

			End If

			' Percent Positive months
			' Months where data does not exist do not count as positive.

			ReturnCount = 0
			PositiveCount = 0
			CustomReturnCount = 0
			CustomPositiveCount = 0
			If (ReturnSeries IsNot Nothing) Then
				For IndexCounter = ReferenceMonthIndex_Actual To 0 Step -1 ' (ReturnSeries.Length - 1) To 0 Step -1
					ReturnCount += 1

					If (IndexCounter <= CustomEndIndex_Valid) AndAlso (IndexCounter >= CustomStartIndex) Then
						CustomReturnCount += 1

						If (IndexCounter < ReturnSeries.Length) AndAlso (ReturnSeries(IndexCounter) >= 0) Then
							CustomPositiveCount += 1
						End If

					End If
					If (IndexCounter < ReturnSeries.Length) Then
						If (ReturnSeries(IndexCounter) >= 0) Then
							PositiveCount += 1
						End If
					End If

					If (ReturnCount = AnnualPeriodCount(StatsDatePeriod)) Then
						RVal.PercentPositive.M12 = PositiveCount / AnnualPeriodCount(StatsDatePeriod)
					End If

					If (ReturnCount = (AnnualPeriodCount(StatsDatePeriod) * 3)) Then
						RVal.PercentPositive.M36 = PositiveCount / (AnnualPeriodCount(StatsDatePeriod) * 3.0)
					End If

					If (ReturnCount = (AnnualPeriodCount(StatsDatePeriod) * 5)) Then
						RVal.PercentPositive.M60 = PositiveCount / (AnnualPeriodCount(StatsDatePeriod) * 5.0)
					End If

					If (CalcCustomToDate) AndAlso (IndexCounter = CustomStartIndex) Then
						RVal.PercentPositive.CTD = CustomPositiveCount / CustomReturnCount
					End If

					If (IndexCounter = 0) Then
						RVal.PercentPositive.ITD = PositiveCount / (ReferenceMonthIndex_Valid + 1) ' (ReturnSeries.Length)
					End If
				Next
			End If

			' Std Deviations

			Dim AvgX As Double
			Dim Sum_X As Double
			Dim Sum_X2 As Double

			' 12 Month

			DataStartIndex = (ReferenceMonthIndex_Actual - (AnnualPeriodCount(StatsDatePeriod))) + 1
			If (DataStartIndex < 0) Then
				DataStartIndex = 0
			End If
			ReturnCount = 0

			If (ReturnSeries IsNot Nothing) AndAlso (DataStartIndex >= 0) AndAlso (DataStartIndex < ReturnSeries.Length) Then
				' Average
				Sum_X = 0
				For IndexCounter = (DataStartIndex) To (ReferenceMonthIndex_Actual)
					If (IndexCounter < ReturnSeries.Length) Then
						Sum_X += ReturnSeries(IndexCounter)
						ReturnCount += 1
					End If
				Next

				If (ReturnCount >= MIN_Periods_For_Stats) Then
					AvgX = (Sum_X / ReturnCount)

					Sum_X2 = 0
					For IndexCounter = (DataStartIndex) To (ReferenceMonthIndex_Actual)
						If (IndexCounter < ReturnSeries.Length) Then
							Sum_X2 += (ReturnSeries(IndexCounter) - AvgX) ^ 2
						End If
					Next

					RVal.StandardDeviation.M12 = Math.Sqrt(Sum_X2 / ReturnCount)
					RVal.StandardDeviation_Series.M12 = Math.Sqrt(Sum_X2 / (ReturnCount - 1))
				End If
			End If

			' 24 Month

			DataStartIndex = (ReferenceMonthIndex_Actual - (AnnualPeriodCount(StatsDatePeriod) * 2)) + 1
			If (DataStartIndex < 0) Then
				DataStartIndex = 0
			End If
			ReturnCount = 0

			If (ReturnSeries IsNot Nothing) AndAlso (DataStartIndex >= 0) AndAlso (DataStartIndex < ReturnSeries.Length) Then
				' Average
				Sum_X = 0
				For IndexCounter = (DataStartIndex) To (ReferenceMonthIndex_Actual)
					If (IndexCounter < ReturnSeries.Length) Then
						Sum_X += ReturnSeries(IndexCounter)
						ReturnCount += 1
					End If
				Next

				If (ReturnCount >= MIN_Periods_For_Stats) Then
					AvgX = (Sum_X / ReturnCount)

					Sum_X2 = 0
					For IndexCounter = (DataStartIndex) To (ReferenceMonthIndex_Actual)
						If (IndexCounter < ReturnSeries.Length) Then
							Sum_X2 += (ReturnSeries(IndexCounter) - AvgX) ^ 2
						End If
					Next

					RVal.StandardDeviation.M24 = Math.Sqrt(Sum_X2 / ReturnCount)
					RVal.StandardDeviation_Series.M24 = Math.Sqrt(Sum_X2 / (ReturnCount - 1))
				End If
			End If

			' 36 Month

			DataStartIndex = (ReferenceMonthIndex_Actual - (AnnualPeriodCount(StatsDatePeriod) * 3)) + 1
			If (DataStartIndex < 0) Then
				DataStartIndex = 0
			End If
			ReturnCount = 0

			If (ReturnSeries IsNot Nothing) AndAlso (DataStartIndex >= 0) AndAlso (DataStartIndex < ReturnSeries.Length) Then
				' Average
				Sum_X = 0
				For IndexCounter = (DataStartIndex) To (ReferenceMonthIndex_Actual)
					If (IndexCounter < ReturnSeries.Length) Then
						Sum_X += ReturnSeries(IndexCounter)
						ReturnCount += 1
					End If
				Next

				If (ReturnCount >= MIN_Periods_For_Stats) Then
					AvgX = (Sum_X / ReturnCount)

					Sum_X2 = 0
					For IndexCounter = (DataStartIndex) To (ReferenceMonthIndex_Actual)
						If (IndexCounter < ReturnSeries.Length) Then
							Sum_X2 += (ReturnSeries(IndexCounter) - AvgX) ^ 2
						End If
					Next

					RVal.StandardDeviation.M36 = Math.Sqrt(Sum_X2 / ReturnCount)
					RVal.StandardDeviation_Series.M36 = Math.Sqrt(Sum_X2 / (ReturnCount - 1))
				End If
			End If


			' 60 Month

			DataStartIndex = (ReferenceMonthIndex_Actual - (AnnualPeriodCount(StatsDatePeriod) * 5)) + 1
			If (DataStartIndex < 0) Then
				DataStartIndex = 0
			End If
			ReturnCount = 0

			If (ReturnSeries IsNot Nothing) AndAlso (DataStartIndex >= 0) AndAlso (DataStartIndex < ReturnSeries.Length) Then
				' Average
				Sum_X = 0
				For IndexCounter = (DataStartIndex) To (ReferenceMonthIndex_Actual)
					If (IndexCounter < ReturnSeries.Length) Then
						Sum_X += ReturnSeries(IndexCounter)
						ReturnCount += 1
					End If
				Next

				If (ReturnCount >= MIN_Periods_For_Stats) Then
					AvgX = (Sum_X / ReturnCount)

					Sum_X2 = 0
					For IndexCounter = (DataStartIndex) To (ReferenceMonthIndex_Actual)
						If (IndexCounter < ReturnSeries.Length) Then
							Sum_X2 += (ReturnSeries(IndexCounter) - AvgX) ^ 2
						End If
					Next

					RVal.StandardDeviation.M60 = Math.Sqrt(Sum_X2 / ReturnCount)
					RVal.StandardDeviation_Series.M60 = Math.Sqrt(Sum_X2 / (ReturnCount - 1))
				End If
			End If

			' ITD

			If (ReturnSeries IsNot Nothing) AndAlso (ReturnSeries.Length >= MIN_Periods_For_Stats) AndAlso ((ReferenceMonthIndex_Valid + 1) >= MIN_Periods_For_Stats) Then
				' Average - Note Return Item '0' is always Zero - so omit it !

				Sum_X = 0
				For IndexCounter = 1 To ReferenceMonthIndex_Valid	' (ReturnSeries.Length - 1)
					Sum_X += ReturnSeries(IndexCounter)
				Next
				AvgX = (Sum_X / ReferenceMonthIndex_Valid) ' (Sum_X / (ReturnSeries.Length - 1.0#))

				Sum_X2 = 0
				For IndexCounter = 1 To ReferenceMonthIndex_Valid	' (ReturnSeries.Length - 1)
					Sum_X2 += (ReturnSeries(IndexCounter) - AvgX) ^ 2
				Next

				RVal.StandardDeviation.ITD = Math.Sqrt(Sum_X2 / CDbl(ReferenceMonthIndex_Valid))	' Math.Sqrt(Sum_X2 / (ReturnSeries.Length - 1.0#))
				RVal.StandardDeviation_Series.ITD = Math.Sqrt(Sum_X2 / CDbl(ReferenceMonthIndex_Valid - 1))	' Math.Sqrt(Sum_X2 / (ReturnSeries.Length - 2))
			End If

			' CTD

			If (CalcCustomToDate) Then
				If (ReturnSeries IsNot Nothing) AndAlso (ReturnSeries.Length >= MIN_Periods_For_Stats) AndAlso (((CustomEndIndex_Valid - CustomStartIndex) + 1) >= MIN_Periods_For_Stats) Then
					' Average - Note Return Item '0' is always Zero - so omit it !

					Sum_X = 0
					For IndexCounter = CustomStartIndex To CustomEndIndex_Valid	 ' (ReturnSeries.Length - 1)
						Sum_X += ReturnSeries(IndexCounter)
					Next
					AvgX = (Sum_X / (CustomEndIndex_Valid - Math.Max(CustomStartIndex - 1, 0)))	 ' (Sum_X / (ReturnSeries.Length - 1.0#))

					Sum_X2 = 0
					For IndexCounter = CustomStartIndex To CustomEndIndex_Valid	' (ReturnSeries.Length - 1)
						Sum_X2 += (ReturnSeries(IndexCounter) - AvgX) ^ 2.0
					Next

					RVal.StandardDeviation.CTD = Math.Sqrt(Sum_X2 / CDbl(CustomEndIndex_Valid - Math.Max(CustomStartIndex - 1, 0)))	 ' Math.Sqrt(Sum_X2 / (ReturnSeries.Length - 1.0#))
					RVal.StandardDeviation_Series.CTD = Math.Sqrt(Sum_X2 / CDbl(CustomEndIndex_Valid - Math.Max(CustomStartIndex - 1, 0) - 1)) ' Math.Sqrt(Sum_X2 / (ReturnSeries.Length - 2))
				End If
			End If

			' Volatility

			RVal.Volatility.M12 = RVal.StandardDeviation.M12 * Sqrt12(StatsDatePeriod)
			RVal.Volatility.M24 = RVal.StandardDeviation.M24 * Sqrt12(StatsDatePeriod)
			RVal.Volatility.M36 = RVal.StandardDeviation.M36 * Sqrt12(StatsDatePeriod)
			RVal.Volatility.M60 = RVal.StandardDeviation.M60 * Sqrt12(StatsDatePeriod)
			RVal.Volatility.ITD = RVal.StandardDeviation.ITD * Sqrt12(StatsDatePeriod)
			RVal.Volatility.CTD = RVal.StandardDeviation.CTD * Sqrt12(StatsDatePeriod)

			RVal.Volatility_Series.M12 = RVal.StandardDeviation_Series.M12 * Sqrt12(StatsDatePeriod)
			RVal.Volatility_Series.M24 = RVal.StandardDeviation_Series.M24 * Sqrt12(StatsDatePeriod)
			RVal.Volatility_Series.M36 = RVal.StandardDeviation_Series.M36 * Sqrt12(StatsDatePeriod)
			RVal.Volatility_Series.M60 = RVal.StandardDeviation_Series.M60 * Sqrt12(StatsDatePeriod)
			RVal.Volatility_Series.ITD = RVal.StandardDeviation_Series.ITD * Sqrt12(StatsDatePeriod)
			RVal.Volatility_Series.CTD = RVal.StandardDeviation_Series.CTD * Sqrt12(StatsDatePeriod)

			' Sharpe Ratio

			If (RVal.Volatility.M12 > 0) Then
				RVal.SharpeRatio.M12 = (RVal.AnnualisedReturn.M12 - pRiskFreeRate) / RVal.Volatility.M12
			End If

			If (RVal.Volatility.M24 > 0) Then
				RVal.SharpeRatio.M24 = (RVal.AnnualisedReturn.M24 - pRiskFreeRate) / RVal.Volatility.M24
			End If

			If (RVal.Volatility.M36 > 0) Then
				RVal.SharpeRatio.M36 = (RVal.AnnualisedReturn.M36 - pRiskFreeRate) / RVal.Volatility.M36
			End If

			If (RVal.Volatility.M60 > 0) Then
				RVal.SharpeRatio.M60 = (RVal.AnnualisedReturn.M60 - pRiskFreeRate) / RVal.Volatility.M60
			End If

			If (RVal.Volatility.ITD > 0) Then
				RVal.SharpeRatio.ITD = (RVal.AnnualisedReturn.ITD - pRiskFreeRate) / RVal.Volatility.ITD
			End If

			If (RVal.Volatility.CTD > 0) Then
				RVal.SharpeRatio.CTD = (RVal.AnnualisedReturn.CTD - pRiskFreeRate) / RVal.Volatility.CTD
			End If

		Catch ex As Exception
		End Try

		Return RVal

	End Function

	Public Class DrawDownInstanceClass
		' **********************************************************************************************
		' Class used to hold details of DrawUps/DrawDowns for an instrument.
		'
		' **********************************************************************************************

		Implements IComparable, IComparer

		Public StatsDatePeriod As DealingPeriod
		Public ScalingFactor As Double
		Public DateFrom As Date
		Public DateTrough As Date
		Public DateTo As Date
		Public DrawDown As Double
		Public RecoveryOngoing As Boolean

		Public Property DrawUp() As Double
			' **********************************************************************************************
			' Shadow to DrawDown local just so it can be called DrawUp aswell.
			'
			' **********************************************************************************************

			Get
				Return DrawDown
			End Get
			Set(ByVal value As Double)
				DrawDown = value
			End Set
		End Property

		Private Sub New()

		End Sub

		Public Sub New(ByVal pStatsDatePeriod As DealingPeriod)
			' **********************************************************************************************
			'
			'
			' **********************************************************************************************

			StatsDatePeriod = pStatsDatePeriod
			ScalingFactor = 1.0#
			DateFrom = Renaissance_BaseDate
			DateTrough = Renaissance_BaseDate
			DateTo = Renaissance_BaseDate
			DrawDown = 0
			RecoveryOngoing = False

		End Sub

		Public Function CompareTo(ByVal obj As Object) As Integer Implements System.IComparable.CompareTo
			' **********************************************************************************************
			'
			'
			' **********************************************************************************************

			If TypeOf (obj) Is DrawDownInstanceClass Then
				' Sort by DrawDown Descending
				Return CType(obj, DrawDownInstanceClass).DrawDown.CompareTo(Me.DrawDown)
			Else
				Return 0
			End If
		End Function

		Public Function Compare(ByVal x As Object, ByVal y As Object) As Integer Implements System.Collections.IComparer.Compare
			' **********************************************************************************************
			'
			'
			' **********************************************************************************************

			If (TypeOf (x) Is DrawDownInstanceClass) AndAlso (TypeOf (y) Is DrawDownInstanceClass) Then
				' Sort by DrawDown Descending
				Return CType(y, DrawDownInstanceClass).DrawDown.CompareTo(CType(x, DrawDownInstanceClass).DrawDown)
			Else
				Return 0
			End If
		End Function

	End Class

	Public Function GetDrawDownDetails(ByVal StatsDatePeriod As DealingPeriod, ByVal pPertracID As ULong, ByVal MatchBackfillVol As Boolean, ByRef eMessage As String) As DrawDownInstanceClass()

		Return GetDrawDownDetails(StatsDatePeriod, pPertracID, MatchBackfillVol, Renaissance_BaseDate, Renaissance_EndDate_Data, 1.0#, eMessage)

	End Function

	Public Function GetDrawDownDetails(ByVal StatsDatePeriod As DealingPeriod, ByVal pPertracID As ULong, ByVal MatchBackfillVol As Boolean, ByVal StartDate As Date, ByVal EndDate As Date, ByVal pScalingFactor As Double, ByRef eMessage As String) As DrawDownInstanceClass()
		' **************************************************************************************
		'
		'
		' **************************************************************************************

		Dim RVal(-1) As DrawDownInstanceClass

		If (pPertracID <= 0) Then
			Return RVal
		End If

		' Get from Cache ?

		Dim CacheKey As ULong = GetStatCacheKey(StatsDatePeriod, pPertracID, MatchBackfillVol)

		SyncLock _StatsCache
			If _DrawDownCache.ContainsKey(CacheKey) Then
				Return _DrawDownCache(CacheKey)
			End If
		End SyncLock

		' Otherwise calculate

		Dim DrawDownArrayList As New ArrayList()
		Dim thisDrawDown As DrawDownInstanceClass

		Dim DateSeries() As Date
		Dim NAVSeries() As Double

		Dim IndexCounter As Integer
		Dim DrawDownStartDate As Date
		Dim DrawDownPeakDate As Date
		Dim DrawDownFinishDate As Date
		Dim thisNAV As Double
		Dim lastNAV As Double
		Dim StartNAV As Double
		Dim TroughNAV As Double
		Dim DrawDownCount As Integer
		Dim RecoveryCount As Integer
		Dim InDrawDown As Boolean

		DateSeries = Me.DateSeries(StatsDatePeriod, pPertracID, MatchBackfillVol, AnnualPeriodCount(StatsDatePeriod), 1, False, StartDate, EndDate, eMessage)
		NAVSeries = Me.NAVSeries(StatsDatePeriod, pPertracID, MatchBackfillVol, AnnualPeriodCount(StatsDatePeriod), 1, False, StartDate, EndDate, pScalingFactor, eMessage)

		If (NAVSeries IsNot Nothing) AndAlso (DateSeries IsNot Nothing) Then
			If (NAVSeries.Length) > 0 Then
				lastNAV = NAVSeries(0)
			End If
			InDrawDown = False

			For IndexCounter = 1 To (NAVSeries.Length - 1)
				thisNAV = NAVSeries(IndexCounter)

				If (Not InDrawDown) Then
					If (thisNAV < lastNAV) Then
						InDrawDown = True

						StartNAV = lastNAV
						TroughNAV = thisNAV
						DrawDownStartDate = DateSeries(IndexCounter)
						DrawDownPeakDate = DateSeries(IndexCounter)
						DrawDownFinishDate = DateSeries(IndexCounter)

						DrawDownCount = 0
						RecoveryCount = (-1) ' 0
					End If
				End If

				If (InDrawDown) Then
					RecoveryCount += 1

					If (thisNAV >= StartNAV) OrElse (IndexCounter = (NAVSeries.Length - 1)) Then

						' DrawDown Finished, or series finished
						If (thisNAV < StartNAV) Then
							DrawDownCount += 1
						End If

						If (IndexCounter = (NAVSeries.Length - 1)) Then
							If (thisNAV < TroughNAV) Then
								TroughNAV = thisNAV
								DrawDownPeakDate = DateSeries(IndexCounter)
								RecoveryCount = 0
							End If
						End If

						DrawDownFinishDate = DateSeries(IndexCounter)

						thisDrawDown = New DrawDownInstanceClass(StatsDatePeriod)
						thisDrawDown.ScalingFactor = pScalingFactor

						thisDrawDown.DateFrom = DrawDownStartDate
						thisDrawDown.DateTrough = DrawDownPeakDate
						thisDrawDown.DateTo = DrawDownFinishDate
						If (StartNAV <> 0) Then
							thisDrawDown.DrawDown = 1.0 - (TroughNAV / StartNAV)
						End If

						If (IndexCounter = (NAVSeries.Length - 1)) And (thisNAV < StartNAV) Then
							thisDrawDown.RecoveryOngoing = True
						Else
							thisDrawDown.RecoveryOngoing = False
						End If

						DrawDownArrayList.Add(thisDrawDown)
						InDrawDown = False
					Else

						' Drawdown is ongoing.

						DrawDownCount += 1

						If (thisNAV < TroughNAV) Then
							TroughNAV = thisNAV
							DrawDownPeakDate = DateSeries(IndexCounter)
							RecoveryCount = 0
						End If

					End If

				End If

				lastNAV = thisNAV
			Next
		End If

		If (DrawDownArrayList.Count > 0) Then
			ReDim RVal(DrawDownArrayList.Count - 1)
			For IndexCounter = 0 To (DrawDownArrayList.Count - 1)
				RVal(IndexCounter) = CType(DrawDownArrayList(IndexCounter), DrawDownInstanceClass)
			Next
		End If

		If (RVal.Length > 1) Then
			Array.Sort(RVal)
		End If

		' Add to cache

		SyncLock _StatsCache
			If _DrawDownCache.ContainsKey(CacheKey) Then
				_DrawDownCache.Remove(CacheKey)
			End If

			_DrawDownCache.Add(CacheKey, RVal)
		End SyncLock

		Return RVal

	End Function

	Public Function GetCurrentDrawdown(ByVal StatsDatePeriod As DealingPeriod, ByVal pPertracID As ULong, ByVal MatchBackfillVol As Boolean, ByRef eMessage As String) As Double

		Return GetCurrentDrawdown(StatsDatePeriod, pPertracID, MatchBackfillVol, Renaissance_BaseDate, Renaissance_EndDate_Data, 1.0#, eMessage)

	End Function

	Public Function GetCurrentDrawdown(ByVal StatsDatePeriod As DealingPeriod, ByVal pPertracID As ULong, ByVal MatchBackfillVol As Boolean, ByVal StartDate As Date, ByVal EndDate As Date, ByVal pScalingFactor As Double, ByRef eMessage As String) As Double
		' **************************************************************************************
		'
		'
		' **************************************************************************************

		Dim RVal As Double = 0.0#

		Try

			If (pPertracID <= 0) Then
				Return RVal
			End If

			Dim DateSeries() As Date
			Dim ReturnSeries() As Double

			Dim IndexCounter As Integer
			Dim thisReturn As Double

			DateSeries = Me.DateSeries(StatsDatePeriod, pPertracID, MatchBackfillVol, AnnualPeriodCount(StatsDatePeriod), 1, False, StartDate, EndDate, eMessage)
			ReturnSeries = Me.ReturnSeries(StatsDatePeriod, pPertracID, MatchBackfillVol, AnnualPeriodCount(StatsDatePeriod), 1, False, StartDate, EndDate, pScalingFactor, eMessage)

			If (ReturnSeries IsNot Nothing) AndAlso (DateSeries IsNot Nothing) Then

				Dim CumulativeLogReturn As Double
				Dim MinimusCumulativeLogReturn As Double
				Dim LastDate As Date

				CumulativeLogReturn = 0
				MinimusCumulativeLogReturn = 0
				LastDate = #1/1/1900#

				Try
					For IndexCounter = (ReturnSeries.Length - 1) To 0 Step -1
						thisReturn = ReturnSeries(IndexCounter)

						CumulativeLogReturn += Math.Log(1 + thisReturn)
						If (CumulativeLogReturn < MinimusCumulativeLogReturn) Then
							MinimusCumulativeLogReturn = CumulativeLogReturn
						End If

					Next
				Catch ex As Exception
				End Try

				RVal = Math.Exp(MinimusCumulativeLogReturn)

			End If

		Catch ex As Exception
		End Try

		Return RVal

	End Function

	Public Function GetDrawUpDetails(ByVal StatsDatePeriod As DealingPeriod, ByVal pPertracID As ULong, ByVal MatchBackfillVol As Boolean, ByRef eMessage As String) As DrawDownInstanceClass()

		Return GetDrawUpDetails(StatsDatePeriod, pPertracID, MatchBackfillVol, Renaissance_BaseDate, Renaissance_EndDate_Data, 1.0#, eMessage)

	End Function

	Public Function GetDrawUpDetails(ByVal StatsDatePeriod As DealingPeriod, ByVal pPertracID As ULong, ByVal MatchBackfillVol As Boolean, ByVal StartDate As Date, ByVal EndDate As Date, ByVal pScalingFactor As Double, ByRef eMessage As String) As DrawDownInstanceClass()
		' **************************************************************************************
		'
		'
		' **************************************************************************************

		Dim RVal(-1) As DrawDownInstanceClass

		If (pPertracID <= 0) Then
			Return RVal
		End If

		Dim DrawUpArrayList As New ArrayList()
		Dim thisDrawUp As DrawDownInstanceClass

		Dim DateSeries() As Date
		Dim NAVSeries() As Double

		Dim IndexCounter As Integer
		Dim DrawUpStartDate As Date
		Dim DrawUpFinishDate As Date
		Dim thisNAV As Double
		Dim lastNAV As Double
		Dim StartNAV As Double
		Dim PeakNAV As Double
		Dim DrawUpCount As Integer
		Dim InDrawUp As Boolean

		Try

			DateSeries = Me.DateSeries(StatsDatePeriod, pPertracID, MatchBackfillVol, AnnualPeriodCount(StatsDatePeriod), 1, False, StartDate, EndDate, eMessage)
			NAVSeries = Me.NAVSeries(StatsDatePeriod, pPertracID, MatchBackfillVol, AnnualPeriodCount(StatsDatePeriod), 1, False, StartDate, EndDate, pScalingFactor, eMessage)

			If (NAVSeries IsNot Nothing) AndAlso (DateSeries IsNot Nothing) Then
				If (NAVSeries.Length) > 0 Then
					lastNAV = NAVSeries(0)
				End If
				InDrawUp = False

				For IndexCounter = 1 To (NAVSeries.Length - 1)
					thisNAV = NAVSeries(IndexCounter)

					If (InDrawUp) Then

						If (thisNAV < lastNAV) OrElse (IndexCounter = (NAVSeries.Length - 1)) Then

							' DrawUp Finished, or series finished

							If (IndexCounter = (NAVSeries.Length - 1)) AndAlso (thisNAV > PeakNAV) Then
								PeakNAV = thisNAV
							End If

							DrawUpFinishDate = DateSeries(IndexCounter)

							thisDrawUp = New DrawDownInstanceClass(StatsDatePeriod)
							thisDrawUp.ScalingFactor = pScalingFactor

							thisDrawUp.DateFrom = DrawUpStartDate
							thisDrawUp.DateTrough = DrawUpStartDate
							thisDrawUp.DateTo = DrawUpFinishDate

							If (StartNAV <> 0) Then
								thisDrawUp.DrawUp = (PeakNAV / StartNAV) - 1.0
							End If

							If (IndexCounter = (NAVSeries.Length - 1)) And (thisNAV >= lastNAV) Then
								thisDrawUp.RecoveryOngoing = True
							Else
								thisDrawUp.RecoveryOngoing = False
							End If

							DrawUpArrayList.Add(thisDrawUp)
							InDrawUp = False
						Else

							' DrawUp is ongoing.

							DrawUpCount += 1

							If (thisNAV > PeakNAV) Then
								PeakNAV = thisNAV
							End If

						End If

					Else
						If (thisNAV > lastNAV) Then
							InDrawUp = True

							StartNAV = lastNAV
							PeakNAV = thisNAV
							DrawUpStartDate = DateSeries(IndexCounter)
							DrawUpFinishDate = DateSeries(IndexCounter)

							DrawUpCount = 1
						End If
					End If

					lastNAV = thisNAV
				Next
			End If

			If (DrawUpArrayList.Count > 0) Then
				ReDim RVal(DrawUpArrayList.Count - 1)
				For IndexCounter = 0 To (DrawUpArrayList.Count - 1)
					RVal(IndexCounter) = CType(DrawUpArrayList(IndexCounter), DrawDownInstanceClass)
				Next
			End If

			If (RVal.Length > 1) Then
				Array.Sort(RVal)
			End If

		Catch ex As Exception
		End Try

		Return RVal

	End Function

#End Region

#Region " Date Mapping Return Function"

	Public Function ReturnFittedDataArray(ByVal StatsDatePeriod As DealingPeriod, ByVal pSourceDateArray() As Date, ByVal DataArray As Array, ByVal pDataItem As DataItemEnum, ByVal pStatsStartDate As Date, ByVal pStatsEndDate As Date, ByVal pScalingFactor As Double, ByVal NormaliseNAVs As Boolean, ByVal InitialNormalisationValue As Double) As System.Array
		' **********************************************************************************************
		'
		'
		' **********************************************************************************************

		Dim RVal As System.Array

		Dim StatsStartDate As Date
		Dim StatsEndDate As Date
		Dim StartDate As Date
		Dim StartIndex As Integer
		Dim EndDate As Date
		Dim EndIndex As Integer
		Dim ScalingRequired As Boolean = False

		If (pSourceDateArray Is Nothing) OrElse (pSourceDateArray.Length <= 0) Then
			Return Nothing
		End If

		If (Math.Abs(pScalingFactor - 1.0#) > EPSILON) Then
			ScalingRequired = True
		End If

		Try
			StatsStartDate = pStatsStartDate
			StatsEndDate = pStatsEndDate
			If (StatsStartDate > pStatsEndDate) Then
				StatsStartDate = pStatsEndDate
				StatsEndDate = pStatsStartDate
			End If

			If (StatsStartDate < pSourceDateArray(0)) Then
				StartDate = pSourceDateArray(0)
				StartIndex = 0
			Else
				StartDate = StatsStartDate
				StartIndex = GetPriceIndex(StatsDatePeriod, pSourceDateArray(0), StatsStartDate)
			End If

			If (StatsEndDate >= pSourceDateArray(pSourceDateArray.Length - 1)) Then
				EndDate = pSourceDateArray(pSourceDateArray.Length - 1)
				EndIndex = pSourceDateArray.Length - 1
			Else
				EndDate = StatsEndDate
				EndIndex = GetPriceIndex(StatsDatePeriod, pSourceDateArray(0), StatsEndDate)
				If (EndIndex >= (pSourceDateArray.Length - 1)) Then
					EndIndex = pSourceDateArray.Length - 1
				End If
			End If

			If (StartIndex >= pSourceDateArray.Length) OrElse (EndIndex < StartIndex) Then
				Return Nothing
			End If

			' Return Whole array ?
			If (StartIndex = 0) AndAlso (EndIndex = (DataArray.Length - 1)) AndAlso (Not ScalingRequired) AndAlso ((NormaliseNAVs = False) Or (pDataItem <> DataItemEnum.NAVs)) Then
				Return DataArray
			End If

			' Isolate Data Array
			Select Case pDataItem
				Case DataItemEnum.Dates
					RVal = Array.CreateInstance(GetType(Date), (EndIndex - StartIndex) + 1)

				Case Else
					RVal = Array.CreateInstance(GetType(Double), (EndIndex - StartIndex) + 1)

			End Select

			Array.ConstrainedCopy(DataArray, StartIndex, RVal, 0, (EndIndex - StartIndex) + 1)

			If (ScalingRequired) AndAlso (pDataItem <> DataItemEnum.Dates) Then
				If (pDataItem = DataItemEnum.NAVs) Then
					Dim StartNAV As Double
					Dim ThisOriginalNAV As Double
					Dim ThisNewNAV As Double
					Dim LastOriginalNAV As Double
					Dim LastNewNAV As Double
					Dim ArrayIndex As Integer

					StartNAV = CDbl(RVal.GetValue(0))
					LastOriginalNAV = StartNAV
					LastNewNAV = StartNAV

					' Scale NAV 
					' Scale NAVs by re-creating the returns and then scaling them.

					If (StartNAV <> 0) Then
						For ArrayIndex = 1 To (RVal.Length - 1)
							ThisOriginalNAV = CDbl(RVal.GetValue(ArrayIndex))

							ThisNewNAV = LastNewNAV * ((((ThisOriginalNAV / LastOriginalNAV) - 1.0#) * pScalingFactor) + 1.0#)
							LastNewNAV = ThisNewNAV
							LastOriginalNAV = ThisOriginalNAV

							RVal.SetValue(ThisNewNAV, ArrayIndex)
						Next
					End If

				Else

					' It is a numeric array and Not NAVs, scale it.

					Dim ArrayIndex As Integer

					For ArrayIndex = 0 To (RVal.Length - 1)
						RVal.SetValue((CDbl(RVal.GetValue(ArrayIndex)) * pScalingFactor), ArrayIndex)
					Next

				End If

			End If

		Catch ex As Exception
			Return Nothing
		End Try

		' Normalise NAVs back to 'InitialNormalisationValue'.

		Try
			If (pDataItem = DataItemEnum.NAVs) AndAlso (NormaliseNAVs) Then
				Try
					' 
					'If (RVal(0) <> 100) AndAlso (RVal(0) <> 0) Then
					If (CDbl(RVal.GetValue(0)) <> InitialNormalisationValue) AndAlso (CDbl(RVal.GetValue(0)) <> 0.0#) Then
						Dim ArrayIndex As Integer
						Dim Divisor As Double

						Divisor = CDbl(RVal.GetValue(0)) / InitialNormalisationValue

						For ArrayIndex = 1 To (RVal.Length - 1)
							RVal.SetValue((CDbl(RVal.GetValue(ArrayIndex)) / Divisor), ArrayIndex)
							' RVal(ArrayIndex) = (RVal(ArrayIndex) / Divisor)
						Next

						' RVal(0) = 100
						RVal.SetValue(InitialNormalisationValue, 0)

					End If
				Catch ex As Exception
				End Try
			End If
		Catch ex As Exception
			Return Nothing
		End Try

		Return RVal

	End Function

	Public Function ReturnFittedDeltasArray(ByVal StatsDatePeriod As DealingPeriod, ByVal pSourceDateArray() As Date, ByVal SourceDeltasArray() As NetDeltaItemClass, ByVal pStatsStartDate As Date, ByVal pStatsEndDate As Date, ByVal pScalingFactor As Double, ByVal NormaliseNAVs As Boolean, ByVal InitialNormalisationValue As Double) As NetDeltaItemClass()
		' **********************************************************************************************
		'
		'
		' **********************************************************************************************

		Dim RVal() As NetDeltaItemClass

		Dim StatsStartDate As Date
		Dim StatsEndDate As Date
		Dim StartDate As Date
		Dim StartIndex As Integer
		Dim EndDate As Date
		Dim EndIndex As Integer
		Dim ScalingRequired As Boolean = False

		If (pSourceDateArray Is Nothing) OrElse (pSourceDateArray.Length <= 0) OrElse (SourceDeltasArray Is Nothing) OrElse (SourceDeltasArray.Length <= 0) Then
			Return Nothing
		End If

		If (Math.Abs(pScalingFactor - 1.0#) > EPSILON) Then
			ScalingRequired = True
		End If

		Try
			StatsStartDate = pStatsStartDate
			StatsEndDate = pStatsEndDate
			If (StatsStartDate > pStatsEndDate) Then
				StatsStartDate = pStatsEndDate
				StatsEndDate = pStatsStartDate
			End If

			If (StatsStartDate < pSourceDateArray(0)) Then
				StartDate = pSourceDateArray(0)
				StartIndex = 0
			Else
				StartDate = StatsStartDate
				StartIndex = GetPriceIndex(StatsDatePeriod, pSourceDateArray(0), StatsStartDate)
			End If

			If (StatsEndDate >= pSourceDateArray(pSourceDateArray.Length - 1)) Then
				EndDate = pSourceDateArray(pSourceDateArray.Length - 1)
				EndIndex = pSourceDateArray.Length - 1
			Else
				EndDate = StatsEndDate
				EndIndex = GetPriceIndex(StatsDatePeriod, pSourceDateArray(0), StatsEndDate)
				If (EndIndex >= (pSourceDateArray.Length - 1)) Then
					EndIndex = pSourceDateArray.Length - 1
				End If
			End If

			If (StartIndex >= pSourceDateArray.Length) OrElse (EndIndex < StartIndex) Then
				Return Nothing
			End If

			' Return Whole array ?
			If (StartIndex = 0) AndAlso (EndIndex = (SourceDeltasArray(0).DeltaArray.Length - 1)) AndAlso (Not ScalingRequired) Then
				Return SourceDeltasArray
			End If

			RVal = CType(Array.CreateInstance(GetType(NetDeltaItemClass), SourceDeltasArray.Length), NetDeltaItemClass())

			For InstrumentIndex As Integer = 0 To (SourceDeltasArray.Length - 1)
				RVal(InstrumentIndex) = New NetDeltaItemClass(SourceDeltasArray(InstrumentIndex).PertracId, CType(Array.CreateInstance(GetType(Double), (EndIndex - StartIndex) + 1), Double()))
				Array.ConstrainedCopy(SourceDeltasArray(InstrumentIndex).DeltaArray, StartIndex, RVal(InstrumentIndex).DeltaArray, 0, (EndIndex - StartIndex) + 1)
			Next

			If (ScalingRequired) Then

				Dim ArrayIndex As Integer

				For InstrumentIndex As Integer = 0 To (RVal.Length - 1)
					For ArrayIndex = 0 To (RVal(InstrumentIndex).DeltaArray.Length - 1)
						RVal(InstrumentIndex).DeltaArray(ArrayIndex) *= pScalingFactor
					Next
				Next

			End If

		Catch ex As Exception
			Return Nothing
		End Try

		Return RVal

	End Function

#End Region

#Region " StdDev / Average / Lease Squares functions"

	Private Function StandardDeviation(ByVal Returns() As Double, ByVal StartIndex As Integer, ByVal EndIndex As Integer, ByVal SampleStDev As Boolean) As Double
		' **********************************************************************************************
		' Function to return the Standard Deviation of a section of a Double() array.
		'
		' **********************************************************************************************

		Dim RVal As Double = 0

		Try

			If (Returns Is Nothing) Then
				Return RVal
			End If

			If (StartIndex > EndIndex) Then
				Dim Temp As Integer

				Temp = StartIndex
				StartIndex = EndIndex
				EndIndex = Temp
			End If

			If (StartIndex < LBound(Returns)) Then
				StartIndex = LBound(Returns)
			End If

			If (EndIndex > UBound(Returns)) Then
				EndIndex = UBound(Returns)
			End If

			If ((EndIndex - StartIndex) < MIN_Periods_For_Stats) Then
				Return RVal
			End If

			' OK, Give it a go.

			Dim Sum_X As Double
			Dim AvgX As Double
			Dim Sum_X2 As Double
			Dim IndexCounter As Integer
			Dim ReturnCount As Integer

			' Average
			ReturnCount = 0
			Sum_X = 0
			For IndexCounter = StartIndex To EndIndex
				Sum_X += Returns(IndexCounter)
				ReturnCount += 1
			Next

			AvgX = (Sum_X / ReturnCount)

			Sum_X2 = 0
			For IndexCounter = StartIndex To EndIndex
				Sum_X2 += (Returns(IndexCounter) - AvgX) ^ 2
			Next

			If (SampleStDev) Then
				' Sample StDev
				Return Math.Sqrt(Sum_X2 / CDbl(ReturnCount - 1))
			Else
				' Population StDev
				Return Math.Sqrt(Sum_X2 / CDbl(ReturnCount))
			End If

		Catch ex As Exception
		End Try

		Return RVal

	End Function

	Private Function SeriesAverage(ByVal Returns() As Double, ByVal StartIndex As Integer, ByVal EndIndex As Integer) As Double
		' **********************************************************************************************
		' Function to return the average of a section of a Double() array.
		'
		' **********************************************************************************************

		Dim RVal As Double = 0

		Try

			If (Returns Is Nothing) Then
				Return RVal
			End If

			If (StartIndex > EndIndex) Then
				Dim Temp As Integer

				Temp = StartIndex
				StartIndex = EndIndex
				EndIndex = Temp
			End If

			If (StartIndex < LBound(Returns)) Then
				StartIndex = LBound(Returns)
			End If

			If (EndIndex > UBound(Returns)) Then
				EndIndex = UBound(Returns)
			End If

			If ((EndIndex - StartIndex) < MIN_Periods_For_Stats) Then
				Return RVal
			End If

			' OK, Give it a go.

			Dim Sum_X As Double
			Dim AvgX As Double
			Dim IndexCounter As Integer
			Dim ReturnCount As Integer

			' Average

			ReturnCount = 0
			Sum_X = 0
			For IndexCounter = StartIndex To EndIndex
				Sum_X += Returns(IndexCounter)
				ReturnCount += 1
			Next

			AvgX = (Sum_X / CDbl(ReturnCount))

			RVal = AvgX

		Catch ex As Exception
		End Try

		Return RVal

	End Function

	Public Function LeastSquares(ByVal FirstDataSet() As Double, ByVal SecondDataSet() As Double, ByVal FirstStartIndex As Integer, ByVal SecondStartIndex As Integer, ByVal ObservationCount As Integer, ByVal NormaliseData As Boolean) As Double
		' **********************************************************************************************
		' Simple Least Squares Function.
		' 
		' Return value is an average so that function return values between different duration series 
		' are somewhat comparable.
		' **********************************************************************************************
		Dim RVal As Double = 0.0#

		Try
			' Validate

			If (FirstStartIndex < 0) OrElse (SecondStartIndex < 0) OrElse (ObservationCount <= 0) Then
				Return 0.0#
			End If

			If (FirstDataSet Is Nothing) OrElse (FirstDataSet.Length <= 0) OrElse (FirstDataSet.Length < FirstStartIndex) Then
				Return RVal
			End If

			If (SecondDataSet Is Nothing) OrElse (SecondDataSet.Length <= 0) OrElse (SecondDataSet.Length < SecondStartIndex) Then
				Return RVal
			End If

			Dim SumOfResiduals As Double = 0.0#
			Dim ThisObservation As Integer
			Dim FirstIndex As Integer = FirstStartIndex
			Dim SecondIndex As Integer = SecondStartIndex
			Dim IterationCount As Integer = 0

			For ThisObservation = 0 To ObservationCount
				If (FirstIndex >= FirstDataSet.Length) OrElse (SecondIndex >= SecondDataSet.Length) Then
					Exit For
				End If

				If (NormaliseData) Then
					SumOfResiduals += ((FirstDataSet(FirstIndex) / FirstDataSet(FirstStartIndex)) - (SecondDataSet(SecondIndex) / SecondDataSet(SecondStartIndex))) ^ 2.0#
				Else
					SumOfResiduals += (FirstDataSet(FirstIndex) - SecondDataSet(SecondIndex)) ^ 2.0#
				End If

				IterationCount += 1
				FirstIndex += 1
				SecondIndex += 1
			Next

			RVal = SumOfResiduals / CDbl(IterationCount)

		Catch ex As Exception
		End Try

		Return RVal
	End Function


#End Region

#Region " Bespoke facilitation function"

	Public Function GetMonthlyReturnsDataSet(ByVal pPertracID As ULong, ByRef pDestinationTable As DataTable, ByRef eMessage As String) As DataTable
		' ****************************************************************************
		' Construct a returns dataset based on the returns data for the given instrument.
		'
		' This function used to use the pertrac data directly, but multiple returns for any
		' month would distort the YTD returns.
		'
		' Facilitates 'GetReturnsDataSet()' function in the Report Handler.
		'
		' Hard coded to get Monthly returns.
		'
		' ****************************************************************************

		Dim RVal As DataTable = pDestinationTable
		Dim RValRow As DataRow = Nothing

		Dim ThisStatsItem As StatFunctions.StatCacheClass = Nothing

		Dim DateCounter As Integer
		Dim ThisDate As Date
		Dim LastYear As Integer = (-1)
		Dim ThisReturn As Double
		Dim YTD_Sum As Double = 1

		Try
			If (pPertracID > 0) Then
				ThisStatsItem = BuildNewStatSeries(DealingPeriod.Monthly, pPertracID, False, AnnualPeriodCount(DealingPeriod.Monthly), 1, eMessage)

				If (ThisStatsItem IsNot Nothing) AndAlso (ThisStatsItem.DateArray.Length > 0) Then

					For DateCounter = 0 To (ThisStatsItem.DateArray.Length - 1)	' Each thisRow In SortedRows

						ThisDate = ThisStatsItem.DateArray(DateCounter)
						ThisReturn = ThisStatsItem.ReturnsArray(DateCounter)

						If (ThisDate.Year <> LastYear) Then
							RValRow = RVal.NewRow
							RValRow("MonYear") = ThisDate.Year
							YTD_Sum = 1

							RVal.Rows.Add(RValRow)
							LastYear = ThisDate.Year
						End If

						YTD_Sum *= (1.0 + ThisReturn)

						RValRow("Mon" & ThisDate.ToString("MMM")) = ThisReturn
						RValRow("MonYTD") = YTD_Sum - 1.0

					Next

				End If
			End If

		Catch ex As Exception
		Finally
			If (ThisStatsItem IsNot Nothing) Then
				ThisStatsItem.Reset()
				ThisStatsItem = Nothing
			End If
		End Try

		Return RVal

	End Function

	Public Function GetPeriodsFromMonth(ByVal StatsDatePeriod As DealingPeriod, ByVal pMonths As Double) As Integer
		Return CInt(Math.Round((AnnualPeriodCount(StatsDatePeriod) * pMonths) / 12.0#))
	End Function

#End Region

#Region " Temp Compatability interface functions"

	Public Function ReturnSeries(ByVal StatsDatePeriod As DealingPeriod, ByVal pPertracID As ULong, ByVal MatchBackfillVol As Boolean, ByVal PeriodCount As Integer, ByVal pLamda As Double, ByVal Annualised As Boolean, ByVal pStatsStartDate As Date, ByVal pStatsEndDate As Date, ByRef eMessage As String) As Double()
		' *****************************************************************************
		'
		' *****************************************************************************

		Return ReturnSeries_Contingent(StatsDatePeriod, pPertracID, 0, MatchBackfillVol, ContingentSelect.ConditionAll, PeriodCount, pLamda, Annualised, pStatsStartDate, pStatsEndDate, 1.0#, eMessage)

	End Function

	Public Function ReturnSeries_Contingent(ByVal StatsDatePeriod As DealingPeriod, ByVal pPertracID As ULong, ByVal pConditionalPertracID As ULong, ByVal MatchBackfillVol As Boolean, ByVal pCondition As ContingentSelect, ByVal PeriodCount As Integer, ByVal pLamda As Double, ByVal Annualised As Boolean, ByVal pStatsStartDate As Date, ByVal pStatsEndDate As Date, ByRef eMessage As String) As Double()
		' *****************************************************************************
		'
		' *****************************************************************************

		Return ReturnSeries_Contingent(StatsDatePeriod, pPertracID, 0, MatchBackfillVol, ContingentSelect.ConditionAll, PeriodCount, pLamda, Annualised, pStatsStartDate, pStatsEndDate, 1.0#, eMessage)

	End Function

	Public Function AverageSeries(ByVal StatsDatePeriod As DealingPeriod, ByVal pPertracID As ULong, ByVal MatchBackfillVol As Boolean, ByVal PeriodCount As Integer, ByVal pLamda As Double, ByVal Annualised As Boolean, ByVal pStatsStartDate As Date, ByVal pStatsEndDate As Date, ByRef eMessage As String) As Double()
		' *****************************************************************************
		'
		' *****************************************************************************

		Return AverageSeries_Contingent(StatsDatePeriod, pPertracID, 0, MatchBackfillVol, ContingentSelect.ConditionAll, PeriodCount, pLamda, Annualised, pStatsStartDate, pStatsEndDate, 1.0#, eMessage)

	End Function

	Public Function AverageSeries_Contingent(ByVal StatsDatePeriod As DealingPeriod, ByVal pPertracID As ULong, ByVal pConditionalPertracID As ULong, ByVal MatchBackfillVol As Boolean, ByVal pCondition As ContingentSelect, ByVal PeriodCount As Integer, ByVal pLamda As Double, ByVal Annualised As Boolean, ByVal pStatsStartDate As Date, ByVal pStatsEndDate As Date, ByRef eMessage As String) As Double()
		' *****************************************************************************
		'
		' *****************************************************************************

		Return AverageSeries_Contingent(StatsDatePeriod, pPertracID, 0, MatchBackfillVol, ContingentSelect.ConditionAll, PeriodCount, pLamda, Annualised, pStatsStartDate, pStatsEndDate, 1.0#, eMessage)

	End Function

	Public Function NAVSeries(ByVal StatsDatePeriod As DealingPeriod, ByVal pPertracID As ULong, ByVal MatchBackfillVol As Boolean, ByVal pPeriodCount As Integer, ByVal pLamda As Double, ByVal Annualised As Boolean, ByVal pStatsStartDate As Date, ByVal pStatsEndDate As Date, ByRef eMessage As String) As Double()
		' *****************************************************************************
		'
		' *****************************************************************************

		Return NAVSeries(StatsDatePeriod, pPertracID, MatchBackfillVol, pPeriodCount, pLamda, Annualised, pStatsStartDate, pStatsEndDate, 1.0#, True, 100.0#, eMessage)

	End Function

	Public Function NAVSeries(ByVal StatsDatePeriod As DealingPeriod, ByVal pPertracID As ULong, ByVal MatchBackfillVol As Boolean, ByVal PeriodCount As Integer, ByVal pLamda As Double, ByVal Annualised As Boolean, ByVal pStatsStartDate As Date, ByVal pStatsEndDate As Date, ByVal pScalingFactor As Double, ByRef eMessage As String) As Double()
		' *****************************************************************************
		'
		' *****************************************************************************

		Return NAVSeries(StatsDatePeriod, pPertracID, MatchBackfillVol, PeriodCount, pLamda, Annualised, pStatsStartDate, pStatsEndDate, pScalingFactor, True, 100.0#, eMessage)

	End Function

	Public Function NetDeltas(ByVal StatsDatePeriod As DealingPeriod, ByVal pPertracID As ULong, ByVal MatchBackfillVol As Boolean, ByVal PeriodCount As Integer, ByVal pLamda As Double, ByVal Annualised As Boolean, ByVal pStatsStartDate As Date, ByVal pStatsEndDate As Date, ByVal pLookthrough As Boolean, ByRef eMessage As String) As NetDeltaItemClass()
		' *****************************************************************************
		'
		' *****************************************************************************

		Return NetDeltas_Contingent(StatsDatePeriod, 0, pPertracID, 0, MatchBackfillVol, ContingentSelect.ConditionAll, PeriodCount, pLamda, Annualised, -1, pStatsStartDate, pStatsEndDate, LookthroughEnum.FullLookthrough, 1.0#, eMessage)

	End Function

	Public Function NetDeltas(ByVal StatsDatePeriod As DealingPeriod, ByVal pPertracID As ULong, ByVal MatchBackfillVol As Boolean, ByVal PeriodCount As Integer, ByVal pLamda As Double, ByVal Annualised As Boolean, ByVal pStatsStartDate As Date, ByVal pStatsEndDate As Date, ByVal pLookthrough As LookthroughEnum, ByRef eMessage As String) As NetDeltaItemClass()
		' *****************************************************************************
		'
		' *****************************************************************************

		Return NetDeltas_Contingent(StatsDatePeriod, 0, pPertracID, 0, MatchBackfillVol, ContingentSelect.ConditionAll, PeriodCount, pLamda, Annualised, -1, pStatsStartDate, pStatsEndDate, pLookthrough, 1.0#, eMessage)

	End Function

	Public Function NetDeltas_Contingent(ByVal StatsDatePeriod As DealingPeriod, ByVal pNestingLevel As Integer, ByVal pPertracID As ULong, ByVal pConditionalPertracID As ULong, ByVal MatchBackfillVol As Boolean, ByVal pCondition As ContingentSelect, ByVal PeriodCount As Integer, ByVal pLamda As Double, ByVal Annualised As Boolean, ByVal pSampleSize As Integer, ByVal pStatsStartDate As Date, ByVal pStatsEndDate As Date, ByVal Lookthrough As LookthroughEnum, ByVal pScalingFactor As Double, ByRef eMessage As String) As NetDeltaItemClass()
		' *****************************************************************************
		'
		'
		' *****************************************************************************

		Static DebugStatCache As StatCacheClass


		Dim Lamda As Double
		Dim thisDateArray() As Date
		Dim thisNetDeltaArray() As NetDeltaItemClass

		Dim RVal() As NetDeltaItemClass = Nothing

		Lamda = pLamda
		If (Lamda < 0) Then
			Lamda = 1
		End If


		Try
			SyncLock Lock_StdDevSeries
				Dim BaseStatCache As StatCacheClass
				Dim ConditionalStatCache As StatCacheClass

				Try
					' Get Existing Pertrac Series ?
					BaseStatCache = Me.GetStatCacheItem(StatsDatePeriod, pPertracID, MatchBackfillVol)

					If (BaseStatCache Is Nothing) Then
						BaseStatCache = BuildNewStatSeries(StatsDatePeriod, pNestingLevel, pPertracID, MatchBackfillVol, PeriodCount, Lamda, pSampleSize, False, RenaissanceGlobals.Globals.KNOWLEDGEDATE_NOW, eMessage)
						SetStatCacheItem(BaseStatCache)

					End If
				Catch ex As Exception
					BaseStatCache = Nothing
				End Try

				If (BaseStatCache Is Nothing) Then
					Return Nothing
				End If


				thisDateArray = BaseStatCache.DateArray
				thisNetDeltaArray = BaseStatCache.NetDeltaArray

				If False Then
					DebugStatCache = BaseStatCache
				End If

				' Look Through...

				If (Lookthrough <> LookthroughEnum.None) AndAlso (pNestingLevel < 10) Then
					' ********************************************************************************
					' In order to process the lookthrough condition, I intend to work through each item
					' in the Net Deltas array and (if necessary) to Substitute it for the underlying
					' Net Deltas.
					'
					' This Code assumes that the NetDelta Property of the StatsObject returns a copy of the 
					' underlying object Array (Copy of the array of NetDeltaItemClass, not the NetDeltaItemClass objects themselves), 
					' not the original. In some cases items of the returned array are overwritten !
					'
					' ********************************************************************************

					Dim AllClear As Boolean = False

					Dim BasePertracID As Integer
					Dim BackfillPertracID As Integer
					Dim BaseInstrumentType As PertracDataClass.PertracInstrumentFlags
					Dim BackfillInstrumentType As PertracDataClass.PertracInstrumentFlags

					Dim SubNetDeltaArray() As NetDeltaItemClass
					Dim tmpNetDeltaArray() As NetDeltaItemClass
					Dim ThisNetDeltaItem As NetDeltaItemClass
					Dim SubDateArray() As Date
					Dim SubInstrumentIndex As Integer
					Dim SubDateIndex As Integer
					Dim SubDateIndexOffset As Integer
					Dim DoDrillDown As Boolean

					While (Not AllClear)
						AllClear = True

						For InstrumentIndex As Integer = (thisNetDeltaArray.Length - 1) To 0 Step -1

							If (thisNetDeltaArray(InstrumentIndex) IsNot Nothing) Then

								DoDrillDown = False

								BasePertracID = CInt(thisNetDeltaArray(InstrumentIndex).PertracId And Ulong_31BitMask) ' Don't use Ulong_32BitMask as the 32nd bit is the sign bit for a signed integer.
								BackfillPertracID = CInt((thisNetDeltaArray(InstrumentIndex).PertracId >> 32) And Ulong_31BitMask)

								BaseInstrumentType = PertracDataClass.GetFlagsFromID(BasePertracID)
								BackfillInstrumentType = PertracDataClass.GetFlagsFromID(BackfillPertracID)

								Select Case Lookthrough

									Case LookthroughEnum.FullLookthrough

										If ((BasePertracID > 0) AndAlso ((BaseInstrumentType <> PertracDataClass.PertracInstrumentFlags.PertracInstrument) And (BaseInstrumentType <> PertracDataClass.PertracInstrumentFlags.VeniceInstrument))) OrElse _
										 ((BackfillPertracID > 0) AndAlso ((BackfillInstrumentType <> PertracDataClass.PertracInstrumentFlags.PertracInstrument) And (BackfillInstrumentType <> PertracDataClass.PertracInstrumentFlags.VeniceInstrument))) Then
											DoDrillDown = True
										End If

									Case LookthroughEnum.JustLookthroughGroups

										If ((BasePertracID > 0) AndAlso ((BaseInstrumentType = PertracDataClass.PertracInstrumentFlags.Group_Mean) Or (BaseInstrumentType = PertracDataClass.PertracInstrumentFlags.Group_Median) Or (BaseInstrumentType = PertracDataClass.PertracInstrumentFlags.Group_Weighted))) OrElse _
										 ((BackfillPertracID > 0) AndAlso ((BackfillInstrumentType = PertracDataClass.PertracInstrumentFlags.Group_Mean) Or (BackfillInstrumentType = PertracDataClass.PertracInstrumentFlags.Group_Median) Or (BackfillInstrumentType = PertracDataClass.PertracInstrumentFlags.Group_Weighted))) Then
											DoDrillDown = True
										End If

									Case LookthroughEnum.LookthroughToSimulations

										If ((BasePertracID > 0) AndAlso ((BaseInstrumentType <> PertracDataClass.PertracInstrumentFlags.PertracInstrument) And (BaseInstrumentType <> PertracDataClass.PertracInstrumentFlags.VeniceInstrument) And (BaseInstrumentType <> PertracDataClass.PertracInstrumentFlags.CTA_Simulation))) OrElse _
										 ((BackfillPertracID > 0) AndAlso ((BackfillInstrumentType <> PertracDataClass.PertracInstrumentFlags.PertracInstrument) And (BackfillInstrumentType <> PertracDataClass.PertracInstrumentFlags.VeniceInstrument) And (BackfillInstrumentType <> PertracDataClass.PertracInstrumentFlags.CTA_Simulation))) Then
											DoDrillDown = True
										End If

								End Select

								If (DoDrillDown) Then

									AllClear = False

									Try
										' pNestingLevel

										SubDateArray = DateSeries(StatsDatePeriod, pNestingLevel + 1, thisNetDeltaArray(InstrumentIndex).PertracId, MatchBackfillVol, PeriodCount, pLamda, pSampleSize, Annualised, pStatsStartDate, pStatsEndDate, eMessage)
										SubNetDeltaArray = NetDeltas_Contingent(StatsDatePeriod, pNestingLevel + 1, thisNetDeltaArray(InstrumentIndex).PertracId, pConditionalPertracID, MatchBackfillVol, pCondition, PeriodCount, pLamda, Annualised, pSampleSize, pStatsStartDate, pStatsEndDate, Lookthrough, pScalingFactor, eMessage)

										' Merge in New Rows

										If (SubNetDeltaArray.Length = 1) Then

											ThisNetDeltaItem = New NetDeltaItemClass(SubNetDeltaArray(0).PertracId, CType(Array.CreateInstance(GetType(Double), thisNetDeltaArray(InstrumentIndex).DeltaArray.Length), Double()))

											' Copy in the Delta information, taking Delta from original item also.

											SubDateIndexOffset = GetPriceIndex(StatsDatePeriod, SubDateArray(0), BaseStatCache.DateArray(0))

											For SubDateIndex = 0 To (ThisNetDeltaItem.DeltaArray.Length - 1)
												If ((SubDateIndex + SubDateIndexOffset) > 0) AndAlso ((SubDateIndex + SubDateIndexOffset) < SubNetDeltaArray(0).DeltaArray.Length) Then
													' Set Delta value, adjusted for the original Group delta.
													ThisNetDeltaItem.DeltaArray(SubDateIndex) = SubNetDeltaArray(0).DeltaArray(SubDateIndex + SubDateIndexOffset) * thisNetDeltaArray(InstrumentIndex).DeltaArray(SubDateIndex)
												Else
													ThisNetDeltaItem.DeltaArray(SubDateIndex) = 0.0#
												End If
											Next

											' Replace Original Entry

											thisNetDeltaArray(InstrumentIndex) = ThisNetDeltaItem

										Else

											' Dimension new Net Deltas array

											tmpNetDeltaArray = CType(Array.CreateInstance(GetType(NetDeltaItemClass), (thisNetDeltaArray.Length + SubNetDeltaArray.Length)), NetDeltaItemClass())

											' Copy in the existing items. 

											Array.Copy(thisNetDeltaArray, tmpNetDeltaArray, thisNetDeltaArray.Length)

											' Copy in the 'Drill Down' Data.

											For SubInstrumentIndex = 0 To (SubNetDeltaArray.Length - 1)
												' BaseStatCache

												ThisNetDeltaItem = New NetDeltaItemClass(SubNetDeltaArray(SubInstrumentIndex).PertracId, CType(Array.CreateInstance(GetType(Double), thisNetDeltaArray(InstrumentIndex).DeltaArray.Length), Double()))
												tmpNetDeltaArray(SubInstrumentIndex + thisNetDeltaArray.Length) = ThisNetDeltaItem

												' Copy in the Delta information

												SubDateIndexOffset = GetPriceIndex(StatsDatePeriod, SubDateArray(0), BaseStatCache.DateArray(0))

												For SubDateIndex = 0 To (ThisNetDeltaItem.DeltaArray.Length - 1)
													If ((SubDateIndex + SubDateIndexOffset) > 0) AndAlso ((SubDateIndex + SubDateIndexOffset) < SubNetDeltaArray(SubInstrumentIndex).DeltaArray.Length) Then
														' Set Delta value, adjusted for the original Group delta.
														ThisNetDeltaItem.DeltaArray(SubDateIndex) = SubNetDeltaArray(SubInstrumentIndex).DeltaArray(SubDateIndex + SubDateIndexOffset) * thisNetDeltaArray(InstrumentIndex).DeltaArray(SubDateIndex)
													Else
														ThisNetDeltaItem.DeltaArray(SubDateIndex) = 0.0#
													End If
												Next

											Next

											thisNetDeltaArray = tmpNetDeltaArray
											thisNetDeltaArray(InstrumentIndex) = Nothing
											tmpNetDeltaArray = Nothing

										End If

									Catch ex As Exception
									End Try

								End If

							End If
						Next

					End While

					' Consolidate Array.

					Try

						Dim ID_Dictionary As New Dictionary(Of ULong, Integer)
						Dim PertracIDs() As ULong

						' Generate a unique list of Pertrac IDs

						For InstrumentIndex As Integer = 0 To (thisNetDeltaArray.Length - 1)

							If (thisNetDeltaArray(InstrumentIndex) IsNot Nothing) Then

								If (Not ID_Dictionary.ContainsKey(thisNetDeltaArray(InstrumentIndex).PertracId)) Then
									ID_Dictionary.Add(thisNetDeltaArray(InstrumentIndex).PertracId, ID_Dictionary.Count)
								End If

							End If

						Next

						' If the number of pertrac IDs is less than the Number of entries in thisNetDeltaArray(), then Consolidation is necessary

						If (ID_Dictionary.Count < thisNetDeltaArray.Length) Then

							' Dimension Temporary Array.

							tmpNetDeltaArray = CType(Array.CreateInstance(GetType(NetDeltaItemClass), ID_Dictionary.Count), NetDeltaItemClass())

							' Get unique Pertrac IDs

							PertracIDs = CType(Array.CreateInstance(GetType(ULong), ID_Dictionary.Count), ULong())
							ID_Dictionary.Keys.CopyTo(PertracIDs, 0)

							' Consolidate entries for each Pertrac ID.

							For InstrumentIndex As Integer = 0 To (PertracIDs.Length - 1)
								ThisNetDeltaItem = Nothing

								For SubInstrumentIndex = 0 To (thisNetDeltaArray.Length - 1)

									If (thisNetDeltaArray(SubInstrumentIndex) IsNot Nothing) AndAlso (thisNetDeltaArray(SubInstrumentIndex).PertracId = PertracIDs(InstrumentIndex)) Then

										' If this is the First Item for the current Pertrac ID, then Copy the existing entry, otherwise add it in.

										If (ThisNetDeltaItem Is Nothing) Then
											ThisNetDeltaItem = New NetDeltaItemClass(thisNetDeltaArray(SubInstrumentIndex).PertracId, CType(Array.CreateInstance(GetType(Double), thisNetDeltaArray(SubInstrumentIndex).DeltaArray.Length), Double()))
											Array.Copy(thisNetDeltaArray(SubInstrumentIndex).DeltaArray, ThisNetDeltaItem.DeltaArray, ThisNetDeltaItem.DeltaArray.Length)
										Else
											' Add in Data 
											For SubDateIndex = 0 To (ThisNetDeltaItem.DeltaArray.Length - 1)
												ThisNetDeltaItem.DeltaArray(SubDateIndex) += thisNetDeltaArray(SubInstrumentIndex).DeltaArray(SubDateIndex)
											Next
										End If
									End If
								Next

								tmpNetDeltaArray(InstrumentIndex) = ThisNetDeltaItem
							Next

							thisNetDeltaArray = tmpNetDeltaArray
							tmpNetDeltaArray = Nothing

						End If

					Catch ex As Exception
					End Try

				End If

				' Quick Exit - All Returns Selected?

				If (pCondition = ContingentSelect.ConditionAll) Then
					Return ReturnFittedDeltasArray(StatsDatePeriod, thisDateArray, thisNetDeltaArray, pStatsStartDate, pStatsEndDate, pScalingFactor, False, 100.0#)
				End If

				Try
					' Get Existing Conditional Series ?
					ConditionalStatCache = Me.GetStatCacheItem(StatsDatePeriod, pConditionalPertracID, MatchBackfillVol)

					If (ConditionalStatCache Is Nothing) Then
						ConditionalStatCache = BuildNewStatSeries(StatsDatePeriod, pConditionalPertracID, MatchBackfillVol, PeriodCount, Lamda, eMessage)
						SetStatCacheItem(ConditionalStatCache)
					End If
				Catch ex As Exception
					ConditionalStatCache = Nothing
				End Try

				If (ConditionalStatCache Is Nothing) Then
					Return Nothing
				End If

				' Derive Partial Deltas series

				Dim ThisStartIndex As Integer = 0
				Dim CompareStartIndex As Integer = 0
				Dim ThisMAXIndex As Integer = (thisDateArray.Length - 1)
				Dim CompareMAXIndex As Integer = (ConditionalStatCache.DateArray.Length - 1)
				Dim thisConditionalArray() As Double = ConditionalStatCache.ReturnsArray

				' Dimension and Initialise NetDeltas Array.

				RVal = CType(Array.CreateInstance(GetType(NetDeltaItemClass), thisNetDeltaArray.Length), NetDeltaItemClass())

				For InstrumentIndex As Integer = 0 To (RVal.Length - 1)
					RVal(InstrumentIndex) = New NetDeltaItemClass(thisNetDeltaArray(InstrumentIndex).PertracId, CType(Array.CreateInstance(GetType(Double), thisDateArray.Length), Double()))
				Next

				'  thisDateArray.Length

				CompareStartIndex = GetPriceIndex(StatsDatePeriod, ConditionalStatCache.DateArray(0), thisDateArray(0))

				If (CompareStartIndex < 0) Then
					ThisStartIndex -= CompareStartIndex
					CompareStartIndex = 0
				End If

				While ((ThisStartIndex <= ThisMAXIndex) And (CompareStartIndex <= CompareMAXIndex))
					If pCondition = ContingentSelect.ConditionDown Then
						If (thisConditionalArray(CompareStartIndex) < 0) Then

							'If (thisStatCache.NetDeltaArrayIsNull) Then
							'  RVal(0).DeltaArray(ThisStartIndex) = 1.0#
							'Else
							'  For InstrumentIndex As Integer = 0 To (RVal.Length - 1)
							'    RVal(InstrumentIndex).DeltaArray(ThisStartIndex) = thisNetDeltaArray(InstrumentIndex).DeltaArray(ThisStartIndex)
							'  Next
							'End If

							For InstrumentIndex As Integer = 0 To (RVal.Length - 1)
								RVal(InstrumentIndex).DeltaArray(ThisStartIndex) = thisNetDeltaArray(InstrumentIndex).DeltaArray(ThisStartIndex)
							Next

						End If
					Else ' Condition Up
						If (thisConditionalArray(CompareStartIndex) > 0) Then

							'If (thisStatCache.NetDeltaArrayIsNull) Then
							'  RVal(0).DeltaArray(ThisStartIndex) = 1.0#
							'Else
							'  For InstrumentIndex As Integer = 0 To (RVal.Length - 1)
							'    RVal(InstrumentIndex).DeltaArray(ThisStartIndex) = thisNetDeltaArray(InstrumentIndex).DeltaArray(ThisStartIndex)
							'  Next
							'End If

							For InstrumentIndex As Integer = 0 To (RVal.Length - 1)
								RVal(InstrumentIndex).DeltaArray(ThisStartIndex) = thisNetDeltaArray(InstrumentIndex).DeltaArray(ThisStartIndex)
							Next

						End If
					End If

					ThisStartIndex += 1
					CompareStartIndex += 1
				End While

				' Return Series

				Return ReturnFittedDeltasArray(StatsDatePeriod, thisDateArray, RVal, pStatsStartDate, pStatsEndDate, pScalingFactor, False, 100.0#)

			End SyncLock
		Catch ex As Exception
			Return Nothing
		End Try

		Return Nothing
	End Function

	Public Function PeriodReturnSeries(ByVal StatsDatePeriod As DealingPeriod, ByVal pPertracID As ULong, ByVal MatchBackfillVol As Boolean, ByVal pPeriodCount As Integer, ByVal pLamda As Double, ByVal Annualised As Boolean, ByVal pStatsStartDate As Date, ByVal pStatsEndDate As Date, ByRef eMessage As String) As Double()
		' *****************************************************************************
		'
		' *****************************************************************************

		Return PeriodReturnSeries(StatsDatePeriod, pPertracID, MatchBackfillVol, pPeriodCount, pLamda, Annualised, pStatsStartDate, pStatsEndDate, 1.0#, eMessage)

	End Function

	Public Function StdDevSeries(ByVal StatsDatePeriod As DealingPeriod, ByVal pPertracID As ULong, ByVal MatchBackfillVol As Boolean, ByVal pPeriodCount As Integer, ByVal pLamda As Double, ByVal Annualised As Boolean, ByVal pStatsStartDate As Date, ByVal pStatsEndDate As Date, ByRef eMessage As String) As Double()
		' *****************************************************************************
		'
		' *****************************************************************************

		Return StdDevSeries(StatsDatePeriod, pPertracID, MatchBackfillVol, pPeriodCount, pLamda, Annualised, pStatsStartDate, pStatsEndDate, 1.0#, eMessage)

	End Function

	Public Function StdDevSeries_Contingent(ByVal StatsDatePeriod As DealingPeriod, ByVal pPertracID As ULong, ByVal pConditionalPertracID As ULong, ByVal MatchBackfillVol As Boolean, ByVal pCondition As ContingentSelect, ByVal pPeriodCount As Integer, ByVal pLamda As Double, ByVal Annualised As Boolean, ByVal pStatsStartDate As Date, ByVal pStatsEndDate As Date, ByRef eMessage As String) As Double()
		' *****************************************************************************
		'
		' *****************************************************************************

		Return StdDevSeries_Contingent(StatsDatePeriod, pPertracID, pConditionalPertracID, MatchBackfillVol, pCondition, pPeriodCount, pLamda, Annualised, pStatsStartDate, pStatsEndDate, 1.0#, eMessage)

	End Function

	Public Function CorrelationSeries_Contingent(ByVal StatsDatePeriod As DealingPeriod, ByVal X_PertracID As ULong, ByVal Y_PertracID As ULong, ByVal MatchBackfillVol As Boolean, ByVal pCondition As ContingentSelect, ByVal PeriodCount As Integer, ByVal Lamda As Double, ByVal pStatsStartDate As Date, ByVal pStatsEndDate As Date, ByRef eMessage As String) As Double()
		' *****************************************************************************
		'
		' *****************************************************************************

		Return CorrelationSeries_Contingent(StatsDatePeriod, X_PertracID, Y_PertracID, MatchBackfillVol, pCondition, PeriodCount, Lamda, pStatsStartDate, pStatsEndDate, 1.0#, 1.0#, eMessage)

	End Function

	Public Function Beta_Contingent(ByVal StatsDatePeriod As DealingPeriod, ByVal X_PertracID As ULong, ByVal Y_PertracID As ULong, ByVal MatchBackfillVol As Boolean, ByVal pCondition As ContingentSelect, ByVal PeriodCount As Integer, ByVal Lamda As Double, ByVal pStatsStartDate As Date, ByVal pStatsEndDate As Date, ByRef eMessage As String) As Double()
		' *****************************************************************************
		'
		' *****************************************************************************

		Return Beta_Contingent(StatsDatePeriod, X_PertracID, Y_PertracID, MatchBackfillVol, pCondition, PeriodCount, Lamda, pStatsStartDate, pStatsEndDate, 1.0#, 1.0#, eMessage)

	End Function

	Public Function Alpha_Contingent(ByVal StatsDatePeriod As DealingPeriod, ByVal X_PertracID As ULong, ByVal Y_PertracID As ULong, ByVal MatchBackfillVol As Boolean, ByVal pCondition As ContingentSelect, ByVal PeriodCount As Integer, ByVal Lamda As Double, ByVal pStatsStartDate As Date, ByVal pStatsEndDate As Date, ByRef eMessage As String) As Double()
		' *****************************************************************************
		'
		' *****************************************************************************

		Return Alpha_Contingent(StatsDatePeriod, X_PertracID, Y_PertracID, MatchBackfillVol, pCondition, PeriodCount, Lamda, pStatsStartDate, pStatsEndDate, 1.0#, 1.0#, eMessage)

	End Function

	Public Function GetComparisonStatsItem(ByVal StatsDatePeriod As DealingPeriod, ByVal pPertracID As ULong, ByVal pReferencePertracID As ULong, ByVal MatchBackfillVol As Boolean, ByVal pListPertracID As ULong, ByVal pDateFrom As Date, ByVal pDateTo As Date, ByVal pLamda As Double, ByVal pRollingPeriod As Integer, ByVal pGroupSampleSize As Integer, ByRef eMessage As String) As ComparisonStatsClass
		' *****************************************************************************
		'
		' *****************************************************************************

		Return GetComparisonStatsItem(StatsDatePeriod, pPertracID, pReferencePertracID, MatchBackfillVol, pListPertracID, pDateFrom, pDateTo, pLamda, pRollingPeriod, pGroupSampleSize, 1.0#, 1.0#, eMessage)

	End Function


#End Region






End Class
