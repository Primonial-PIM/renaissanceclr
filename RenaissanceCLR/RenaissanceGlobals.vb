﻿Public Class RenaissanceGlobals

	Public Enum DealingPeriod As Integer
		[Annually] = 1
		[SemiAnnually] = 2
		[Quarterly] = 3
		[Monthly] = 4
		[Weekly] = 5
		[Daily] = 6
		[Fortnightly] = 7
		[test1] = 999
		[test2] = 1000
	End Enum

	Public Class Globals

		Public Const KNOWLEDGEDATE_NOW As Date = #1/1/1900#
		Public Const Renaissance_BaseDate As Date = #1/1/1900#

		Public Const Renaissance_EndDate_Data As Date = #1/1/2100#

		Public Const LAST_SECOND As Integer = 86399

		Public Const DEFAULT_DATA_PERIOD As DealingPeriod = DealingPeriod.Daily

		' Standard Date Formats

		Public Const DISPLAYMEMBER_DATEFORMAT As String = "dd MMM yyyy"
		Public Const DISPLAYMEMBER_LONGDATEFORMAT As String = "dd MMM yyyy HH:mm:ss"
		Public Const DISPLAYMEMBER_INTEGERFORMAT As String = "#,##0"
		Public Const DISPLAYMEMBER_DOUBLEFORMAT As String = "#,##0.00"
		Public Const DISPLAYMEMBER_LONGDOUBLEFORMAT As String = "#,##0.0000"
		Public Const DISPLAYMEMBER_PERCENTAGEFORMAT As String = "#,##0.00%"
		Public Const DISPLAYMEMBER_PERCENTAGEFORMATLong As String = "#,##0.0000%"

		Public Const REPORT_DATEFORMAT As String = "dd MMMM yyyy"
		Public Const REPORT_LONGDATEFORMAT As String = "dd MMM yyyy HH:mm:ss"
		Public Const REPORT_MonthAndYear_DATEFORMAT As String = "MMMM yyyy"

		Public Const QUERY_SHORTDATEFORMAT As String = "yyyy-MM-dd"
		Public Const QUERY_LONGDATEFORMAT As String = "yyyy-MM-dd HH:mm:ss"

		Public Const FILENAME_DATEFORMAT As String = "yyyyMMdd_HHmmss"

		Public Const TIMEFORMAT As String = "HH:mm:ss"

		Public Const SUBFUND_EFFECTIVEFUNDMODIFIER As Integer = 2 ^ 30 ' Single Bit to Convert to <Effective Fund> ID
		Public Const SUBFUND_ID_MASK As Integer = (2 ^ 30) - 1 ' Mask to Convert Back again.

		Public Const PRIMONIAL_BNP_TOPIC_DEFAULT_SENDER_ID As String = "TROPICPFQ"
		Public Const PRIMONIAL_BNP_TOPIC_DEFAULT_BANK_CODE As String = "30026"
		Public Const PRIMONIAL_BNP_TOPIC_DEFAULT_AGENCY_CODE As String = "01000"

	End Class

End Class
