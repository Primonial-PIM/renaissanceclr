﻿Option Strict On

Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.SqlTypes
Imports Microsoft.SqlServer.Server
Imports RenaissanceCLR.PertracDataClass
Imports RenaissanceCLR.RenaissanceGlobals

' http://technet.microsoft.com/en-us/library/ms131103.aspx?cs-save-lang=1&cs-lang=vb#code-snippet-4
' http://stackoverflow.com/questions/7823488/sql-server-could-not-find-type-in-the-assembly

'sp_configure 'show advanced options', 1;
'GO
'RECONFIGURE;
'GO
'sp_configure 'clr enabled', 1;
'GO
'RECONFIGURE;
'GO

'ALTER DATABASE renaissance SET TRUSTWORTHY ON


'CREATE ASSEMBLY RenaissanceCLR 
'FROM 'C:\Development\RenaissanceCLR\RenaissanceCLR\bin\Debug\RenaissanceCLR.dll' 
'WITH PERMISSION_SET = UNSAFE; -- , EXTERNAL_ACCESS , UNDSAFE;
'GO

'CREATE FUNCTION fn_clr_GetFundNames(@fundID int)
'RETURNS TABLE (FundID int, FundName nvarchar(300))
'AS 
'EXTERNAL NAME RenaissanceCLR.[RenaissanceCLR.RenaissanceFunctions].fn_clr_GetFundNames;
'GO
'
'SELECT TOP 100 *
'FROM fn_clr_GetFundNames(0)
'go



Partial Public Class RenaissanceFunctions

	Private _PertracData As PertracDataClass = Nothing
	Private _StatFunctions As StatFunctions = Nothing

  Public Sub New()

		'_myRenaissanceMain = New fauxMainClass

		_PertracData = New PertracDataClass()
		_StatFunctions = New StatFunctions(_PertracData)

  End Sub



  Public Class FundResult
    Public FundID As SqlInt32
    Public FundName As SqlString

    Public Sub New(ByVal pFundID As SqlInt32, ByVal pFundname As SqlString)
      FundID = pFundID
      FundName = pFundname
    End Sub
  End Class

  <SqlFunction(DataAccess:=DataAccessKind.Read, SystemDataAccess:=SystemDataAccessKind.Read, FillRowMethodName:="fn_clr_GetFundNames_fillRow", TableDefinition:="FundID int, FundName nvarchar(300)")> _
  Public Shared Function fn_clr_GetFundNames(ByVal FundID As SqlInt32) As IEnumerable

    Dim resultCollection As New ArrayList()

		Using connection As New SqlConnection(SQL_ConnectionString)
			connection.Open()

			'      Using selectFunds As New SqlCommand("SELECT [FundID], [FundName] FROM [dbo].[fn_tblFund_SelectKD](Null) WHERE (FundID=" & FundID.Value.ToString & ") OR (" & FundID.Value.ToString & "=0)", connection)
			Using selectFunds As New SqlCommand("SELECT [FundID], [FundName] FROM [dbo].[fn_tblFund_SelectKD](Null) WHERE (FundID=" & FundID.Value.ToString & ") OR (" & FundID.Value.ToString & "=0)", connection)

				Using fundsReader As SqlDataReader = selectFunds.ExecuteReader()
					While fundsReader.Read()

						Dim thisFundID As SqlInt32 = fundsReader.GetSqlInt32(0)
						Dim thisFundName As SqlString = fundsReader.GetSqlString(1)

						resultCollection.Add(New FundResult(thisFundID, thisFundName))
					End While
				End Using
			End Using

			connection.Close()
		End Using

    Return resultCollection
  End Function

  Public Shared Sub fn_clr_GetFundNames_fillRow(ByVal emailResultObj As Object, ByRef FundID As SqlInt32, ByRef FundName As SqlString)
    Dim thisFundResult As FundResult = DirectCast(emailResultObj, FundResult)

    FundID = thisFundResult.FundID
    FundName = thisFundResult.FundName
  End Sub

	' DROP PROCEDURE spu_clr_GetInstrumentReturns
	' GO
	' DROP ASSEMBLY RenaissanceCLR
	' GO

	'CREATE ASSEMBLY RenaissanceCLR 
	'FROM 'C:\Development\RenaissanceCLR\RenaissanceCLR\bin\Debug\RenaissanceCLR.dll' 
	'WITH PERMISSION_SET = UNSAFE; -- , EXTERNAL_ACCESS , UNDSAFE;
	'GO

	'CREATE PROCEDURE spu_clr_GetInstrumentReturns (@InstrumentID bigint, @StartDate datetime, @EndDate datetime)
	'AS EXTERNAL NAME RenaissanceCLR.[RenaissanceCLR.RenaissanceFunctions].spu_clr_GetInstrumentReturns

	'GRANT EXECUTE ON [dbo].[spu_clr_GetInstrumentReturns] TO [InvestMaster_Sales] AS [dbo]
	'GO
	'GRANT EXECUTE ON [dbo].[spu_clr_GetInstrumentReturns] TO [web_server] AS [dbo]
	'GO
	'GRANT EXECUTE ON [dbo].[spu_clr_GetInstrumentReturns] TO [web_TradeProcessing] AS [dbo]
	'GO
	'GRANT EXECUTE ON [dbo].[spu_clr_GetInstrumentReturns] TO [CLR_Virginia] AS [dbo]
	'GO

	'exec spu_clr_GetInstrumentReturns
	'@InstrumentID = 1141419743,
	'@StartDate = '2010-08-18',
	'@EndDate = '2013-09-01'

	'DROP PROCEDURE spu_clr_GetInstrumentReturns 
	'GO
	'DROP ASSEMBLY RenaissanceCLR
	'GO

	<Microsoft.SqlServer.Server.SqlProcedure()> _
 Public Shared Sub spu_clr_GetInstrumentReturns(ByVal InstrumentID As SqlInt64, ByVal StartDate As SqlDateTime, ByVal EndDate As SqlDateTime)
		' Create a record object that represents an individual row, including it's metadata.
		Dim record As New SqlDataRecord(New SqlMetaData() {New SqlMetaData("PriceDate", SqlDbType.Date), New SqlMetaData("PriceNAV", SqlDbType.Float), New SqlMetaData("PriceReturn", SqlDbType.Float)})

		Dim PertracData As PertracDataClass = Nothing
		Dim StatFunctions As StatFunctions = Nothing

		Dim DateOrdinal As Integer = record.GetOrdinal("PriceDate")
		Dim NAVOrdinal As Integer = record.GetOrdinal("PriceNAV")
		Dim ReturnOrdinal As Integer = record.GetOrdinal("PriceReturn")

		Dim eMessage As String = ""

		Try

			'Using connection As New SqlConnection(SQL_ConnectionString)
			'	connection.Open()

			'	Using selectFunds As New SqlCommand("SELECT [FundID], [FundName] FROM [dbo].[fn_tblFund_SelectKD](Null)", connection)

			'		Using fundsReader As SqlDataReader = selectFunds.ExecuteReader()

			'			fundsReader.Read()

			'			record.SetSqlString(0, "Fund Name : " & CStr(fundsReader.GetSqlString(fundsReader.GetOrdinal("FundName"))))

			'			fundsReader.Close()
			'		End Using

			'	End Using

			'End Using

			PertracData = New PertracDataClass()
			StatFunctions = New StatFunctions(PertracData)
			PertracData.StatFunctionsObject = StatFunctions

			Dim Counter As Integer

			Dim CombinedID As ULong = CULng(InstrumentID.Value)
			Dim DFrom As Date = StartDate.Value.Date
			Dim DTo As Date = EndDate.Value.Date

			Dim NAV_Series() As Double = StatFunctions.NAVSeries(DealingPeriod.Daily, CombinedID, False, 0, 1.0#, False, DFrom, DTo, 1.0#, True, 100.0#, eMessage)
			Dim Date_Series() As Date = StatFunctions.DateSeries(DealingPeriod.Daily, CombinedID, False, 0, 1.0#, False, DFrom, DTo, eMessage)
			Dim Return_Series() As Double = StatFunctions.ReturnSeries(DealingPeriod.Daily, CombinedID, False, 0, 1.0#, False, DFrom, DTo, 1.0#, eMessage)

			If (NAV_Series Is Nothing) Then
				SqlContext.Pipe.Send(eMessage)
			Else
				Try
					SqlContext.Pipe.SendResultsStart(record)

					For Counter = 0 To (Date_Series.Length - 1)

						record.SetSqlDateTime(DateOrdinal, Date_Series(Counter))
						record.SetSqlDouble(NAVOrdinal, NAV_Series(Counter))
						record.SetSqlDouble(ReturnOrdinal, Return_Series(Counter))

						' Send the record to the client.

						SqlContext.Pipe.SendResultsRow(record)
					Next
				Catch ex As Exception
				Finally
					SqlContext.Pipe.SendResultsEnd()
				End Try



			End If

		Catch ex As Exception
			SqlContext.Pipe.Send(eMessage)

		End Try

	End Sub

End Class

