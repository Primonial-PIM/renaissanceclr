﻿Option Strict On

Imports System.IO
Imports System.Data.SqlClient
Imports RenaissanceCLR.RenaissanceUtilities.DatePeriodFunctions
Imports RenaissanceCLR.PertracAndStatsGlobals
Imports RenaissanceCLR.PertracAndStatsGlobals.Globals
Imports RenaissanceCLR.RenaissanceGlobals
Imports System.Text
Imports Microsoft.SqlServer.Server

Partial Public Class PertracDataClass

	Public Const SQL_ConnectionString As String = "context connection=true"

#Region " Locals"

	'Private _MainForm As RenaissanceGlobals.StandardRenaissanceMainForm
	Private _StatFunctionsObject As StatFunctions

	Private PertracDataCache As Hashtable	' Integer Keyed hash table used to cache Pertrac Returns data.
	Private PertracInformationCache As Hashtable ' Integer Keyed hash table used to cache Pertrac Instrument Information.
	Private InformationColumnCache As Dictionary(Of String, Integer()) = Nothing

	Private _MAX_DataCacheSize As Integer
	Public Const MIN_DataCacheSize As Integer = 100
	Public Const Default_MAX_DataCacheSize As Integer = 600

	Private _MAX_InformationCacheSize As Integer
	Public Const MIN_InformationCacheSize As Integer = 20
	Public Const Default_MAX_InformationCacheSize As Integer = 150

	Private InformationColumnMap() As Integer	' PertracInformationFields
	Private MiniDataPeriodCache As New Dictionary(Of Integer, DealingPeriod)

	Private Const Information_ColumnOrdinal As Integer = 0

	' Please note : 
	'
	' The 'FlagBitRangeStart' constant is implicitly used in creating Pseudo Pertrac Instruments
	' (Created in the Mastername Select function). Modify with care.
	' As of Mar 2009 it is assumed t be 27.
	'

	Private Const FlagBitRangeStart As Integer = 27	' Flag range starts at and includes bit 27 (Base Bit Zero, i.e. is the 28th bit.)
	Private Const FlagBitRangeLength As Integer = 4	' Flag range length in bits.

	Private Const ID_Mask_ExFlags As UInteger = CUInt((2 ^ FlagBitRangeStart) - 1)
	Private Const ID_Mask_FlagsOnly As UInteger = CUInt((2 ^ (FlagBitRangeStart + FlagBitRangeLength)) - (2 ^ FlagBitRangeStart))


#End Region

#Region " Properties"

	'Private ReadOnly Property MainForm() As RenaissanceGlobals.StandardRenaissanceMainForm
	'	Get
	'		Return _MainForm
	'	End Get
	'End Property

	Public Property MAX_DataCacheSize() As Integer
		Get
			Return _MAX_DataCacheSize
		End Get
		Set(ByVal value As Integer)
			If value >= MIN_DataCacheSize Then
				_MAX_DataCacheSize = value
			End If
		End Set
	End Property

	Public Property MAX_InformationCacheSize() As Integer
		Get
			Return _MAX_InformationCacheSize
		End Get
		Set(ByVal value As Integer)
			If value >= MIN_InformationCacheSize Then
				_MAX_InformationCacheSize = value
			End If
		End Set
	End Property

	Public Property StatFunctionsObject() As StatFunctions
		Get
			Return _StatFunctionsObject
		End Get
		Set(ByVal value As StatFunctions)
			_StatFunctionsObject = value
		End Set
	End Property

	Public ReadOnly Property AnnualPeriodCount(ByVal StatsDatePeriod As RenaissanceGlobals.DealingPeriod) As Integer
		Get
			Select Case StatsDatePeriod
				Case DealingPeriod.Daily
					Return 261 ' Business days

				Case DealingPeriod.Weekly
					Return 52	' (365 / 7)

				Case DealingPeriod.Fortnightly
					Return 26	' (365 / 14)

				Case DealingPeriod.Monthly
					Return 12

				Case DealingPeriod.Quarterly
					Return 4

				Case DealingPeriod.SemiAnnually
					Return 2

				Case DealingPeriod.Annually
					Return 1

				Case Else
					Return 12
			End Select
		End Get
	End Property

#End Region

#Region " Enumerations"
	' Please note : 
	' Various of the 'PertracInstrumentFlags' enumeration values are used in creating Pseudo Pertrac Instruments
	' (Created in the Mastername Select function). Modify these enumeration values with care.
	' see tblPertracInstrumentFlags
	' see dbo.fn_GetMastername
	' see dbo.adp_tblGroupItems_SelectGroup

	Public Enum PertracInstrumentFlags As UInteger
		' Current range allows values 0 to 15.
		'
		' NOTE !! : 'NetDeltas_Contingent()' function uses this enumeration to determine the requirement to lookthrough Net Deltas.
		' Instrument values other than 'PertracInstrument' and 'VeniceInstrument' are assumed to be a Simulation or Group of some sort 
		' and to require lookthrough processing, so If you introduce a new 'base' instrument type then you will nedd to modify the 
		' NetDeltas function accordingly.
		'

		PertracInstrument = 0	' Pertrac really has to be Zero as an absence of flags denotes a default type of Pertrac Instrument.

		Group_Median = 1
		Group_Mean = 2
		Group_Weighted = 3
		Dynamic_Group_Median = 4
		Dynamic_Group_Mean = 5
		Dynamic_Group_Weighted = 6

		DrillDown_Group_Median = 7
		DrillDown_Group_Mean = 9

		VeniceInstrument = 8 ' For legacy reasons (not now necessary I believe) Venice Instrument is 8 so that the single Bit set is bit 31 : i.e. the same as it used to be.

		CTA_Simulation = 10

	End Enum

#End Region

#Region " Classes"

	Private Class PertracCacheObject
		Private _PertracID As Integer
		Private _InstrumentFlags As PertracDataClass.PertracInstrumentFlags
		Private _NativeDataPeriod As RenaissanceGlobals.DealingPeriod
		Public PertracData As DataTable
		Public LastAccessed As Date
		Public ToDelete As Boolean

		Public ReadOnly Property IsVenicePriceSeries() As Boolean
			Get
				If (_InstrumentFlags = PertracInstrumentFlags.VeniceInstrument) Then
					Return True
				Else
					Return False
				End If
			End Get
		End Property

		Public ReadOnly Property IsGroupPriceSeries() As Boolean
			Get
				If (_InstrumentFlags = PertracInstrumentFlags.Group_Mean) Or (_InstrumentFlags = PertracInstrumentFlags.Group_Median) Or (_InstrumentFlags = PertracInstrumentFlags.Group_Weighted) Then
					Return True
				Else
					Return False
				End If
			End Get
		End Property

		Public ReadOnly Property IsDynamicGroupPriceSeries() As Boolean
			Get
				If (_InstrumentFlags = PertracInstrumentFlags.Dynamic_Group_Mean) OrElse (_InstrumentFlags = PertracInstrumentFlags.Dynamic_Group_Median) OrElse (_InstrumentFlags = PertracInstrumentFlags.Dynamic_Group_Weighted) OrElse (_InstrumentFlags = PertracInstrumentFlags.DrillDown_Group_Median) Then
					Return True
				Else
					Return False
				End If
			End Get
		End Property

		Public ReadOnly Property IsDrilldownGroupPriceSeries() As Boolean
			Get
				If (_InstrumentFlags = PertracInstrumentFlags.DrillDown_Group_Median) Then
					Return True
				Else
					Return False
				End If
			End Get
		End Property

		Public ReadOnly Property IsCTA_SimulationSeries() As Boolean
			Get
				If (_InstrumentFlags = PertracInstrumentFlags.CTA_Simulation) Then
					Return True
				Else
					Return False
				End If
			End Get
		End Property

		Public Property PertracID() As Integer
			Get
				Return _PertracID
			End Get
			Set(ByVal value As Integer)

				Try
					If (value >= 0) Then
						Dim UIntID As UInteger = CUInt(value)
						_PertracID = value ' CInt(UIntID And Uint_30BitMask)
						_InstrumentFlags = PertracDataClass.GetFlagsFromID(CUInt(_PertracID))
					End If

				Catch ex As Exception
				End Try

			End Set
		End Property

		Public Property DataPeriod() As RenaissanceGlobals.DealingPeriod
			Get
				Return _NativeDataPeriod
			End Get
			Set(ByVal value As RenaissanceGlobals.DealingPeriod)
				_NativeDataPeriod = value
			End Set
		End Property

		Public ReadOnly Property ID_ExFlags() As Integer
			Get
				Return CInt(GetInstrumentFromID(CUInt(_PertracID)))
			End Get
		End Property

		Public Property VeniceInstrumentID() As Integer

			Get
				If (IsVenicePriceSeries) Then
					Return ID_ExFlags
				Else
					Return 0
				End If
			End Get

			Set(ByVal value As Integer)
				_PertracID = CInt(SetFlagsToID(CUInt(value), PertracInstrumentFlags.VeniceInstrument))
			End Set

		End Property

		Public Property CTA_SimulationID() As Integer

			Get
				If (IsCTA_SimulationSeries) Then
					Return ID_ExFlags
				Else
					Return 0
				End If
			End Get

			Set(ByVal value As Integer)
				_PertracID = CInt(SetFlagsToID(CUInt(value), PertracInstrumentFlags.CTA_Simulation))
			End Set

		End Property

		Public ReadOnly Property GroupID() As Integer

			Get
				If (IsGroupPriceSeries) Then
					Return ID_ExFlags
				Else
					Return 0
				End If
			End Get

		End Property

		Private Sub New()
			_PertracID = (-1)
			_InstrumentFlags = PertracInstrumentFlags.PertracInstrument
			_NativeDataPeriod = DealingPeriod.Monthly

			PertracData = Nothing
			LastAccessed = Now
			ToDelete = False
		End Sub

		Public Sub New(ByVal pPertracID As Integer, ByVal pNativeDataPeriod As RenaissanceGlobals.DealingPeriod)
			Me.New()

			PertracID = pPertracID
			_NativeDataPeriod = pNativeDataPeriod

		End Sub

		Public Sub New(ByVal pPertracID As Integer, ByVal pNativeDataPeriod As RenaissanceGlobals.DealingPeriod, ByVal pPertracData As DataTable)
			Me.New()

			PertracID = pPertracID
			_NativeDataPeriod = pNativeDataPeriod
			PertracData = pPertracData
		End Sub

	End Class

#End Region

#Region " Constructors"

	Public Sub New()

		' Initialise Properties
		_MAX_DataCacheSize = Default_MAX_DataCacheSize
		_MAX_InformationCacheSize = Default_MAX_InformationCacheSize
		_StatFunctionsObject = Nothing

		' Initialise Pertrac Data Structures
		PertracDataCache = New Hashtable
		PertracInformationCache = New Hashtable

	End Sub

	Public Sub New(ByVal pStatObject As StatFunctions)

		Me.New()

		Me._StatFunctionsObject = pStatObject

	End Sub

#End Region

#Region " Cache Management Functions"

	Public Sub ClearDataCache()
		' ******************************************************************************************
		' Clear cache unselectively.
		' ******************************************************************************************

		ClearDataCache(False, False, False, False, False, "")

	End Sub

	Public Sub ClearDataCache(ByVal ClearVenicePriceSeries As Boolean, ByVal ClearGroupSeries As Boolean, ByVal ClearCustomGroupSeries As Boolean, ByVal ClearDrilldownGroupSeries As Boolean, ByVal ClearCTA_SimulationSeries As Boolean, ByVal InstrumentIdentifier As String)
		' ******************************************************************************************
		' Clear cache unselectively.
		' ******************************************************************************************

		ClearDataCache(ClearVenicePriceSeries, ClearGroupSeries, ClearCustomGroupSeries, ClearDrilldownGroupSeries, ClearCTA_SimulationSeries, InstrumentIdentifier, True)

	End Sub

	Public Sub ClearDataCache(ByVal ClearVenicePriceSeries As Boolean, ByVal ClearGroupSeries As Boolean, ByVal ClearCustomGroupSeries As Boolean, ByVal ClearDrilldownGroupSeries As Boolean, ByVal ClearCTA_SimulationSeries As Boolean, ByVal InstrumentIdentifier As String, ByVal pClearMiniDataPeriodCache As Boolean)
		' ******************************************************************************************
		' Routine to Clear the PertracDataCache, Selectively if specified.
		'
		' ******************************************************************************************

		'Dim ThisID As Integer = 0
		Dim IDArray(-1) As String
		Dim ID_Provided As Boolean = False

		If (pClearMiniDataPeriodCache) Then
			SyncLock MiniDataPeriodCache
				If (MiniDataPeriodCache IsNot Nothing) AndAlso (MiniDataPeriodCache.Count > 0) Then
					MiniDataPeriodCache.Clear()
				End If
			End SyncLock
		End If

		If (InstrumentIdentifier IsNot Nothing) AndAlso (Trim(InstrumentIdentifier).Length > 0) Then ' AndAlso (IsNumeric(InstrumentIdentifier)) Then
			IDArray = InstrumentIdentifier.Split(New Char() {","c, "|"c}, StringSplitOptions.RemoveEmptyEntries)

			If (IDArray.Length > 0) Then
				ID_Provided = True
			End If
		End If

		' Default to one element array.
		If (IDArray.Length <= 0) Then
			IDArray = New String() {"0"}
		End If


		If (ID_Provided) OrElse (ClearVenicePriceSeries) OrElse (ClearGroupSeries) OrElse (ClearCustomGroupSeries) OrElse (ClearDrilldownGroupSeries) Then

			'Dim ItemCounter As Integer
			Dim ThisArrayElement As String
			Dim ThisID As Integer
			Dim KeyArray(PertracDataCache.Count) As Integer
			Dim KeyCount As Integer = 0
			Dim thisPertracCacheObject As PertracCacheObject

			For Each ThisArrayElement In IDArray
				If IsNumeric(Trim(ThisArrayElement)) Then
					ThisID = CInt(Trim(ThisArrayElement))
				Else
					ThisID = 0
				End If

				For Each thisPertracCacheObject In PertracDataCache.Values

					If (thisPertracCacheObject IsNot Nothing) Then

						Try
							If (KeyArray.Contains(thisPertracCacheObject.PertracID) = False) Then
								If (ClearVenicePriceSeries) AndAlso (thisPertracCacheObject.IsVenicePriceSeries) AndAlso ((Not ID_Provided) OrElse (thisPertracCacheObject.VeniceInstrumentID = ThisID)) Then
									KeyArray(KeyCount) = thisPertracCacheObject.PertracID
									KeyCount += 1
								ElseIf (ClearGroupSeries) AndAlso (thisPertracCacheObject.IsGroupPriceSeries) AndAlso ((Not ID_Provided) OrElse (thisPertracCacheObject.GroupID = ThisID)) Then
									KeyArray(KeyCount) = thisPertracCacheObject.PertracID
									KeyCount += 1
								ElseIf (ClearCustomGroupSeries) AndAlso (thisPertracCacheObject.IsDynamicGroupPriceSeries) AndAlso ((Not ID_Provided) OrElse (thisPertracCacheObject.GroupID = ThisID)) Then
									KeyArray(KeyCount) = thisPertracCacheObject.PertracID
									KeyCount += 1
								ElseIf (ClearDrilldownGroupSeries) AndAlso (thisPertracCacheObject.IsDrilldownGroupPriceSeries) AndAlso ((Not ID_Provided) OrElse (thisPertracCacheObject.GroupID = ThisID)) Then
									KeyArray(KeyCount) = thisPertracCacheObject.PertracID
									KeyCount += 1
								ElseIf (ClearCTA_SimulationSeries) AndAlso (thisPertracCacheObject.IsCTA_SimulationSeries) AndAlso ((Not ID_Provided) OrElse (thisPertracCacheObject.CTA_SimulationID = ThisID)) Then
									KeyArray(KeyCount) = thisPertracCacheObject.PertracID
									KeyCount += 1
								ElseIf (Not (ClearVenicePriceSeries Or ClearGroupSeries Or ClearCustomGroupSeries)) AndAlso (ID_Provided) AndAlso (thisPertracCacheObject.PertracID = ThisID) Then
									KeyArray(KeyCount) = thisPertracCacheObject.PertracID
									KeyCount += 1
								ElseIf (thisPertracCacheObject.IsDynamicGroupPriceSeries) Then
									' Always flush Dynamic Group series. Not possible to tell easily what they depend on.

									KeyArray(KeyCount) = thisPertracCacheObject.PertracID
									KeyCount += 1
								End If
							End If

						Catch ex As Exception
						End Try

					End If

				Next ' For Each thisPertracCacheObject

			Next ' For Each ThisArrayElement

			Dim ItemCounter As Integer

			If (KeyCount > 0) Then
				SyncLock PertracDataCache
					For ItemCounter = 0 To (KeyCount - 1)
						Try
							If (PertracDataCache.ContainsKey(KeyArray(ItemCounter))) Then
								PertracDataCache.Remove(KeyArray(ItemCounter))
							End If
						Catch ex As Exception
						End Try
					Next
				End SyncLock
			End If

		Else

			' Clear All 

			Try
				SyncLock PertracDataCache
					PertracDataCache.Clear()
				End SyncLock
			Catch ex As Exception
			End Try

		End If



	End Sub


	Public Sub ClearInformationCache(Optional ByVal pUpdateDetail As String = "")
		' ************************************************************************************************
		' Clear the Information Cache.
		' pUpdateDetail, if given, may be a ',' or '|' separated list of integer identifiers.
		' ************************************************************************************************
		Try
			Dim IDArray(-1) As String
			Dim ThisArrayElement As String
			Dim ThisID As Integer

			Try
				If (Trim(pUpdateDetail).Length > 0) Then
					IDArray = pUpdateDetail.Split(New Char() {","c, "|"c}, StringSplitOptions.RemoveEmptyEntries)
				End If
			Catch ex As Exception
			End Try

			SyncLock PertracInformationCache
				If (IDArray.Length > 0) Then
					For Each ThisArrayElement In IDArray
						Try
							If IsNumeric(Trim(ThisArrayElement)) Then
								ThisID = CInt(Trim(ThisArrayElement))

								If (PertracInformationCache.ContainsKey(ThisID)) Then
									PertracInformationCache.Remove(ThisID)
								End If

							End If
						Catch ex As Exception
						End Try
					Next
				Else
					PertracInformationCache.Clear()
				End If
			End SyncLock
		Catch ex As Exception
		End Try

	End Sub

	Private Class PertracCacheDateComparer
		Implements IComparer

		Public Function Compare(ByVal x As Object, ByVal y As Object) As Integer Implements System.Collections.IComparer.Compare
			Dim X_CacheItem As PertracCacheObject
			Dim Y_CacheItem As PertracCacheObject

			Try
				If (TypeOf x Is PertracCacheObject) Then
					X_CacheItem = CType(x, PertracCacheObject)
				Else
					Return 0
				End If

				If (TypeOf y Is PertracCacheObject) Then
					Y_CacheItem = CType(y, PertracCacheObject)
				Else
					Return 0
				End If

				Return X_CacheItem.LastAccessed.CompareTo(Y_CacheItem.LastAccessed)
			Catch ex As Exception

				Return 0
			End Try

		End Function
	End Class

	Private Sub TrimPertracCache(ByVal pPertracCache As Hashtable, ByVal pNewCacheSize As Integer)
		' *****************************************************************************
		'
		'
		' *****************************************************************************

		Try
			If (pPertracCache.Count <= pNewCacheSize) Then
				Exit Sub
			End If
		Catch ex As Exception
		End Try

		Try
			If (pNewCacheSize <= 0) Then
				SyncLock pPertracCache
					pPertracCache.Clear()
				End SyncLock

				Exit Sub
			End If
		Catch ex As Exception
		End Try

		SyncLock pPertracCache
			Try
				Dim ValueArray(pPertracCache.Count - 1) As PertracCacheObject
				Dim ItemCounter As Integer

				pPertracCache.Values.CopyTo(ValueArray, 0)
				Array.Sort(ValueArray, New PertracCacheDateComparer)

				For ItemCounter = 0 To ((pPertracCache.Count - pNewCacheSize) - 1)
					pPertracCache.Remove(ValueArray(ItemCounter).PertracID)
				Next
			Catch ex As Exception
			End Try
		End SyncLock

	End Sub

#End Region

#Region " Data Functions"

	Public Function GetPertracDataPeriod(ByVal pFirstPertracID As Integer, ByVal pSecondPertracID As Integer, ByRef eMessage As String) As DealingPeriod
		' *****************************************************************************
		' 
		' Return the coarsest Native date perid of two Pertrac IDs.
		'
		' *****************************************************************************

		Dim RVal As DealingPeriod = DealingPeriod.Monthly

		If (pFirstPertracID > 0) AndAlso (pSecondPertracID > 0) Then
			Dim FirstDP As DealingPeriod = GetPertracDataPeriodRecursive(pFirstPertracID, 0, eMessage)
			Dim SecondDP As DealingPeriod = GetPertracDataPeriodRecursive(pSecondPertracID, 0, eMessage)

			If AnnualPeriodCount(FirstDP) < AnnualPeriodCount(SecondDP) Then
				Return FirstDP
			Else
				Return SecondDP
			End If

		ElseIf (pFirstPertracID > 0) Then

			Return GetPertracDataPeriodRecursive(pFirstPertracID, 0, eMessage)

		ElseIf (pSecondPertracID > 0) Then

			Return GetPertracDataPeriodRecursive(pSecondPertracID, 0, eMessage)

		End If

		Return RVal

	End Function

	Public Function GetPertracDataPeriod(ByVal pPertracID As Integer, ByRef eMessage As String) As DealingPeriod
		' *****************************************************************************
		'
		'
		' *****************************************************************************

		Return GetPertracDataPeriodRecursive(pPertracID, 0, eMessage)

	End Function

	Public Sub Set_MiniDataPeriodCache(ByVal pPertracID As Integer, ByVal pDataPeriod As DealingPeriod)
		' *****************************************************************************
		' Allow an entry in the MiniDataPeriodCache to be set.
		'
		' Presented as a workaround to improve performance in the Genoa / Optimisation / Marginals code.
		'
		' *****************************************************************************

		Try

			SyncLock MiniDataPeriodCache
				If (Not MiniDataPeriodCache.ContainsKey(pPertracID)) Then
					MiniDataPeriodCache.Add(pPertracID, pDataPeriod)
				End If
			End SyncLock

		Catch ex As Exception
		End Try

	End Sub

	Public Function GetPertracDataPeriodRecursive(ByVal pPertracID As Integer, ByVal pLevel As Integer, ByRef eMessage As String) As DealingPeriod
		' *****************************************************************************
		'
		'
		' *****************************************************************************

		Dim DataCommand As SqlCommand = Nothing
		Dim DataTable As DataTable = Nothing

		Try

			If (pPertracID <= 0) OrElse (pLevel > 10) Then

				Return DealingPeriod.Monthly

			Else

				' Check the Pertrac Data cache for this item. Return Data Period from there if possible.

				SyncLock PertracDataCache

					If (PertracDataCache.Contains(pPertracID)) Then

						Return CType(PertracDataCache(pPertracID), PertracCacheObject).DataPeriod

					End If

				End SyncLock

				' Check Mini-Cache.
				' This Mini Cache is used to prevent repeated DB queries during the Stats Cache Creation process where
				' This function may be called multiple times before the Pertrac Data Cache is populated.

				SyncLock MiniDataPeriodCache

					If MiniDataPeriodCache.ContainsKey(pPertracID) Then

						Return MiniDataPeriodCache(pPertracID)

					End If

					If (MiniDataPeriodCache.Count > 50) Then ' 50 is arbitrary.

						MiniDataPeriodCache.Clear()

					End If

				End SyncLock


			End If

			' Not in cache, retrieve from DB.

			Dim thisFlags As PertracDataClass.PertracInstrumentFlags
			Dim DataPeriodField As String = ""

			thisFlags = GetFlagsFromID(pPertracID)

			Try
				DataCommand = New SqlCommand
				DataTable = New DataTable

				DataCommand.Connection = New SqlConnection(SQL_ConnectionString)

				Select Case thisFlags

					Case PertracInstrumentFlags.PertracInstrument

						DataCommand.CommandType = CommandType.Text
						DataCommand.CommandText = "SELECT * FROM [dbo].[fn_tblMastername]() WHERE ID = @ID"
						DataCommand.CommandTimeout = 600
						DataCommand.Parameters.Add("@ID", SqlDbType.Int).Value = GetInstrumentFromID(pPertracID)
						DataPeriodField = "DataPeriod"

					Case PertracInstrumentFlags.VeniceInstrument

						DataCommand.CommandType = CommandType.Text
						DataCommand.CommandText = "SELECT [InstrumentID], [InstrumentDataPeriod] FROM [dbo].[fn_tblInstrument_SelectSingle](" & GetInstrumentFromID(pPertracID).ToString & ", '1900-01-01')"
						DataCommand.CommandTimeout = 600
						DataPeriodField = "InstrumentDataPeriod"

					Case PertracInstrumentFlags.CTA_Simulation

						DataCommand.CommandType = CommandType.Text
						DataCommand.CommandText = "SELECT [SimulationID], [SimulationDataPeriod] FROM [dbo].[fn_tblCTA_Simulation_SelectKD]('1900-01-01') WHERE [SimulationID] = " & GetInstrumentFromID(pPertracID).ToString
						DataCommand.CommandTimeout = 600
						DataPeriodField = "SimulationDataPeriod"

					Case PertracInstrumentFlags.DrillDown_Group_Median, PertracInstrumentFlags.DrillDown_Group_Mean

						' ****************************************************************************
						' Hardcode Drilldown groups as Monthly
						'
						' This is done partly for performance reasons and partly because, at present, the 
						' Drilldown forms only use a Monthly Period.
						'
						' If this is changed, then allow selection to the next section which is coded to 
						' handle DrillDown Groups also.
						'
						' ****************************************************************************

						Return DealingPeriod.Monthly

					Case PertracInstrumentFlags.Group_Mean, PertracInstrumentFlags.Group_Median, PertracInstrumentFlags.Group_Weighted, _
						PertracInstrumentFlags.Dynamic_Group_Mean, PertracInstrumentFlags.Dynamic_Group_Median, PertracInstrumentFlags.Dynamic_Group_Weighted
						' PertracInstrumentFlags.DrillDown_Group_Mean, PertracInstrumentFlags.DrillDown_Group_Median, _

						' ****************************************************************************
						' If the Statistics Object is set, use the Group members functions to establish a native Data Period.
						' Otherwise, assume Monthly.
						' ****************************************************************************

						If (_StatFunctionsObject Is Nothing) Then

							Return DealingPeriod.Monthly

						Else

							Dim GroupMembers() As Integer = Nothing
							Dim GroupWeights() As Double = Nothing
							Dim GroupIndex As Integer

							Select Case thisFlags

								Case PertracInstrumentFlags.Group_Mean, PertracInstrumentFlags.Group_Median, PertracInstrumentFlags.Group_Weighted

									_StatFunctionsObject.GetGroupMembers(CInt(GetInstrumentFromID(pPertracID)), GroupMembers, GroupWeights, eMessage)

								Case PertracInstrumentFlags.Dynamic_Group_Mean, PertracInstrumentFlags.Dynamic_Group_Median, PertracInstrumentFlags.Dynamic_Group_Weighted

									_StatFunctionsObject.GetDynamicGroupMembers(CInt(GetInstrumentFromID(pPertracID)), GroupMembers, GroupWeights, -1, False)

								Case PertracInstrumentFlags.DrillDown_Group_Median, PertracInstrumentFlags.DrillDown_Group_Mean

									_StatFunctionsObject.GetDynamicGroupMembers(CInt(GetInstrumentFromID(pPertracID)), GroupMembers, GroupWeights, -1, False)

							End Select

							If (GroupMembers Is Nothing) OrElse (GroupMembers.Length <= 0) Then

								Return DealingPeriod.Monthly
							End If

							' Get DataPeriod for each Group Member and return the Coarsest Date Period 

							Dim RVal As DealingPeriod = DealingPeriod.Daily
							Dim ThisDataPeriod As DealingPeriod

							For GroupIndex = 0 To (GroupMembers.Length - 1)
								ThisDataPeriod = GetPertracDataPeriodRecursive(GroupMembers(GroupIndex), pLevel + 1, eMessage)

								If (ThisDataPeriod <> RVal) AndAlso (AnnualPeriodCount(ThisDataPeriod) < AnnualPeriodCount(RVal)) Then
									RVal = ThisDataPeriod

									' Monthly Get Out
									' Assume period will not be worse than Monthly and exit For it we get to Monthly.

									If AnnualPeriodCount(RVal) <= AnnualPeriodCount(DealingPeriod.Monthly) Then
										Exit For
									End If

								End If
							Next

							' Add to MiniCache.

							SyncLock MiniDataPeriodCache
								If (Not MiniDataPeriodCache.ContainsKey(pPertracID)) Then
									MiniDataPeriodCache.Add(pPertracID, RVal)
								End If
							End SyncLock

							Return RVal

						End If

					Case Else

						' Default period for unrecognised instrument type.

						Return DealingPeriod.Monthly

				End Select

				If (DataCommand.CommandText.Length > 0) Then
					DataCommand.Connection.Open()

					SyncLock DataCommand.Connection
						DataTable.Load(DataCommand.ExecuteReader)
					End SyncLock
				End If

			Catch ex As Exception
			Finally
				Try
					If (DataCommand IsNot Nothing) AndAlso (DataCommand.Connection IsNot Nothing) Then
						DataCommand.Connection.Close()
						DataCommand.Connection = Nothing
					End If
				Catch ex As Exception
				End Try
			End Try

			If (DataTable IsNot Nothing) AndAlso (DataTable.Rows.Count > 0) AndAlso (DataPeriodField.Length > 0) Then
				Try
					SyncLock MiniDataPeriodCache
						If (Not MiniDataPeriodCache.ContainsKey(pPertracID)) Then
							MiniDataPeriodCache.Add(pPertracID, CType(DataTable.Rows(0)(DataPeriodField), DealingPeriod))
						End If
					End SyncLock
				Catch ex As Exception
				End Try

				Return CType(DataTable.Rows(0)(DataPeriodField), DealingPeriod)
			End If

		Catch ex As Exception
		End Try

		Return DealingPeriod.Monthly

	End Function

	Public Function GetPertracDataPeriod(ByVal pPertracID As ULong, ByRef eMessage As String) As DealingPeriod

		Dim PertracID As Integer = CInt(pPertracID And Ulong_31BitMask)
		Dim BackFillID As Integer = CInt((pPertracID >> 32) And Ulong_31BitMask)

		Return GetPertracDataPeriod(PertracID, BackFillID, eMessage)

	End Function

	Public Function GetPertracDataPeriod(ByVal pFirstPertracID As ULong, ByVal pSecondPertracID As ULong, ByRef eMessage As String) As DealingPeriod

		If (pFirstPertracID > 0) AndAlso (pSecondPertracID > 0) Then

			Dim FirstDP As DealingPeriod = GetPertracDataPeriod(pFirstPertracID, eMessage)
			Dim SecondDP As DealingPeriod = GetPertracDataPeriod(pSecondPertracID, eMessage)

			If AnnualPeriodCount(FirstDP) < AnnualPeriodCount(SecondDP) Then
				Return FirstDP
			Else
				Return SecondDP
			End If

		ElseIf (pFirstPertracID > 0) Then

			Return GetPertracDataPeriod(pFirstPertracID, eMessage)

		ElseIf (pSecondPertracID > 0) Then

			Return GetPertracDataPeriod(pSecondPertracID, eMessage)

		End If

	End Function

	Public Function GetPertracDataPeriods(ByVal pPertracIDs() As Integer, ByRef eMessage As String) As DealingPeriod()
		' *****************************************************************************
		'
		'
		' *****************************************************************************

		Dim RVal(-1) As DealingPeriod

		Try
			If (pPertracIDs Is Nothing) OrElse (pPertracIDs.Length <= 0) Then
				Return RVal
			End If

			ReDim RVal(pPertracIDs.Length - 1)
			Dim IndexDictionary As New Dictionary(Of Integer, Integer)
			Dim Index As Integer

			Dim UserPertracCounter As Integer = 0
			Dim VeniceInstrumentCounter As Integer = 0
			Dim GroupInstrumentCounter As Integer = 0
			Dim SimulationInstrumentCounter As Integer = 0
			Dim UserPertracs As ArrayList = Nothing
			Dim VeniceInstruments As ArrayList = Nothing
			Dim GroupInstruments As ArrayList = Nothing
			Dim SimulationInstruments As ArrayList = Nothing
			Dim thisFlags As PertracDataClass.PertracInstrumentFlags
			Dim DataPeriodFound As Boolean
			Dim ThisPertracID As Integer

			SyncLock MiniDataPeriodCache
				SyncLock PertracDataCache

					For Index = 0 To (pPertracIDs.Length - 1)

						ThisPertracID = pPertracIDs(Index)
						DataPeriodFound = False

						' First chech caches for the required data priod.
						' Failing that, organise the retrieval of the required information.

						If (ThisPertracID <= 0) Then

							RVal(Index) = DealingPeriod.Monthly
							DataPeriodFound = True

						Else

							' Check the Pertrac Data cache for this item. Return Data Period from there if possible.


							If (PertracDataCache.Contains(ThisPertracID)) Then

								RVal(Index) = CType(PertracDataCache(ThisPertracID), PertracCacheObject).DataPeriod
								DataPeriodFound = True

							End If

							' Check Mini-Cache.
							' This Mini Cache is used to prevent repeated DB queries during the Stats Cache Creation process where
							' This function may be called multiple times before the Pertrac Data Cache is populated.


							If MiniDataPeriodCache.ContainsKey(ThisPertracID) Then

								RVal(Index) = MiniDataPeriodCache(ThisPertracID)
								DataPeriodFound = True

							End If

						End If

						' Not in the cache, organise retrieval. 

						If (Not DataPeriodFound) Then

							thisFlags = GetFlagsFromID(ThisPertracID)

							Select Case thisFlags

								Case PertracInstrumentFlags.PertracInstrument

									If (GetInstrumentFromID(ThisPertracID) < 1048576) Then
										RVal(Index) = DealingPeriod.Monthly	' Original Pertrac instrument, assume Monthly.
									Else

										If (UserPertracs Is Nothing) Then
											UserPertracs = New ArrayList
										End If

										UserPertracs.Add(ThisPertracID)
										IndexDictionary.Add(ThisPertracID, Index)
										UserPertracCounter += 1

									End If

								Case PertracInstrumentFlags.VeniceInstrument

									If (VeniceInstruments Is Nothing) Then
										VeniceInstruments = New ArrayList
									End If

									VeniceInstruments.Add(ThisPertracID)
									IndexDictionary.Add(ThisPertracID, Index)
									VeniceInstrumentCounter += 1

								Case PertracInstrumentFlags.CTA_Simulation

									If (SimulationInstruments Is Nothing) Then
										SimulationInstruments = New ArrayList
									End If

									SimulationInstruments.Add(ThisPertracID)
									IndexDictionary.Add(ThisPertracID, Index)
									SimulationInstrumentCounter += 1

								Case PertracInstrumentFlags.Group_Mean, PertracInstrumentFlags.Group_Median, PertracInstrumentFlags.Group_Weighted, _
									PertracInstrumentFlags.DrillDown_Group_Mean, PertracInstrumentFlags.DrillDown_Group_Median, _
									PertracInstrumentFlags.Dynamic_Group_Mean, PertracInstrumentFlags.Dynamic_Group_Median, PertracInstrumentFlags.Dynamic_Group_Weighted

									RVal(Index) = GetPertracDataPeriodRecursive(ThisPertracID, 0, eMessage)	 ' DealingPeriod.Monthly

									' ****************************************************************************
									' Groups do not yet allow you to specify a periodicity, but if they did ....
									' ****************************************************************************

									'If (GroupInstruments Is Nothing) Then
									'  GroupInstruments = New ArrayList
									'End If

									'GroupInstruments.Add(ThisPertracID)
									'IndexDictionary.Add(ThisPertracID, Index)

									'GroupInstrumentCounter += 1

								Case Else	' Assume Monthly

									RVal(Index) = DealingPeriod.Monthly

							End Select

						End If

					Next

				End SyncLock
			End SyncLock

			' Get UserPertrac, Venice, Simulation and Standard Group Native Data Periods.

			If (UserPertracCounter + VeniceInstrumentCounter + SimulationInstrumentCounter + GroupInstrumentCounter) > 0 Then

				Dim DataCommand As New SqlCommand
				Dim DataTable As New DataTable
				Dim ID_String As StringBuilder
				Dim ThisID As Integer

				Try
					DataCommand.Connection = New SqlConnection(SQL_ConnectionString)
					DataCommand.Connection.Open()

					If (UserPertracCounter > 0) Then
						Dim WholePertracIDs() As Integer
						Dim StrippedIDs(UserPertracs.Count - 1) As Integer

						WholePertracIDs = CType(UserPertracs.ToArray(GetType(Integer)), Integer())
						ID_String = New StringBuilder

						For Index = 0 To (WholePertracIDs.Length - 1)
							StrippedIDs(Index) = CInt(GetInstrumentFromID(CInt(WholePertracIDs(Index))) - 1048576)

							If (Index > 0) Then
								ID_String.Append(",")
							End If
							ID_String.Append(StrippedIDs(Index).ToString)
						Next

						UserPertracs.Clear()

						DataCommand.CommandType = CommandType.Text
						DataCommand.CommandText = "SELECT [InstrumentID], [DataPeriod] FROM [MASTERSQL].[dbo].[tblInstrumentList] WHERE [InstrumentID] IN (" & ID_String.ToString & ")"
						DataCommand.CommandTimeout = 600

						DataTable.Clear()
						DataTable.Columns.Clear()

						SyncLock DataCommand.Connection
							DataTable.Load(DataCommand.ExecuteReader)
						End SyncLock

						For Each thisRow As DataRow In DataTable.Rows
							ThisID = CInt(thisRow("InstrumentID"))

							For Index = 0 To (WholePertracIDs.Length - 1)

								If (StrippedIDs(Index) = ThisID) Then
									RVal(IndexDictionary(WholePertracIDs(Index))) = CType(CInt(thisRow("DataPeriod")), DealingPeriod)
								End If

							Next

						Next

					End If

					If (VeniceInstrumentCounter > 0) Then

						Dim WholePertracIDs() As Integer
						Dim StrippedIDs(VeniceInstruments.Count - 1) As Integer

						WholePertracIDs = CType(VeniceInstruments.ToArray(GetType(Integer)), Integer())
						ID_String = New StringBuilder

						For Index = 0 To (WholePertracIDs.Length - 1)
							StrippedIDs(Index) = CInt(GetInstrumentFromID(CInt(WholePertracIDs(Index))) - 1048576)

							If (Index > 0) Then
								ID_String.Append(",")
							End If
							ID_String.Append(StrippedIDs(Index).ToString)
						Next

						VeniceInstruments.Clear()

						DataCommand.CommandType = CommandType.Text
						DataCommand.CommandText = "SELECT [InstrumentID], [InstrumentDataPeriod] FROM [dbo].[fn_tblInstrument_SelectKD]('1900-01-01') WHERE [InstrumentID] IN (" & ID_String.ToString & ")"
						DataCommand.CommandTimeout = 600

						DataTable.Clear()
						DataTable.Columns.Clear()

						SyncLock DataCommand.Connection
							DataTable.Load(DataCommand.ExecuteReader)
						End SyncLock

						For Each thisRow As DataRow In DataTable.Rows
							ThisID = CInt(thisRow("InstrumentID"))

							For Index = 0 To (WholePertracIDs.Length - 1)

								If (StrippedIDs(Index) = ThisID) Then
									RVal(IndexDictionary(WholePertracIDs(Index))) = CType(CInt(thisRow("InstrumentDataPeriod")), DealingPeriod)
								End If

							Next

						Next

					End If

					If (SimulationInstrumentCounter > 0) Then

						Dim WholePertracIDs() As Integer
						Dim StrippedIDs(SimulationInstruments.Count - 1) As Integer

						WholePertracIDs = CType(SimulationInstruments.ToArray(GetType(Integer)), Integer())
						ID_String = New StringBuilder

						For Index = 0 To (WholePertracIDs.Length - 1)
							StrippedIDs(Index) = CInt(GetInstrumentFromID(CInt(WholePertracIDs(Index))))

							If (Index > 0) Then
								ID_String.Append(",")
							End If
							ID_String.Append(StrippedIDs(Index).ToString)
						Next

						SimulationInstruments.Clear()

						DataCommand.CommandType = CommandType.Text
						DataCommand.CommandText = "SELECT [SimulationID], [SimulationDataPeriod] FROM [dbo].[fn_tblCTA_Simulation_SelectKD]('1900-01-01') WHERE [SimulationID] IN (" & ID_String.ToString & ")"
						DataCommand.CommandTimeout = 600

						DataTable.Clear()
						DataTable.Columns.Clear()

						SyncLock DataCommand.Connection
							DataTable.Load(DataCommand.ExecuteReader)
						End SyncLock

						For Each thisRow As DataRow In DataTable.Rows
							ThisID = CInt(thisRow("SimulationID"))

							For Index = 0 To (WholePertracIDs.Length - 1)

								If (StrippedIDs(Index) = ThisID) Then
									RVal(IndexDictionary(WholePertracIDs(Index))) = CType(CInt(thisRow("SimulationDataPeriod")), DealingPeriod)
								End If

							Next

						Next

					End If

					If (GroupInstrumentCounter > 0) Then

						' ****************************************************************************
						' Groups do not yet allow you to specify a periodicity, but if they did ....
						' ****************************************************************************

						'Dim WholePertracIDs() As Integer
						'Dim StrippedIDs(GroupInstruments.Count - 1) As Integer

						'WholePertracIDs = CType(GroupInstruments.ToArray(GetType(Integer)), Integer())
						'ID_String = New StringBuilder

						'For Index = 0 To (WholePertracIDs.Length - 1)
						'  StrippedIDs(Index) = CInt(GetInstrumentFromID(CInt(WholePertracIDs(Index))))

						'  If (Index > 0) Then
						'    ID_String.Append(",")
						'  End If
						'  ID_String.Append(StrippedIDs(Index).ToString)
						'Next

						'GroupInstruments.Clear()

						'DataCommand.CommandType = CommandType.Text
						'DataCommand.CommandText = "SELECT [GroupListID], [GroupListDataPeriod] FROM [dbo].[fn_tblGroupList_SelectKD]('1900-01-01') WHERE [GroupListID] IN (" & ID_String.ToString & ")"
						'DataCommand.CommandTimeout = 600

						'DataTable.Clear()
						'DataTable.Columns.Clear()

						'SyncLock DataCommand.Connection
						'  DataTable.Load(DataCommand.ExecuteReader)
						'End SyncLock

						'For Each thisRow As DataRow In DataTable.Rows
						'  ThisID = CInt(thisRow("GroupListID"))

						'  For Index = 0 To (WholePertracIDs.Length - 1)

						'    If (StrippedIDs(Index) = ThisID) Then
						'      RVal(IndexDictionary(WholePertracIDs(Index))) = CType(CInt(thisRow("GroupListDataPeriod")), DealingPeriod)
						'    End If

						'  Next

						'Next

					End If

				Catch ex As Exception
				Finally

					Try
						If (DataCommand IsNot Nothing) AndAlso (DataCommand.Connection IsNot Nothing) Then
							DataCommand.Connection.Close()
							DataCommand.Connection = Nothing
						End If
					Catch ex As Exception
					End Try

				End Try

				' Default value, just in case.

				For Index = 0 To (RVal.Length - 1)
					If CInt(RVal(Index)) = 0 Then
						RVal(Index) = DealingPeriod.Monthly
					End If
				Next
			End If

		Catch ex As Exception

		End Try

		Return RVal

	End Function

	Public Function CoarsestDataPeriod(ByVal pDP1 As DealingPeriod, ByVal pDP2 As DealingPeriod) As DealingPeriod

		Try

			If (AnnualPeriodCount(pDP1) < AnnualPeriodCount(pDP2)) Then
				Return pDP1
			Else
				Return pDP2
			End If

		Catch ex As Exception
		End Try

		Return pDP1

	End Function

	Public Sub LoadPertracTables(ByVal pPertracIDs() As Integer, ByRef eMessage As String)
		' *****************************************************************************
		' Routine to Bulk-Load a set of Pertrac tables.
		' There seems to be a large overhead in getting data from the Server, thus getting the Data for
		' 20 funds takes the same time as for One Fund. 
		' Thus it made sense to enable the class to bulk-load data.
		'
		' If Bit31 is Set then the Instrument is assumed to be a Venice Instrument rather than a PERTRAC
		' Instrument.
		'
		' 'GetPertracTable()' only uses the StatsDatePeriod parameter for Group and Simulation instruments
		' So a default 'Monthly' value is provided here.
		' *****************************************************************************

		' Validate PertracID array.

		If (pPertracIDs Is Nothing) OrElse (pPertracIDs.Length <= 0) Then
			Exit Sub
		End If

		Dim IDsToGet As String = ""
		Dim PertracCounter As Integer
		Dim RowCounter As Integer
		Dim PertracDataPeriods() As DealingPeriod

		Dim MissingIDs(pPertracIDs.Length - 1) As Integer
		Dim DataCommand As New SqlCommand
		Dim DataTable As New DataTable

		Try
			SyncLock PertracDataCache

				' Build 'IN' string of missing (not already loaded) PertracIDs

				For PertracCounter = 0 To (pPertracIDs.Length - 1)
					MissingIDs(PertracCounter) = 0

					If Not (PertracDataCache.Contains(pPertracIDs(PertracCounter))) Then

						' Don't Pre - Load Group or Simulation series.

						Select Case GetFlagsFromID(CUInt(pPertracIDs(PertracCounter)))

							Case PertracInstrumentFlags.VeniceInstrument

								' Venice ID or Group

								Dim thisCacheObject As New PertracCacheObject(pPertracIDs(PertracCounter), GetPertracDataPeriod(pPertracIDs(PertracCounter), eMessage))

								thisCacheObject.PertracData = GetPertracTable(DealingPeriod.Monthly, pPertracIDs(PertracCounter), eMessage)

								' Not necessary : Std call to GetPertracTable() adds table to PertracDataCache
								' PertracDataCache.Add(pPertracIDs(PertracCounter), thisCacheObject)

							Case PertracInstrumentFlags.PertracInstrument

								' Normal Pertrac ID
								If (IDsToGet.Length > 0) Then
									IDsToGet &= ","
								End If

								IDsToGet &= pPertracIDs(PertracCounter).ToString
								MissingIDs(PertracCounter) = pPertracIDs(PertracCounter)

						End Select

					End If
				Next

				If (IDsToGet.Length <= 0) Then
					Exit Sub
				End If

				' Get Bulk Data.

				DataCommand.CommandType = CommandType.Text
				DataCommand.CommandText = "SELECT [ID], [Date], [Return], [FundsManaged], [NAV], [Estimate], [LastUpdated] FROM [MASTERSQL].[dbo].[Performance] WHERE [ID] IN (" & IDsToGet & ")"
				DataCommand.CommandTimeout = 600

				Try
					DataCommand.Connection = New SqlConnection(SQL_ConnectionString)
					DataCommand.Connection.Open()

					SyncLock DataCommand.Connection
						DataTable.Load(DataCommand.ExecuteReader)
					End SyncLock
				Catch ex As Exception
				Finally
					Try
						If (DataCommand IsNot Nothing) AndAlso (DataCommand.Connection IsNot Nothing) Then
							DataCommand.Connection.Close()
							DataCommand.Connection = Nothing
						End If
					Catch ex As Exception
					End Try
				End Try

				' If data was sucessfully loaded...

				If (DataTable.Rows.Count > 0) Then
					Dim SelectedRows() As DataRow

					PertracDataPeriods = GetPertracDataPeriods(MissingIDs, eMessage)

					' For each ID...

					For PertracCounter = 0 To (MissingIDs.Length - 1)
						If MissingIDs(PertracCounter) > 0 Then

							' Select Sub-Set of Returns.

							SelectedRows = DataTable.Select("ID=" & MissingIDs(PertracCounter), "Date")

							' Build ID Specific table.

							If (SelectedRows IsNot Nothing) AndAlso (SelectedRows.Length > 0) Then
								Dim ThisDataRow As DataRow
								Dim NewDataRow As DataRow
								Dim NewTable As New DataTable

								NewTable.Columns.Add("ID", GetType(Integer))
								NewTable.Columns.Add("PerformanceDate", GetType(Date))
								NewTable.Columns.Add("PerformanceReturn", GetType(Double))
								NewTable.Columns.Add("FundsManaged", GetType(Double))
								NewTable.Columns.Add("NAV", GetType(Double))
								NewTable.Columns.Add("Estimate", GetType(Boolean))
								NewTable.Columns.Add("LastUpdated", GetType(Date))

								Try
									For RowCounter = 0 To (SelectedRows.Length - 1)
										ThisDataRow = SelectedRows(RowCounter)

										NewDataRow = NewTable.NewRow
										NewDataRow("ID") = CInt(ThisDataRow("ID"))
										NewDataRow("PerformanceDate") = CDate(ThisDataRow("Date"))
										NewDataRow("PerformanceReturn") = CDbl(ThisDataRow("Return"))
										NewDataRow("FundsManaged") = CDbl(ThisDataRow("FundsManaged"))
										NewDataRow("NAV") = CDbl(ThisDataRow("NAV"))
										NewDataRow("Estimate") = CBool(ThisDataRow("Estimate"))
										NewDataRow("LastUpdated") = CDate(ThisDataRow("LastUpdated"))

										NewTable.Rows.Add(NewDataRow)
									Next
								Catch ex As Exception
								End Try

								' Add to Cache.

								If (NewTable IsNot Nothing) Then
									Dim thisCacheObject As PertracCacheObject

									NewTable.TableName = MissingIDs(PertracCounter).ToString

									thisCacheObject = New PertracCacheObject(MissingIDs(PertracCounter), PertracDataPeriods(PertracCounter), NewTable)
									PertracDataCache.Add(MissingIDs(PertracCounter), thisCacheObject)
								End If
							End If
						End If
					Next

				End If

			End SyncLock
		Catch ex As Exception
		End Try

	End Sub

	Public Function GetPertracTable(ByVal StatsDatePeriod As DealingPeriod, ByVal pPertracID As Integer, ByRef eMessage As String) As DataTable
		' *****************************************************************************
		'
		' *****************************************************************************

		Try
			Return GetPertracTable(StatsDatePeriod, pPertracID, False, RenaissanceGlobals.Globals.KNOWLEDGEDATE_NOW, eMessage)
		Catch ex As Exception
		End Try

		Return Nothing

	End Function

	Public Function GetPertracTable(ByVal StatsDatePeriod As DealingPeriod, ByVal pPertracID As Integer, ByVal NoCache As Boolean, ByVal KnowledgeDate As Date, ByRef eMessage As String) As DataTable

		Return GetPertracTable(StatsDatePeriod, 0, pPertracID, -1, NoCache, KnowledgeDate, eMessage)

	End Function

	Public Function GetPertracTable(ByVal StatsDatePeriod As DealingPeriod, ByVal pNestingLevel As Integer, ByVal pPertracID As Integer, ByRef pSampleSize As Integer, ByVal NoCache As Boolean, ByVal KnowledgeDate As Date, ByRef eMessage As String) As DataTable
		' *****************************************************************************
		' Function to return a DataTable with Pertrac data for the given Instrument.
		'
		' Simple Data Caching is performed using a hash table.
		'
		' High order bits may be set consistent with 'FlagBitRangeStart', 'FlagBitRangeLength' 
		' and Instrument Flag conventions.
		'
		' *****************************************************************************

		Dim DataCommand As New SqlCommand
		Dim DataTable As New DataTable
		Dim thisCacheObject As PertracCacheObject

		' Retrieve Pertrac Index data.

		' Manage Cache size.
		Try
			If (Not NoCache) Then
				SyncLock PertracDataCache
					If (PertracDataCache.Count > MAX_DataCacheSize) Then
						Call TrimPertracCache(PertracDataCache, CInt(MAX_DataCacheSize / 2))
					End If
				End SyncLock
			End If
		Catch ex As Exception
		End Try

		Try
			If (pPertracID > 0) Then

				SyncLock PertracDataCache

					If (Not NoCache) AndAlso PertracDataCache.Contains(pPertracID) Then

						' Get Table from cache.

						thisCacheObject = CType(PertracDataCache(pPertracID), PertracCacheObject)
						DataTable = thisCacheObject.PertracData
						thisCacheObject.LastAccessed = Now()

					Else
						' Load Data.

						thisCacheObject = New PertracCacheObject(pPertracID, GetPertracDataPeriod(pPertracID, eMessage))

						If (thisCacheObject.IsGroupPriceSeries) OrElse (thisCacheObject.IsDynamicGroupPriceSeries) Then

							' Build Data Table reflecting specified Group 

							DataTable = BuildGroupReturnTable(StatsDatePeriod, pNestingLevel, thisCacheObject.PertracID, pSampleSize, NoCache, KnowledgeDate, eMessage)	 ' Id, Including Flags

						Else
							' For Pertrac or Venice Instruments, retrieve price data directly.

							If (thisCacheObject.IsVenicePriceSeries) Then
								' Venice Instrument Data.

								DataCommand.CommandType = CommandType.Text
								DataCommand.CommandText = "SELECT PriceInstrument AS [ID], PriceDate AS PerformanceDate, PricePercent AS PerformanceReturn, 0 as FundsManaged, PriceNAV AS [NAV], 0 AS [Estimate], PriceIsPercent, PriceIsMilestone FROM fn_tblPrice_SelectByInstrument(@PriceInstrument, @ThisKnowledgeDate) ORDER BY PriceInstrument, PriceDate"
								DataCommand.Parameters.Add("@PriceInstrument", SqlDbType.Int).Value = thisCacheObject.VeniceInstrumentID
								DataCommand.Parameters.Add("@ThisKnowledgeDate", SqlDbType.DateTime).Value = KnowledgeDate ' MainForm.Main_Knowledgedate ' RenaissanceGlobals.Globals.KNOWLEDGEDATE_NOW

							Else
								' Pertrac Data.

								DataCommand.CommandType = CommandType.StoredProcedure
								DataCommand.CommandText = "adp_tblPerformance_SelectCommand"
								DataCommand.Parameters.Add("@ID", SqlDbType.Int).Value = thisCacheObject.ID_ExFlags

							End If

							Try
								DataCommand.Connection = New SqlConnection(SQL_ConnectionString)
								DataCommand.Connection.Open()

								SyncLock DataCommand.Connection
									DataTable.Load(DataCommand.ExecuteReader)
								End SyncLock

							Catch ex As Exception
							Finally
								Try
									If (DataCommand IsNot Nothing) AndAlso (DataCommand.Connection IsNot Nothing) Then
										DataCommand.Connection.Close()
										DataCommand.Connection = Nothing
									End If
								Catch ex As Exception
								End Try
							End Try

						End If

						If (DataTable IsNot Nothing) Then
							DataTable.TableName = pPertracID.ToString

							If (Not NoCache) Then
								thisCacheObject.PertracData = DataTable
								PertracDataCache.Add(pPertracID, thisCacheObject)
							Else
								thisCacheObject = Nothing
							End If
						End If

					End If
				End SyncLock

			End If

		Catch ex As Exception
		End Try

		Return DataTable
	End Function

	Dim GetInformationRowLock As New Threading.ReaderWriterLock

	Public Sub GetReturnsArray(ByVal pNestingLevel As Integer, ByVal pPertracID As Integer, ByRef pSampleSize As Integer, ByVal NoCache As Boolean, ByVal KnowledgeDate As Date, ByVal StatsDatePeriod As DealingPeriod, ByRef pDateArray() As Date, ByRef pReturnsArray() As Double, ByRef pNAVArray() As Double, ByRef pIsMilestoneArray() As Boolean, ByRef eMessage As String)
		' **************************************************************************************
		' Return Date, Return, NAV etc arrays for the given Pertrac ID
		'
		'
		' **************************************************************************************

		Dim DataStartDate As Date
		Dim DataTable As DataTable = Nothing
		Dim SortedRows(-1) As DataRow
		Dim ArrayLength As Integer
		Dim RealReturnArray() As Boolean = Nothing

		Dim PerformanceDateOrdinal As Integer
		Dim ReturnOrdinal As Integer
		Dim IsMilestoneOrdinal As Integer = (-1)
		Dim NAVOrdinal As Integer
		Dim ArrayIndex As Integer
		Dim RowCounter As Integer

		Dim IsVeniceID As Boolean = False
		Dim IsNavSeries As Boolean = False
		If PertracDataClass.GetFlagsFromID(CUInt(Math.Max(pPertracID, 0))) = PertracDataClass.PertracInstrumentFlags.VeniceInstrument Then
			IsVeniceID = True
		ElseIf PertracDataClass.GetFlagsFromID(CUInt(Math.Max(pPertracID, 0))) = PertracDataClass.PertracInstrumentFlags.CTA_Simulation Then
			IsNavSeries = True
		End If


		Try
			If pPertracID <= 0 Then
				DataTable = GetPertracTable(StatsDatePeriod, 0, eMessage)
			Else
				DataTable = GetPertracTable(StatsDatePeriod, pNestingLevel + 1, pPertracID, pSampleSize, NoCache, KnowledgeDate, eMessage)
			End If

			' Ensure the data is in date order

			If (DataTable.Columns.Contains("PerformanceDate")) Then
				SortedRows = DataTable.Select("True", "PerformanceDate")
			End If

		Catch ex As Exception
			ReDim SortedRows(-1)
		End Try

		If (SortedRows.Length <= 0) Then
			' Exit with default values

			pDateArray = CType(Array.CreateInstance(GetType(Date), 0), Date())
			pReturnsArray = CType(Array.CreateInstance(GetType(Double), 0), Double())
			pNAVArray = CType(Array.CreateInstance(GetType(Double), 0), Double())
			pIsMilestoneArray = CType(Array.CreateInstance(GetType(Boolean), 0), Boolean())
			Exit Sub
		End If

		' Get Data Column Ordinals, used as a performance improver.

		Try
			PerformanceDateOrdinal = DataTable.Columns.IndexOf("PerformanceDate")
			ReturnOrdinal = DataTable.Columns.IndexOf("PerformanceReturn")
			NAVOrdinal = DataTable.Columns.IndexOf("NAV")

			If (DataTable.Columns.Contains("PriceIsMilestone")) Then
				IsMilestoneOrdinal = DataTable.Columns.IndexOf("PriceIsMilestone")
			End If
		Catch ex As Exception
			pDateArray = CType(Array.CreateInstance(GetType(Date), 0), Date())
			pReturnsArray = CType(Array.CreateInstance(GetType(Double), 0), Double())
			pNAVArray = CType(Array.CreateInstance(GetType(Double), 0), Double())
			pIsMilestoneArray = CType(Array.CreateInstance(GetType(Boolean), 0), Boolean())
			Exit Sub
		End Try

		' Initialise Data Arrays

		Try

			DataStartDate = CDate(SortedRows(0)(PerformanceDateOrdinal))

			If (IsVeniceID) AndAlso (SortedRows.Length > 1) Then
				' Some (Many) Venice price series start with a default starting value on 1 Jan 1970, 
				' Check for this and eliminate it.

				Dim StartIndex As Integer = 0

				' If the NAVs of the first two dates are the same, then ignore the first one, otherwise dont.

				While (StartIndex < (SortedRows.Length - 1)) AndAlso ((CDate(SortedRows(StartIndex + 1)(PerformanceDateOrdinal)) - CDate(SortedRows(StartIndex)(PerformanceDateOrdinal))).TotalDays > 50) AndAlso (Math.Abs(CDbl(SortedRows(StartIndex + 1)(NAVOrdinal)) - CDbl(SortedRows(StartIndex)(NAVOrdinal))) < 0.00000001#)
					DataStartDate = CDate(SortedRows(StartIndex + 1)(PerformanceDateOrdinal))

					StartIndex += 1
				End While

			End If

			DataStartDate = FitDateToPeriod(StatsDatePeriod, DataStartDate, False)	' move to start of period.

		Catch ex As Exception
			pDateArray = CType(Array.CreateInstance(GetType(Date), 0), Date())
			pReturnsArray = CType(Array.CreateInstance(GetType(Double), 0), Double())
			pNAVArray = CType(Array.CreateInstance(GetType(Double), 0), Double())
			pIsMilestoneArray = CType(Array.CreateInstance(GetType(Boolean), 0), Boolean())
			Exit Sub
		End Try

		If (IsVeniceID) OrElse (IsNavSeries) Then

			' If it's a Venice Series, Build returns from NAV data as returns are not always present for Venice Instruments.
			' Also Build IsMilestone Array (Only for Venice Instruments).

			' Fill the returns array using the latest value in each period.
			' Note, this may not work if the StatsDatePeriod does not match the returns period in the pertrac DB.

			Dim ThisRow As DataRow

			ArrayLength = GetPeriodCount(StatsDatePeriod, DataStartDate, CDate(SortedRows(SortedRows.Length - 1)(PerformanceDateOrdinal)))

			' NAVArray
			' RealReturnArray
			' IsMilestoneArray

			RealReturnArray = CType(Array.CreateInstance(GetType(Boolean), ArrayLength + 1), Boolean())	' Flag to indicate 'real' returns - don't fill.
			pIsMilestoneArray = CType(Array.CreateInstance(GetType(Boolean), ArrayLength + 1), Boolean())
			pNAVArray = CType(Array.CreateInstance(GetType(Double), ArrayLength + 1), Double())

			Try
				Dim InitIndex As Integer

				If (RealReturnArray IsNot Nothing) AndAlso (RealReturnArray.Length > 0) Then
					For InitIndex = 0 To (RealReturnArray.Length - 1)
						RealReturnArray(InitIndex) = False
					Next
				End If
			Catch ex As Exception
			End Try


			For RowCounter = 0 To (SortedRows.Length - 1)
				ThisRow = SortedRows(RowCounter)
				ArrayIndex = Math.Max(GetPriceIndex(StatsDatePeriod, DataStartDate, CDate(ThisRow(PerformanceDateOrdinal))), 0)

				pNAVArray(ArrayIndex) = CDbl(ThisRow(NAVOrdinal))
				RealReturnArray(ArrayIndex) = True
				If (IsMilestoneOrdinal >= 0) Then
					pIsMilestoneArray(ArrayIndex) = CBool(ThisRow(IsMilestoneOrdinal))
				End If
			Next

			' Check for missing prices - Forward fill and Back fill.

			For RowCounter = 1 To (pNAVArray.Length - 1)
				If (pNAVArray(RowCounter) = 0) AndAlso (RealReturnArray(RowCounter) = False) Then
					pNAVArray(RowCounter) = pNAVArray(RowCounter - 1)
				End If
			Next

			For RowCounter = (pNAVArray.Length - 2) To 0
				If (pNAVArray(RowCounter) = 0) AndAlso (RealReturnArray(RowCounter) = False) Then
					pNAVArray(RowCounter) = pNAVArray(RowCounter + 1)
				End If
			Next

			' Venice Obsolete data points fix.
			' OK, We are trying to eliminate the PreStart Default (often '100.0') fund valuations in Venice.
			' Now these values play an important role for Venice, but thay are not valid for the purpose of Statistics
			' or The Reporter App.

			If (IsVeniceID) Then

				Try
					Dim InitIndex As Integer
					InitIndex = 0

					While (InitIndex < (RealReturnArray.Length - 2)) AndAlso (Not (RealReturnArray(InitIndex) And RealReturnArray(InitIndex + 1)) AndAlso (Math.Abs(pNAVArray(InitIndex) - pNAVArray(InitIndex + 1)) < 0.00000001#))
						InitIndex += 1
					End While

					If (InitIndex > 0) Then
						DataStartDate = AddPeriodToDate(StatsDatePeriod, DataStartDate, InitIndex)
						DataStartDate = FitDateToPeriod(StatsDatePeriod, DataStartDate, False)	' move to start of period.

						ArrayLength -= InitIndex

						Dim OldDoubleArray() As Double
						Dim OldBooleanArray() As Boolean

						OldDoubleArray = pNAVArray
						pNAVArray = CType(Array.CreateInstance(GetType(Double), ArrayLength + 1), Double())
						Array.Copy(OldDoubleArray, InitIndex, pNAVArray, 0, ArrayLength + 1)

						OldBooleanArray = RealReturnArray
						RealReturnArray = CType(Array.CreateInstance(GetType(Boolean), ArrayLength + 1), Boolean())
						Array.Copy(OldBooleanArray, InitIndex, RealReturnArray, 0, ArrayLength + 1)

						OldBooleanArray = pIsMilestoneArray
						pIsMilestoneArray = CType(Array.CreateInstance(GetType(Boolean), ArrayLength + 1), Boolean())
						Array.Copy(OldBooleanArray, InitIndex, pIsMilestoneArray, 0, ArrayLength + 1)

						OldDoubleArray = Nothing
						OldBooleanArray = Nothing
					End If
				Catch ex As Exception
					pDateArray = CType(Array.CreateInstance(GetType(Date), 0), Date())
					pReturnsArray = CType(Array.CreateInstance(GetType(Double), 0), Double())
					pNAVArray = CType(Array.CreateInstance(GetType(Double), 0), Double())
					pIsMilestoneArray = CType(Array.CreateInstance(GetType(Boolean), 0), Boolean())
					Exit Sub
				End Try

			End If

			' Note ArrayLength returns the actual number of periods, thus dimensioning to 'ArrayLength + 1'
			' Creates space for the initial NAV figure.

			pDateArray = CType(Array.CreateInstance(GetType(Date), ArrayLength + 1), Date())
			pReturnsArray = CType(Array.CreateInstance(GetType(Double), ArrayLength + 1), Double())

			' Now Generate Return series.

			For RowCounter = 1 To (pNAVArray.Length - 1)
				If (pNAVArray(RowCounter - 1) <> 0) Then
					pReturnsArray(RowCounter) = (pNAVArray(RowCounter) / pNAVArray(RowCounter - 1)) - 1.0#
				Else
					pReturnsArray(RowCounter) = 0
				End If
			Next

		Else
			' Not Venice Instrument.

			Try

				' Note ArrayLength returns the actual number of periods, thus dimensioning to 'ArrayLength + 1'
				' Creates space for the initial NAV figure.

				ArrayLength = GetPeriodCount(StatsDatePeriod, DataStartDate, CDate(SortedRows(SortedRows.Length - 1)(PerformanceDateOrdinal)))

				RealReturnArray = CType(Array.CreateInstance(GetType(Boolean), ArrayLength + 1), Boolean())	' Flag to indicate 'real' returns - don't fill.
				pDateArray = CType(Array.CreateInstance(GetType(Date), ArrayLength + 1), Date())
				pReturnsArray = CType(Array.CreateInstance(GetType(Double), ArrayLength + 1), Double())
				pIsMilestoneArray = CType(Array.CreateInstance(GetType(Boolean), ArrayLength + 1), Boolean())
				pNAVArray = Nothing

				Try
					Dim InitIndex As Integer

					If (RealReturnArray IsNot Nothing) AndAlso (RealReturnArray.Length > 0) Then
						For InitIndex = 0 To (RealReturnArray.Length - 1)
							RealReturnArray(InitIndex) = False
						Next
					End If
				Catch ex As Exception
				End Try

			Catch ex As Exception
				pDateArray = CType(Array.CreateInstance(GetType(Date), 0), Date())
				pReturnsArray = CType(Array.CreateInstance(GetType(Double), 0), Double())
				pNAVArray = CType(Array.CreateInstance(GetType(Double), 0), Double())
				pIsMilestoneArray = CType(Array.CreateInstance(GetType(Boolean), 0), Boolean())
				Exit Sub
			End Try

			' Build Returns Array

			DataStartDate = DataStartDate.AddMonths(-1)

			Try
				' Fill the returns array using the latest value in each period.
				' Note, this may not work if the StatsDatePeriod does not match the returns period in the pertrac DB.

				Dim ThisRow As DataRow

				For RowCounter = 0 To (SortedRows.Length - 1)
					ThisRow = SortedRows(RowCounter)
					ArrayIndex = GetPriceIndex(StatsDatePeriod, DataStartDate, CDate(ThisRow(PerformanceDateOrdinal)))

					pReturnsArray(ArrayIndex) = CDbl(ThisRow(ReturnOrdinal))
					RealReturnArray(ArrayIndex) = True

				Next
			Catch ex As Exception
				pDateArray = CType(Array.CreateInstance(GetType(Date), 0), Date())
				pReturnsArray = CType(Array.CreateInstance(GetType(Double), 0), Double())
				pNAVArray = CType(Array.CreateInstance(GetType(Double), 0), Double())
				pIsMilestoneArray = CType(Array.CreateInstance(GetType(Boolean), 0), Boolean())
				Exit Sub
			End Try

		End If

	End Sub

#End Region

	Public Shared Function GetFlagsFromID(ByVal pPertraciD As UInteger) As PertracDataClass.PertracInstrumentFlags
		' ********************************************************************************
		' Function to strip the InstrumentFlags from a Combined Pertrac ID 
		'
		' ********************************************************************************

		Return CType(((pPertraciD And ID_Mask_FlagsOnly) >> FlagBitRangeStart), PertracDataClass.PertracInstrumentFlags)

	End Function

	Public Shared Function GetFlagsFromID(ByVal pPertraciD As Integer) As PertracDataClass.PertracInstrumentFlags
		' ********************************************************************************
		' Function to strip the InstrumentFlags from a Combined Pertrac ID 
		'
		' ********************************************************************************

		Return GetFlagsFromID(CUInt(pPertraciD))

	End Function

	Public Shared Function GetInstrumentFromID(ByVal pPertraciD As UInteger) As UInteger
		' ********************************************************************************
		' Function to strip the InstrumentFlags from a Combined Pertrac ID 
		'
		' ********************************************************************************

		Return CUInt(pPertraciD And ID_Mask_ExFlags)

	End Function

	Public Shared Function GetInstrumentFromID(ByVal pPertraciD As Integer) As UInteger
		' ********************************************************************************
		' Function to strip the InstrumentFlags from a Combined Pertrac ID 
		'
		' ********************************************************************************

		Return CUInt(CUInt(pPertraciD) And ID_Mask_ExFlags)

	End Function

	Public Shared Function SetFlagsToID(ByVal pPertraciD As UInteger, ByVal pFlags As PertracDataClass.PertracInstrumentFlags) As UInteger
		' ********************************************************************************
		' Function to strip the InstrumentFlags from a Combined Pertrac ID 
		'
		' ********************************************************************************

		Return CType((pPertraciD And ID_Mask_ExFlags) Or (pFlags << FlagBitRangeStart), UInteger)

	End Function

	Private Function BuildGroupReturnTable(ByVal StatsDatePeriod As DealingPeriod, ByVal pNestingLevel As Integer, ByVal pGroupID As Integer, ByVal pSampleSize As Integer, ByVal NoCache As Boolean, ByVal KnowledgeDate As Date, ByRef eMessage As String) As DataTable
		' ******************************************************************************************
		'
		'
		'
		' ******************************************************************************************

		Dim RVal As New DataTable
		Dim DatesArray() As Date = Nothing
		Dim ReturnsArray() As Double = Nothing
		Dim NetDeltaArray() As RenaissanceCLR.PertracAndStatsGlobals.NetDeltaItemClass = Nothing

		Dim NewTableRow As DataRow
		Dim ReturnCounter As Integer

		Try

			RVal.Columns.Add("Estimate", GetType(Boolean))
			RVal.Columns.Add("FundsManaged", GetType(Double))
			RVal.Columns.Add("ID", GetType(Integer))
			RVal.Columns.Add("NAV", GetType(Double))
			RVal.Columns.Add("PerformanceDate", GetType(Date))
			RVal.Columns.Add("PerformanceReturn", GetType(Double))

			If (_StatFunctionsObject IsNot Nothing) Then

				_StatFunctionsObject.GetGroupSeries(pNestingLevel + 1, pGroupID, NoCache, KnowledgeDate, StatsDatePeriod, DatesArray, ReturnsArray, NetDeltaArray, 0UL, Nothing, pSampleSize, eMessage)

				If (DatesArray IsNot Nothing) AndAlso (ReturnsArray IsNot Nothing) Then

					For ReturnCounter = 0 To (DatesArray.Length - 1)
						If (ReturnCounter < ReturnsArray.Length) Then

							NewTableRow = RVal.NewRow
							NewTableRow("Estimate") = False
							NewTableRow("FundsManaged") = 0
							NewTableRow("ID") = pGroupID
							NewTableRow("NAV") = 0
							NewTableRow("PerformanceDate") = DatesArray(ReturnCounter)
							NewTableRow("PerformanceReturn") = ReturnsArray(ReturnCounter)

							RVal.Rows.Add(NewTableRow)
						End If
					Next

				End If

			End If

		Catch ex As Exception
		End Try

		Return RVal

	End Function

	Public Sub SetDynamicGroupMembers(ByVal pGroupID As Integer, ByVal pGroupMembers() As Integer, ByVal pGroupWeights() As Double)
		' ******************************************************************************************
		' Set Array of Integers representing the members for the Given Custom Group ID.
		'
		'
		' ******************************************************************************************


		Try
			If (_StatFunctionsObject IsNot Nothing) Then
				_StatFunctionsObject.SetDynamicGroupMembers(pGroupID, pGroupMembers, pGroupWeights)
			End If

		Catch ex As Exception
		End Try

	End Sub

	Public Sub GetDynamicGroupMembers(ByVal pGroupID As Integer, ByRef pGroupMembers() As Integer, ByRef pGroupWeights() As Double, ByRef pSampleSize As Integer, ByVal pLookThrough As Boolean)
		' ******************************************************************************************
		' Get Array of Integers representing the members for the Given Custom Group ID.
		'
		'
		' ******************************************************************************************

		Dim StrippedGroupID As UInteger = PertracDataClass.GetInstrumentFromID(CUInt(pGroupID))

		Try

			If (_StatFunctionsObject IsNot Nothing) Then
				_StatFunctionsObject.GetDynamicGroupMembers(pGroupID, pGroupMembers, pGroupWeights, pSampleSize, pLookThrough)
			End If

		Catch ex As Exception

		End Try

	End Sub

End Class
